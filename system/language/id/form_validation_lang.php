<?php 

$lang["required"] = "Kolom %s wajib diisi.";
$lang["isset"] = "Kolom %s tidak boleh kosong.";
$lang["valid_email"] = "Kolom %s harus berisi alamat e-mail.";
$lang["valid_emails"] = "Kolom %s harus berisi alamat e-mail.";
$lang["valid_url"] = "Kolom %s harus berisi tautan (URL).";
$lang["valid_ip"] = "Kolom %s harus berisi alamat IP.";
$lang["min_length"] = "Kolom %s harus setidaknya %s huruf.";
$lang["max_length"] = "Kolom %s tidak boleh lebih dari %s huruf.";
$lang["exact_length"] = "Kolom %s harus berisi %s huruf.";
$lang["alpha"] = "Kolom %s harus berisi huruf alfabet.";
$lang["alpha_numeric"] = "Kolom %s harus berisi huruf alfabet dan angka.";
$lang["alpha_dash"] = "Kolom %s harus berisi huruf alfabet dan angka, garis bawah, dan tanda pisah (-).";
$lang["numeric"] = "Kolom %s harus berisi angka.";
$lang["is_numeric"] = "Kolom %s harus berisi angka numeric.";
$lang["integer"] = "Kolom %s harus berisi angka integer.";
$lang["regex_match"] = "Kolom %s tidak sesuai dengan format yang ditetapkan.";
$lang["matches"] = "Kolom %s tidak cocok dengan kolom %s.";
$lang["is_unique"] = "Kolom %s harus unik.";
$lang["is_natural"] = "Kolom %s harus berisi angka positif.";
$lang["is_natural_no_zero"] = "Kolom %s harus berisi angka lebih dari 0.";
$lang["decimal"] = "Kolom %s harus berisi angka desimal.";
$lang["less_than"] = "Kolom %s harus berisi angka lebih dari %s.";
$lang["greater_than"] = "Kolom %s harus berisi angka lebih dari %s.";
