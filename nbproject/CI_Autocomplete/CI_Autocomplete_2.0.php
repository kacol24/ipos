<?php

/**
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 * @property CI_Benchmark $benchmark
 * @property CI_Calendar $calendar
 * @property CI_Cart $cart
 * @property CI_Config $config
 * @property CI_Controller $controller
 * @property CI_Email $email
 * @property CI_Encrypt $encrypt
 * @property CI_Exceptions $exceptions
 * @property CI_Form_validation $form_validation
 * @property CI_Ftp $ftp
 * @property CI_Hooks $hooks
 * @property CI_Image_lib $image_lib
 * @property CI_Input $input
 * @property CI_Language $language
 * @property CI_Loader $load
 * @property CI_Log $log
 * @property CI_Model $model
 * @property CI_Output $output
 * @property CI_Pagination $pagination
 * @property CI_Parser $parser
 * @property CI_Profiler $profiler
 * @property CI_Router $router
 * @property CI_Session $session
 * @property CI_Sha1 $sha1
 * @property CI_Table $table
 * @property CI_Trackback $trackback
 * @property CI_Typography $typography
 * @property CI_Unit_test $unit_test
 * @property CI_Upload $upload
 * @property CI_URI $uri
 * @property CI_User_agent $agent
 * @property CI_Validation $validation
 * @property CI_Xmlrpc $xmlrpc
 * @property CI_Xmlrpcs $xmlrpcs
 * @property CI_Zip $zip
 *
 * @property Appconfig 				$Appconfig
 * @property Customer 				$Customer
 * @property Employee 				$Employee
 * @property Giftcard 				$Giftcard
 * @property Inventory 				$Inventory
 * @property Item 					$Item
 * @property Item_kit 				$Item_kit
 * @property Item_kit_items 		$Item_kit_items
 * @property Item_quantities 		$Item_quantities
 * @property Item_taxes 			$Item_taxes
 * @property Module 				$Module
 * @property Payment_record 		$Payment_record
 * @property Person 				$Person
 * @property Receiving 				$Receiving
 * @property Report 				$Report
 * @property Sale 					$Sale
 * @property Sale_suspended 		$Sale_suspended
 * @property Sales_payment 			$Sales_payment
 * @property Stock_locations 		$Stock_locations
 * @property Supplier 				$Supplier
 * @property Item_custom_unit_price $Item_custom_unit_price
 * @property Event 					$Event
 *
 * @property reports/Detailed_receivings 	$Detailed_receivings
 * @property reports/Detailed_sales			$Detailed_sales
 * @property reports/Inventory_low 			$Inventory_low
 * @property reports/Inventory_summary 		$Inventory_summary
 * @property reports/Specific_customer 		$Specific_customer
 * @property reports/Specific_discount		$Specific_discount
 * @property reports/Specific_employee		$Specific_employee
 * @property reports/Summary_categories		$Summary_categories
 * @property reports/Summary_customers		$Summary_customers
 * @property reports/Summary_discounts		$Summary_discounts
 * @property reports/Summary_employees		$Summary_employees
 * @property reports/Summary_items			$Summary_items
 * @property reports/Summary_payments		$Summary_payments
 * @property reports/Summary_sales			$Summary_sales
 * @property reports/Summary_suppliers		$Summary_suppliers
 * @property reports/Summary_taxes			$Summary_taxes
 *
 * @property Breadcrumbs  	$breadcrumbs
 * @property Export  		$export
 * @property Item_lib  		$item_lib
 * @property MY_Language 	$my_language
 * @property MY_Session  	$my_session
 * @property Receiving_lib  	$receiving_lib
 * @property Sale_lib 		$sale_lib
 * @property Setup_lib   	$setup_lib
 */

class CI_Controller {};

/**
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 * @property CI_Config $config
 * @property CI_Loader $load
 * @property CI_Session $session
 *
 * @property Appconfig 				$Appconfig
 * @property Customer 				$Customer
 * @property Employee 				$Employee
 * @property Giftcard 				$Giftcard
 * @property Inventory 				$Inventory
 * @property Item 					$Item
 * @property Item_kit 				$Item_kit
 * @property Item_kit_items 		$Item_kit_items
 * @property Item_quantities 		$Item_quantities
 * @property Item_taxes 			$Item_taxes
 * @property Module 				$Module
 * @property Payment_record 		$Payment_record
 * @property Person 				$Person
 * @property Receiving 				$Receiving
 * @property Report 				$Report
 * @property Sale 					$Sale
 * @property Sale_suspended 		$Sale_suspended
 * @property Sales_payment 			$Sales_payment
 * @property Stock_locations 		$Stock_locations
 * @property Supplier 				$Supplier
 * @property Item_custom_unit_price $Item_custom_unit_price
 * @property Event 					$Event
 *
 * @property reports/Detailed_receivings 	$Detailed_receivings
 * @property reports/Detailed_sales			$Detailed_sales
 * @property reports/Inventory_low 			$Inventory_low
 * @property reports/Inventory_summary 		$Inventory_summary
 * @property reports/Specific_customer 		$Specific_customer
 * @property reports/Specific_discount		$Specific_discount
 * @property reports/Specific_employee		$Specific_employee
 * @property reports/Summary_categories		$Summary_categories
 * @property reports/Summary_customers		$Summary_customers
 * @property reports/Summary_discounts		$Summary_discounts
 * @property reports/Summary_employees		$Summary_employees
 * @property reports/Summary_items			$Summary_items
 * @property reports/Summary_payments		$Summary_payments
 * @property reports/Summary_sales			$Summary_sales
 * @property reports/Summary_suppliers		$Summary_suppliers
 * @property reports/Summary_taxes			$Summary_taxes
 *
 * @property Breadcrumbs  	$breadcrumbs
 * @property Export  		$export
 * @property Item_lib  		$item_lib
 * @property MY_Language 	$my_language
 * @property MY_Session  	$my_session
 * @property Receiving_lib  	$receiving_lib
 * @property Sale_lib 		$sale_lib
 * @property Setup_lib   	$setup_lib
 */

class CI_Model {};

/**
 * @property CI_DB_active_record 	$db
 * @property CI_DB_forge	 $dbforge
 * @property CI_Config 	$config
 * @property CI_Loader 	$load
 * @property CI_Session 	$session
 *
 * @property Appconfig 				$Appconfig
 * @property Customer 				$Customer
 * @property Employee 				$Employee
 * @property Giftcard 				$Giftcard
 * @property Inventory 				$Inventory
 * @property Item 					$Item
 * @property Item_kit 				$Item_kit
 * @property Item_kit_items 		$Item_kit_items
 * @property Item_quantities 		$Item_quantities
 * @property Item_taxes 			$Item_taxes
 * @property Module 				$Module
 * @property Payment_record 		$Payment_record
 * @property Person 				$Person
 * @property Receiving 				$Receiving
 * @property Report 				$Report
 * @property Sale 					$Sale
 * @property Sale_suspended 		$Sale_suspended
 * @property Sales_payment 			$Sales_payment
 * @property Stock_locations 		$Stock_locations
 * @property Supplier 				$Supplier
 * @property Item_custom_unit_price $Item_custom_unit_price
 * @property Event 					$Event
 *
 * @property reports/Detailed_receivings 	$Detailed_receivings
 * @property reports/Detailed_sales			$Detailed_sales
 * @property reports/Inventory_low 			$Inventory_low
 * @property reports/Inventory_summary 		$Inventory_summary
 * @property reports/Specific_customer 		$Specific_customer
 * @property reports/Specific_discount		$Specific_discount
 * @property reports/Specific_employee		$Specific_employee
 * @property reports/Summary_categories		$Summary_categories
 * @property reports/Summary_customers		$Summary_customers
 * @property reports/Summary_discounts		$Summary_discounts
 * @property reports/Summary_employees		$Summary_employees
 * @property reports/Summary_items			$Summary_items
 * @property reports/Summary_payments		$Summary_payments
 * @property reports/Summary_sales			$Summary_sales
 * @property reports/Summary_suppliers		$Summary_suppliers
 * @property reports/Summary_taxes			$Summary_taxes
 *
 * @property Breadcrumbs  	$breadcrumbs
 * @property Export  		$export
 * @property Item_lib  		$item_lib
 * @property MY_Language 	$my_language
 * @property MY_Session  	$my_session
 * @property Receiving_lib  $receiving_lib
 * @property Sale_lib 		$sale_lib
 * @property Setup_lib   	$setup_lib
 */

class CIUnit_TestCase {};

?>