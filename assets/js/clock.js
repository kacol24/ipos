(function ($) {
    $.fn.easyClock = function (options) {
        var defaults = {
            language: 'en',
            format: '24',
            timezone: 0
        },
        options = $.extend(defaults, options);
        var element = this;
        GetClock();
        this.each(function () {
            setInterval(function () {
                GetClock();
            }, 1000);
        });
        function GetClock() {
            var tday = new Array("en", "id");
            var tmonth = new Array("en", "id");
            tday['en'] = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
            tmonth['en'] = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
            tday['id'] = new Array("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu");
            tmonth['id'] = new Array("Januari", "Febuari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
            Date.prototype.addHours = function (h) {
                this.setHours(this.getHours() + h);
                return this;
            };
            offset = new Date().getTimezoneOffset();
            utc = new Date().getTime() + (offset * 60 * 1000);
            d = new Date(utc).addHours(options.timezone);
            nday = d.getDay();
            nmonth = d.getMonth();
            ndate = d.getDate();
            nyear = d.getYear();
            nhour = d.getHours();
            nmin = d.getMinutes();
            nsec = d.getSeconds();
            if (nyear < 1000) {
                nyear += 1900;
            }
            if (nhour <= 9) {
                nhour = "0" + nhour;
            }
            if (nmin <= 9) {
                nmin = "0" + nmin;
            }
            if (nsec <= 9) {
                nsec = "0" + nsec;
            }
            var ap = '';
            if (options.format === '12') {
                if (nhour === 0) {
                    ap = " AM";
                    nhour = 12;
                } else if (nhour <= 11) {
                    ap = " AM";
                } else if (nhour === 12) {
                    ap = " PM";
                } else if (nhour >= 13) {
                    ap = " PM";
                    nhour -= 12;
                }
            }
            element.html(tday[options.language][nday] + ", " + ndate + " " + tmonth[options.language][nmonth] + " " + nyear + "<p><b>" + nhour + ":" + nmin + ":" + nsec + " " + ap + "</b></p>");
        }
    };
})(jQuery);