// --------------------------------------------------------------------
// Plugins
// --------------------------------------------------------------------

var gulp = require("gulp"),
        sass = require("gulp-sass"),
        concat = require("gulp-concat"),
        plumber = require("gulp-plumber"),
        minify_css = require("gulp-minify-css"),
        uglify = require("gulp-uglify"),
        sourcemaps = require("gulp-sourcemaps"),
        prefix = require("gulp-autoprefixer"),
        jshint = require("gulp-jshint");


// --------------------------------------------------------------------
// Settings
// --------------------------------------------------------------------

var src = {
    sass: [
        'assets/css/font-awesome.min.css',
        'assets/css/ionicons.min.css',
        'assets/css/dataTables.bootstrap.css',
        'assets/css/dataTables.responsive.css',
        'assets/css/morris.css',
        'assets/css/daterangepicker-bs3.css',
        'assets/css/selectize.css',
        'assets/css/selectize.bootstrap3.css',
        'assets/css/jquery.bootstrap-touchspin.min.css'
    ],
    js: [
        'assets/js/clock.js',
        'assets/js/jquery.autocomplete.min.js',
        'assets/js/jquery.jkey.min.js',
        'assets/js/bootstrap.min.js',
        'assets/js/placard.js',
        'assets/js/raphael-min.js',
        'assets/js/morris.min.js',
        'assets/js/moment.min.js',
        'assets/js/daterangepicker.js',
        'assets/js/selectize.min.js',
        'assets/js/jquery.bootstrap-touchspin.min.js',
        'assets/js/jquery.dataTables.js',
        'assets/js/dataTables.responsive.js',
        'assets/js/dataTables.bootstrap.js'
    ]
};

var output = {
    js: "dist/assets/js",
    css: "dist/assets/css",
    js_dev: "assets/js",
    css_dev: "assets/css",
    html: "output/**/*.html",
    min_css: 'app.min.css',
    min_js: 'app.min.js'
};


// --------------------------------------------------------------------
// Error Handler
// --------------------------------------------------------------------

var onError = function (err) {
    console.log(err);
    this.emit('end');
};


// --------------------------------------------------------------------
// Task: Sass
// --------------------------------------------------------------------

gulp.task('sass', function () {

    return gulp.src(src.sass)
            .pipe(plumber({
                errorHandler: onError
            }))
            .pipe(sass())
            .pipe(prefix('last 2 versions'))
            .pipe(concat(output.min_css))
            .pipe(gulp.dest(output.css))
            .pipe(minify_css());
});


// --------------------------------------------------------------------
// Task: Sass-dev
// --------------------------------------------------------------------

gulp.task('sass-dev', function () {

    return gulp.src(src.sass)
            .pipe(plumber({
                errorHandler: onError
            }))
            .pipe(sass())
            .pipe(prefix('last 2 versions'))
            .pipe(concat(output.min_css))
            .pipe(gulp.dest(output.css_dev))
            .pipe(minify_css())
            .pipe(sourcemaps.init())
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(output.css_dev));
});


// --------------------------------------------------------------------
// Task: JS
// --------------------------------------------------------------------

gulp.task('js', function () {

    return gulp.src(src.js)
            .pipe(plumber({
                errorHandler: onError
            }))
            .pipe(uglify())
            .pipe(concat(output.min_js))
            .pipe(gulp.dest(output.js));
});


// --------------------------------------------------------------------
// Task: JS-dev
// --------------------------------------------------------------------

gulp.task('js-dev', function () {

    return gulp.src(src.js)
            .pipe(plumber({
                errorHandler: onError
            }))
            .pipe(uglify())
            .pipe(concat(output.min_js))
            .pipe(sourcemaps.init())
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(output.js_dev));
});


// --------------------------------------------------------------------
// Task: Default
// --------------------------------------------------------------------

gulp.task('default', ['sass', 'js', 'sass-dev', 'js-dev']);