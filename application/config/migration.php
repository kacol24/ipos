<?php
/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
defined('BASEPATH') OR exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Enable/Disable Migrations
  |--------------------------------------------------------------------------
  |
  | Migrations are disabled by default but should be enabled
  | whenever you intend to do a schema migration.
  |
 */
$config['migration_enabled'] = FALSE;


/*
  |--------------------------------------------------------------------------
  | Migrations version
  |--------------------------------------------------------------------------
  |
  | This is used to set migration version that the file system should be on.
  | If you run $this->migration->latest() this is the version that schema will
  | be upgraded / downgraded to.
  |
 */
$config['migration_version'] = 0;


/*
  |--------------------------------------------------------------------------
  | Migrations Path
  |--------------------------------------------------------------------------
  |
  | Path to your migrations folder.
  | Typically, it will be within your application path.
  | Also, writing permission is required within the migrations path.
  |
 */
$config['migration_path'] = APPPATH . 'migrations/';


/* End of file migration.php */
