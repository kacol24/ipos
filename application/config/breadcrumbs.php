<?php
/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
if ( ! defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  | -------------------------------------------------------------------
  | BREADCRUMB CONFIG
  | -------------------------------------------------------------------
  | This file will contain some breadcrumbs' settings.
  |
  | $config['crumb_divider']		The string used to divide the crumbs
  | $config['tag_open'] 			The opening tag for breadcrumb's holder.
  | $config['tag_close'] 			The closing tag for breadcrumb's holder.
  | $config['crumb_open'] 		The opening tag for breadcrumb's holder.
  | $config['crumb_close'] 		The closing tag for breadcrumb's holder.
  |
  | Defaults provided for twitter bootstrap 2.0
 */

$config['crumb_divider'] = '';
$config['tag_open'] = '';
$config['tag_close'] = '';
$config['crumb_open'] = '<li>';
$config['crumb_last_open'] = '<li class="active">';
$config['crumb_close'] = '</li>';


/* End of file breadcrumbs.php */
/* Location: ./application/config/breadcrumbs.php */