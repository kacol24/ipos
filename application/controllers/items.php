<?php
/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");

class Items extends Secure_area implements iData_controller
{

    function __construct()
    {
        parent::__construct('items');
        $this->load->library('item_lib');
    }

    /**
     * [GET] Display table of items
     */
    function index()
    {
        $this->breadcrumbs->push($this->lang->line('module_items'), '/items');
        $data['controller_name'] = strtolower(get_class());
        $stock_location = $this->item_lib->get_item_location();
        $stock = $this->Stock_locations->get_undeleted_all()->result_array();
        foreach ($stock as $location) {
            $stock_locations[$location['location_id']] = $location['location_name'];
        }
        $data['stock_location'] = $stock_location;
        $data['stock_locations'] = $stock_locations;
        $data['manage_table'] = get_items_manage_table($this->Item->get_all_and_deleted($stock_location), $this);
        $this->load->view('template/header', $data);
        $this->load->view('items/manage', $data);
        $this->load->view('template/footer');
    }

    /**
     * [GET] Refresh the items table
     *
     * Populate new data to the table according to the parameters
     * that were given using post request
     */
    function refresh()
    {
        $is_serialized = $this->input->post('is_serialized');
        $no_description = $this->input->post('no_description');
        $search_custom = $this->input->post('search_custom'); //GARRISON ADDED 4/13/2013
        $is_deleted = $this->input->post('is_deleted'); // Parq 131215
        /*
         * ------------------------------------------------------
         * added 25/02/2015 -- for displaying items based on
         * current stock location selected by user
         * ------------------------------------------------------
         */
        $stock_location = $this->input->post('stock_location');
        $this->item_lib->set_item_location($stock_location);

        $data['search_section_state'] = $this->input->post('search_section_state');
        $data['is_serialized'] = $this->input->post('is_serialized');
        $data['no_description'] = $this->input->post('no_description');
        $data['search_custom'] = $this->input->post('search_custom'); //GARRISON ADDED 4/13/2013
        $data['is_deleted'] = $this->input->post('is_deleted'); // Parq 131215
        $data['controller_name'] = strtolower(get_class());
        $data['form_width'] = $this->get_form_width();
        $data['manage_table'] = get_items_manage_table($this->Item->get_all_filtered($is_serialized, $no_description, $search_custom, $is_deleted), $this); //GARRISON MODIFIED 4/13/2013, Parq 131215
        redirect(site_url('items'));
    }

    /**
     * This method might not be needed and therefore being deprecated
     *
     * @todo Remove on v1
     */
    function find_item_info()
    {
        $item_number = $this->input->post('scan_item_number');
        echo json_encode($this->Item->find_item_info($item_number));
    }

    /**
     * This method might not be needed and therefore being deprecated
     *
     * @todo Remove on v1
     */
    function search()
    {
        $search = $this->input->post('search');
        $data_rows = get_items_manage_table_data_rows($this->Item->search($search), $this);
        echo $data_rows;
    }

    /**
     * This method might not be needed and therefore being deprecated
     *
     * @todo Remove on v1
     */
    function suggest()
    {
        $suggestions = $this->Item->get_search_suggestions($this->input->post('q'), $this->input->post('limit'));
        echo implode("\n", $suggestions);
    }

    /**
     * [POST] Ajax request to search items for autocomplete
     */
    function item_search()
    {
        $query = $this->input->get('query');
        $suggestions = $this->Item->get_item_search_suggestions($query);
        $this->jsonify($suggestions);
    }

    /**
     * [POST] Ajax request to search categories for autocomplete
     */
    function suggest_category()
    {
        $query = $this->input->get('query');
        $suggestions = $this->Item->get_category_suggestions($query);
        $this->jsonify($suggestions);
    }

    /**
     * This method might not be needed and therefore being deprecated
     *
     * @todo Remove on v1
     */
    function suggest_location()
    {
        $suggestions = $this->Item->get_location_suggestions($this->input->post('q'));
        echo implode("\n", $suggestions);
    }

    /**
     * This method might not be needed and therefore being deprecated
     *
     * @todo Remove on v1
     */
    function get_row()
    {
        $item_id = $this->input->post('row_id');
        $data_row = get_item_data_row($this->Item->get_info($item_id), $this);
        echo $data_row;
    }

    /**
     * [GET] Display the items new/edit form
     *
     * @param int $item_id default -1
     */
    function view($item_id = -1)
    {
        $data['custom_subtitle'] = '';
        $this->breadcrumbs->push('Items', '/items');
        $data['item_info'] = $this->Item->get_info($item_id);
        $data['item_tax_info'] = $this->Item_taxes->get_info($item_id);
        if ($item_id == -1) {
            $this->breadcrumbs->push($this->lang->line('items_new'), '/new');
            $data['custom_title'] = $this->lang->line('items_new');
        } else {
            $this->breadcrumbs->push($this->lang->line('items_update'), '/update');
            $data['custom_title'] = $this->lang->line('items_update');
            $data['custom_subtitle'] = $data['item_info']->item_number != '' ? '#' . $data['item_info']->item_number . ' - ' . $data['item_info']->name : $data['item_info']->name;
        }
        $suppliers = array('' => $this->lang->line('items_none'));
        foreach ($this->Supplier->get_all()->result_array() as $row) {
            $suppliers[$row['person_id']] = $row['company_name'] . ' (' . $row['first_name'] . ' ' . $row['last_name'] . ')';
        }

        $data['suppliers'] = $suppliers;
        $data['selected_supplier'] = $this->Item->get_info($item_id)->supplier_id;
        $data['default_tax_1_rate'] = ($item_id == -1) ? $this->Appconfig->get('default_tax_1_rate') : '';
        $data['default_tax_2_rate'] = ($item_id == -1) ? $this->Appconfig->get('default_tax_2_rate') : '';

        $locations_data = $this->Stock_locations->get_undeleted_all()->result_array();
        $location_array = array();
        foreach ($locations_data as $location) {
            $quantity = ($item_id == -1) ? null : $this->Item_quantities->get_item_quantity($item_id, $location['location_id'])->quantity;
            $location_array[$location['location_id']] = array('location_name' => $location['location_name'],
                'quantity' => $quantity);
        }
        $data['custom_price'] = $this->Item_custom_unit_price->get_customers($item_id)->result();
        $data['controller_name'] = strtolower(get_class());
        $data['stock_locations'] = $location_array;
        $this->load->view('template/header', $data);
        $this->load->view('items/form', $data);
        $this->load->view('template/footer');
    }

    /**
     * This method might not be needed and therefore being deprecated
     *
     * @todo Remove on v1
     */
    function inventory($item_id = -1)
    {
        $data['item_info'] = $this->Item->get_info($item_id);

        $data['stock_locations'] = array();
        $stock_locations = $this->Stock_locations->get_undeleted_all()->result_array();
        foreach ($stock_locations as $location_data) {
            $data['stock_locations'][$location_data['location_id']] = $location_data['location_name'];
            $data['item_quantities'][$location_data['location_id']] = $this->Item_quantities->get_item_quantity($item_id, $location_data['location_id'])->quantity;
        }

        $this->load->view("items/inventory", $data);
    }

    /**
     * [GET] Display the inventory tracking details of a item
     *
     * @param int $item_id default -1
     */
    function count_details($item_id = -1)
    {
        $this->breadcrumbs->push($this->lang->line('module_items'), '/items');
        $this->breadcrumbs->push(lang('items_inventory_tracking'), '/count_details');
        $item_info = $this->Item->get_info($item_id);
        $data['item_info'] = $item_info;

        $data['stock_locations'] = array();
        $stock_locations = $this->Stock_locations->get_undeleted_all()->result_array();
        foreach ($stock_locations as $location_data) {
            $data['stock_locations'][$location_data['location_id']] = $location_data['location_name'];
            $data['item_quantities'][$location_data['location_id']] = $this->Item_quantities->get_item_quantity($item_id, $location_data['location_id'])->quantity;
        }
        $data['controller_name'] = strtolower(get_class());
        $data['custom_title'] = lang('items_inventory_tracking');
        $data['custom_subtitle'] = $item_info->item_number . ' - ' . $item_info->name;
        $this->load->view('template/header', $data);
        $this->load->view("items/count_details", $data);
        $this->load->view('template/footer');
    }

    /**
     * [GET] Display the form to update multiple items
     */
    function bulk_edit()
    {
        $data = array();
        $suppliers = array('' => $this->lang->line('items_none'));
        foreach ($this->Supplier->get_all()->result_array() as $row) {
            $suppliers[$row['person_id']] = $row['first_name'] . ' ' . $row['last_name'];
        }
        $data['suppliers'] = $suppliers;
        $data['allow_alt_description_choices'] = array(
            '' => $this->lang->line('items_do_nothing'),
            1 => $this->lang->line('items_change_all_to_allow_alt_desc'),
            0 => $this->lang->line('items_change_all_to_not_allow_allow_desc'));

        $data['serialization_choices'] = array(
            '' => $this->lang->line('items_do_nothing'),
            1 => $this->lang->line('items_change_all_to_serialized'),
            0 => $this->lang->line('items_change_all_to_unserialized'));
        $this->load->view("items/form_bulk", $data);
    }

    /**
     * [POST/PUT] Save item info to database
     *
     * @param int $item_id default -1
     */
    function save($item_id = -1)
    {
        $this->form_validation->set_rules('name', 'lang:items_name', 'trim|required');
        $this->form_validation->set_rules('category', 'lang:items_category', 'trim|required');
        $this->form_validation->set_rules('cost_price', 'lang:items_cost_price', 'trim|required|numeric');
        $this->form_validation->set_rules('unit_price', 'lang:items_unit_price', 'trim|required|numeric');
        if ($item_id == -1) {
            $this->form_validation->set_rules('item_number', 'lang:items_item_number', 'trim|is_unique[items.item_number]');
        }
        $this->form_validation->set_rules('reorder_level', 'lang:items_reorder_level', 'trim|requried|numeric');

        if ($this->form_validation->run() == FALSE) {
            $this->view($item_id);
        } else {
            $name = $this->input->post('name');
            $description = $this->input->post('description');
            $category = $this->input->post('category');
            $supplier_id = $this->input->post('supplier_id') == '' ? null : $this->input->post('supplier_id');
            $item_number = $this->input->post('item_number') == '' ? null : $this->input->post('item_number');
            $cost_price = $this->input->post('cost_price');
            $unit_price = $this->input->post('unit_price');
            $reorder_level = $this->input->post('reorder_level');
            $is_serialized = $this->input->post('is_serialized') == 'on' ? 1 : 0;
            $deleted = $this->input->post('is_deleted') == 'on' ? 1 : 0;

            //Save item data
            $item_data = array(
                'name' => $name,
                'description' => $description,
                'category' => $category,
                'supplier_id' => $supplier_id,
                'item_number' => $item_number,
                'cost_price' => $cost_price,
                'unit_price' => $unit_price,
                'reorder_level' => $reorder_level,
                'is_serialized' => $is_serialized,
                'deleted' => $deleted
            );

            $employee_id = $this->Employee->get_logged_in_employee_info()->person_id;

            $this->db->trans_start();
            $this->Item->save($item_data, $item_id);
            if ($item_id == -1) {
                // New item
                $new_item = true;
                $item_id = $this->db->insert_id();
            } else {
                // previous item
                $new_item = false;
            }

            $items_taxes_data = array();
            $tax_names = $this->input->post('tax_names');
            $tax_percents = $this->input->post('tax_percents');
            for ($k = 0; $k < count($tax_percents); $k ++ ) {
                if (is_numeric($tax_percents[$k])) {
                    $items_taxes_data[] = array('name' => $tax_names[$k], 'percent' => $tax_percents[$k]);
                }
            }
            $this->Item_taxes->save($items_taxes_data, $item_id);

            //Save item quantity
            $stock_locations = $this->Stock_locations->get_undeleted_all()->result_array();
            foreach ($stock_locations as $location_data) {
                $updated_quantity = $this->input->post($location_data['location_id'] . '_quantity');
                $location_detail = array(
                    'item_id' => $item_id,
                    'location_id' => $location_data['location_id'],
                    'quantity' => $updated_quantity
                );
                $item_quantity = $this->Item_quantities->get_item_quantity($item_id, $location_data['location_id']);
                if ($item_quantity->quantity != $updated_quantity) {
                    $this->Item_quantities->save($location_detail, $item_id, $location_data['location_id']);

                    $inv_data = array(
                        'trans_date' => date('Y-m-d H:i:s'),
                        'trans_items' => $item_id,
                        'trans_user' => $employee_id,
                        'trans_location' => $location_data['location_id'],
                        'trans_comment' => $this->lang->line('items_manually_editing_of_quantity'),
                        'trans_inventory' => $updated_quantity - $item_quantity->quantity
                    );
                    $this->Inventory->insert($inv_data);
                }
            }
            if ($this->config->item('enable_custom_price')) {
                $custom_price = $this->input->post('custom_price');
                if ($custom_price) {
                    $temp = array();
                    foreach ($custom_price as $person_id => $unit_price) {
                        $temp[] = array(
                            'item_id' => $item_id,
                            'person_id' => $person_id,
                            'unit_price' => $unit_price
                        );
                    }
                    $this->Item_custom_unit_price->save($temp, $item_id);
                }
            }
            $this->db->trans_complete();

            if ($this->db->trans_status()) {
                if ($new_item) {
                    set_notif('success', $this->lang->line('items_successful_adding') . ' ' . $item_data['name']);
                } else {
                    set_notif('success', $this->lang->line('items_successful_updating') . ' [' . $item_data['item_number'] . '] ' . $item_data['name']);
                }
            } else {
                set_notif('danger', $this->lang->line('items_error_adding_updating') . ' ' . $item_data['name']);
            }
            redirect(site_url('items'));
        }
    }

    /**
     * [POST/PUT] Save the inventory tracking details
     *
     * @param int $item_id default -1
     */
    function save_inventory($item_id = -1)
    {
        $employee_id = $this->Employee->get_logged_in_employee_info()->person_id;
        $cur_item_info = $this->Item->get_info($item_id);
        $location_id = $this->input->post('stock_location');
        $inv_data = array(
            'trans_date' => date('Y-m-d H:i:s'),
            'trans_items' => $item_id,
            'trans_user' => $employee_id,
            'trans_location' => $location_id,
            'trans_comment' => $this->input->post('trans_comment') ? $this->input->post('trans_comment') : lang('items_manually_editing_of_quantity'),
            'trans_inventory' => $this->input->post('newquantity')
        );
        $this->Inventory->insert($inv_data);

        //Update stock quantity
        $item_quantity = $this->Item_quantities->get_item_quantity($item_id, $location_id);
        $item_quantity_data = array(
            'item_id' => $item_id,
            'location_id' => $location_id,
            'quantity' => $item_quantity->quantity + $this->input->post('newquantity')
        );
        if ($this->Item_quantities->save($item_quantity_data, $item_id, $location_id)) {
            set_notif('success', $this->lang->line('items_successful_updating') . ' ' . $cur_item_info->item_number . ' - ' . $cur_item_info->name);
        } else {
            //failure
            set_notif('danger', $this->lang->line('items_error_adding_updating') . ' ' . $cur_item_info->name);
        }
        redirect(site_url("items/count_details/{$item_id}"));
    }

    /**
     * [PUT] Save items from bulk edit
     */
    function bulk_update()
    {
        $items_to_update = $this->input->post('item_ids');
        $item_data = array();
        foreach ($this->input->post() as $key => $value) {
            //This field is nullable, so treat it differently
            if ($key == 'supplier_id') {
                $item_data["$key"] = $value == '' ? null : $value;
            } elseif ($value != '' and ! (in_array($key, array('submit', 'item_ids', 'tax_names', 'tax_percents')))) {
                $item_data["$key"] = $value;
            }
        }

        //Item data could be empty if tax information is being updated
        if (empty($item_data) || $this->Item->update_multiple($item_data, $items_to_update)) {
            $items_taxes_data = array();
            $tax_names = $this->input->post('tax_names');
            $tax_percents = $this->input->post('tax_percents');
            for ($k = 0; $k < count($tax_percents); $k ++ ) {
                if (is_numeric($tax_percents[$k])) {
                    $items_taxes_data[] = array('name' => $tax_names[$k], 'percent' => $tax_percents[$k]);
                }
            }
            $this->Item_taxes->save_multiple($items_taxes_data, $items_to_update);
            set_notif('success', lang('items_successful_bulk_edit'));
        } else {
            set_notif('danger', lang('items_error_updating_multiple'));
        }
        redirect(site_url('items'));
    }

    /**
     * [DELETE] Soft delete the item
     *
     * @param int $item_id
     */
    function delete($item_id)
    {

        if ($this->Item->delete($item_id)) {
            set_notif('success', $this->lang->line('items_successful_deleted') . ' ' . count($item_id) . ' ' . $this->lang->line('items_one_or_multiple'));
        } else {
            set_notif('danger', $this->lang->line('items_cannot_be_deleted'));
        }
        redirect(site_url('items'));
    }

    /**
     * Let the user download import template
     */
    function excel()
    {
        $data = file_get_contents("import_items.csv");
        $name = 'import_items.csv';
        force_download($name, $data);
    }

    /**
     * [GET] Display the excel import form
     */
    function excel_import()
    {
        $this->breadcrumbs->push($this->lang->line('module_items'), '/items');
        $this->breadcrumbs->push('Excel Import', '/excel_import');
        $data['controller_name'] = strtolower(get_class());
        $this->load->view('template/header', $data);
        $this->load->view("items/excel_import", null);
        $this->load->view('template/footer');
    }

    /**
     * [POST] Save the imported excel file to database
     */
    function do_excel_import()
    {
        $msg = 'do_excel_import';
        $failCodes = array();
        if ($_FILES['file_path']['error'] != UPLOAD_ERR_OK) {
            $msg = $this->lang->line('items_excel_import_failed');
            set_notif('danger', $msg);
            redirect(site_url('/items/excel_import'));
        } else {
            if (($handle = fopen($_FILES['file_path']['tmp_name'], "r")) !== FALSE) {
                //Skip first row
                fgetcsv($handle);

                $i = 1;
                while (($data = fgetcsv($handle)) !== FALSE) {
                    $item_data = array(
                        'item_number' => $data[0],
                        'name' => $data[1],
                        'category' => $data[2],
                        'supplier_id' => $this->Supplier->exists($data[3]) ? $data[3] : null,
                        'cost_price' => $data[4],
                        'unit_price' => $data[5],
                        'reorder_level' => $data[10],
                        'description' => $data[11],
                        'is_serialized' => $data[12] != '' ? '1' : '0'
                    );
                    $item_number = $data[0];

                    if ($item_number != "") {
                        $item_data['item_number'] = $item_number;
                    }

                    if ($this->Item->save($item_data)) {
                        $items_taxes_data = null;
                        //tax 1
                        if (is_numeric($data[7]) && $data[6] != '') {
                            $items_taxes_data[] = array('name' => $data[6], 'percent' => $data[7]);
                        }

                        //tax 2
                        if (is_numeric($data[9]) && $data[8] != '') {
                            $items_taxes_data[] = array('name' => $data[8], 'percent' => $data[9]);
                        }

                        // save tax values
                        if (count($items_taxes_data) > 0) {
                            $this->Item_taxes->save($items_taxes_data, $item_data['item_id']);
                        }

                        $location_detail = array(
                            'item_id' => $item_data['item_id'],
                            'location_id' => $data[13],
                            'quantity' => $data[14]
                        );
                        $this->Item_quantities->save($location_detail, $location_detail['item_id'], $location_detail['location_id']);

                        $employee_id = $this->Employee->get_logged_in_employee_info()->person_id;
                        $emp_info = $this->Employee->get_info($employee_id);
                        $comment = 'Qty CSV Imported';
                        $excel_data = array(
                            'trans_items' => $item_data['item_id'],
                            'trans_user' => $employee_id,
                            'trans_comment' => $comment,
                            'trans_inventory' => $data[10],
                            'trans_location' => $location_detail['location_id']
                        );
                        $this->db->insert('inventory', $excel_data);
                    } else {
                        //insert or update item failure
                        $failCodes[] = $i;
                    }
                }

                $i ++;
            } else {
                set_notif('success', 'Your upload file has no data or not in supported format.');
                redirect(site_url('items/excel_import'));
            }
        }

        $success = true;
        if (count($failCodes) > 1) {
            $msg = "Most items imported. But some were not, here is list of their CODE (" . count($failCodes) . "): " . implode(", ", $failCodes);
            $success = false;
            $type = 'warning';
        } else {
            $msg = "Import items successful";
            $type = 'success';
        }

        set_notif($type, $msg);
        redirect(site_url('items/excel_import'));
    }

    /**
     * This method might not be needed and therefore being deprecated
     *
     * @todo Remove on v1
     */
    function is_sale_store_item($item_number)
    {
        echo $this->Item->is_sale_store_item_exist($item_number);
    }
}

/* End of file items.php */
/* Location: ./application/controllers/items.php */