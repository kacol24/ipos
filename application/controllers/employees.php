<?php
/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
require_once ("person_controller.php");

class Employees extends Person_controller
{

    function __construct()
    {
        parent::__construct('employees');
    }

    /**
     * [GET] Display table for employees
     */
    function index()
    {
        $this->breadcrumbs->push($this->lang->line('module_employees'), '/employees');
        $data['controller_name'] = strtolower(get_class());
        $data['manage_table'] = get_people_manage_table($this->Employee->get_all(), $this);
        $this->load->view('template/header', $data);
        $this->load->view('people/manage', $data);
        $this->load->view('template/footer');
    }

    /**
     * This method might not be needed and therefore being deprecated
     *
     * @todo Remove on v1
     */
    function search()
    {
        $search = $this->input->post('search');
        $data_rows = get_people_manage_table_data_rows($this->Employee->search($search), $this);
        echo $data_rows;
    }

    /**
     * This method might not be needed and therefore being deprecated
     *
     * @todo Remove on v1
     */
    function suggest()
    {
        $suggestions = $this->Employee->get_search_suggestions($this->input->post('q'), $this->input->post('limit'));
        echo implode("\n", $suggestions);
    }

    /**
     * [GET] Display the employee new/edit form
     *
     * @param int $employee_id default -1
     */
    function view($employee_id = -1)
    {
        $this->breadcrumbs->push($this->lang->line('module_employees'), '/employees');
        $data['custom_subtitle'] = '';
        if ($employee_id == -1) {
            $this->breadcrumbs->push($this->lang->line('employees_new'), '/new');
            $data['custom_title'] = $this->lang->line('employees_new');
        } else {
            $this->breadcrumbs->push($this->lang->line('employees_update'), '/update');
            $data['custom_title'] = $this->lang->line('employees_update');
        }
        $data['person_info'] = $this->Employee->get_info($employee_id);
        $data['all_modules'] = $this->Module->get_all_modules();
        $data['controller_name'] = strtolower(get_class());
        $this->load->view("template/header", $data);
        $this->load->view("employees/form", $data);
        $this->load->view("template/footer");
    }

    /**
     * [POST/PUT] Save employee info to database
     *
     * @param int $employee_id default -1
     */
    function save($employee_id = -1)
    {
        $person_data = array(
            'first_name' => $this->input->post('first_name'),
            'last_name' => $this->input->post('last_name'),
            'email' => $this->input->post('email'),
            'phone_number' => $this->input->post('phone_number'),
            'address_1' => $this->input->post('address_1'),
            'address_2' => $this->input->post('address_2'),
            'city' => $this->input->post('city'),
            'state' => $this->input->post('state'),
            'zip' => $this->input->post('zip'),
            'country' => $this->input->post('country'),
            'comments' => $this->input->post('comments')
        );
        $permission_data = $this->input->post("permissions") != false ? $this->input->post("permissions") : array();

        //Password has been changed OR first time password set
        if ($this->input->post('password') != '') {
            $salt = random_string('unique');
            $employee_data = array(
                'username' => $this->input->post('username'),
                'password' => encrypt($this->input->post('password'), $salt),
                'salt' => $salt
            );
        } else {
            //Password not changed
            $employee_data = array('username' => $this->input->post('username'));
        }

        if ($this->Employee->save($person_data, $employee_data, $permission_data, $employee_id)) {
            if ($employee_id == -1) {
                //New employee
                set_notif('success', lang('employees_successful_adding') . ' ' . $person_data['first_name'] . ' ' . $person_data['last_name']);
            } else {
                //previous employee
                set_notif('success', lang('employees_successful_updating') . ' ' . $person_data['first_name'] . ' ' . $person_data['last_name']);
            }
        } else {
            //failure
            set_notif('danger', lang('employees_error_adding_updating') . ' ' . $person_data['first_name'] . ' ' . $person_data['last_name']);
        }
        redirect(site_url('employees'));
    }

    /**
     * [DELETE] Soft delete the employee
     *
     * @param int $person_id person_id
     */
    function delete($person_id)
    {
        if ($this->Employee->delete_list($$person_id)) {
            set_notif('success', $this->lang->line('employees_successful_deleted') . ' ' . count($$person_id) . ' ' . $this->lang->line('employees_one_or_multiple'));
        } else {
            set_notif('danger', $this->lang->line('employees_cannot_be_deleted'));
        }
        redirect(site_url('employees'));
    }
}

/* End of file employees.php */
/* Location: ./application/controllers/employees.php */