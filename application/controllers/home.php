<?php
/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
require_once ("secure_area.php");

class Home extends Secure_area
{

    function __construct()
    {
        parent::__construct('home');
    }

    /**
     * [GET] Display the dashboard
     */
    function index()
    {
        /*
         * ------------------------------------------------------
         *  Low Inventory Notice
         * ------------------------------------------------------
         */
        $this->load->model('reports/Inventory_low');
        $data['low_inventory'] = $this->Inventory_low->getData(array());

        /*
         * ------------------------------------------------------
         *  Sales Notice
         * ------------------------------------------------------
         */
        $this->load->model('reports/Detailed_sales');
        $data['sales_today'] = $this->Detailed_sales->getData(array(
            'start_date' => date('Y-m-d'),
            'end_date' => date('Y-m-d'),
            'sale_type' => 'all'
        ));

        /*
         * ------------------------------------------------------
         *  Receivings Notice
         * ------------------------------------------------------
         */
        $this->load->model('reports/Detailed_receivings');
        $data['receivings_today'] = $this->Detailed_receivings->getData(array(
            'start_date' => date('Y-m-d'),
            'end_date' => date('Y-m-d'),
            'receiving_type' => 'all'
        ));

        /*
         * ------------------------------------------------------
         *  Sales Chart
         * ------------------------------------------------------
         */
        $this->load->model('reports/Summary_sales');
        $param = array(
            'start_date' => date('Y-m-d', mktime(0, 0, 0, date("m"), 1, date("Y"))), // start of this motnh
            'end_date' => date('Y-m-d', strtotime('-1 second', strtotime('+1 month', strtotime(date('m') . '/01/' . date('Y') . ' 00:00:00')))), // end of this month
            'sale_type' => 'sale'
        );
        $result = $this->Summary_sales->graphical_summary_sales_graph($param['start_date'], $param['end_date'], $param['sale_type']);

        /*
         * ------------------------------------------------------
         *  Suspended sales
         * ------------------------------------------------------
         */
        $this->load->model('Sale_suspended');
        $data['suspended_sales'] = $this->Sale_suspended->get_all()->result_array();


        $data['summary_sales'] = $result;
        $data['controller_name'] = strtolower(get_class());
        $this->load->view("template/header", $data);
        $this->load->view("home");
        $this->load->view("template/footer");
    }

    /**
     * Logout the current employee
     */
    function logout()
    {
        $this->Employee->logout();
    }
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */