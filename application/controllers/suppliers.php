<?php
/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
require_once ("person_controller.php");

class Suppliers extends Person_controller
{

    function __construct()
    {
        parent::__construct('suppliers');
    }

    /**
     * [GET] Display table of suppliers
     */
    function index()
    {
        $this->breadcrumbs->push($this->lang->line('module_suppliers'), '/suppliers');
        $data['controller_name'] = strtolower(get_class());
        $data['manage_table'] = get_supplier_manage_table($this->Supplier->get_all(), $this);
        $this->load->view('template/header', $data);
        $this->load->view('people/manage', $data);
        $this->load->view('template/footer');
    }

    /**
     * This method might not be needed and therefore being deprecated
     *
     * @todo Remove on v1
     */
    function search()
    {
        $search = $this->input->post('search');
        $data_rows = get_supplier_manage_table_data_rows($this->Supplier->search($search), $this);
        echo $data_rows;
    }

    /**
     * This method might not be needed and therefore being deprecated
     *
     * @todo Remove on v1
     */
    function suggest()
    {
        $suggestions = $this->Supplier->get_search_suggestions($this->input->post('q'), $this->input->post('limit'));
        echo implode("\n", $suggestions);
    }

    /**
     * [GET] Display the supplier new/edit form
     *
     * @param int $supplier_id default -1
     */
    function view($supplier_id = -1)
    {
        $this->breadcrumbs->push($this->lang->line('module_suppliers'), '/suppliers');
        $data['custom_subtitle'] = '';
        if ($supplier_id == -1) {
            $this->breadcrumbs->push($this->lang->line('suppliers_new'), '/new');
            $data['custom_title'] = $this->lang->line('suppliers_new');
        } else {
            $this->breadcrumbs->push($this->lang->line('suppliers_update'), '/update');
            $data['custom_title'] = $this->lang->line('suppliers_update');
        }
        $data['person_info'] = $this->Supplier->get_info($supplier_id);
        $data['controller_name'] = strtolower(get_class());
        $this->load->view("template/header", $data);
        $this->load->view("suppliers/form", $data);
        $this->load->view("template/footer");
    }

    /**
     * [POST/PUT] Save supplier to database
     *
     * @param int $supplier_id default -1
     */
    function save($supplier_id = -1)
    {
        $person_data = array(
            'first_name' => $this->input->post('first_name'),
            'last_name' => $this->input->post('last_name'),
            'email' => $this->input->post('email'),
            'phone_number' => $this->input->post('phone_number'),
            'address_1' => $this->input->post('address_1'),
            'address_2' => $this->input->post('address_2'),
            'city' => $this->input->post('city'),
            'state' => $this->input->post('state'),
            'zip' => $this->input->post('zip'),
            'country' => $this->input->post('country'),
            'comments' => $this->input->post('comments')
        );
        $supplier_data = array(
            'company_name' => $this->input->post('company_name'),
            'account_number' => $this->input->post('account_number') == '' ? null : $this->input->post('account_number'),
        );
        if ($this->Supplier->save($person_data, $supplier_data, $supplier_id)) {

            if ($supplier_id == -1) {
                //New supplier
                set_notif('success', $this->lang->line('suppliers_successful_adding') . ' ' . $supplier_data['company_name']);
            } else {
                //previous supplier
                set_notif('success', $this->lang->line('suppliers_successful_updating') . ' ' . $supplier_data['company_name']);
            }
        } else {
            //failure
            set_notif('danger', $this->lang->line('suppliers_error_adding_updating') . ' ' . $supplier_data['company_name']);
        }
        redirect(site_url('suppliers'));
    }

    /**
     * [DELETE] Soft delete the supplier
     *
     * @param int $supplier_id
     */
    function delete($supplier_id)
    {
        if ($this->Supplier->delete_list($supplier_id)) {
            set_notif('success', $this->lang->line('suppliers_successful_deleted') . ' ' . count($supplier_id) . ' ' . $this->lang->line('suppliers_one_or_multiple'));
        } else {
            set_notif('danger', $this->lang->line('suppliers_cannot_be_deleted'));
        }
        redirect(site_url('suppliers'));
    }

    /**
     * This method might not be needed and therefore being deprecated
     *
     * @todo Remove on v1
     */
    function get_row()
    {
        $person_id = $this->input->post('row_id');
        $data_row = get_supplier_data_row($this->Supplier->get_info($person_id), $this);
        echo $data_row;
    }
}

/* End of file suppliers.php */
/* Location: ./application/controllers/suppliers.php */