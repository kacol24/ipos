<?php
/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
if ( ! defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class might not be needed and therefore is being deprecated
 *
 * @todo Remove on v1
 */
class Languagecheck extends CI_Controller
{
    /*
     * use this language as comparison reference.
     * this should be the one that is complete.
     */

    private $_reference = 'en';
    private $_lang_path = 'language';

    /*
     * controller constructor
     */

    function Languagecheck()
    {
        parent::__construct();
    }

    // -----------------------------------------------------------------

    /*
     * use remap to capture all calls to this controller
     */
    function _remap()
    {
        // load the required helpers
        $this->load->helper('directory');

        // for simplicity, we don't use views
        $this->output('h1', 'iPOS Point of Sales System from Inertia');

        // determine the language file path
        if ( ! is_dir($this->_lang_path)) {
            $this->_lang_path = APPPATH . $this->_lang_path;

            if ( ! is_dir($this->_lang_path)) {
                $this->output('h2', 'Defined language path "' . $this->_lang_path . '" not found!', TRUE);
                exit;
            }
        }

        // fetch the languages directory map
        $languages = directory_map($this->_lang_path, TRUE);

        // is our reference language present?
        if ( ! in_array($this->_reference, $languages)) {
            $this->output('h2', 'Reference language "' . $this->_reference . '" not found!', TRUE);
            exit;
        }

        // load the list of language files for the reference language
        $references = directory_map($this->_lang_path . '/' . $this->_reference, TRUE);

        // now process the list
        foreach ($references as $reference) {
            // skip non-language files in the language directory
            if (strpos($reference, '_lang' . EXT) === FALSE) {
                continue;
            }

            // process it
            $this->output('h2', 'Processing ' . $this->_reference . ' &raquo; ' . $reference);

            // load the language file
            include $this->_lang_path . '/' . $this->_reference . '/' . $reference;

            // did the file contain any language strings?
            if (empty($lang)) {
                // language file was empty or not properly defined
                $this->output('h3', 'Language file doesn\'t contain any language strings. Skipping file!', TRUE);
                continue;
            }

            // store the loaded language strings
            $lang_ref = $lang;
            unset($lang);

            // now loop through the available languages
            foreach ($languages as $language) {
                // skip the reference language
                if ($language == $this->_reference) {
                    continue;
                }

                // language file to check
                $file = $this->_lang_path . '/' . $language . '/' . $reference;

                // check if the language file exists for this language
                if ( ! file_exists($file)) {
                    // file not found
                    $this->output('h3', 'Language file doesn\'t exist for the language ' . $language . '!', TRUE);
                } else {
                    // load the file to compare
                    include $file;

                    // did the file contain any language strings?
                    if (empty($lang)) {
                        // language file was empty or not properly defined
                        $this->output('h3', 'Language file for the language ' . $language . ' doesn\'t contain any language strings!', TRUE);
                    } else {
                        // start comparing
                        $this->output('h3', 'Comparing with the ' . $language . ' version:');

                        // assume all goes well
                        $failures = 0;

                        // start comparing language keys
                        foreach ($lang_ref as $key => $value) {
                            if ( ! isset($lang[$key])) {
                                // report the missing key
                                $this->output('', 'Missing language string "' . $key . '"', TRUE);

                                // increment the failure counter
                                $failures ++;
                            }
                        }

                        if ( ! $failures) {
                            $this->output('', 'The two language files have matching strings.');
                        }
                    }

                    // make sure the lang array is deleted before the next check
                    if (isset($lang)) {
                        unset($lang);
                    }
                }
            }
        }

        $this->output('h2', 'Language file checking and validation completed');
    }

    // -----------------------------------------------------------------

    private function output($type = '', $line = '', $highlight = FALSE)
    {
        switch ($type) {
            case 'h1':
                $html = "<h1>{line}</h1>\n<hr />\n";
                break;

            case 'h2':
                $html = "<h2>{line}</h2>\n";
                break;

            case 'h3':
                $html = "<h3>&nbsp;&nbsp;&nbsp;{line}</h3>\n";
                break;

            default:
                $html = "&nbsp;&nbsp;&nbsp;&nbsp;&raquo;&nbsp;{line}<br />";
                break;
        }

        if ($highlight) {
            $line = '<span style="color:red;font-weight:bold;">' . $line . '</span>';
        }

        echo str_replace('{line}', $line, $html);
    }
    // -----------------------------------------------------------------
}

/* End of file languagecheck.php */
/* Location: ./application/controllers/languagecheck.php */
