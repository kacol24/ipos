<?php
/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
require_once ("secure_area.php");

class Events extends Secure_area
{

    function __construct()
    {
        parent::__construct('events');
    }

    public function index()
    {
        $this->breadcrumbs->push($this->lang->line('module_events'), '/events');
        $data['manage_table'] = get_event_log_manage_table($this->Event->get_all(), $this);
        $data['controller_name'] = strtolower(get_class());
        $this->load->view('template/header', $data);
        $this->load->view("event_log", $data);
        $this->load->view('template/footer');
    }
}
