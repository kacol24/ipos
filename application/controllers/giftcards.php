<?php
/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");

class Giftcards extends Secure_area implements iData_controller
{

    function __construct()
    {
        parent::__construct('giftcards');
    }

    /**
     * [GET] Display table for
     */
    function index()
    {
        $this->breadcrumbs->push($this->lang->line('module_giftcards'), '/giftcards');
        $data['controller_name'] = strtolower(get_class());
        $data['manage_table'] = get_giftcards_manage_table($this->Giftcard->get_all(), $this);
        $this->load->view('template/header', $data);
        $this->load->view('giftcards/manage', $data);
        $this->load->view('template/footer');
    }

    /**
     * This method might not be needed and therefore being deprecated
     *
     * @todo Remove on v1
     */
    function search()
    {
        $search = $this->input->post('search');
        $data_rows = get_giftcards_manage_table_data_rows($this->Giftcard->search($search), $this);
        echo $data_rows;
    }

    /**
     * This method might not be needed and therefore being deprecated
     *
     * @todo Remove on v1
     */
    function suggest()
    {
        $suggestions = $this->Giftcard->get_search_suggestions($this->input->post('q'), $this->input->post('limit'));
        echo implode("\n", $suggestions);
    }

    /**
     * [POST] Ajax request for searching customers using autocomplete
     */
    function suggest_person()
    {
        $query = $this->input->post('query');
        $suggestions = $this->Giftcard->get_person_search_suggestions($query);
        $this->jsonify($suggestions);
    }

    /**
     * This method might not be needed and therefore being deprecated
     *
     * @todo Remove on v1
     */
    function get_row()
    {
        $giftcard_id = $this->input->post('row_id');
        $data_row = get_giftcard_data_row($this->Giftcard->get_info($giftcard_id), $this);
        echo $data_row;
    }

    /**
     * [GET] Display the giftcard new/edit form
     *
     * @param int $giftcard_id default -1
     */
    function view($giftcard_id = -1)
    {
        $this->breadcrumbs->push($this->lang->line('module_giftcards'), '/giftcards');
        $giftcard_info = $this->Giftcard->get_info($giftcard_id);
        $data['custom_subtitle'] = '';
        if ($giftcard_id == -1) {
            $this->breadcrumbs->push($this->lang->line('giftcards_new'), '/new');
            $data['custom_title'] = $this->lang->line('giftcards_new');
            $data['giftcard_number'] = $this->Giftcard->get_max_number()->giftcard_number + 1;
        } else {
            $this->breadcrumbs->push($this->lang->line('giftcards_update'), '/update');
            $data['custom_title'] = $this->lang->line('giftcards_update');
            $data['giftcard_number'] = $giftcard_info->giftcard_number;
        }
        $data['giftcard_info'] = $giftcard_info;
        $data['customer_info'] = $this->Customer->get_info($data['giftcard_info']->person_id);
        $data['controller_name'] = strtolower(get_class());
        $this->load->view('template/header', $data);
        $this->load->view('giftcards/form', $data);
        $this->load->view('template/footer');
    }

    /**
     * [POST/PUT] Save giftcard info to database
     *
     * @param int $giftcard_id default -1
     */
    function save($giftcard_id = -1)
    {
        $giftcard_data = array(
            'giftcard_number' => $this->input->post('giftcard_number'),
            'value' => $this->input->post('value'),
            'person_id' => $this->input->post('customer_id')
        );

        if ($this->Giftcard->save($giftcard_data, $giftcard_id)) {
            if ($giftcard_id == -1) {
                //New giftcard
                set_notif('success', $this->lang->line('giftcards_successful_adding') . ' ' . $giftcard_data['giftcard_number']);
                $giftcard_id = $giftcard_data['giftcard_id'];
            } else {
                //previous giftcard
                set_notif('success', $this->lang->line('giftcards_successful_updating') . ' ' . $giftcard_data['giftcard_number']);
            }
        } else {
            //failure
            set_notif('danger', $this->lang->line('giftcards_error_adding_updating') . ' ' . $giftcard_data['giftcard_number']);
        }
        redirect(site_url('giftcards'));
    }

    /**
     * [DELETE] Soft delete giftcard information
     * @param int $giftcard_id giftcard_id
     */
    function delete($giftcard_id)
    {
        if ($this->Giftcard->delete($giftcard_id)) {
            set_notif('success', $this->lang->line('giftcards_successful_deleted') . ' ' . count($giftcard_id) . ' ' . $this->lang->line('giftcards_one_or_multiple'));
        } else {
            set_notif('danger', $this->lang->line('giftcards_cannot_be_deleted'));
        }
        redirect(site_url('giftcards'));
    }

    /**
     * Clears the giftcard number for reuseable purposes
     */
    function clean_up()
    {
        if ($this->Giftcard->delete_all_depleted_giftcard()) {
            redirect(site_url('giftcards'));
        }
    }
}

/* End of file giftcards.php */
/* Location: ./application/controllers/giftcards.php */