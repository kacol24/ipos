<?php
/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");

/**
 * @todo Make item kits as a separate item, complete with product code and custom price
 */
class Item_kits extends Secure_area implements iData_controller
{

    function __construct()
    {
        parent::__construct('item_kits');
    }

    /**
     * [GET] Display table of item_kits
     */
    function index()
    {
        $this->breadcrumbs->push($this->lang->line('module_item_kits'), '/item_kits');
        $data['controller_name'] = strtolower(get_class());
        $data['manage_table'] = get_item_kits_manage_table($this->Item_kit->get_all(), $this);
        $this->load->view('template/header', $data);
        $this->load->view('item_kits/manage', $data);
        $this->load->view('template/footer');
    }

    /**
     * This method might not be needed and therefore being deprecated
     *
     * @todo Remove on v1
     */
    function search()
    {
        $search = $this->input->post('search');
        $data_rows = get_item_kits_manage_table_data_rows($this->Item_kit->search($search), $this);
        echo $data_rows;
    }

    /**
     * This method might not be needed and therefore being deprecated
     *
     * @todo Remove on v1
     */
    function suggest()
    {
        $suggestions = $this->Item_kit->get_search_suggestions($this->input->post('q'), $this->input->post('limit'));
        echo implode("\n", $suggestions);
    }

    /**
     * This method might not be needed and therefore being deprecated
     *
     * @todo Remove on v1
     */
    function get_row()
    {
        $item_kit_id = $this->input->post('row_id');
        $data_row = get_item_kit_data_row($this->Item_kit->get_info($item_kit_id), $this);
        echo $data_row;
    }

    /**
     * [GET] Display the item kit new/edit form
     *
     * @param int $item_kit_id default -1
     */
    function view($item_kit_id = -1)
    {
        $this->breadcrumbs->push($this->lang->line('module_item_kits'), '/item_kits');
        $data['custom_subtitle'] = '';
        if ($item_kit_id == -1) {
            $this->breadcrumbs->push($this->lang->line('item_kits_new'), '/new');
            $data['custom_title'] = $this->lang->line('item_kits_new');
        } else {
            $this->breadcrumbs->push($this->lang->line('item_kits_update'), '/update');
            $data['custom_title'] = $this->lang->line('item_kits_update');
        }
        $data['item_kit_info'] = $this->Item_kit->get_info($item_kit_id);
        $data['item_kit_items'] = $this->Item_kit_items->get_info($item_kit_id);
        foreach ($data['item_kit_items'] as $item) {
            $data['item_info'][] = $this->Item->get_info($item['item_id']);
        }
        $data['controller_name'] = strtolower(get_class());
        $this->load->view('template/header', $data);
        $this->load->view('item_kits/form', $data);
        $this->load->view('template/footer');
    }

    /**
     * [POST/PUT] Save item kit information to database
     *
     * @param int $item_kit_id default -1
     */
    function save($item_kit_id = -1)
    {
        $item_kit_data = array(
            'name' => $this->input->post('name'),
            'description' => $this->input->post('description')
        );

        if ($this->Item_kit->save($item_kit_data, $item_kit_id)) {
            if ($item_kit_id == -1) {
                //New item kit
                set_notif('success', $this->lang->line('item_kits_successful_adding') . ' ' . $item_kit_data['name']);
                $item_kit_id = $item_kit_data['item_kit_id'];
            } else {
                //previous item
                set_notif('success', $this->lang->line('item_kits_successful_updating') . ' ' . $item_kit_data['name']);
            }

            if ($this->input->post('item_kit_item')) {
                $item_kit_items = array();
                foreach ($this->input->post('item_kit_item') as $item_id => $quantity) {
                    $item_kit_items[] = array(
                        'item_id' => $item_id,
                        'quantity' => $quantity
                    );
                }

                $this->Item_kit_items->save($item_kit_items, $item_kit_id);
            }
        } else {
            //failure
            set_notif('danger', $this->lang->line('item_kits_error_adding_updating') . ' ' . $item_kit_data['name']);
        }
        redirect(site_url('item_kits'));
    }

    /**
     * [DELETE] Delete the item kit from database
     *
     * @todo Make item kit delete using soft delete
     *
     * @param int $item_kit_id item_kit_id
     */
    function delete($item_kit_id)
    {
        if ($this->Item_kit->delete($item_kit_id)) {
            set_notif('success', $this->lang->line('item_kits_successful_deleted') . ' ' . count($item_kit_id) . ' ' . $this->lang->line('item_kits_one_or_multiple'));
        } else {
            set_notif('danger', $this->lang->line('item_kits_cannot_be_deleted'));
        }
        redirect(site_url('item_kits'));
    }
}

/* End of file item_kits.php */
/* Location: ./application/controllers/item_kits.php */