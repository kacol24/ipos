<?php
/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
if ( ! defined('BASEPATH'))
    exit('No direct script access allowed');

require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");

/**
 * Location Module
 *
 * Manage store locations, table based display, with CRUD functionality
 *
 * @todo Need some more work, make the locations a separate module
 */
class Locations extends Secure_area implements iData_controller
{

    function __construct()
    {
        parent::__construct('locations');
    }

    function index()
    {

    }

    public function delete($location_id)
    {

    }

    public function get_row()
    {

    }

    public function save($location_id = -1)
    {

    }

    public function search()
    {

    }

    public function suggest()
    {

    }

    public function view($location_id = -1)
    {

    }
}

/* End of file locations.php */
/* Location: ./application/controllers/locations.php */