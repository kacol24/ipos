<?php
/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
require_once ("person_controller.php");

class Customers extends Person_controller
{

    function __construct()
    {
        parent::__construct('customers');
    }

    /**
     * [GET] Display table of customers
     */
    function index()
    {
        $this->breadcrumbs->push($this->lang->line('module_customers'), '/customers');
        $data['controller_name'] = strtolower(get_class());
        $data['manage_table'] = get_people_manage_table($this->Customer->get_all(), strtolower(get_class()));
        $this->load->view('template/header', $data);
        $this->load->view('people/manage', $data);
        $this->load->view('template/footer');
    }

    /**
     * This method might not be needed and therefore being deprecated
     *
     * @todo Remove on v1
     */
    function search()
    {
        $search = $this->input->post('search');
        $data_rows = get_people_manage_table_data_rows($this->Customer->search($search), $this);
        echo $data_rows;
    }

    /**
     * This method might not be needed and therefore being deprecated
     *
     * @todo Remove on v1
     */
    function suggest()
    {
        $suggestions = $this->Customer->get_search_suggestions($this->input->post('q'), $this->input->post('limit'));
        echo implode("\n", $suggestions);
    }

    /**
     * [GET] Display the customer new/edit form
     *
     * @param int $customer_id default -1
     */
    function view($customer_id = -1)
    {
        $data['person_info'] = $this->Customer->get_info($customer_id);
        if ($this->uri->segment(1) == 'sales') {
            // means new customer directly from sales
            $this->breadcrumbs->push($this->lang->line('module_sales'), '/sales');
            $this->breadcrumbs->push($this->lang->line('customers_new'), '/new');
            $data['controller_name'] = 'sales';
            $data['from_sales'] = true;
            $data['person_info']->person_id = -1;
        } else {
            // general new customer
            $data['from_sales'] = false;
            $data['controller_name'] = strtolower(get_class());
            $this->breadcrumbs->push($this->lang->line('module_customers'), '/customers');
        }
        if ($customer_id == -1) {
            $this->breadcrumbs->push($this->lang->line('customers_new'), '/new');
            $data['custom_title'] = $this->lang->line('customers_new');
        } else {
            $this->breadcrumbs->push($this->lang->line('customers_update'), '/update');
            $data['custom_title'] = $this->lang->line('customers_update');
        }
        $data['custom_subtitle'] = '';
        $data['custom_price'] = $this->Item_custom_unit_price->get_items($customer_id)->result();
        $this->load->view("template/header", $data);
        $this->load->view("customers/form", $data);
        $this->load->view("template/footer");
    }

    /**
     * [POST/PUT] Save customer info to database
     *
     * @param int $customer_id default -1
     * @param boolean $from_sales default FALSE
     */
    function save($customer_id = -1, $from_sales = FALSE)
    {
        $person_data = array(
            'first_name' => $this->input->post('first_name'),
            'last_name' => $this->input->post('last_name'),
            'email' => $this->input->post('email'),
            'phone_number' => $this->input->post('phone_number'),
            'address_1' => $this->input->post('address_1'),
            'address_2' => $this->input->post('address_2'),
            'city' => $this->input->post('city'),
            'state' => $this->input->post('state'),
            'zip' => $this->input->post('zip'),
            'country' => $this->input->post('country'),
            'comments' => $this->input->post('comments')
        );
        $customer_data = array(
            'courier' => $this->input->post('courier'),
            'taxable' => $this->input->post('taxable') == '' ? 0 : 1,
            'customer_discount' => (int) $this->input->post('customer_discount')
        );
        $id = $this->Customer->save($person_data, $customer_data, $customer_id, $from_sales);
        if ($id) {
            if ($customer_id == -1) {
                //New customer
                set_notif('success', lang('customers_successful_adding') . ' ' . $person_data['first_name'] . ' ' . $person_data['last_name']);
            } else {
                //previous customer
                set_notif('success', lang('customers_successful_updating') . ' ' . $person_data['first_name'] . ' ' . $person_data['last_name']);
            }
        } else {
            //failure
            set_notif('danger', lang('customers_error_adding_updating') . ' ' . $person_data['first_name'] . ' ' . $person_data['last_name']);
        }
        if ($this->config->item('enable_custom_price')) {
            $custom_price = $this->input->post('custom_price');
            if ($custom_price) {
                $temp = array();
                foreach ($custom_price as $item_id => $unit_price) {
                    $temp[] = array(
                        'item_id' => $item_id,
                        'person_id' => $customer_id,
                        'unit_price' => $unit_price
                    );
                }
                $this->Item_custom_unit_price->save($temp, false, $customer_id);
            }
        }
        if ($from_sales) {
            $this->load->library('sale_lib');
            $this->sale_lib->set_customer($id);
            redirect(site_url('sales'));
        }
        redirect(site_url('customers'));
    }

    /**
     * [DELETE] Soft delete the customer
     *
     * @param int $person_id person_id
     */
    function delete($person_id)
    {
        if ($this->Customer->delete($person_id)) {
            set_notif('success', $this->lang->line('customers_successful_deleted') . ' ' . count($$person_id) . ' ' . $this->lang->line('customers_one_or_multiple'));
        } else {
            set_notif('danger', $this->lang->line('customers_cannot_be_deleted'));
        }
        redirect(site_url('customers'));
    }

    /**
     * Let the user download import template
     */
    function excel()
    {
        $data = file_get_contents("import_customers.csv");
        $name = 'import_customers.csv';
        force_download($name, $data);
    }

    /**
     * [GET] Display the excel import form
     */
    function excel_import()
    {
        $this->breadcrumbs->push($this->lang->line('module_customers'), '/customers');
        $this->breadcrumbs->push('Excel Import', '/excel_import');
        $data['controller_name'] = strtolower(get_class());
        $this->load->view('template/header', $data);
        $this->load->view("customers/excel_import", null);
        $this->load->view('template/footer');
    }

    /**
     * [POST] Save the imported excel file to database
     */
    function do_excel_import()
    {
        $msg = 'do_excel_import';
        $failCodes = array();
        if ($_FILES['file_path']['error'] != UPLOAD_ERR_OK) {
            $msg = $this->lang->line('items_excel_import_failed');
            set_notif('danger', $msg);
        } else {
            if (($handle = fopen($_FILES['file_path']['tmp_name'], "r")) !== FALSE) {
                //Skip first row
                fgetcsv($handle);
                $i = 1;
                while (($data = fgetcsv($handle)) !== FALSE) {
                    $person_data = array(
                        'first_name' => $data[0],
                        'last_name' => $data[1],
                        'email' => $data[2],
                        'phone_number' => $data[3],
                        'address_1' => $data[4],
                        'address_2' => $data[5],
                        'city' => $data[6],
                        'state' => $data[7],
                        'zip' => $data[8],
                        'country' => $data[9],
                        'comments' => $data[10]
                    );
                    $customer_data = array(
                        'courier' => $data[12],
                        'customer_discount' => $data[13] == '' ? 0 : $data[13],
                        'taxable' => $data[14] == 'y' ? 1 : 0,
                    );
                    if ( ! $this->Customer->save($person_data, $customer_data)) {
                        $failCodes[] = $i;
                    }
                    $i ++;
                }
            } else {
                set_notif('danger', 'Your upload file has no data or not in supported format.');
                redirect(site_url('customer/excel_import'));
            }
        }
        $success = true;
        if (count($failCodes) > 1) {
            $msg = "Most customers imported. But some were not, here is list of their CODE (" . count($failCodes) . "): " . implode(", ", $failCodes);
            $success = false;
            $type = 'danger';
        } else {
            $msg = "Import Customers successful";
            $type = 'success';
        }
        set_notif($type, $msg);
        redirect(site_url('customers/excel_import'));
    }

    function suggest_courier()
    {
        $query = $this->input->get('query');
        $suggestions = $this->Customer->get_courier_suggestions($query);
//        echo $suggestions;
//        $this->output->set_content_type('application/json')->set_output(json_encode());
        $this->jsonify($suggestions);
    }
}

/* End of file customers.php */
/* Location: ./application/controllers/customers.php */