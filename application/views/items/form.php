<div class="col-xs-12">
    <?php if (validation_errors()): ?>
        <div class="callout callout-danger">
            <?php echo validation_errors(); ?>
        </div>
    <?php endif; ?>
    <div class="box box-solid">
        <div class="box-body">
            <?php echo form_open("items/save/" . $item_info->item_id, array('id' => 'item_form', 'class' => 'form-horizontal')); ?>
            <fieldset>
                <legend>
                    <?php echo $this->lang->line('items_basic_information'); ?>
                </legend>
                <div class="form-group">
                    <label for="item_number" class="control-label col-sm-2"><?php echo $this->lang->line('items_item_number'); ?></label>
                    <div class="col-sm-5">
                        <input type="text" name="item_number" id="item_number" class="form-control" value="<?php echo $item_info->item_number; ?>" autofocus>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="control-label col-sm-2 text-red"><?php echo $this->lang->line('items_name'); ?></label>
                    <div class="col-sm-5">
                        <input type="text" name="name" id="name" class="form-control" value="<?php echo $item_info->name; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="category" class="control-label col-sm-2 text-red"><?php echo $this->lang->line('items_category'); ?></label>
                    <div class="col-sm-5">
                        <input type="text" name="category" id="category" class="form-control" value="<?php echo $item_info->category; ?>" autocomplete="off">
                    </div>
                </div>
                <div class="form-group">
                    <label for="supplier" class="control-label col-sm-2 text-red"><?php echo $this->lang->line('items_supplier'); ?></label>
                    <div class="col-sm-5">
                        <?php echo form_dropdown('supplier_id', $suppliers, $selected_supplier, 'class="form-control" id="supplier"'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="description" class="control-label col-sm-2"><?php echo $this->lang->line('items_description'); ?></label>
                    <div class="col-sm-5">
                        <textarea name="description" id="description" class="form-control"><?php echo $item_info->description; ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="is_serialized" class="control-label col-sm-2 no-padding-top"><?php echo $this->lang->line('items_is_serialized'); ?></label>
                    <div class="col-sm-5">
                        <input type="checkbox" name="is_serialized" id="is_serialized" class="iCheck" <?php echo $item_info->is_serialized ? 'checked' : ''; ?>/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="is_deleted" class="control-label col-sm-2 no-padding-top"><?php echo $this->lang->line('items_is_deleted'); ?></label>
                    <div class="col-sm-5">
                        <input type="checkbox" name="is_deleted" id="is_deleted" class="iCheck" <?php echo $item_info->deleted ? 'checked' : ''; ?>/>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend>
                    <?php echo $this->lang->line('items_price_information'); ?>
                </legend>
                <div class="form-group">
                    <label for="cost_price" class="control-label col-sm-2 text-red"><?php echo $this->lang->line('items_cost_price'); ?></label>
                    <div class="col-sm-5 input-group add-padding-right-left">
                        <div class="input-group-addon"><?php echo $this->config->item('currency_symbol'); ?></div>
                        <input type="number" name="cost_price" id="cost_price" class="form-control text-right" min="0" max="999999999999" value="<?php echo $item_info->cost_price; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="unit_price" class="control-label col-sm-2 text-red"><?php echo $this->lang->line('items_unit_price'); ?></label>
                    <div class="col-sm-5 input-group add-padding-right-left">
                        <div class="input-group-addon"><?php echo $this->config->item('currency_symbol'); ?></div>
                        <input type="number" name="unit_price" id="unit_price" class="form-control text-right" min="0" max="999999999999" value="<?php echo $item_info->unit_price; ?>">
                    </div>
                </div>
                <!--
                <div class="form-group">
                    <label for="tax_name_1" class="control-label col-sm-2 col-xs-12"><?php echo $this->lang->line('items_tax_1'); ?></label>
                    <div class="col-sm-3 col-xs-7">
                        <input type="text" name="tax_names[]" id="tax_name_1" class="form-control" value="<?php echo isset($item_tax_info[0]['name']) ? $item_tax_info[0]['name'] : $this->config->item('default_tax_1_name'); ?>"/>
                    </div>
                    <div class="input-group col-sm-2 col-xs-5 add-padding-right-left">
                        <input type="number" name="tax_percents[]" id="tax_percent_name_1" class="form-control text-right" min="0" max="100" value="<?php echo isset($item_tax_info[0]['percent']) ? $item_tax_info[0]['percent'] : $default_tax_1_rate; ?>"/>
                        <div class="input-group-addon">%</div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="tax_name_2" class="control-label col-sm-2 col-xs-12"><?php echo $this->lang->line('items_tax_2'); ?></label>
                    <div class="col-sm-3 col-xs-7">
                        <input type="text" name="tax_names[]" id="tax_name_2" class="form-control" value="<?php echo isset($item_tax_info[1]['name']) ? $item_tax_info[1]['name'] : $this->config->item('default_tax_2_name'); ?>"/>
                    </div>
                    <div class="input-group col-sm-2 col-xs-5 add-padding-right-left">
                        <input type="number" name="tax_percents[]" id="tax_percent_name_2" class="form-control text-right" min="0" max="100" value="<?php echo isset($item_tax_info[1]['percent']) ? $item_tax_info[1]['percent'] : $default_tax_2_rate; ?>"/>
                        <div class="input-group-addon">%</div>
                    </div>
                </div>
                -->
                <?php if ($this->config->item('enable_custom_price')): ?>
                    <div class="form-group">
                        <label class="control-label col-sm-2"><?php echo lang('items_custom_price'); ?></label>
                        <div class="col-sm-5">
                            <input type="text" id="customer" class="form-control" placeholder="<?php echo lang('sales_start_typing_customer_name'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12 col-sm-7">
                            <div class="table-responsive">
                                <table class="table table-condensed table-bordered" id="custom_price_table">
                                    <thead>
                                        <tr>
                                            <th><?php echo lang('customers_customer'); ?></th>
                                            <th><?php echo lang('items_unit_price'); ?></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($custom_price as $row): ?>
                                            <tr class="no-transition">
                                                <td class="vertical-middle">
                                                    <?php echo $row->first_name . ' ' . $row->last_name; ?>
                                                </td>
                                                <td>
                                                    <input type="number" name="custom_price[<?php echo $row->person_id; ?>]" class="form-control text-right" min="0" max="999999999999" value="<?php echo $row->unit_price; ?>">
                                                </td>
                                                <td class="text-center">
                                                    <button class="btn btn-sm btn-danger" onclick="return delete_custom_price_row(this);"> <i class="fa fa-trash-o"> </i></button>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </fieldset>
            <fieldset>
                <legend>
                    <?php echo $this->lang->line('items_inventory_information'); ?>
                </legend>
                <div class="form-group">
                    <label for="stock_location" class="control-label col-sm-2 text-red"><?php echo $this->lang->line('items_quantity'); ?></label>
                    <div class="col-sm-5">
                        <table class="table no-margin" id="stock_location">
                            <tbody>
                                <?php foreach ($stock_locations as $key => $location_detail): ?>
                                    <tr>
                                        <td>
                                            <?php echo $location_detail['location_name']; ?>
                                        </td>
                                        <td class="no-padding-right">
                                            <input type="number" name="<?php echo $key; ?>_quantity" id="<?php echo $key; ?>_quantity" class="form-control text-right" value="<?php echo $location_detail['quantity']; ?>">
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="form-group">
                    <label for="reorder_level" class="control-label col-sm-2 text-red"><?php echo $this->lang->line('items_reorder_level'); ?></label>
                    <div class="col-sm-5" data-toggle="tooltip" title="<?php echo $this->lang->line('items_reorder_level_desc'); ?>" data-placement="right">
                        <input type="number" name="reorder_level" id="reorder_level" class="form-control text-right" min="0" max="999999" value="<?php echo $item_info->reorder_level; ?>">
                    </div>
                </div>
            </fieldset>
            <div class="form-group">
                <div class="col-sm-7">
                    <input type="submit" name="submit" id="submit" class="btn btn-primary pull-right" value="<?php echo $this->lang->line('common_submit'); ?>"/>
                </div><!-- /.col-sm-7 -->
            </div><!-- /.form-group -->
        </div><!-- /.box-body -->
        <?php echo form_close(); ?>
        <?php echo form_open('items/add_custom_price_customer', array('id' => 'customer_form', 'class' => 'form-horizontal')); ?>
        <input type="hidden" name="unit_price" value="<?php echo $item_info->unit_price; ?>">
        <input type="hidden" name="customer_id" id="customer_id">
        <?php echo form_close(); ?>
    </div><!-- /.box -->
</div><!-- /.col-xs-12 -->
<script>
    function delete_custom_price_row(link) {
        var row = $(link).parent().parent();
        row.fadeOut();
        setTimeout(function () {
            row.remove();
        }, 500);
        return false;
    }
    $(document).ready(function () {
        $('#category').devbridgeAutocomplete({
            serviceUrl: '<?php echo site_url(); ?>/items/suggest_category',
            minChars: 1,
            onSearchStart: function (query) {
                console.log(query);
                $(this).addClass('loading-state');
            },
            onSearchComplete: function (query, suggestions) {
                console.log(query);
                console.log(suggestions);
                $(this).removeClass('loading-state');
            }
        });
        $('#customer').devbridgeAutocomplete({
            serviceUrl: '<?php echo site_url(); ?>/sales/customer_search',
            minChars: 1,
            onSearchStart: function (query) {
                console.log(query);
                $(this).addClass('loading-state');
            },
            onSearchComplete: function (query, suggestions) {
                console.log(query);
                console.log(suggestions);
                $(this).removeClass('loading-state');
            },
            onSelect: function (suggestion) {
                var construct_table = '';
                construct_table += '<tr>';
                construct_table += '<td>';
                construct_table += suggestion['value'];
                construct_table += '</td>';
                construct_table += '<td>';
                construct_table += '<input type="number" name="custom_price[' + suggestion['data'] + ']" class="form-control text-right" min="0" max="999999999999" value="<?php echo $item_info->unit_price; ?>">';
                construct_table += '</td>';
                construct_table += '<td class="text-center">';
                construct_table += '<button class="btn btn-sm btn-danger" onclick="return delete_custom_price_row(this);"> <i class="fa fa-trash-o"></i> </button>';
                construct_table += '</td>';

                construct_table += '</tr>';
                $('#custom_price_table tbody').append(construct_table);
                $(this).val('');
            }
        });
    });
</script>