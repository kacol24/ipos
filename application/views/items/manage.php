<div class="col-xs-12">
    <div class="box">
        <div class="box-header">
            <div class="pull-left add-padding-right">
                <div class="form-group">
                    <a href="<?php echo site_url($controller_name); ?>/new" class="btn btn-primary"><i class="glyphicon glyphicon-plus-sign"></i> <?php echo $this->lang->line($controller_name . '_new'); ?></a>
                    <?php echo anchor("$controller_name/excel_import", '<i class="fa fa-upload"></i> ' . $this->lang->line('common_import'), array('class' => 'btn btn-primary', 'title' => 'Import Items from Excel')); ?>
                </div>
            </div>
            <div class="pull-right">
                <a href="<?php echo site_url('items/bulk_edit'); ?>" class="btn btn-success" id="bulk_edit" disabled><i class="fa fa-edit"></i> <?php echo lang('items_bulk_edit'); ?></a>
            </div>
            <?php if (count($stock_locations) > 1): ?>
                <?php echo form_open('items/refresh', array('id' => 'stock_filter_form')); ?>
                <div class="col-sm-4 col-md-2">
                    <div class="row form-group">
                        <?php echo form_dropdown('stock_location', $stock_locations, $stock_location, 'id="stock_location" class="form-control" onchange="$(\'#stock_filter_form\').submit();"'); ?>
                    </div>
                </div>
                <?php echo form_close(); ?>
            <?php endif; ?>
        </div><!-- /.box-header -->
        <div class="box-body">
            <?php echo $manage_table; ?>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div><!-- /.col-xs-12 -->
<script>
    var table;
    $(function () {
        table = $("#item_table").DataTable({
            columnDefs: [
                {
                    targets: 0,
                    orderable: false,
                    searchable: false
                }
            ],
            order: [2, 'asc'],
//            language: {
//                url: "assets/datatables-lang/indonesian.json"
//            }
        });
        $('#item_table tbody').on('click', 'tr', function () {
            $(this).toggleClass('active');
            $(this).find('.iCheck').iCheck('toggle');
        });
        $('.iCheck').on('ifChecked', function () {
            $(this).parents('tr').addClass('active');
            if (table.rows('.active').data().length > 0) {
                $('#bulk_edit').removeAttr('disabled');
            }
        }).on('ifUnchecked', function () {
            $(this).parents('tr').removeClass('active');
            if (table.rows('.active').data().length === 0) {
                $('#bulk_edit').attr('disabled', 'disabled');
            }
        });
        $('#select_all').on('ifChecked', function () {
            $('#item_table .iCheck').each(function () {
                $(this).iCheck('check');
            });
        }).on('ifUnchecked', function () {
            $('#item_table .iCheck').each(function () {
                $(this).iCheck('uncheck');
            });
        });

        $('#bulk_edit').click(function (e) {
            e.preventDefault();
            if (table.rows('.active').data().length > 0) {
                var url = $(this).attr('href');
                $.get(url, function (response) {
                    $('#modal_holder').empty();
                    $('#modal_holder').html(response);
                    $('#modal_holder').find('.modal').modal('show');
                }).success(function () {
                    $('input:text:visible:first').focus();
                });
            }
            else {
                alert("<?php echo lang('items_none_selected'); ?>");
            }
        });
    });
</script>