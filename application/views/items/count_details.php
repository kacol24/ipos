<div class="col-xs-12">
    <div class="box box-solid">
        <div class="box-body clearfix">
            <?php echo form_open("items/save_inventory/{$item_info->item_id}", array('id' => 'item_form', 'class' => '')); ?>
            <div class="col-md-5">
                <fieldset>
                    <legend>
                        <?php echo $this->lang->line('items_basic_information'); ?>
                    </legend>
                    <div class="form-group">
                        <label class="control-label"><?php echo $this->lang->line('items_item_number'); ?></label>
                        <input type="text" class="form-control" value="<?php echo $item_info->item_number; ?>" readonly>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?php echo $this->lang->line('items_name'); ?></label>
                        <input type="text" class="form-control" value="<?php echo $item_info->name; ?>" readonly>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?php echo $this->lang->line('items_category'); ?></label>
                        <input type="text" class="form-control" value="<?php echo $item_info->category; ?>" readonly>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?php echo $this->lang->line('items_current_quantity'); ?></label>
                        <input type="text" class="form-control" id="quantity" value="<?php echo current($item_quantities); ?>" readonly>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?php echo $this->lang->line('items_stock_location'); ?></label>
                        <?php echo form_dropdown('stock_location', $stock_locations, current($stock_locations), 'id="location_control" class="form-control" onchange="display_stock(this.value)"'); ?>
                    </div>
                </fieldset>
            </div>
            <?php
            $inventory_array = $this->Inventory->get_inventory_data_for_item($item_info->item_id)->result_array();
            $employee_name = array();
            foreach ($inventory_array as $row)
            {
                $person_id = $row['trans_user'];
                $employee = $this->Employee->get_info($person_id);
                array_push($employee_name, $employee->first_name . ' ' . $employee->last_name);
            }
            ?>
            <div class="col-md-7">
                <fieldset>
                    <legend>
                        <?php echo lang('items_update_inventory'); ?>
                    </legend>
                    <div class="form-group">
                        <label class="control-label text-red"><?php echo $this->lang->line('items_add_minus'); ?></label>
                        <input type="number" class="form-control text-right" name="newquantity">
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?php echo $this->lang->line('items_inventory_comments'); ?></label>
                        <textarea class="form-control" rows="3" name="trans_comment"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="submit" id="submit" class="btn btn-primary pull-right" value="<?php echo $this->lang->line('common_submit'); ?>"/>
                    </div><!-- /.form-group -->
                </fieldset>
                <fieldset>
                    <legend>
                        <?php echo lang('items_inventory_tracking'); ?>
                    </legend>
                    <?php foreach ($stock_locations as $location_id => $name): ?>
                        <div class="no-transition inventory_details" data-location="<?php echo $location_id; ?>">
                            <table class="table table-bordered table-condensed table-striped simple-table dt-responsive no-wrap" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="all"><?php echo lang('reports_date'); ?></th>
                                        <th class="all"><?php echo lang('reports_employee'); ?></th>
                                        <th class="all"><?php echo lang('items_in_out'); ?></th>
                                        <th class="min-tablet-p"><?php echo lang('items_remarks'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($inventory_array as $key => $data):
                                        if ($data['trans_location'] == $location_id):
                                            ?>
                                            <tr>
                                                <td><?php echo $data['trans_date']; ?></td>
                                                <td><?php echo $employee_name[$key]; ?></td>
                                                <td class="text-right"><?php echo $data['trans_inventory'] > 0 ? '+' . $data['trans_inventory'] : $data['trans_inventory']; ?></td>
                                                <td><?php echo $data['trans_comment']; ?></td>
                                            </tr>
                                            <?php
                                        endif;
                                    endforeach;
                                    ?>
                                </tbody>
                            </table>
                        </div><!-- /.table-responsive -->
                    <?php endforeach; ?>
                </fieldset>
            </div>
        </div><!-- /.box-body -->
        <?php echo form_close(); ?>
    </div><!-- /.box -->
</div><!-- /.col-xs-12 -->
<script>
<?php reset($stock_locations); ?>
    var quantities = <?php echo json_encode($item_quantities); ?>;
    $(function () {
        $('.simple-table').DataTable({
            language: {
                url: "<?php echo $this->config->item('language') == 'en' ? '' : 'assets/datatables-lang/indonesian.json'; ?>"
            },
            searching: false,
            info: false,
            columnDefs: [],
            order: [0, 'desc']
        });
        var stock_location = <?php echo key($stock_locations); ?>;
        display_stock(stock_location);
    });
    function display_stock(location_id) {
        $('#quantity').val(quantities[location_id]);
        $('[data-location]').hide();
        $('[data-location="' + location_id + '"]').fadeIn();
    }
</script>