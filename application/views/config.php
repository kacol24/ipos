<div class="col-xs-12">
    <div class="box box-solid">
        <div class="box-body">
            <?php echo form_open('config/save/', array('id' => 'config_form', 'class' => 'form-horizontal', 'role' => 'form')); ?>
            <fieldset>
                <legend>
                    <?php echo $this->lang->line('config_info'); ?>
                    <span class="pull-right small"><small class="small"><em class="small"><?php echo $this->config->item('application_version'); ?></em></small></span>
                </legend>
                <p class="text-muted">
                    <small>* <?php echo $this->lang->line('common_fields_required_message'); ?></small>
                </p>
                <div class="form-group">
                    <label for="company" class="col-sm-2 control-label text-red"><?php echo $this->lang->line('config_company'); ?></label>
                    <div class="col-sm-5">
                        <input type="text" name="company" required id="company" class="form-control" placeholder="" value="<?php echo $this->config->item('company'); ?>"/>
                    </div><!-- /.col-sm-5 -->
                </div><!-- /.form-group -->
                <div class="form-group">
                    <label for="address" class="col-sm-2 control-label text-red"><?php echo $this->lang->line('config_address'); ?></label>
                    <div class="col-sm-5">
                        <input type="text" name="address" id="address" class="form-control" placeholder="" value="<?php echo $this->config->item('address'); ?>" required>
                    </div><!-- /.col-sm-5 -->
                </div><!-- /.form-group -->
                <div class="form-group">
                    <label for="city" class="col-sm-2 control-label"><?php echo $this->lang->line('config_city'); ?></label>
                    <div class="col-sm-5">
                        <input type="text" name="city" required id="city" class="form-control" placeholder="" value="<?php echo $this->config->item('city'); ?>"/>
                    </div><!-- /.col-sm-5 -->
                </div><!-- /.form-group -->
                <div class="form-group">
                    <label for="state" class="col-sm-2 control-label"><?php echo $this->lang->line('config_state'); ?></label>
                    <div class="col-sm-5">
                        <input type="text" name="state" id="state" class="form-control" placeholder="" value="<?php echo $this->config->item('state'); ?>"/>
                    </div><!-- /.col-sm-5 -->
                </div><!-- /.form-group -->
                <div class="form-group">
                    <label for="zip" class="col-sm-2 control-label"><?php echo $this->lang->line('config_zip'); ?></label>
                    <div class="col-sm-5">
                        <input type="text" name="zip" id="zip" class="form-control" placeholder="" value="<?php echo $this->config->item('zip'); ?>"/>
                    </div><!-- /.col-sm-5 -->
                </div><!-- /.form-group -->
                <div class="form-group">
                    <label for="country" class="col-sm-2 control-label text-red"><?php echo $this->lang->line('config_country'); ?></label>
                    <div class="col-sm-5">
                        <input type="text" name="country" required id="country" class="form-control" placeholder="" value="<?php echo $this->config->item('country'); ?>"/>
                    </div><!-- /.col-sm-5 -->
                </div><!-- /.form-group -->
                <div class="form-group">
                    <label for="phone" class="col-sm-2 control-label text-red"><?php echo $this->lang->line('config_phone'); ?></label>
                    <div class="col-sm-5">
                        <input type="tel" name="phone" required id="phone" class="form-control" placeholder="" value="<?php echo $this->config->item('phone'); ?>"/>
                    </div><!-- /.col-sm-5 -->
                </div><!-- /.form-group -->
                <div class="form-group">
                    <label for="fax" class="col-sm-2 control-label"><?php echo $this->lang->line('config_fax'); ?></label>
                    <div class="col-sm-5">
                        <input type="tel" name="fax" id="fax" class="form-control" placeholder="" value="<?php echo $this->config->item('fax'); ?>"/>
                    </div><!-- /.col-sm-5 -->
                </div><!-- /.form-group -->
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label"><?php echo $this->lang->line('common_email'); ?></label>
                    <div class="col-sm-5">
                        <input type="email" name="email" id="email" class="form-control" placeholder="" value="<?php echo $this->config->item('email'); ?>"/>
                    </div><!-- /.col-sm-5 -->
                </div><!-- /.form-group -->
            </fieldset>
            <fieldset>
                <legend>
                    <?php echo $this->lang->line('config_app_info'); ?>
                </legend>
                <div class="form-group">
                    <label for="language" class="col-sm-2 control-label"><?php echo $this->lang->line('config_language'); ?></label>
                    <div class="col-sm-5">
                        <?php
                        echo form_dropdown('language', array('en' => 'English', 'id' => 'Indonesian'), $this->config->item('language'), 'class="form-control custom-dropdown readonly" id="language"');

                        ?>
                    </div><!-- /.col-sm-5 -->
                </div><!-- /.form-group -->
                <div class="form-group">
                    <label for="timezone" class="col-sm-2 control-label"><?php echo $this->lang->line('config_timezone'); ?></label>
                    <div class="col-sm-5">
                        <?php
                        echo form_dropdown('timezone', array(
                            'Pacific/Midway' => '(GMT-11:00) Midway Island, Samoa',
                            'America/Adak' => '(GMT-10:00) Hawaii-Aleutian',
                            'Etc/GMT+10' => '(GMT-10:00) Hawaii',
                            'Pacific/Marquesas' => '(GMT-09:30) Marquesas Islands',
                            'Pacific/Gambier' => '(GMT-09:00) Gambier Islands',
                            'America/Anchorage' => '(GMT-09:00) Alaska',
                            'America/Ensenada' => '(GMT-08:00) Tijuana, Baja California',
                            'Etc/GMT+8' => '(GMT-08:00) Pitcairn Islands',
                            'America/Los_Angeles' => '(GMT-08:00) Pacific Time (US & Canada)',
                            'America/Denver' => '(GMT-07:00) Mountain Time (US & Canada)',
                            'America/Chihuahua' => '(GMT-07:00) Chihuahua, La Paz, Mazatlan',
                            'America/Dawson_Creek' => '(GMT-07:00) Arizona',
                            'America/Belize' => '(GMT-06:00) Saskatchewan, Central America',
                            'America/Cancun' => '(GMT-06:00) Guadalajara, Mexico City, Monterrey',
                            'Chile/EasterIsland' => '(GMT-06:00) Easter Island',
                            'America/Chicago' => '(GMT-06:00) Central Time (US & Canada)',
                            'America/New_York' => '(GMT-05:00) Eastern Time (US & Canada)',
                            'America/Havana' => '(GMT-05:00) Cuba',
                            'America/Bogota' => '(GMT-05:00) Bogota, Lima, Quito, Rio Branco',
                            'America/Caracas' => '(GMT-04:30) Caracas',
                            'America/Santiago' => '(GMT-04:00) Santiago',
                            'America/La_Paz' => '(GMT-04:00) La Paz',
                            'Atlantic/Stanley' => '(GMT-04:00) Faukland Islands',
                            'America/Campo_Grande' => '(GMT-04:00) Brazil',
                            'America/Goose_Bay' => '(GMT-04:00) Atlantic Time (Goose Bay)',
                            'America/Glace_Bay' => '(GMT-04:00) Atlantic Time (Canada)',
                            'America/St_Johns' => '(GMT-03:30) Newfoundland',
                            'America/Araguaina' => '(GMT-03:00) UTC-3',
                            'America/Montevideo' => '(GMT-03:00) Montevideo',
                            'America/Miquelon' => '(GMT-03:00) Miquelon, St. Pierre',
                            'America/Godthab' => '(GMT-03:00) Greenland',
                            'America/Argentina/Buenos_Aires' => '(GMT-03:00) Buenos Aires',
                            'America/Sao_Paulo' => '(GMT-03:00) Brasilia',
                            'America/Noronha' => '(GMT-02:00) Mid-Atlantic',
                            'Atlantic/Cape_Verde' => '(GMT-01:00) Cape Verde Is.',
                            'Atlantic/Azores' => '(GMT-01:00) Azores',
                            'Europe/Belfast' => '(GMT) Greenwich Mean Time : Belfast',
                            'Europe/Dublin' => '(GMT) Greenwich Mean Time : Dublin',
                            'Europe/Lisbon' => '(GMT) Greenwich Mean Time : Lisbon',
                            'Europe/London' => '(GMT) Greenwich Mean Time : London',
                            'Africa/Abidjan' => '(GMT) Monrovia, Reykjavik',
                            'Europe/Amsterdam' => '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna',
                            'Europe/Belgrade' => '(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague',
                            'Europe/Brussels' => '(GMT+01:00) Brussels, Copenhagen, Madrid, Paris',
                            'Africa/Algiers' => '(GMT+01:00) West Central Africa',
                            'Africa/Windhoek' => '(GMT+01:00) Windhoek',
                            'Asia/Beirut' => '(GMT+02:00) Beirut',
                            'Africa/Cairo' => '(GMT+02:00) Cairo',
                            'Asia/Gaza' => '(GMT+02:00) Gaza',
                            'Africa/Blantyre' => '(GMT+02:00) Harare, Pretoria',
                            'Asia/Jerusalem' => '(GMT+02:00) Jerusalem',
                            'Europe/Minsk' => '(GMT+02:00) Minsk',
                            'Asia/Damascus' => '(GMT+02:00) Syria',
                            'Europe/Moscow' => '(GMT+03:00) Moscow, St. Petersburg, Volgograd',
                            'Africa/Addis_Ababa' => '(GMT+03:00) Nairobi',
                            'Asia/Tehran' => '(GMT+03:30) Tehran',
                            'Asia/Dubai' => '(GMT+04:00) Abu Dhabi, Muscat',
                            'Asia/Yerevan' => '(GMT+04:00) Yerevan',
                            'Asia/Kabul' => '(GMT+04:30) Kabul',
                            'Asia/Baku' => '(GMT+05:00) Baku', /* GARRISON ADDED 4/20/2013 */
                            'Asia/Yekaterinburg' => '(GMT+05:00) Ekaterinburg',
                            'Asia/Tashkent' => '(GMT+05:00) Tashkent',
                            'Asia/Kolkata' => '(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi',
                            'Asia/Katmandu' => '(GMT+05:45) Kathmandu',
                            'Asia/Dhaka' => '(GMT+06:00) Astana, Dhaka',
                            'Asia/Novosibirsk' => '(GMT+06:00) Novosibirsk',
                            'Asia/Rangoon' => '(GMT+06:30) Yangon (Rangoon)',
                            'Asia/Bangkok' => '(GMT+07:00) Bangkok, Hanoi, Jakarta',
                            'Asia/Krasnoyarsk' => '(GMT+07:00) Krasnoyarsk',
                            'Asia/Hong_Kong' => '(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi',
                            'Asia/Irkutsk' => '(GMT+08:00) Irkutsk, Ulaan Bataar',
                            'Australia/Perth' => '(GMT+08:00) Perth',
                            'Australia/Eucla' => '(GMT+08:45) Eucla',
                            'Asia/Tokyo' => '(GMT+09:00) Osaka, Sapporo, Tokyo',
                            'Asia/Seoul' => '(GMT+09:00) Seoul',
                            'Asia/Yakutsk' => '(GMT+09:00) Yakutsk',
                            'Australia/Adelaide' => '(GMT+09:30) Adelaide',
                            'Australia/Darwin' => '(GMT+09:30) Darwin',
                            'Australia/Brisbane' => '(GMT+10:00) Brisbane',
                            'Australia/Hobart' => '(GMT+10:00) Hobart',
                            'Asia/Vladivostok' => '(GMT+10:00) Vladivostok',
                            'Australia/Lord_Howe' => '(GMT+10:30) Lord Howe Island',
                            'Etc/GMT-11' => '(GMT+11:00) Solomon Is., New Caledonia',
                            'Asia/Magadan' => '(GMT+11:00) Magadan',
                            'Pacific/Norfolk' => '(GMT+11:30) Norfolk Island',
                            'Asia/Anadyr' => '(GMT+12:00) Anadyr, Kamchatka',
                            'Pacific/Auckland' => '(GMT+12:00) Auckland, Wellington',
                            'Etc/GMT-12' => '(GMT+12:00) Fiji, Kamchatka, Marshall Is.',
                            'Pacific/Chatham' => '(GMT+12:45) Chatham Islands',
                            'Pacific/Tongatapu' => '(GMT+13:00) Nuku\'alofa',
                            'Pacific/Kiritimati' => '(GMT+14:00) Kiritimati'
                            ), $this->config->item('timezone') ? $this->config->item('timezone') : date_default_timezone_get(), 'class="form-control"');

                        ?>
                    </div><!-- /.col-sm-5 -->
                </div><!-- /.form-group -->
                <div class="form-group">
                    <label for="date_format" class="col-sm-2 control-label"><?php echo $this->lang->line('config_date_format'); ?></label>
                    <div class="col-sm-5">
                        <?php
                        echo form_dropdown('date_format', array(
                            'd/m/Y' => '30/12/2000',
                            'm/d/Y' => '12/30/2000',
                            'd-m-Y' => '30-12-2000',
                            'm-d-Y' => '12-30-2000'
                            ), $this->config->item('date_format'), 'class="form-control" id="date_format"');

                        ?>
                    </div><!-- /.col-sm-5 -->
                </div><!-- /.form-group -->
                <div class="form-group">
                    <label for="time_format" class="col-sm-2 control-label"><?php echo $this->lang->line('config_time_format'); ?></label>
                    <div class="col-sm-5">
                        <?php
                        echo form_dropdown('time_format', array(
                            'h:i A' => '01:00 PM',
                            'H:i' => '13:00',
                            'h:i:s A' => '01:30:59 PM',
                            'H:i:s' => '13:30:59'
                            ), $this->config->item('time_format'), 'class="form-control" id="time_format"');

                        ?>
                    </div><!-- /.col-sm-5 -->
                </div><!-- /.form-group -->
                <div class="form-group">
                    <label for="currency_symbol" class="col-sm-2 control-label"><?php echo $this->lang->line('config_currency_symbol'); ?></label>
                    <div class="col-sm-3">
                        <input type="text" name="currency_symbol" id="currency_symbol" class="form-control" placeholder="" value="<?php echo $this->config->item('currency_symbol'); ?>"/>
                    </div><!-- /.col-sm-5 -->
                    <div class="col-sm-2">
                        <label class="control-label">
                            <?php echo $this->lang->line('config_currency_side'); ?>
                            <input type="checkbox" name="currency_side" class="iCheck form-control checkbox" <?php echo $this->config->item('currency_side') ? 'checked' : ''; ?>>
                        </label>
                    </div>
                </div><!-- /.form-group -->
            </fieldset>
            <fieldset>
                <legend>
                    <?php echo $this->lang->line('config_inv_info'); ?>
                </legend>
                <div class="form-group">
                    <label for="fax" class="col-sm-2 control-label text-red"><?php echo $this->lang->line('config_stock_location'); ?></label>
                    <div class="col-sm-5">
                        <input type="text" name="stock_location" id="stock_location" class="form-control" placeholder="" value="<?php echo $location_names; ?>"/>
                    </div><!-- /.col-sm-5 -->
                </div><!-- /.form-group -->
                <div class="form-group">
                    <label for="default_tax_1_name" class="col-sm-2 control-label"><?php echo $this->lang->line('config_default_tax_rate_1'); ?></label>
                    <div class="col-sm-3">
                        <input type="text" name="default_tax_1_name" id="default_tax_1_name" class="form-control" placeholder="" value="<?php echo $this->config->item('default_tax_1_name') !== FALSE ? $this->config->item('default_tax_1_name') : $this->lang->line('items_sales_tax_1'); ?>"/>
                    </div><!-- /.col-sm-3 -->
                    <div class="col-sm-2">
                        <div class="input-group">
                            <input type="number" name="default_tax_1_rate" id="default_tax_1_rate" class="form-control text-right" min="0" max="100" value="<?php echo $this->config->item('default_tax_1_rate'); ?>">
                            <span class="input-group-addon">%</span>
                        </div>
                    </div>
                </div><!-- /.form-group -->
                <div class="form-group">
                    <label for="default_tax_2_name" class="col-sm-2 control-label"><?php echo $this->lang->line('config_default_tax_rate_2'); ?></label>
                    <div class="col-sm-3">
                        <input type="text" name="default_tax_2_name" id="default_tax_2_name" class="form-control" placeholder="" value="<?php echo $this->config->item('default_tax_2_name') !== FALSE ? $this->config->item('default_tax_2_name') : $this->lang->line('items_sales_tax_2'); ?>"/>
                    </div><!-- /.col-sm-3 -->
                    <div class="col-sm-2">
                        <div class="input-group">
                            <input type="number" name="default_tax_2_rate" id="default_tax_2_rate" class="form-control text-right" min="0" max="100" value="<?php echo $this->config->item('default_tax_2_rate'); ?>">
                            <span class="input-group-addon">%</span>
                        </div>
                    </div>
                </div><!-- /.form-group -->
                <div class="form-group">
                    <label for="enable_custom_price" class="col-sm-2 control-label no-padding-top"><?php echo lang('config_enable_custom_price'); ?></label><!-- TODO add to language -->
                    <div class="col-sm-5">
                        <input type="checkbox" name="enable_custom_price" id="enable_custom_price" class="form-control iCheck" <?php echo $this->config->item('enable_custom_price') ? 'checked' : ''; ?>/>
                    </div><!-- /.col-sm-5 -->
                </div><!-- /.form-group -->
            </fieldset>
            <fieldset>
                <legend>
                    <?php echo $this->lang->line('config_sales_and_receipt_info'); ?>
                </legend>
                <div class="form-group">
                    <label for="return_policy" class="col-sm-2 control-label"><?php echo $this->lang->line('config_return_policy'); ?></label>
                    <div class="col-sm-5">
                        <textarea name="return_policy" id="return_policy" class="form-control col-sm-10" rows="3" placeholder=""><?php echo $this->config->item('return_policy'); ?></textarea>
                    </div><!-- /.col-sm-5 -->
                </div><!-- /.form-group -->
                <div class="form-group">
                    <label for="show_return_policy" class="col-sm-2 control-label no-padding-top"><?php echo lang('config_show_return_policy'); ?></label><!-- TODO add to language -->
                    <div class="col-sm-5">
                        <input type="checkbox" name="show_return_policy" id="show_return_policy" class="form-control iCheck" <?php echo $this->config->item('show_return_policy') ? 'checked' : ''; ?>/>
                    </div><!-- /.col-sm-5 -->
                </div><!-- /.form-group -->
                <div class="form-group">
                    <label for="show_payment_records" class="col-sm-2 control-label no-padding-top"><?php echo lang('config_show_payment_records'); ?></label><!-- TODO add to language -->
                    <div class="col-sm-5">
                        <input type="checkbox" name="show_payment_records" id="show_payment_records" class="form-control iCheck" <?php echo $this->config->item('show_payment_records') ? 'checked' : ''; ?>/>
                    </div><!-- /.col-sm-5 -->
                </div><!-- /.form-group -->
                <div class="form-group">
                    <label for="print_after_sale" class="col-sm-2 control-label no-padding-top"><?php echo $this->lang->line('config_print_after_sale'); ?></label>
                    <div class="col-sm-5">
                        <input type="checkbox" name="print_after_sale" id="print_after_sale" class="form-control iCheck" <?php echo $this->config->item('print_after_sale') ? 'checked' : ''; ?>/>
                    </div><!-- /.col-sm-5 -->
                </div><!-- /.form-group -->
            </fieldset>
            <div class="form-group">
                <div class="col-sm-7">
                    <input type="submit" name="submit" id="submit" class="btn btn-primary pull-right" value="<?php echo $this->lang->line('common_submit'); ?>"/>
                </div><!-- /.col-sm-7 -->
            </div><!-- /.form-group -->
            <?php echo form_close(); ?>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div><!-- /.col-xs-12 -->
<script>
    $(function () {
        $('#stock_location').selectize({
            persist: false,
            openOnFocus: false,
            create: function (input) {
                return {
                    value: input,
                    text: input
                };
            }
        });
    });
</script>