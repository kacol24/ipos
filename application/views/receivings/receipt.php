<div class="col-sm-12">
    <h2 class="page-header clearfix">
        <div class="col-xs-6">
            <?php echo $receipt_title; ?>
        </div>
        <div class="col-xs-6 text-right">
            <small id="printed_on"></small>
        </div>
    </h2>
</div>
<!-- /.col -->
</div>
<!-- info row -->
<div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
        <?php if (isset($supplier)): ?>
            <?php echo $this->lang->line('suppliers_supplier'); ?>:
            <address>
                <strong><?php echo $supplier->company_name; ?></strong><br>
                <?php echo $supplier->first_name . ' ' . $supplier->last_name; ?><br>
                <?php echo $supplier->address_1; ?>
                <?php if ($supplier->city != '' && $supplier->zip != '' && $supplier->state != '' && $supplier->country != ''): ?>
                    <br><?php echo $supplier->city . ' ' . $supplier->zip . ', ' . $supplier->state . ', ' . $supplier->country; ?>
                <?php endif; ?>
                <?php if ($supplier->phone_number != ''): ?>
                    <br><?php echo $this->lang->line('common_phone_number'); ?>: <?php echo $supplier->phone_number; ?>
                <?php endif; ?>
                <?php if ($supplier->email != ''): ?>
                    <br><?php echo $this->lang->line('common_email'); ?>: <?php echo $supplier->email; ?>
                <?php endif; ?>
            </address>
        <?php endif; ?>
    </div>
    <div class="col-sm-4 invoice-col col-sm-push-4">
        <address>
            <strong><?php echo lang('recvs_receipt_#'); ?>: </strong><?php echo $receiving_id; ?><br/>
            <strong><?php echo lang('recvs_issue_date'); ?>: </strong><?php echo $transaction_time; ?><br/>
            <strong><?php echo lang('recvs_employee'); ?>: </strong><?php echo $employee; ?>
        </address>
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

<!-- Table row -->
<div class="row">
    <div class="col-sm-12 table-responsive">
        <table class="table table-condensed table-bordered invoice-items" width="100%">
            <thead>
                <tr>
                    <th class="col-xs-1"><?php echo $this->lang->line('sales_item_number'); ?></th>
                    <th class="col-xs-4"><?php echo $this->lang->line('sales_item_name'); ?></th>
                    <th class="col-xs-1 text-center"><?php echo $this->lang->line('sales_quantity'); ?></th>
                    <th class="col-xs-3 text-center"><?php echo $this->lang->line('sales_price'); ?></th>
                    <th class="col-xs-3 text-center"><?php echo $this->lang->line('sales_total'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($cart as $line => $item): ?>
                    <tr>
                        <td>
                            <?php echo ! empty($item['item_number']) ? "[{$item['item_number']}]" : ''; ?>
                        </td>
                        <td>
                            <dl>
                                <dt><?php echo $item['name']; ?><?php echo $show_stock_locations ? " [{$item['stock_name']}]" : ''; ?></dt>
                                <?php if ( ! empty($item['description']) || ! empty($item['serialnumber'])): ?>
                                    <dd>
                                        <?php echo $item['description'] !== '' ? "<small>" . $item['description'] . "</small>" : ''; ?><br>
                                        <?php echo $item['serialnumber'] !== '' && $item['serialnumber'] !== '0' ? "<small><em>" . $item['serialnumber'] . "</em></small>" : ''; ?>
                                    </dd>
                                <?php endif; ?>
                            </dl>
                        </td>
                        <td class="text-center"><?php echo $item['quantity']; ?></td>
                        <td class="text-right"><?php echo to_currency($item['price']); ?></td>
                        <td class="text-right"><?php echo to_currency($item['price'] * $item['quantity']); ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

<div class="row" id="payment_details">
    <!-- accepted payments column -->
    <!-- /.col -->
    <div class="col-xs-12 col-sm-6 col-md-4 pull-right payment_col">
        <p class="lead"><?php echo $this->lang->line('sales_amount_due'); ?></p>
        <div class="table-responsive">
            <table class="table">
<!--                <tr>
                    <th><?php echo $this->lang->line('sales_sub_total'); ?>:</th>
                    <td class="text-right"><?php echo to_currency($subtotal); ?></td>
                </tr>-->
                <tr>
                    <th><?php echo $this->lang->line('sales_grand_total'); ?>:</th>
                    <td class="text-right"><strong><u><?php echo to_currency($total); ?></u></strong></td>
                </tr>
                <?php if (isset($amount_change)): ?>
                    <tr>
                        <th><?php echo $this->lang->line('sales_payment'); ?>:</th>
                        <td>
                            <div class="clearfix">
                                <em class="pull-left"><?php echo $payment_type; ?></em>
                                <span class="pull-right"><?php echo to_currency($amount_tendered * -1); ?></span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo $this->lang->line('sales_change_due'); ?>:</th>
                        <td class="text-right"><u><?php echo $amount_change; ?></u></td>
                    </tr>
                <?php else: ?>
                    <tr>
                        <th><?php echo $this->lang->line('sales_payment'); ?>:</th>
                        <td>
                            <div class="clearfix">
                                <span class="pull-right"><?php echo $payment_type; ?></span>
                            </div>
                        </td>
                    </tr>
                <?php endif; ?>
            </table>
        </div>
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
<script>
    $(window).load(function () {
        if ($('html').find('.invoice')) {
            $('html, body').css('min-height', '0');
            $('html, body').removeAttr('style');
        }
    });
    function print_page() {
        window.print();
    }
    $(function () {
        var printed_on = '<strong>Printed on: </strong>' + moment().format('DD/MM/YYYY H:mm:ss') + '(<?php echo $employee; ?>)';
        var beforePrint = function () {
            $('#printed_on').html(printed_on);
        };
        var afterPrint = function () {
            $('#printed_on').empty();
        };
        if (window.matchMedia) {
            var mediaQueryList = window.matchMedia('print');
            mediaQueryList.addListener(function (mql) {
                if (mql.matches) {
                    beforePrint();
                } else {
                    afterPrint();
                }
            });
        }
        window.onbeforeprint = beforePrint;
        window.onafterprint = afterPrint;
    });
</script>
<!-- this row will not appear when printing -->
<div class="row no-print">
    <div class="col-xs-12">
        <div class="form-group checkbox">
            <button class="btn btn-default" onclick="print_page();"><i class="fa fa-print"></i> Print</button>
            <!--
            <label>
                <input type="checkbox" class="iCheck"> Print Duplicate
            </label>
            -->
        </div>
    </div>