<div class="error-page">
    <div id="no-access" class="error-content">
        <h3><i class="fa fa-warning text-yellow"></i> Oops!</h3>
        <p><?php echo lang('error_no_permission_module') . ' ' . $module_name; ?></p>
    </div>
</div>