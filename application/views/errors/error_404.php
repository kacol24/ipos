<div class="error-page">
    <h2 class="headline text-info"> 404</h2>
    <div class="error-content">
        <h3><i class="fa fa-warning text-yellow"></i> <?php echo lang('error_404_oops'); ?></h3>
        <p>
            <?php echo lang('error_404_message'); ?>
            <a href="<?php echo index_page(); ?>"><i class="fa fa-hand-o-left"></i></a>
        </p>
    </div><!-- /.error-content -->
</div><!-- /.error-page -->