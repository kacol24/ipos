<div class="col-xs-12">
    <div class="box">
        <div class="box-header">
            <a href="<?php echo site_url() . '/' . $controller_name; ?>/new" class="btn btn-primary"><i class="glyphicon glyphicon-plus-sign"></i> <?php echo $this->lang->line($controller_name . '_new'); ?></a>
            <?php
            if ($controller_name == 'customers') {
                echo anchor("$controller_name/excel_import", '<i class="fa fa-upload"></i> ' . $this->lang->line('common_import'), array('class' => 'btn btn-primary', 'title' => 'Import Items from Excel'));
            };
            ?>
        </div><!-- /.box-header -->
        <div class="box-body">
            <?php echo $manage_table; ?>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div><!-- /.col-xs-12 -->