<div class="col-md-8"><!-- cash register section -->
    <div class="panel panel-default">
        <div class="panel-heading clearfix">
            <?php echo form_open('sales/change_mode', array('id' => 'mode_form', 'class' => 'form-inline')); ?>
            <div class="input-group form-group">
                <label for="mode" class="control-label input-group-addon"><?php echo $this->lang->line('sales_mode'); ?></label>
                <?php echo form_dropdown('mode', $modes, $mode, 'class="form-control" id="mode" onchange="$(\'#mode_form\').submit();"'); ?>
            </div>
            <?php if ($show_stock_locations): ?>
                <div class="input-group form-group">
                    <label for="stock_location" class="control-label input-group-addon"><?php echo $this->lang->line('sales_stock_location'); ?></label>
                    <?php echo form_dropdown('stock_location', $stock_locations, $stock_location, 'class="form-control" id="stock_location" onchange="$(\'#mode_form\').submit();"'); ?>
                </div>
            <?php endif; ?>
            <div class="form-group pull-right">
                <a href="<?php echo site_url('sales/suspended'); ?>" class="btn btn-warning ajax_modal" title="<?php echo $this->lang->line('sales_suspended_sales'); ?>">
                    <?php echo $this->lang->line('sales_suspended_sales'); ?>
                    <?php if (count($suspended_sales) > 0): ?>
                        <small class="badge pull-right bg-red"><?php echo count($suspended_sales); ?></small>
                    <?php endif; ?>
                </a>
            </div>
            <?php echo form_close(); ?>
        </div><!-- /.panel-heading -->
        <div class="panel-body">
            <div class="input-group">
                <label id="item_label" for="item" class="control-label input-group-addon hidden-xs">
                    <?php
                    if ($mode == 'sale') {
                        echo $this->lang->line('sales_find_or_scan_item');
                    } else {
                        echo $this->lang->line('sales_find_or_scan_item_or_receipt');
                    }
                    ?>
                </label>
                <?php echo form_open("sales/add", array('id' => 'add_item_form', 'class' => '')); ?>
                <input type="text" id="item" name="query" autocomplete="off" class="form-control col-xs-12" placeholder="<?php echo $this->lang->line('sales_start_typing_item_name'); ?>" autofocus>
                <input type="hidden" id="item_id" name="item_id" value=""/>
                <?php echo form_close(); ?>
                <span class="input-group-btn">
                    <a onclick="scanBarcode();" data-toggle="tooltip" title="EXPERIMENTAL FEATURE" class="btn btn-primary">
                        <i class="fa fa-barcode"></i>
                    </a>
                </span>
            </div>
            <table id="register" class="table table-bordered table-condensed table-hover dt-responsive no-wrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="all">
                            <?php echo $this->lang->line('sales_item_number'); ?>
                        </th>
                        <th class="min-tablet-p">
                            <?php echo $this->lang->line('sales_item_name'); ?>
                        </th>
                        <th class="min-desktop">
                            <?php echo $this->lang->line('sales_price'); ?>
                        </th>
                        <th class="all">
                            <?php echo $this->lang->line('sales_quantity'); ?>
                        </th>
                        <th class="none">
                            <?php echo $this->lang->line('sales_discount'); ?>
                        </th>
                        <th class="min-tablet-l">
                            <?php echo $this->lang->line('sales_total'); ?>
                        </th>
                        <th class="all">
                            <?php echo $this->lang->line('common_delete'); ?>
                        </th>
                        <th class="none">
                            <?php echo $this->lang->line('sales_description_abbrv'); ?>
                        </th>
                    </tr>
                </thead>
                <tbody id="cart-contents">
                    <?php
                    $sum_quantity = 0;
                    foreach (array_reverse($cart, TRUE) as $line => $item):
                        $cur_item_info = $this->Item->get_info($item['item_id']);
                        $sum_quantity += $item['quantity'];
                        ?>
                        <tr>
                            <td class="control">
                                <?php echo $item['item_number']; ?>
                                <?php echo form_open("sales/edit_item/$line", array('id' => 'edit_item_form_' . $line)); ?>
                                <input type="hidden" name="default_quantity" value="<?php echo $item['quantity']; ?>">
                                <input type="hidden" name="default_price" value="<?php echo $item['price']; ?>">
                                <input type="hidden" name="defualt_location" value="<?php echo $item['item_location']; ?>">
                                <input type="hidden" name="default_discount" value="<?php echo $item['discount']; ?>">
                                <input type="hidden" name="default_serialnumber" value="<?php echo $item['serialnumber']; ?>">
                                <input type="hidden" name="default_description" value="<?php echo $item['description'] != '' ? $item['description'] : ''; ?>">
                                <?php echo form_close(); ?>
                            </td>
                            <td>
                                <?php if ($show_stock_locations): ?>
                                    <div data-toggle="tooltip" title="<?php echo $item['name'] . '(' . $item['in_stock'] . ') [' . $item['stock_name'] . ']'; ?>">
                                        <?php echo character_limiter($item['name'] . '(' . $item['in_stock'] . ') [' . $item['stock_name'] . ']', 20); ?>
                                    </div>
                                <?php else: ?>
                                    <div data-toggle="tooltip" title="<?php echo $item['name'] . '(' . $item['in_stock'] . ')'; ?>">
                                        <?php echo character_limiter($item['name'] . '(' . $item['in_stock'] . ')', 20); ?>
                                    </div>
                                <?php endif; ?>
                            </td>
                            <td class="text-right">
                                <?php echo to_currency($item['price']); ?>
                            </td>
                            <td class="text-center">
                                <?php if ($item['is_serialized']): ?>
                                    <?php echo $item['quantity']; ?>
                                <?php else: ?>
                                    <input type="number" name="quantity" form="edit_item_form_<?php echo $line; ?>" id="quantity_field" value="<?php echo $item['quantity']; ?>" <?php echo $mode == 'sale' ? 'min="1"' : 'min="-999"'; ?> max="999" autocomplete="off" class="text-right form-control input-sm"/>
                                <?php endif; ?>
                            </td>
                            <td>
                                <input type="number" name="discount" form="edit_item_form_<?php echo $line; ?>" value="<?php echo $item['discount']; ?>" min="0" max="100" autocomplete="off" class="text-right form-control input-sm"/>
                            </td>
                            <td class="text-right">
                                <?php echo to_currency($item['price'] * $item['quantity'] - $item['price'] * $item['quantity'] * $item['discount'] / 100); ?>
                            </td>
                            <td class="text-center">
                                <?php echo anchor("sales/delete_item/$line", ' <i class="fa fa-trash-o"></i> ', array('class' => 'btn btn-sm btn-danger')); ?>
                            </td>
                            <td>
                                <?php
                                if ($item['description'] != '') {
                                    echo $item['description'];
                                } else {
                                    echo $this->lang->line('sales_no_description');
                                }
                                if ($item['is_serialized']):
                                    ?>
                                    <br>
                                    <input type="text" name="serialnumber" form="edit_item_form_<?php echo $line; ?>" class="form-control input-sm" value="<?php echo $item['serialnumber']; ?>" placeholder="<?php echo $this->lang->line('sales_serial') ?>" autocomplete="off">
                                <?php endif; ?>
                            </td>
                        </tr>
                        <?php
                    endforeach;
                    ?>
                </tbody>
            </table>
        </div><!-- /.panel-body -->
        <div class="panel-footer clearfix">
            <div class="row">
                <div class="pull-left col-sm-4 col-xs-5">
                    <label class="form-control-static">
                        <?php echo $this->lang->line('sales_items_in_cart') . ': ' . $sum_quantity . '(' . count($cart) . ')'; ?>
                    </label>
                </div>
                <?php // echo $customer_discount;   ?>
                <?php if (count($cart) > 0): ?>
                    <div class="pull-right col-sm-4 col-xs-7">
                        <?php echo form_open('sales/global_discount'); ?>
                        <div class="input-group">
                            <label class="input-group-addon control-label hidden-xs">Global Discount</label>
                            <input type="number" name="discount" min="0" max="100" class="form-control input-sm text-right" placeholder="Global Discount">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-primary btn-sm">Add</button>
                            </span>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div><!-- /.panel -->
    <?php if (isset($recent_sales)): ?>
        <div class="box box-solid box-primary">
            <div class="box-header">
                <h3 class="box-title">Recent Sales - <?php echo $customer; ?></h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-sm btn-primary" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body no-transition">
                <div class="table-responsive">
                    <table class="table table-condensed table-striped">
                        <thead>
                            <tr>
                                <th><?php echo lang('sales_id'); ?></th>
                                <th><?php echo lang('sales_date'); ?></th>
                                <th><?php echo lang('reports_items_purchased'); ?></th>
                                <th><?php echo lang('sales_total'); ?></th>
                                <th><?php echo lang('sales_comment'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($recent_sales as $row): ?>
                                <tr>
                                    <td><a href="<?php echo site_url('/sales/receipt/' . $row['sale_id']); ?>">POS <?php echo $row['sale_id']; ?></a></td>
                                    <td><?php echo $row['sale_time']; ?></td>
                                    <td><?php echo $row['items_purchased']; ?></td>
                                    <td class="text-right"><?php echo to_currency($row['total']); ?></td>
                                    <td><?php echo $row['comment']; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div><!-- /.col-md-8 -->
<div class="col-md-4 sales-info"><!-- customer/transaction info section -->
    <div class="panel panel-default">
        <div class="panel-heading clearfix">
            <?php if (isset($customer)): ?>
                <div class="input-group">
                    <label class="control-label input-group-addon"><?php echo $this->lang->line('sales_customer'); ?></label>
                    <input type="text" class="form-control" value="<?php echo $customer; ?>" readonly>
                    <a href="<?php echo site_url(); ?>/sales/remove_customer" class="input-group-addon"><i class="fa fa-times text-red"></i></a>
                </div>
            <?php else: ?>
                <?php echo form_open('sales/select_customer', array('id' => 'select_customer_form', 'class' => '')); ?>
                <div class="input-group">
                    <label id="customer_label" for="customer" class="control-label input-group-addon hidden-xs"><?php echo $this->lang->line('sales_select_customer'); ?></label>
                    <input type="text" id="customer" name="query" autocomplete="off" class="form-control input-sm" placeholder="<?php echo $this->lang->line('sales_start_typing_customer_name'); ?>"/>
                    <span class="input-group-btn">
                        <a href="<?php echo site_url('sales/new_customer'); ?>" class="btn btn-primary btn-sm">New</a>
                    </span>
                </div>
                <input type="hidden" id="customer_id" name="customer_id" value=""/>
                <?php echo form_close(); ?>
            <?php endif; ?>
        </div><!-- /.panel-heading -->
        <div class="panel-body row">
            <?php $calc = $subtotal; ?>
            <table class="table no-margin">
                <tbody>
                    <?php if (count($cart) > 0): ?>
                        <?php echo form_open('sales/suspend'); ?>
                        <tr>
                            <td class="no-border no-padding-top">
                                <button class="btn btn-sm btn-danger" type="submit" name="cancel" value="true"><?php echo lang('sales_cancel_sale'); ?></button>
                            </td>
                            <td class="text-right no-border no-padding-top">
                                <button type="submit" class="btn btn-sm btn-warning"><?php echo lang('sales_suspend_sale'); ?></button>
                            </td>
                        </tr>
                        <?php echo form_close(); ?>
                    <?php endif; ?>
                    <tr>
                        <th class="no-border">
                            <?php echo lang('sales_total'); ?>:
                        </th>
                        <td class="text-right no-border">
                            <strong><?php echo to_currency($subtotal); ?></strong>
                        </td>
                    </tr>
                    <?php if (isset($customer_discount) && $customer_discount > 0 && count($cart) > 0 && $has_customer_discount > 0): ?>
                        <tr>
                            <td class="add-padding-left">
                                <?php echo form_open('sales/toggle_customer_discount', array('id' => 'customer_discount')); ?>
                                <label class="text-normal-weight">
                                    <?php echo $customer_discount . '% ' . lang('sales_customer_discount'); ?>
                                    <input type="checkbox" class="iCheck form-control" name="include_discount" id="include_discount" <?php echo $has_customer_discount ? 'checked' : ''; ?>>
                                    <input type="hidden" name="discount_value" value="<?php echo $customer_discount; ?>">
                                </label>
                                <?php echo form_close(); ?>
                            </td>
                            <td class="text-right">
                                <?php
                                if (count($has_customer_discount) > 0) {
                                    $subtract_by = $calc * $customer_discount / 100;
                                    $calc -=$subtract_by;
                                    echo to_currency(-$subtract_by);
                                }
                                ?>
                            </td>
                        </tr>
                    <?php endif; ?>
                    <?php foreach ($global_discounts as $key => $discount): ?>
                        <tr>
                            <td class="add-padding-left">
                                <a href="<?php echo site_url('/sales/global_discount/' . $key); ?>"><i class="fa fa-times text-red"></i></a>
                                <?php echo $discount . $this->lang->line('sales_discount_included'); ?>
                            </td>
                            <td class="text-right">
                                <?php
                                $subtract_by = $calc * $discount / 100;
                                $calc -= $subtract_by;
                                echo to_currency(-$subtract_by);
                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <tr>
                        <th>
                            <?php echo lang('sales_sub_total'); ?>:
                        </th>
                        <td class="text-right">
                            <strong><?php echo to_currency($calc); ?></strong>
                        </td>
                    </tr>
                    <?php foreach ($taxes as $name => $value): ?>
                        <?php if ($value != 0): ?>
                            <tr>
                                <td class="add-padding-left">
                                    <?php echo $name; ?>
                                </td>
                                <td class="text-right">
                                    <?php
                                    $calc += $value;
                                    echo to_currency($value);
                                    ?>
                                </td>
                            </tr>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <tr>
                        <th>
                            <?php echo lang('sales_grand_total'); ?>:
                        </th>
                        <td class="text-right">
                            <strong><?php echo to_currency($total); ?></strong>
                        </td>
                    </tr>
                    <?php if (count($cart) > 0): ?>
                        <?php if (count($payments) > 0): ?>
                            <tr>
                                <td colspan="2" class="no-padding-left no-padding-right no-border">
                                    <table class="table table-condensed no-margin-bottom no-border">
                                        <thead>
                                            <tr>
                                                <th><?php echo $this->lang->line('sales_payment_type'); ?></th>
                                                <th class="text-right"><?php echo $this->lang->line('sales_payment_amount'); ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($payments as $payment_id => $payment): ?>
                                                <tr>
                                                    <td class="vertical-middle">
                                                        <a href="<?php echo site_url(); ?>/sales/delete_payment/<?php echo $payment_id; ?>" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></a>
                                                        <?php echo $payment['payment_type'] == 'sales_giftcard' ? lang($payment['payment_type']) . ':' . $payment['giftcard_number'] : lang($payment['payment_type']); ?>
                                                    </td>
                                                    <td class="text-right vertical-middle">
                                                        <?php echo to_currency($payment['payment_amount']); ?>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        <?php endif; ?>
                        <?php if ($amount_due >= 0): ?>
                            <tr class="bg-success">
                                <th>
                                    <?php echo lang('sales_amount_due'); ?>:
                                </th>
                                <td class="text-right">
                                    <strong><?php echo to_currency($amount_due); ?></strong>
                                </td>
                            </tr>
                        <?php else: ?>
                            <tr class="bg-info">
                                <th>
                                    <?php echo lang('sales_change_due'); ?>:
                                </th>
                                <td class="text-right">
                                    <strong><?php echo to_currency($amount_due); ?></strong>
                                </td>
                            </tr>
                        <?php endif; ?>
                    <?php endif; ?>
                </tbody>
            </table>
        </div><!-- /.panel-body -->
        <?php if (count($cart) > 0): ?>
            <div class="panel-footer clearfix">
                <?php echo form_open('sales/add_payment', array('id' => 'add_payment_form', 'class' => 'form-horizontal')); ?>
                <div class="form-group">
                    <label for="payment_types" class="control-label col-md-5 force-text-left"><?php echo $this->lang->line('sales_payment'); ?></label>
                    <div class="col-md-7">
                        <?php echo form_dropdown('payment_type', $payment_options, array(), 'id="payment_types" class="form-control"'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="amount_tendered" id="amount_tendered_label" class="control-label col-md-5 force-text-left"><?php echo $this->lang->line('sales_amount_tendered'); ?></label>
                    <div class="col-md-7">
                        <input type="number" value="<?php echo $amount_due; ?>" name="amount_tendered" id="amount_tendered" class="text-right number form-control" step="any">
                    </div>
                </div>
                <?php echo form_close(); ?>
                <?php echo form_open('sales/complete', array('id' => 'finish_sale_form', 'class' => 'form-horizontal')); ?>
                <div class="form-group">
                    <div class="col-xs-12 text-right">
                        <?php if ($payments_cover_total && $mode == 'sale'): ?>
                            <a href="#" id="pay_button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#confirm_sale" data-backdrop="static"><?php echo $this->lang->line('sales_complete_sale'); ?></a>
                        <?php elseif ($mode == 'return'): ?>
                            <div class="pull-left">
                                <a href="#" id="pay_button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#confirm_sale" data-backdrop="static"><?php echo $this->lang->line('sales_complete_sale'); ?></a>
                            </div>
                            <div class="pull-right">
                                <button class="btn btn-sm btn-primary" form="add_payment_form"><?php echo $this->lang->line('sales_add_payment'); ?></button>
                            </div>
                        <?php else: ?>
                            <button class="btn btn-sm btn-primary" form="add_payment_form"><?php echo $this->lang->line('sales_add_payment'); ?></button>
                        <?php endif; ?>
                    </div>
                </div>
                <?php echo form_close(); ?>
                <?php echo form_open('sales/set_comment', array('id' => 'comment_form')); ?>
                <div class="form-group">
                    <div class="placard col-xs-12 no-padding-left no-padding-right" data-ellipsis="true" data-initialize="placard" id="comment_placard">
                        <div class="placard-popup"></div>
                        <div class="placard-header"><h5><?php echo lang('sales_comment'); ?></h5></div>
                        <textarea id="comment" class="form-control placard-field col-xs-12" name="comment" placeholder="<?php echo $this->lang->line('common_comments'); ?>"><?php echo $comment; ?></textarea>
                        <div class="placard-footer">
                            <button class="btn btn-primary btn-xs placard-accept" type="button">Save</button>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        <?php endif; ?>
    </div><!-- /.panel .panel-default -->
    <div class="modal fade" id="confirm_sale" tabindex="-1" role="dialog" aria-labelledby="confirm_sale" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Finish Sale?</h4>
                </div>
                <div class="modal-body">
                    <?php echo lang('sales_confirm_finish_sale'); ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('common_cancel'); ?></button>
                    <button type="button" class="btn btn-primary btn-ok"><?php echo lang('common_submit'); ?></button>
                </div>
            </div>
        </div>
    </div>
</div><!-- /.col-md-4 -->
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        // General functions
        $('#item').focus();
        $('#amount_tendered').focus(function () {
            $(this).select();
        });
        $('#include_discount').on('ifToggled', function () {
            $('#customer_discount').submit();
        });
        var quantity_changed = false;
        $("input[name='quantity'], input[name='price'], input[name='discount']").focus(function () {
            $(this).select();
        }).change(function () {
            quantity_changed = true;
        }).blur(function () {
            if (quantity_changed) {
                var form = $(this).attr('form');
                $("#" + form).submit();
            }
        }).keyup(function (e) {
            if (e.keyCode === 13 && quantity_changed) {
                var form = $(this).attr('form');
                $("#" + form).submit();
            }
        });
        $('#comment_placard').placard({
            onAccept: function () {
                $('#comment_form').submit();
            }
        });
        $('#register .control').on('click', function () {
            var changed = false;
            setTimeout(function () {
                $('.edit_item_placard').placard({
                    onAccept: function () {
                        $('#edit_item_form').submit();
                    }
                });
                $('input[name="serialnumber"], input[name="discount"]').focus(function () {
                    $(this).select();
                }).change(function () {
                    changed = true;
                }).keypress(function (e) {
                    if (e.keyCode === 13) {
                        e.preventDefault();
                        if (changed) {
                            $('#edit_item_form').submit();
                        }
                    }
                });
            }, 500);
        });
        function paymentTypeIsGiftcard() {
            if ($("#payment_types").val() === "sales_giftcard") {
                $("#amount_tendered_label").html("<?php echo $this->lang->line('sales_giftcard_number'); ?>");
                $("#amount_tendered_currency").hide();
                $("#amount_tendered").val('');
                $("#amount_tendered").focus();
            } else {
                $("#amount_tendered_label").html("<?php echo $this->lang->line('sales_amount_tendered'); ?>");
                $("#amount_tendered_currency").show();
                $("#amount_tendered").val(<?php echo $amount_due; ?>);
                $("#amount_tendered").focus();
                $("#amount_tendered").focus(function () {
                    $(this).select();
                });
            }
        }
        $("#payment_types").change(paymentTypeIsGiftcard).ready(paymentTypeIsGiftcard);
        // end of General functions


        // Hotkey assignments
        $('#item, #customer').jkey('esc', function () {
            $(this).val('');
        });
        // end of Hotkey assignments


        // Autocomplete initialization
        $('#item').devbridgeAutocomplete({
        serviceUrl: '<?php echo site_url(); ?>/sales/item_search',
                minChars: 1,
                onSearchStart: function (query) {
                    console.log(query);
                    $(this).addClass('loading-state');
                },
                onSearchComplete: function (query, suggestions) {
                    console.log(query);
                    console.log(suggestions);
                    $(this).removeClass('loading-state');
                },
                onSelect: function (suggestion) {
                    $('#item_id').val(suggestion.data);
                    $("#add_item_form").submit();
                },
<?php if ($mode == 'sale'): ?>
            showNoSuggestionNotice: true,
                    noSuggestionNotice: '<?php echo $this->lang->line('sales_no_item_found'); ?>'
<?php endif; ?>
    });
    $('#customer').devbridgeAutocomplete({
        serviceUrl: '<?php echo site_url(); ?>/sales/customer_search',
        minChars: 1,
        onSearchStart: function (query) {
            console.log(query);
            $(this).addClass('loading-state');
        },
        onSearchComplete: function (query, suggestions) {
            console.log(query);
            console.log(suggestions);
            $(this).removeClass('loading-state');
        },
        onSelect: function (suggestion) {
            $('#customer_id').val(suggestion.data);
            $("#select_customer_form").submit();
        },
        showNoSuggestionNotice: true,
        noSuggestionNotice: '<?php echo $this->lang->line('sales_no_customer_found'); ?>'
    });
    $('#confirm_sale').on('shown.bs.modal', function (e) {
        $(this).find('.btn-ok').click(function () {
            $('#finish_sale_form').submit();
        });
    });
    });
            function scanBarcode() {
                var haha = window.open('', '_parent', '');
                var hehe = window.open('', '_self', '');
                window.opener = self;
                window.location.href = "zxing://scan/?ret=<?php echo urlencode(site_url('sales/add/{CODE}')); ?>";
                haha.close();
                hehe.close();
            }
    // end of Autocomplete initialization
</script>