<?php if (isset($warning)): ?>
    <div class="alert alert-warning alert-dismissable fade-in" role="alert">
        <i class="fa fa-warning"></i>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <?php echo $warning; ?>
    </div>
<?php elseif (isset($error)): ?>
    <div class="alert alert-danger alert-dismissable fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <?php echo $error; ?>
    </div>
<?php endif; ?>
<div class="col-md-8"><!-- cash register section -->
    <div class="panel panel-default">
        <div class="panel-heading clearfix">
            <?php echo form_open('sales/change_mode', array('id' => 'mode_form', 'class' => 'form-inline')); ?>
            <div class="input-group form-group">
                <label for="mode" class="control-label input-group-addon"><?php echo $this->lang->line('sales_mode'); ?></label>
                <?php echo form_dropdown('mode', $modes, $mode, 'class="form-control" id="mode" onchange="$(\'#mode_form\').submit();"'); ?>
            </div>
            <div class="form-group pull-right">
                <a href="sales/suspended/width:425" class="btn btn-warning" title="<?php echo $this->lang->line('sales_suspended_sales'); ?>"><?php echo $this->lang->line('sales_suspended_sales'); ?></a>
            </div>
            <?php echo form_close(); ?>
        </div><!-- /.panel-heading -->
        <div class="panel-body">
            <?php echo form_open("sales/add", array('id' => 'add_item_form', 'class' => '')); ?>
            <div class="input-group">
                <label id="item_label" for="item" class="control-label input-group-addon bg-light-blue">
                    <?php
                    if ($mode == 'sale') {
                        echo $this->lang->line('sales_find_or_scan_item');
                    } else {
                        echo $this->lang->line('sales_find_or_scan_item_or_receipt');
                    }
                    ?>
                </label>
                <input type="text" id="item" name="query" autocomplete="off" class="form-control col-xs-12" placeholder="<?php echo $this->lang->line('sales_start_typing_item_name'); ?>" autofocus="on"/>
                <input type="hidden" id="item_id" name="item_id" value=""/>
            </div>
            <?php echo form_close(); ?>
            <table id="register" class="table table-bordered table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="all">
                            <?php echo $this->lang->line('sales_item_number'); ?>
                        </th>
                        <th class="min-tablet-l">
                            <?php echo $this->lang->line('sales_item_name'); ?>
                        </th>
                        <th class="min-desktop">
                            <?php echo $this->lang->line('sales_price'); ?>
                        </th>
                        <th class="all">
                            <?php echo $this->lang->line('sales_quantity'); ?>
                        </th>
                        <th class="none">
                            <?php echo $this->lang->line('sales_discount'); ?>
                        </th>
                        <th class="all">
                            <?php echo $this->lang->line('sales_sub_total'); ?>
                        </th>
                        <th class="all">
                            <?php echo $this->lang->line('common_delete'); ?>
                        </th>
                        <th class="none">
                            <?php echo $this->lang->line('sales_edit'); ?>
                        </th>
                        <th class="none">
                            <?php echo $this->lang->line('sales_description_abbrv'); ?>
                        </th>
                    </tr>
                </thead>
                <tbody id="cart-contents">
                    <?php
                    foreach ($cart as $line => $item):
                        $cur_item_info = $this->Item->get_info($item['item_id']);
                        ?>
                        <tr>
                            <td>
                                <?php echo $item['item_number']; ?>
                            </td>
                            <td>
                                <?php echo $item['name'] . ' [' . $item['in_stock'] . ']'; ?>
                            </td>
                            <td class="text-right">
                                <?php echo to_currency($item['price']); ?>
                                <input type="hidden" name="price" value="<?php echo $item['price']; ?>"/>
                            </td>
                            <td class="text-center">
                                <?php echo form_open("sales/edit_item/$line", array('id' => 'qty_edit_form')); ?>
                                <?php
                                if ($item['is_serialized'] == 1):
                                    echo $item['quantity'];
                                    ?>
                                    <input type="hidden" name="quantity" value="<?php echo $item['quantity']; ?>"/>
                                <?php else: ?>
                                    <input type="number" name="quantity" value="<?php echo $item['quantity']; ?>" min="1" max="999" autocomplete="off" class="text-right form-control"/>
                                <?php endif; ?>
                                <input type="hidden" name="location" value="<?php echo $item['item_location']; ?>"/>
                                <input type="hidden" name="discount" value="<?php echo $item['discount']; ?>"/>
                                <?php echo form_close(); ?>
                            </td>
                            <td class="">
                                <?php echo form_open("sales/edit_item/$line", array('id' => 'disc_edit_form', 'class' => 'form-horizontal', 'role' => 'form')); ?>
                                <input type="number" name="discount" value="<?php echo $item['discount']; ?>" min="0" max="100" autocomplete="off" class="text-right form-control"/>
                                <input type="hidden" name="quantity" value="<?php echo $item['quantity']; ?>"/>
                                <input type="hidden" name="location" value="<?php echo $item['item_location']; ?>"/>
                                <?php echo form_close(); ?>
                            </td>
                            <td class="text-right">
                                <?php echo to_currency($item['price'] * $item['quantity'] - $item['price'] * $item['quantity'] * $item['discount'] / 100); ?>
                            </td>
                            <td class="text-center">
                                <?php echo anchor("sales/delete_item/$line", ' <i class="fa fa-trash-o"></i> ', array('class' => 'btn btn-sm btn-danger')); ?>
                            </td>
                            <td>
                                <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-edit"></i> <?php echo $this->lang->line('sales_edit_item'); ?></button>
                            </td>
                            <td>
                                <?php
                                if ($item['description'] != ''):
                                    echo $item['description'];
                                    ?>
                                    <input type="hidden" name="description" value="<?php echo $item['description']; ?>"/>
                                    <?php
                                else:
                                    echo $this->lang->line('sales_no_description');
                                    ?>
                                    <input type="hidden" name="description" value=""/>
                                <?php
                                endif;
                                ?>
                            </td>
                        </tr>
                        <?php
                    endforeach;
                    ?>
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div><!-- /.col-md-8 -->
<div class="col-md-4 sales-info"><!-- customer/transaction info section -->
    <div class="panel panel-default">
        <div class="panel-heading clearfix">
            <?php if (isset($customer)): ?>
                <div class="col-xs-12 no-padding">
                    <strong><?php echo $this->lang->line('sales_customer'); ?> :</strong> <?php echo $customer; ?> <a href="<?php echo site_url(); ?>/sales/remove_customer"><i class="fa fa-times text-red"></i></a>
                </div>
            <?php else: ?>
                <?php echo form_open('sales/select_customer', array('id' => 'select_customer_form', 'class' => '')); ?>
                <div class="input-group">
                    <label id="customer_label" for="customer" class="control-label input-group-addon bg-light-blue"><?php echo $this->lang->line('sales_select_customer'); ?></label>
                    <div class="">
                        <input type="text" id="customer" name="query" autocomplete="off" class="form-control" placeholder="<?php echo $this->lang->line('sales_start_typing_customer_name'); ?>"/>
                        <input type="hidden" id="customer_id" name="customer_id" value=""/>
                    </div><!-- /.col-sm-3 -->
                </div>
                <?php echo form_close(); ?>
            <?php endif; ?>
        </div><!-- /.panel-heading -->
        <div class="panel-body">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-4 no-padding-top force-text-left"><?php echo $this->lang->line('sales_total'); ?> :</label>
                    <div class="col-sm-8 text-right">
                        <?php echo to_currency($subtotal); ?>
                    </div>
                </div>
                <?php if (isset($customer)): ?>
                    <div class="form-group text-red"> <!-- TODO customer discount -->
                        <label class="control-label col-sm-6 no-padding-top force-text-left">
                            <?php echo $this->lang->line('sales_discount'); ?> <input type="checkbox"> :
                        </label>
                        <div class="col-sm-6 text-right">
                            <?php
                            $temp_discount = $subtotal * 30 / 100;
                            echo to_currency(-$temp_discount);
                            ?>
                        </div>
                    </div>
                <?php endif; ?>
                <hr>
                <div class="form-group"> <!-- TODO grand total/subtract the customer's discount -->
                    <label class="control-label col-sm-5 no-padding-top force-text-left"><?php echo $this->lang->line('sales_grand_total'); ?> :</label>
                    <div class="col-sm-7 text-right">
                        <?php echo to_currency($total); ?>
                    </div>
                </div>
                <?php if (count($cart) > 0): ?>
                    <hr>
                    <div class="form-group">
                        <label class="control-label col-sm-5 no-padding-top force-text-left"><?php echo $this->lang->line('sales_payments_total'); ?> :</label>
                        <div class="col-sm-7 text-right">
                            <?php echo to_currency($payments_total); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5 no-padding-top force-text-left"><?php echo $this->lang->line('sales_amount_due'); ?> :</label>
                        <div class="col-sm-7 text-right">
                            <?php echo to_currency($amount_due); ?>
                        </div>
                    </div>
                </div>
            </div><!-- /.panel-body -->
            <div class="panel-footer">
                <?php echo form_open('sales/add_payment', array('id' => 'add_payment_form', 'class' => 'form-horizontal')); ?>
                <div class="form-group">
                    <label for="payment_types" class="control-label col-sm-5 force-text-left"><?php echo $this->lang->line('sales_payment'); ?> <span class="text-right">:</span></label>
                    <div class="col-sm-7">
                        <?php echo form_dropdown('payment_type', $payment_options, array(), 'id="payment_types" class="form-control"'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="amount_tendered" class="control-label col-sm-5 force-text-left"><?php echo $this->lang->line('sales_amount_tendered'); ?>:</label>
                    <div class="col-sm-7 input-group add-padding-right-left">
                        <div class="input-group-addon"><?php echo $this->config->item('currency_symbol'); ?></div><input type="number" name="amount_tendered" id="amount_tendered" class="text-right form-control" min="0" max="999999999999" autocomplete="off" step="50" value="<?php echo $amount_due; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-6">
                        <button class="btn btn-sm btn-danger"><?php echo $this->lang->line('sales_cancel_sale'); ?></button>
                    </div>
                    <div class="col-xs-6 text-right">
                        <button class="btn btn-sm btn-primary"><?php echo $this->lang->line('sales_add_payment'); ?></button>
                    </div>
                </div>
                <?php echo form_close(); ?>
                <div class="form-group">
                    <div class="box box-solid table-responsive no-padding">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center"><?php echo $this->lang->line('common_delete'); ?></th>
                                    <th class="text-center"><?php echo $this->lang->line('sales_payment_type'); ?></th>
                                    <th class="text-center"><?php echo $this->lang->line('sales_payment_amount'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($payments as $payment_id => $payment): ?>
                                    <tr class="text-center">
                                        <td><a href="<?php echo base_url(); ?>sales/delete_payment/<?php echo $payment_id; ?>"><i class="fa fa-times text-red"></i></a></td>
                                        <td><?php echo $payment['payment_type']; ?></td>
                                        <td class="text-right"><?php echo to_currency($payment['payment_amount']); ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div><!-- /.panel-footer -->
        <?php endif; ?>
    </div><!-- /.panel .panel-default -->
</div><!-- /.col-md-4 -->
<script src="<?php echo base_url(); ?>assets/js/jquery.autocomplete.min.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
// General functions
//        $('#item').focus();
        $('#amount_tendered').focus(function () {
            $(this).select();
        });

        var oldVal, newVal;
        $("input[name='quantity']").focus(function () {
            oldVal = $(this).val();
            console.log(oldVal);
            $(this).select();
        }).blur(function () {
            newVal = $(this).val();
            console.log(newVal);
            if (oldVal !== newVal) {
                $(this).parent('form').submit();
            }
        });
// end of General functions


// Hotkey assignments
        $('#item, #customer').jkey('esc', function () {
            $(this).val('');
        });
// end of Hotkey assignments


// Autocomplete initialization
        $('#item').devbridgeAutocomplete({
            serviceUrl: '<?php echo site_url(); ?>/sales/item_search',
            minChars: 2,
            onSearchStart: function (query) {
                console.log(query);
                $(this).addClass('loading-state');
            },
            onSearchComplete: function (query, suggestions) {
                console.log(query);
                console.log(suggestions);
                $(this).removeClass('loading-state');
            },
            onSelect: function (suggestion) {
                $('#item_id').val(suggestion.data);
                $("#add_item_form").submit();
            },
            showNoSuggestionNotice: true,
            noSuggestionNotice: '<?php echo $this->lang->line('sales_no_item_found'); ?>'
        });

        $('#customer').devbridgeAutocomplete({
            serviceUrl: '<?php echo site_url(); ?>/sales/customer_search',
            minChars: 2,
            onSearchStart: function (query) {
                console.log(query);
                $(this).addClass('loading-state');
            },
            onSearchComplete: function (query, suggestions) {
                console.log(query);
                console.log(suggestions);
                $(this).removeClass('loading-state');
            },
            onSelect: function (suggestion) {
                $('#customer_id').val(suggestion.data);
                $("#select_customer_form").submit();
            },
            showNoSuggestionNotice: true,
            noSuggestionNotice: '<?php echo $this->lang->line('sales_no_customer_found'); ?>'
        });
    });
// end of Autocomplete initialization
</script>
<?php
$this->load->view('template/footer');
