<div class="col-xs-12">
    <h2 class="page-header">
        <span><?php echo $receipt_title; ?> -  <small><em>Owner's Copy</em></small></span>
        <small class="pull-right">
            <?php echo $transaction_time; ?>
        </small>
    </h2>
</div>
<!-- /.col -->
</div>
<!-- info row -->
<div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
        From
        <address>
            <strong><?php echo $this->config->item('company'); ?></strong><br>
            <?php echo $this->config->item('address'); ?><br>
            <?php echo $this->lang->line('common_phone_number'); ?>: <?php echo $this->config->item('phone'); ?><br/>
            <?php echo $this->lang->line('common_email'); ?>: <?php echo $this->config->item('email'); ?>
        </address>
    </div>
    <!-- /.col -->
    <div class="col-sm-4 invoice-col">
        <?php if (isset($customer)): ?>
            To
            <address>
                <strong><?php echo $customer->first_name . ' ' . $customer->last_name; ?></strong><br>
                <?php echo $customer->address_1; ?><br>
                <?php echo $customer->city . ', ' . $customer->state . ' ' . $customer->zip . ' - ' . $customer->country; ?><br>
                <?php echo $this->lang->line('common_phone_number'); ?>: <?php echo $customer->phone_number; ?><br/>
                <?php echo $this->lang->line('common_email'); ?>: <?php echo $customer->email; ?>
            </address>
        <?php endif; ?>
    </div><!-- /.col -->
    <div class="col-sm-4 invoice-col">
        <b>Invoice #007612</b>
        <br/>
        <br/>
        <b>Order ID:</b> 4F3S8J
        <br/>
        <b>Payment Due:</b> 2/22/2014
        <br/>
        <b>Account:</b> 968-34567
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

<!-- Table row -->
<div class="row">
    <div class="col-xs-12 table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="col-xs-3"><?php echo $this->lang->line('sales_item_number'); ?></th>
                    <th class="col-xs-4"><?php echo $this->lang->line('sales_item_name'); ?></th>
                    <th class="col-xs-1 text-center"><?php echo $this->lang->line('sales_quantity'); ?></th>
                    <th class="col-xs-2 text-center"><?php echo $this->lang->line('sales_price'); ?></th>
                    <th class="col-xs-2 text-center"><?php echo $this->lang->line('sales_total'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($cart as $line => $item): ?>
                    <tr>
                        <td><?php echo $item['item_number']; ?></td>
                        <td>
                            <?php echo $item['name']; ?>
                            <?php echo $item['description'] !== '' ? "<br><small>" . $item['description'] . "</small>" : ''; ?>
                            <?php echo $item['serialnumber'] !== '' && $item['serialnumber'] !== '0' ? "<br><small><em>" . $item['serialnumber'] . "</em></small>" : ''; ?>
                        </td>
                        <td class="text-right"><?php echo $item['quantity']; ?></td>
                        <td class="text-right"><?php echo to_currency($item['price']); ?></td>
                        <td class="text-right"><?php echo to_currency($item['price'] * $item['quantity']); ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

<div class="row">
    <!-- accepted payments column -->
    <!-- /.col -->
    <div class="col-sm-8 col-md-6 pull-right">
        <p class="lead">Amount Due</p>
        <div class="table-responsive">
            <table class="table">
                <tr>
                    <th style="width:50%"><?php echo $this->lang->line('sales_sub_total'); ?> :</th>
                    <td class="text-right"><?php echo to_currency($subtotal); ?></td>
                </tr>
                <?php foreach ($taxes as $name => $value): ?>
                    <tr>
                        <td><?php echo $name; ?> :</td>
                        <td><?php echo to_currency($value); ?></td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <th><?php echo $this->lang->line('sales_grand_total'); ?> :</th>
                    <td class="text-right"><?php echo to_currency($total); ?></td>
                </tr>
                <tr>
                    <th><?php echo $this->lang->line('sales_payment'); ?> :</th>
                    <td>
                        <?php foreach ($payments as $payment_id => $payment): ?>
                            <?php $splitpayment = explode(':', $payment['payment_type']); ?>
                            <p>
                                <em><?php echo $payment['payment_type']; ?></em>
                                <span class="pull-right"><?php echo to_currency($payment['payment_amount'] * -1); ?></span>
                            </p>
                        <?php endforeach; ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo $this->lang->line('sales_change_due'); ?></th>
                    <td class="text-right"><?php echo $amount_change; ?></td>
                </tr>
            </table>
        </div>
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

<!-- this row will not appear when printing -->
<div class="row no-print">
    <div class="col-xs-12">
        <button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
        <button class="btn btn-primary"><i class="fa fa-download"></i> Generate PDF</button>
    </div>