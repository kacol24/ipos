<div class="col-sm-12">
    <h2 class="page-header clearfix">
        <div class="col-xs-6">
            <?php echo $receipt_title; ?><span class="copy_details"></span>
        </div>
        <div class="col-xs-6 text-right">
            <small id="printed_on"></small>
        </div>
    </h2>
</div>
<!-- /.col -->
</div>
<!-- info row -->
<div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
        <?php echo $this->lang->line('common_from'); ?>
        <address>
            <strong><?php echo $this->config->item('company'); ?></strong><br>
            <span class="text-6pt"><?php echo $this->config->item('address'); ?></span><br>
            <span class="text-6pt"><?php echo $this->config->item('city') . ' ' . $this->config->item('zip') . ', ' . $this->config->item('state') . ', ' . $this->config->item('country'); ?></span><br>
            <span class="text-6pt"><?php echo $this->lang->line('common_phone_number'); ?>: <?php echo $this->config->item('phone'); ?></span><br/>
            <?php if ($this->config->item('email') != ''): ?>
                <span class="text-6pt"><?php echo $this->lang->line('common_email'); ?>: <?php echo $this->config->item('email'); ?></span>
            <?php endif; ?>
        </address>
    </div>
    <!-- /.col -->
    <div class="col-sm-4 invoice-col">
        <?php if (isset($customer)): ?>
            <?php echo $this->lang->line('common_to'); ?>
            <address>
                <strong><?php echo $customer->first_name . ' ' . $customer->last_name; ?></strong><br>
                <?php if (isset($customer->address_1) && ! empty($customer->address_1)): ?>
                    <span class="text-6pt"><?php echo $customer->address_1; ?></span><br>
                <?php endif; ?>
                <?php if ( ! empty($customer->city) OR ! empty($customer->zip) OR ! empty($customer->country)): ?>
                    <span class="text-6pt"><?php echo $customer->city . ' ' . $customer->zip . ', ' . $customer->state . ', ' . $customer->country; ?></span><br>
                <?php endif; ?>
                <?php if ( ! empty($customer->phone_number)): ?>
                    <span class="text-6pt"><?php echo $this->lang->line('common_phone_number'); ?>: <?php echo $customer->phone_number; ?></span><br/>
                <?php endif; ?>
                <?php if ( ! empty($customer->email)): ?>
                    <span class="text-6pt"><?php echo $this->lang->line('common_email'); ?>: <?php echo $customer->email; ?></span>
                <?php endif; ?>
            </address>
        <?php endif; ?>
    </div><!-- /.col -->
    <div class="col-sm-4 invoice-col" id="invoice_data">
        <address>
            <strong><?php echo lang('sales_receipt_#'); ?>: </strong><?php echo $sale_id; ?><br/>
            <strong><?php echo lang('sales_issue_date'); ?>: </strong><?php echo $transaction_time; ?><br/>
            <strong><?php echo lang('sales_employee'); ?>: </strong><?php echo $employee; ?>
        </address>
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

<!-- Table row -->
<div class="row">
    <div class="col-sm-12 table-responsive">
        <table class="table table-condensed table-bordered invoice-items" width="100%">
            <thead>
                <tr>
                    <th class="col-xs-1"><?php echo $this->lang->line('sales_item_number'); ?></th>
                    <th class="col-xs-4"><?php echo $this->lang->line('sales_item_name'); ?></th>
                    <th class="col-xs-1 text-center"><?php echo $this->lang->line('sales_quantity'); ?></th>
                    <th class="col-xs-2 text-center"><?php echo $this->lang->line('sales_price'); ?></th>
                    <th class="col-xs-2 text-center"><?php echo $this->lang->line('sales_discount'); ?></th>
                    <th class="col-xs-2 text-center"><?php echo $this->lang->line('sales_total'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($cart as $line => $item): ?>
                    <tr>
                        <td>
                            <?php echo $item['item_number']; ?>
                        </td>
                        <td>
                            <dl>
                                <dt><?php echo $item['name']; ?></dt>
                                <?php if ( ! empty($item['description']) OR ! empty($item['serialnumber'])): ?>
                                    <dd>
                                        <?php echo $item['description'] !== '' ? "<small>" . $item['description'] . "</small><br>" : ''; ?>
                                        <?php echo $item['serialnumber'] !== '' && $item['serialnumber'] !== '0' ? "<small><em>" . $item['serialnumber'] . "</em></small>" : ''; ?>
                                    </dd>
                                <?php endif; ?>
                            </dl>
                        </td>
                        <td class="text-center"><?php echo $item['quantity'] . ($show_stock_locations ? " [" . $item['stock_name'] . "]" : ""); ?></td>
                        <td class="text-right"><?php echo to_currency($item['price']); ?></td>
                        <td class="text-right"><?php echo (int) $item['discount'] != 0 ? to_currency($item['price'] * (int) $item['discount'] / 100) . ' [' . (int) $item['discount'] . '%] ' : (int) $item['discount'] . '%'; ?></td>
                        <td class="text-right"><?php echo to_currency($item['price'] * $item['quantity'] - $item['price'] * $item['quantity'] * $item['discount'] / 100); ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div><!-- /.col -->
</div><!-- /.row -->
<div class="row"  id="payment_details">
    <!-- accepted payments column -->
    <div class="col-xs-12 col-sm-6 col-md-5 pull-right payment_col">
        <p class="lead"><?php echo $this->lang->line('sales_amount_due'); ?></p>
        <div class="table-responsive">
            <table class="table table-condensed">
                <tr>
                    <th><?php echo $this->lang->line('sales_sub_total'); ?>:</th>
                    <td class="text-right"><?php echo to_currency($subtotal); ?></td>
                </tr>
                <?php $calc = $subtotal; ?>
                <tr>
                    <?php if ($customer_discount != NULL && $customer_discount != 0): ?>
                        <td>-<small><?php echo $customer_discount . lang('sales_discount_short') . ' ' . lang('sales_customer_discount') . ':'; ?></small></td>
                        <td class="text-right">
                            <?php
                            $subtract_by = $calc * $customer_discount / 100;
                            $calc -=$subtract_by;
                            echo '- ' . to_currency($subtract_by);
                            ?>
                        </td>
                    <?php endif; ?>
                </tr>
                <?php
                foreach ($global_discounts as $discount):
                    ?>
                    <tr>
                        <td>
                            <small>-<?php echo $discount['discount_percent'] . $this->lang->line('sales_discount_included'); ?>:</small>
                        </td>
                        <td class="text-right">
                            <?php
                            $subtract_by = $calc * $discount['discount_percent'] / 100;
                            $calc -= $subtract_by;
                            echo to_currency(-$subtract_by);
                            ?>
                        </td>
                    <tr>
                    <?php endforeach; ?>
                    <?php foreach ($taxes as $name => $value): ?>
                        <?php if ($value != 0): ?>
                        <tr>
                            <td><small>-<?php echo $name; ?>:</small></td>
                            <td class="text-right"><?php echo to_currency($value); ?></td>
                        </tr>
                    <?php endif; ?>
                <?php endforeach; ?>
                <tr>
                    <th><?php echo $this->lang->line('sales_grand_total'); ?>:</th>
                    <td class="text-right"><strong><u><?php echo to_currency($total); ?></u></strong></td>
                </tr>
                <tr>
                    <th><?php echo $this->lang->line('sales_payment'); ?>:</th>
                    <td>
                        <?php foreach ($payments as $payment_id => $payment): ?>
                            <?php $splitpayment = explode(':', $payment['payment_type']); ?>
                            <div class="clearfix">
                                <em class="pull-left"><?php echo $payment['payment_type'] == 'sales_giftcard' ? lang($payment['payment_type']) . ':' . $payment['giftcard_number'] : lang($payment['payment_type']); ?></em>
                                <span class="pull-right"><?php echo to_currency($payment['payment_amount'] * -1); ?></span>
                            </div>
                        <?php endforeach; ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo $this->lang->line('sales_change_due'); ?>:</th>
                    <td class="text-right"><?php echo $amount_change; ?></td>
                </tr>
            </table>
        </div>
    </div>
    <?php if (isset($has_store_account) && $has_store_account && count($payment_records) && $this->config->item('show_payment_records')): ?>
        <div class="col-xs-12 col-sm-6 col-md-5 payment_col">
            <p class="lead"><?php echo lang('sales_payment_history'); ?></p>
            <div class="table-responsive">
                <table class="table table-condensed no-wrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th><?php echo lang('sales_payment_time'); ?></th>
                            <th><?php echo lang('sales_payment_type'); ?></th>
                            <th><?php echo lang('sales_payment_amount'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($payment_records as $record): ?>
                            <tr>
                                <td><?php echo $record->payment_time; ?></td>
                                <td><?php echo lang($record->payment_type); ?></td>
                                <td class="text-right"><?php echo to_currency($record->payment_amount); ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div><!-- /.col -->
    <?php endif; ?>
</div><!-- /.row -->
<?php if ($this->config->item('show_return_policy')): ?>
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-push-2 col-md-6 col-md-push-3 text-center">
            <p><?php echo $this->config->item('return_policy'); ?></p>
        </div>
    </div>
<?php endif; ?>
<script>
    $(window).load(function () {
        if ($('html').find('.invoice')) {
            $('html, body').css('min-height', '0');
            $('html, body').removeAttr('style');
        }
    });
    function print_page() {
        window.print();
    }
    $(function () {
        $('#duplicate-invoice').on('ifChecked', function () {
            var invoice = $('.content.invoice').clone();
            $('.copy_holder').empty();
            $('.invoice .copy_details').empty();
            $('.copy_holder .copy_details').empty();
            $('.copy_holder').html(invoice);
            $('.invoice .copy_details').html(" -  <small><em><?php echo lang('sales_merchants_copy'); ?></em></small>");
            $('.copy_holder .copy_details').html(" -  <small><em><?php echo lang('sales_customers_copy'); ?></em></small>");
        }).on('ifUnchecked', function () {
            $('.copy_holder').empty();
            $('.invoice .copy_details').empty();
            $('.copy_holder .copy_details').empty();
        });

        var printed_on = '<strong>Printed on: </strong>' + moment().format('DD/MM/YYYY H:mm:ss') + '(<?php echo $current_employee->first_name . ' ' . $current_employee->last_name; ?>)';
        var beforePrint = function () {
            $('#printed_on').html(printed_on);
        };
        var afterPrint = function () {
            $('#printed_on').empty();
        };
        if (window.matchMedia) {
            var mediaQueryList = window.matchMedia('print');
            mediaQueryList.addListener(function (mql) {
                if (mql.matches) {
                    beforePrint();
                } else {
                    afterPrint();
                }
            });
        }
        window.onbeforeprint = beforePrint;
        window.onafterprint = afterPrint;
    });
</script>
<?php if ($print == 'print' || $this->config->item('print_after_sale')): ?>
    <script>
        $(function () {
            print_page();
        });
    </script>
<?php endif; ?>
<!-- this row will not appear when printing -->
<div class="row no-print">
    <div class="col-xs-12">
        <div class="form-group checkbox">
            <button class="btn btn-default" onclick="print_page();"><i class="fa fa-print"></i> <?php echo lang('sales_print'); ?></button>
            <!--<button class="btn btn-primary"><i class="fa fa-download"></i> Generate PDF</button>-->
            <label>
                <input type="checkbox" name="duplicate-invoice" class="iCheck" id="duplicate-invoice"> <?php echo lang('sales_print_duplicate'); ?>
            </label>
        </div>
    </div>