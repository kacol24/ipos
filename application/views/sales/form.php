<?php get_notif(); ?>
<div class="col-xs-12">
    <div class="box box-solid">
        <div class="box-body">
            <?php echo form_open("sales/save/" . $sale_info['sale_id'], array('id' => 'customer_form', 'class' => 'form-horizontal')); ?>
            <fieldset>
                <legend>
                    <?php echo $this->lang->line('sales_basic_information'); ?>
                </legend>
                <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo $this->lang->line('sales_id'); ?></label>
                    <div class="col-sm-5">
                        <p class="form-control-static pull-left"><?php echo anchor('sales/receipt/' . $sale_info['sale_id'], $this->lang->line('sales_receipt_number') . $sale_info['sale_id'], array('target' => '_blank')); ?></p>
                        <a href="<?php echo site_url('sales/receipt/' . $sale_info['sale_id'] . '/print'); ?>" class="btn btn-primary pull-right"><i class="fa fa-print"></i></a>
                    </div>
                </div>
                <div class="form-group">
                    <label for="date" class="col-sm-2 control-label"><?php echo $this->lang->line('sales_date'); ?></label>
                    <div class="col-sm-5">
                        <div class="input-group">
                            <input disabled type="text" name="date" class="form-control datepicker" id="date" value="<?php echo date('Y-m-d h:i:s', strtotime($sale_info['sale_time'])); ?>">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="customer" class="control-label col-sm-2"><?php echo $this->lang->line('sales_customer'); ?></label>
                    <div class="col-sm-5">
                        <input type="text" name="customer" id="customer" class="form-control" value="<?php echo $selected_customer['name']; ?>">
                        <input type="hidden" name="customer_id" id="customer_id" value="<?php echo $selected_customer['customer_id']; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="employee_id" class="control-label col-sm-2"><?php echo $this->lang->line('sales_employee'); ?></label>
                    <div class="col-sm-5">
                        <?php echo form_dropdown('employee_id', $employees, $sale_info['employee_id'], 'id="employee_id" class="form-control"'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="comment" class="control-label col-sm-2"><?php echo $this->lang->line('sales_comment'); ?></label>
                    <div class="col-sm-5">
                        <textarea name="comment" id="comment" class="form-control" rows="3" placeholder=""><?php echo $sale_info['comment']; ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo lang('sales_payment'); ?></label>
                    <div class="col-sm-5">
                        <table class="table table-condensed table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th><?php echo lang('sales_payment_type'); ?></th>
                                    <th><?php echo lang('sales_payment_amount'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($payments as $payment): ?>
                                    <tr>
                                        <td><?php echo lang($payment->payment_type); ?><?php echo $has_store_account && $payment->payment_type == 'sales_store_account' ? '<span class="pull-right"><a href="' . site_url('sales/finish_payment/' . $sale_info['sale_id']) . '" class="btn btn-xs btn-success pull-right"><i class="fa fa-money"></i></a></span>' : ''; ?></td>
                                        <td class="text-right"><?php echo to_currency($payment->payment_amount); ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-7">
                        <input type="submit" name="submit" id="submit" class="btn btn-primary pull-right" value="<?php echo $this->lang->line('common_submit'); ?>"/>
                    </div><!-- /.col-sm-7 -->
                </div><!-- /.form-group -->
            </fieldset>
            <?php echo form_close(); ?>
        </div><!-- /.box-body -->
        <?php if (count($payment_records)): ?>
            <div class="box-footer">
                <table class="table table-condensed table-striped table-bordered">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Payment Time</th>
                            <th>Payment Type</th>
                            <th>Payment Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($payment_records as $record): ?>
                            <tr>
                                <td class="text-center"><a href="<?php echo site_url('sales/withdraw_payment/' . $record->record_id); ?>" class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i></a></td>
                                <td><?php echo $record->payment_time; ?></td>
                                <td><?php echo lang($record->payment_type); ?></td>
                                <td class="text-right"><?php echo to_currency($record->payment_amount); ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>
    </div><!-- /.box -->
</div><!-- /.col-xs-12 -->
<script src="<?php echo base_url(); ?>assets/js/jquery.autocomplete.min.js" type="text/javascript"></script>
<script>
    $(function () {
        $('.datepicker').daterangepicker({
            opens: 'right',
            singleDatePicker: true
        }, function (start, end, label) {
            $(this).val(moment(start).format('DD/MM/YYYY'));
        }).click(function (e) {
            e.preventDefault();
        });
        $('#customer').devbridgeAutocomplete({
            serviceUrl: '<?php echo site_url(); ?>/sales/customer_search',
            minChars: 2,
            onSearchStart: function (query) {
                console.log(query);
                $(this).addClass('loading-state');
            },
            onSearchComplete: function (query, suggestions) {
                console.log(query);
                console.log(suggestions);
                $(this).removeClass('loading-state');
            },
            onSelect: function (suggestion) {
                $('#customer_id').val(suggestion.data);
            },
            showNoSuggestionNotice: true,
            noSuggestionNotice: '<?php echo $this->lang->line('sales_no_customer_found'); ?>'
        });
    });
</script>