<!DOCTYPE html>
<html lang="<?php echo $this->config->item('language'); ?>">
    <head>
        <script data-pace-options='{ "target":"#loadstate" }' src="<?php echo base_url(); ?>assets/js/pace.min.js"></script>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta content = 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name = 'viewport'>
        <base href="<?php echo base_url(); ?>" />

        <!--TODO change this title-->
        <title><?php echo $this->config->item('company') ?></title>

        <!--bootstrap 3.2.0 -->
        <?php if (ENVIRONMENT != 'production'): ?>
            <link href = "<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel = "stylesheet" type = "text/css" />
        <?php else: ?>
            <link href = "//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel = "stylesheet" type = "text/css" />
        <?php endif; ?>

        <!-- compiled -->
        <link href = "<?php echo base_url(); ?>assets/css/app.min.css" rel = "stylesheet" type = "text/css" />

        <!--Theme style -->
        <link href = "<?php echo base_url(); ?>assets/css/AdminLTE.css" rel = "stylesheet" type = "text/css" />

        <!--HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <?php if (isset($set_invoice)): ?>
            <style>
                *{
                    -webkit-transition: none;
                    -moz-transition: none;
                    -o-transition: none;
                    transition: none;
                }
            </style>
        <?php endif; ?>
        <!-- jQuery 2.1.3 -->
        <?php if (ENVIRONMENT != 'production'): ?>
            <script src="<?php echo base_url(); ?>assets/js/jquery-2.1.3.min.js" type="text/javascript"></script>
        <?php else: ?>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <?php endif; ?>
        <script>BASE_URL = '<?php echo site_url(); ?>';</script>
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="<?php echo base_url(); ?>assets/images/apple-touch-icon.png" />
    </head>
    <body class="skin-blue fixed no-transition">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="<?php echo base_url(); ?>" class="logo <?php echo strlen($this->config->item('company')) > 15 ? 'smaller' : ''; ?><?php echo strlen($this->config->item('company')) > 30 ? ' multi-line' : ''; ?>">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                <?php echo $this->config->item('company'); ?>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="<?php echo isset($controller_name) ? $controller_name == 'sales' || $controller_name == 'receivings' ? 'fa fa-bars' : 'fa fa-times' : ''; ?>"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span><?php echo "{$user_info->first_name} {$user_info->last_name}"; ?><i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <p>
                                        <?php echo "{$user_info->first_name} {$user_info->last_name} - {$user_info->username}"; ?>
                                    </p>
                                </li>
                                <li class="user-footer">
                                    <div class="pull-right">
                                        <a href="<?php echo site_url('logout'); ?>" class="btn btn-default btn-flat"><?php echo lang('login_logout'); ?></a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas <?php echo isset($menu_hide) ? 'collapse-left' : ''; ?>">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar no-transition">
                    <!-- Sidebar user panel -->
                    <div class="user-panel bg-gray">
                        <div class="info clearfix">
                            <p class="pull-left"><i class='fa fa-clock-o'></i> </p>
                            <p id="clockbox" class="pull-left">Not Available</p>
                        </div>
                    </div>
                    <ul class="sidebar-menu no-transition">
                        <?php foreach ($allowed_modules->result() as $i => $module): ?>
                            <li <?php echo $module->module_id == (isset($controller_name) ? $controller_name : '') ? 'class="active"' : ''; ?>>
                                <a href="<?php echo site_url($module->module_id); ?>" id="module_<?php echo $module->module_id; ?>">
                                    <i class="<?php echo $module->module_icon; ?>"></i> <span><?php echo $this->lang->line($module->name_lang_key); ?> <small><em class="text-muted"></em></small></span>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side <?php echo isset($menu_hide) == true ? 'stretch' : ''; ?>">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <?php
                        echo isset($custom_title) ? $custom_title : $this->lang->line($current_module->name_lang_key);

                        ?>
                        <small>
                            <?php
                            echo isset($custom_subtitle) ? $custom_subtitle : $this->lang->line($current_module->desc_lang_key);

                            ?>
                        </small>
                    </h1>
                    <ol class="breadcrumb">
                        <?php if ($this->uri->segment(1) == '' || $this->uri->segment(1) == 'home'): ?>
                            <li><i class="fa fa-dashboard"></i>&nbsp;&nbsp;&nbsp; <?php echo $this->lang->line('module_home'); ?></li>
                        <?php else: ?>
                            <li><a href="<?php echo site_url('/home'); ?>"><i class="fa fa-dashboard"></i> <?php echo $this->lang->line('module_home'); ?></a></li>
                        <?php endif; ?>
                        <?php echo $this->breadcrumbs->show(); ?>
                    </ol>
                    <div class="no-transition" id="loadstate"></div>
                </section>

                <!-- Main content -->
                <section class="content <?php echo isset($custom_title) && isset($set_invoice) ? 'invoice' : ''; ?>">

                    <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <?php echo get_notif(); ?>