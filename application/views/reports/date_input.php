<div class="col-xs-12">
    <div class="box box-solid">
        <div class="box-body">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-2"><?php echo $this->lang->line('reports_date_range'); ?></label>
                    <div class="col-sm-5">
                        <div class="input-group">
                            <button class="btn btn-default pull-right" id="daterange-btn">
                                <i class="fa fa-calendar"></i> <span><?php echo $this->lang->line('reports_today'); ?></span>
                                <input type="hidden" name="date_range" value="<?php echo date('Y-m-d') . '/' . date('Y-m-d'); ?>">
                                <i class="fa fa-caret-down"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <?php if (isset($discount_input)): ?>
                    <label class="col-sm-2 control-label"><?php echo $this->lang->line('reports_discount_prefix'); ?></label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="selected_discount" id="selected_discount" value="0">
                        <?php echo $this->lang->line('reports_discount_suffix'); ?>
                    </div>
                <?php endif; ?>
                <?php if ($mode == 'sale'): ?>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo $this->lang->line('reports_sale_type'); ?></label>
                        <div class="col-sm-5">
                            <select name="sale_type" class="form-control" id="input_type">
                                <option selected value="all"><?php echo $this->lang->line('reports_all'); ?></option>
                                <option value="sales"><?php echo $this->lang->line('reports_sales'); ?></option>
                                <option value="returns"><?php echo $this->lang->line('reports_returns'); ?></option>
                            </select>
                        </div>
                    </div>
                <?php elseif ($mode == 'receiving'): ?>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo $this->lang->line('reports_receiving_type'); ?></label>
                        <div class="col-sm-5">
                            <select name="receiving_type" class="form-control"  id="input_type">
                                <option selected value="all"><?php echo $this->lang->line('reports_all'); ?></option>
                                <option value="receiving"><?php echo $this->lang->line('reports_receivings'); ?></option>
                                <option value="returns"><?php echo $this->lang->line('reports_returns'); ?></option>
                                <!--<option value="requisition"><?php echo $this->lang->line('reports_requisitions'); ?></option>-->
                            </select>
                        </div>
                    </div>
                <?php elseif ($mode == 'requisition'): ?>
                <?php endif; ?>
                <div class="form-group">
                    <div class="col-sm-7">
                        <button name="submit" id="generate_report" class="btn btn-primary pull-right"><?php echo $this->lang->line('common_submit'); ?></button>
                    </div><!-- /.col-sm-7 -->
                </div><!-- /.form-group -->
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div><!-- /.col-xs-12 -->
<script type="text/javascript">
    $(function () {
        $('#daterange-btn').unbind();
        var custom_range = [];
        custom_range = {
            "<?php echo $this->lang->line('reports_today'); ?>": [moment(), moment()],
            "<?php echo $this->lang->line('reports_yesterday'); ?>": [moment().subtract('days', 1), moment().subtract('days', 1)],
                    "<?php echo $this->lang->line('reports_last_7'); ?>": [moment().subtract('days', 6), moment()],
                    "<?php echo $this->lang->line('reports_this_month'); ?>": [moment().startOf('month'), moment().endOf('month')],
                    "<?php echo $this->lang->line('reports_last_month'); ?>": [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')],
                    "<?php echo $this->lang->line('reports_this_year'); ?>": [moment().startOf('year'), moment().endOf('year')],
                    "<?php echo $this->lang->line('reports_last_year'); ?>": [moment().subtract('year', 1).startOf('year'), moment().subtract('year', 1).endOf('year')],
                    "<?php echo $this->lang->line('reports_all_time'); ?>": [moment(0), moment()]
        };
        $('#daterange-btn').daterangepicker({
            ranges: custom_range,
            opens: 'right'
        }, function (start, end, label) {
            $('#daterange-btn span').html(label);
            $('input[name="date_range"]').attr('value', start.format('YYYY-MM-DD') + '/' + end.format('YYYY-MM-DD'));
        });
        $('#generate_report').click(function () {
            var date = $('input[name="date_range"]').attr('value');
            var type = $('#input_type').val();
            window.location = window.location + '/' + date + '/' + type;
        });
    });
</script>