<div class="col-xs-12">
    <div class="box box-solid">
        <div class="box-body">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-2"><?php echo $this->lang->line('reports_date_range'); ?></label>
                    <div class="col-sm-5">
                        <div class="input-group">
                            <button class="btn btn-default pull-right" id="daterange-btn">
                                <i class="fa fa-calendar"></i> <span><?php echo $this->lang->line('reports_today'); ?></span>
                                <input type="hidden" name="date_range" value="<?php echo date('Y-m-d') . '/' . date('Y-m-d'); ?>">
                                <i class="fa fa-caret-down"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="specific_input_data" class="control-label col-sm-2"><?php echo $specific_input_name; ?></label>
                    <div class="col-sm-5">
                        <input type="text" id="item" name="query" autocomplete="off" class="form-control">
                        <input type="hidden" id="item_id" name="item_id" value=""/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo $this->lang->line('reports_sale_type'); ?></label>
                    <div class="col-sm-5">
                        <select name="sale_type" class="form-control" id="input_type">
                            <option selected value="all"><?php echo $this->lang->line('reports_all'); ?></option>
                            <option value="sales"><?php echo $this->lang->line('reports_sales'); ?></option>
                            <option value="returns"><?php echo $this->lang->line('reports_returns'); ?></option>
                        </select>
                    </div>
                </div>
                <?php if (isset($discount_input)): ?>
                    <label class="col-sm-2 control-label"><?php echo $this->lang->line('reports_discount_prefix'); ?></label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="selected_discount" id="selected_discount" value="0">
                        <?php echo $this->lang->line('reports_discount_suffix'); ?>
                    </div>
                <?php endif; ?>
                <!--<div class="form-group">
                    <label class="control-label col-sm-2">Export to Excel*</label>
                    <label class="radio-inline">
                        <input type="radio" class="form-control iRadio" name="export_excel" value="1" id="export_excel_yes"> Yes
                    </label>
                    <label class="radio-inline">
                        <input type="radio" class="form-control iRadio" name="export_excel" value="0" id="export_excel_no" checked> No
                    </label>
                </div>-->
                <div class="form-group">
                    <div class="col-sm-7">
                        <button name="submit" id="generate_report" class="btn btn-primary pull-right"><?php echo $this->lang->line('common_submit'); ?></button>
                    </div><!-- /.col-sm-7 -->
                </div><!-- /.form-group -->
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div><!-- /.col-xs-12 -->
<script type="text/javascript">
    $(function () {
        var custom_range = [];
<?php foreach ($report_date_range_simple as $key => $value): ?>
            custom_range['<?php echo $key; ?>'] = [new Date(<?php echo $value['start']; ?>) * 1000, new Date(<?php echo $value['end']; ?>) * 1000];
<?php endforeach; ?>
        $('#daterange-btn').daterangepicker({
            ranges: custom_range,
            opens: 'right'
        }, function (start, end, label) {
            $('#daterange-btn span').html(label);
            $('input[name="date_range"]').attr('value', start.format('YYYY-MM-DD') + '/' + end.format('YYYY-MM-DD'));
        });
        $('#generate_report').click(function () {
            var date = $('input[name="date_range"]').attr('value');
            var type = $('#input_type').val();
            var input = $('#item_id').val();
            window.location = window.location + '/' + date + '/' + input + '/' + type;
        });

        $('#item').devbridgeAutocomplete({
            serviceUrl: '<?php echo site_url(); ?>/sales/item_search',
            minChars: 1,
            onSearchStart: function (query) {
                console.log(query);
                $(this).addClass('loading-state');
            },
            onSearchComplete: function (query, suggestions) {
                console.log(query);
                console.log(suggestions);
                $(this).removeClass('loading-state');
            },
            onSelect: function (suggestion) {
                $('#item_id').val(suggestion.data);
            },
            showNoSuggestionNotice: true,
            noSuggestionNotice: '<?php echo $this->lang->line('sales_no_item_found'); ?>'
        });
    });
</script>