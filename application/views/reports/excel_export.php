<div class="col-xs-12">
    <div class="box box-solid">
        <div class="box-body">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-2"><?php echo lang('reports_export_excel'); ?></label>
                    <label class="radio-inline">
                        <input type="radio" class="form-control iRadio" name="export_excel" value="1" id="export_excel_yes"> <?php echo lang('common_yes'); ?>
                    </label>
                    <label class="radio-inline">
                        <input type="radio" class="form-control iRadio" name="export_excel" value="0" id="export_excel_no" checked> <?php echo lang('common_no'); ?>
                    </label>
                </div>
                <div class="form-group">
                    <div class="col-sm-7">
                        <button name="submit" id="generate_report" class="btn btn-primary pull-right"><?php echo $this->lang->line('common_submit'); ?></button>
                    </div><!-- /.col-sm-7 -->
                </div><!-- /.form-group -->
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div><!-- /.col-xs-12 -->
<script type="text/javascript">
    $(function () {
        $('#generate_report').click(function () {
            var export_excel = 0;
            if ($('#export_excel_yes').prop('checked')) {
                export_excel = 1;
            }
            window.location = window.location + '/' + export_excel;
        });
    });
</script>