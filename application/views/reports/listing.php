<div class="col-xs-12">
    <p class="lead">
        <?php echo lang('reports_welcome_message'); ?>
    </p>
</div>
<div class="col-md-4 col-md-push-8">
    <div class="box box-solid box-success">
        <div class="box-header">
            <i class="fa fa-thumb-tack"></i>
            <h3 class="box-title"><?php echo lang('reports_pinned'); ?></h3>
            <div class="box-tools pull-right">
                <button class="btn btn-sm btn-default" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body no-transition">
            <ul class="list-unstyled">
                <?php foreach ($reports_pinned as $pinned): ?>
                    <li>
                        <div class="input-group margin truncate">
                            <a href="<?php echo site_url("reports/{$pinned->link}"); ?>" class="btn btn-sm btn-block btn-social btn-default">
                                <i class="<?php echo $pinned->icon; ?>"><?php echo $pinned->name == 'reports_discounts' ? '%' : ''; ?></i>&nbsp;
                                <?php
                                $split = explode(' ', $this->lang->line($pinned->type));
                                echo ($this->config->item('language') == 'en' ? $split[0] : $split[1]) . '-' . $this->lang->line($pinned->name);
                                ?>
                            </a>
                            <span class="input-group-btn"><a href="<?php echo site_url('reports/unpin/' . $pinned->name . '/' . $pinned->type); ?>" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></a></span>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>
<div class="col-md-8 col-md-pull-4">
    <div class="box box-solid box-primary">
        <div class="box-header">
            <i class="fa fa-bar-chart-o"></i>
            <h3 class="box-title"><?php echo $this->lang->line('reports_category') ?></h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="box-group" id="accordion">
                <div class="panel box">
                    <div class="box-header">
                        <h3 class="box-title col-xs-12">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="col-xs-12 no-padding-left no-padding-right">
                                <?php echo $this->lang->line('reports_graphical_reports'); ?>
                            </a>
                        </h3>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="box-body">
                            <ul class="list-unstyled">
                                <?php
                                foreach ($reports as $graphical):
                                    if ($graphical->type == 'reports_graphical_reports'):
                                        ?>
                                        <li>
                                            <div class="input-group margin">
                                                <a href="<?php echo site_url("reports/{$graphical->link}"); ?>" class="btn btn-block btn-social btn-default">
                                                    <i class="<?php echo $graphical->icon; ?>"><?php echo $graphical->name == 'reports_discounts' ? '%' : ''; ?></i> <?php echo $this->lang->line($graphical->name); ?>
                                                </a>
                                                <span class="input-group-btn"><a href="<?php echo site_url('reports/pin/' . $graphical->name . '/' . $graphical->type); ?>" class="btn btn-primary" <?php echo $graphical->pinned ? 'disabled' : ''; ?>><i class="fa fa-thumb-tack"></i></a></span>
                                            </div>
                                        </li>
                                        <?php
                                    endif;
                                endforeach;
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel box">
                    <div class="box-header">
                        <h3 class="box-title col-xs-12">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="col-xs-12 no-padding-left no-padding-right">
                                <?php echo $this->lang->line('reports_summary_reports'); ?>
                            </a>
                        </h3>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="box-body">
                            <ul class="list-unstyled">
                                <?php
                                foreach ($reports as $summary):
                                    if ($summary->type == 'reports_summary_reports'):
                                        ?>
                                        <li>
                                            <div class="input-group margin">
                                                <a href="<?php echo site_url("reports/{$summary->link}"); ?>" class="btn btn-block btn-social btn-default">
                                                    <i class="<?php echo $summary->icon; ?>"><?php echo $summary->name == 'reports_discounts' ? '%' : ''; ?></i> <?php echo $this->lang->line($summary->name); ?>
                                                </a>
                                                <span class="input-group-btn">
                                                    <a href="<?php echo site_url('reports/pin/' . $summary->name . '/' . $summary->type); ?>" class="btn btn-primary" <?php echo $summary->pinned ? 'disabled' : ''; ?>>
                                                        <i class="fa fa-thumb-tack"></i>
                                                    </a>
                                                </span>
                                            </div>
                                        </li>
                                        <?php
                                    endif;
                                endforeach;
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel box">
                    <div class="box-header">
                        <h3 class="box-title col-xs-12">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="col-xs-12 no-padding-left no-padding-right">
                                <?php echo $this->lang->line('reports_detailed_reports'); ?>
                            </a>
                        </h3>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                        <div class="box-body">
                            <ul class="list-unstyled">
                                <?php
                                foreach ($reports as $detailed):
                                    if ($detailed->type == 'reports_detailed_reports'):
                                        ?>
                                        <li>
                                            <div class="input-group margin">
                                                <a href="<?php echo site_url("reports/{$detailed->link}"); ?>" class="btn btn-block btn-social btn-default">
                                                    <i class="<?php echo $detailed->icon; ?>"><?php echo $detailed->name == 'reports_discounts' ? '%' : ''; ?></i> <?php echo $this->lang->line($detailed->name); ?>
                                                </a>
                                                <span class="input-group-btn">
                                                    <a href="<?php echo site_url('reports/pin/' . $detailed->name . '/' . $detailed->type); ?>" class="btn btn-primary" <?php echo $detailed->pinned ? 'disabled' : ''; ?>>
                                                        <i class="fa fa-thumb-tack"></i>
                                                    </a>
                                                </span>
                                            </div>
                                        </li>
                                        <?php
                                    endif;
                                endforeach;
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel box">
                    <div class="box-header">
                        <h3 class="box-title col-xs-12">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" class="col-xs-12 no-padding-left no-padding-right">
                                <?php echo $this->lang->line('reports_inventory_reports'); ?>
                            </a>
                        </h3>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse">
                        <div class="box-body">
                            <ul class="list-unstyled">
                                <?php
                                foreach ($reports as $inventory):
                                    if ($inventory->type == 'reports_inventory_reports'):
                                        ?>
                                        <li>
                                            <div class="input-group margin">
                                                <a href="<?php echo site_url("reports/{$inventory->link}"); ?>" class="btn btn-block btn-social btn-default">
                                                    <i class="<?php echo $inventory->icon; ?>"></i> <?php echo $this->lang->line($inventory->name); ?>
                                                </a>
                                                <span class="input-group-btn">
                                                    <a href="<?php echo site_url('reports/pin/' . $inventory->name . '/' . $inventory->type); ?>" class="btn btn-primary" <?php echo $inventory->pinned ? 'disabled' : ''; ?>>
                                                        <i class="fa fa-thumb-tack"></i>
                                                    </a>
                                                </span>
                                            </div>
                                        </li>
                                        <?php
                                    endif;
                                endforeach;
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div>
<script>     $(document).ready(function () {
        $('#daterange-btn').daterangepicker({
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                'Last 7 Days': [moment().subtract('days', 6), moment()],
                'Last 30 Days': [moment().subtract('days', 29), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')],
                'All Time': []
            },
            startDate: moment(),
            endDate: moment()
        }, function (start, end) {
            $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        });
        $('input[name="report_type"]').parent().hide();
        var category;
        $('input[name="report_category"]').change(function () {
            category = $(this).attr('value');
            if (category !== 'inventory_low' || category !== 'inventory_summary') {
                $('input[name="report_type"]').parent().fadeIn();
            }
        });
    });
</script>