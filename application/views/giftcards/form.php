<div class="col-xs-12">
    <div class="box box-solid">
        <div class="box-body">
            <?php echo form_open("giftcards/save/" . $giftcard_info->giftcard_id, array('id' => 'giftcard_form', 'class' => 'form-horizontal')); ?>
            <fieldset>
                <legend>
                    <?php echo $this->lang->line('giftcards_basic_information'); ?>
                </legend>
                <div class="form-group">
                    <label for="customer" class="control-label col-sm-2"><?php echo $this->lang->line('giftcards_person_id'); ?></label>
                    <div class="col-sm-5">
                        <input type="text" id="customer" name="query" autocomplete="off" class="form-control" placeholder="<?php echo $this->lang->line('sales_start_typing_customer_name'); ?>" value="<?php echo!empty($customer_info->first_name) ? $customer_info->first_name . ' ' . $customer_info->last_name : ''; ?>">
                        <input type="hidden" name="customer_id" id="customer_id" value="<?php echo $giftcard_info->person_id; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="giftcard_number" class="control-label col-sm-2"><?php echo $this->lang->line('giftcards_giftcard_number'); ?></label>
                    <div class="col-sm-5">
                        <input type="number" name="giftcard_number" id="giftcard_number" class="form-control text-right" value="<?php echo $giftcard_number; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="value" class="control-label col-sm-2"><?php echo $this->lang->line('giftcards_card_value'); ?></label>
                    <div class="col-sm-5">
                        <input type="text" name="value" id="value" class="text-right number" value="<?php echo $giftcard_info->value; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-7">
                        <input type="submit" name="submit" id="submit" class="btn btn-primary pull-right" value="<?php echo $this->lang->line('common_submit'); ?>"/>
                    </div><!-- /.col-sm-7 -->
                </div><!-- /.form-group -->
            </fieldset>
            <?php echo form_close(); ?>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div><!-- /.col-xs-12 -->
<script src="<?php echo base_url(); ?>assets/js/jquery.autocomplete.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        $('#customer').devbridgeAutocomplete({
            serviceUrl: '<?php echo site_url(); ?>/sales/customer_search',
            minChars: 2,
            onSearchStart: function (query) {
                console.log(query);
                $(this).addClass('loading-state');
            },
            onSearchComplete: function (query, suggestions) {
                console.log(query);
                console.log(suggestions);
                $(this).removeClass('loading-state');
            },
            onSelect: function (suggestion) {
                $('#customer_id').val(suggestion.data);
            },
            showNoSuggestionNotice: true,
            noSuggestionNotice: '<?php echo $this->lang->line('sales_no_customer_found'); ?>'
        });
    });
</script>