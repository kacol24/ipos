<!DOCTYPE html>
<html lang='en' class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title>Setup Wizard</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.2.0 -->
        <link href = "//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel = "stylesheet" type = "text/css" />

        <!--font Awesome -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <style>
            .fuelux .wizard {
                *zoom: 1;
                border: 1px solid #d4d4d4;
                border-radius: 4px;
                box-shadow: 0 1px 4px rgba(0, 0, 0, 0.065);
                background-color: #f9f9f9;
                position: relative;
                min-height: 48px;
                overflow: hidden;
            }
            .fuelux .wizard:before,
            .fuelux .wizard:after {
                display: table;
                content: "";
                line-height: 0;
            }
            .fuelux .wizard:after {
                clear: both;
            }
            .fuelux .wizard > .steps {
                list-style: none outside none;
                padding: 0;
                margin: 0;
                width: 4000px;
            }
            .fuelux .wizard > .steps.previous-disabled li.complete {
                cursor: default;
            }
            .fuelux .wizard > .steps.previous-disabled li.complete:hover {
                background: #f3f4f5;
                color: #468847;
                cursor: default;
            }
            .fuelux .wizard > .steps.previous-disabled li.complete:hover .chevron:before {
                border-left-color: #f3f4f5;
            }
            .fuelux .wizard > .steps li {
                float: left;
                margin: 0;
                padding: 0 20px 0 30px;
                height: 46px;
                line-height: 46px;
                position: relative;
                background: #ededed;
                color: #999999;
                font-size: 16px;
                cursor: not-allowed;
            }
            .fuelux .wizard > .steps li .chevron {
                border: 24px solid transparent;
                border-left: 14px solid #d4d4d4;
                border-right: 0;
                display: block;
                position: absolute;
                right: -14px;
                top: 0;
                z-index: 1;
            }
            .fuelux .wizard > .steps li .chevron:before {
                border: 24px solid transparent;
                border-left: 14px solid #ededed;
                border-right: 0;
                content: "";
                display: block;
                position: absolute;
                right: 1px;
                top: -24px;
            }
            .fuelux .wizard > .steps li.complete {
                background: #f3f4f5;
                color: #468847;
            }
            .fuelux .wizard > .steps li.complete:hover {
                background: #e7eff8;
                cursor: pointer;
            }
            .fuelux .wizard > .steps li.complete:hover .chevron:before {
                border-left: 14px solid #e7eff8;
            }
            .fuelux .wizard > .steps li.complete .chevron:before {
                border-left: 14px solid #f3f4f5;
            }
            .fuelux .wizard > .steps li.active {
                background: #f1f6fc;
                color: #3a87ad;
                cursor: default;
            }
            .fuelux .wizard > .steps li.active .chevron:before {
                border-left: 14px solid #f1f6fc;
            }
            .fuelux .wizard > .steps li.active .badge {
                background-color: #3a87ad;
            }
            .fuelux .wizard > .steps li .badge {
                margin-right: 8px;
            }
            .fuelux .wizard > .steps li .badge-success {
                background-color: #468847;
            }
            .fuelux .wizard > .steps li:first-child {
                border-radius: 4px 0 0 4px;
                padding-left: 20px;
            }
            .fuelux .wizard > .actions {
                z-index: 1000;
                position: absolute;
                right: 0;
                line-height: 46px;
                float: right;
                padding-left: 15px;
                padding-right: 15px;
                vertical-align: middle;
                background-color: #e5e5e5;
                border-left: 1px solid #d4d4d4;
            }
            .fuelux .wizard > .actions a {
                line-height: 45px;
                font-size: 12px;
                margin-right: 8px;
            }
            .fuelux .wizard > .actions .btn-prev[disabled] {
                cursor: not-allowed;
            }
            .fuelux .wizard > .actions .btn-prev span {
                margin-right: 5px;
            }
            .fuelux .wizard > .actions .btn-next[disabled] {
                cursor: not-allowed;
            }
            .fuelux .wizard > .actions .btn-next span {
                margin-left: 5px;
            }
            .fuelux .wizard .step-content {
                border-top: 1px solid #D4D4D4;
                padding: 10px;
                float: left;
                width: 100%;
            }
            .fuelux .wizard .step-content .step-pane {
                display: none;
            }
            .fuelux .wizard .step-content > .active {
                display: block;
            }
            .fuelux .wizard .step-content > .active .btn-group .active {
                display: inline-block;
            }
            .fuelux .wizard.complete > .actions .glyphicon-arrow-right:before {
                display: none;
            }
            .fuelux .wizard.complete > .actions .glyphicon-arrow-right {
                margin-left: 0;
            }
            body{
                padding-top: 20px;
                padding-bottom: 70px;
            }
        </style>
    </head>
    <body class="fuelux">
        <div class="container">
            <?php if (validation_errors()): ?>
                <div class="row">
                    <div class="alert alert-danger">
                        <?php echo validation_errors(); ?>
                    </div>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-xs-12">
                    <div class="wizard no-transition" data-initialize="wizard" id="setup_wizard">
                        <ul class="steps">
                            <li data-step="1" class="active"><span class="badge">1</span>Your First User<span class="chevron"></span></li>
                            <li data-step="2"><span class="badge">2</span>Your Credentials<span class="chevron"></span></li>
                            <li data-step="3"><span class="badge">3</span>Your Store<span class="chevron"></span></li>
                        </ul>
                        <div class="actions">
                            <button type="button" class="btn btn-default btn-prev"><span class="glyphicon glyphicon-arrow-left"></span>Prev</button>
                            <button type="button" class="btn btn-default btn-next" data-last="Complete">Next<span class="glyphicon glyphicon-arrow-right"></span></button>
                        </div>
                        <?php echo form_open('install/setup', array('class' => 'form-horizontal', 'id' => 'data_form')); ?>
                        <div class="step-content">
                            <div class="step-pane active sample-pane alert" data-step="1">
                                <fieldset>
                                    <legend>
                                        Create a master administrator
                                        <span class="pull-right small"><small class="small"><em class="small"><?php echo $this->config->item('application_version'); ?></em></small></span>
                                    </legend>
                                    <div class="form-group">
                                        <label for="first_name" class="control-label col-sm-2 text-red"><?php echo $this->lang->line('common_first_name'); ?></label>
                                        <div class="col-sm-5">
                                            <input type="text" name="first_name" id="first_name" class="form-control" autofocus/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="last_name" class="control-label col-sm-2 text-red"><?php echo $this->lang->line('common_last_name'); ?></label>
                                        <div class="col-sm-5">
                                            <input type="text" name="last_name" id="last_name" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email" class="control-label col-sm-2"><?php echo $this->lang->line('common_email'); ?></label>
                                        <div class="col-sm-5">
                                            <input type="email" name="email" id="email" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="phone_number" class="control-label col-sm-2"><?php echo $this->lang->line('common_phone_number'); ?></label>
                                        <div class="col-sm-5">
                                            <input type="text" name="phone_number" id="phone_number" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="address_1" class="control-label col-sm-2"><?php echo $this->lang->line('common_address_1'); ?></label>
                                        <div class="col-sm-5">
                                            <input type="text" name="address_1" id="address_1" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="address_2" class="control-label col-sm-2"><?php echo $this->lang->line('common_address_2'); ?></label>
                                        <div class="col-sm-5">
                                            <input type="text" name="address_2" id="address_2" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="city" class="control-label col-sm-2"><?php echo $this->lang->line('common_city'); ?></label>
                                        <div class="col-sm-5">
                                            <input type="text" name="city" id="city" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="state" class="control-label col-sm-2"><?php echo $this->lang->line('common_state'); ?></label>
                                        <div class="col-sm-5">
                                            <input type="text" name="state" id="state" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="zip" class="control-label col-sm-2"><?php echo $this->lang->line('common_zip'); ?></label>
                                        <div class="col-sm-5">
                                            <input type="text" name="zip" id="zip" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="country" class="control-label col-sm-2"><?php echo $this->lang->line('common_country'); ?></label>
                                        <div class="col-sm-5">
                                            <input type="text" name="country" id="country" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="comments" class="control-label col-sm-2"><?php echo $this->lang->line('common_comments'); ?></label>
                                        <div class="col-sm-5">
                                            <textarea name="comments" id="comments" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="step-pane sample-pane alert" data-step="2">
                                <fieldset>
                                    <legend>
                                        Choose your username and password
                                        <span class="pull-right small"><small class="small"><em class="small"><?php echo $this->config->item('application_version'); ?></em></small></span>
                                    </legend>
                                    <div class="form-group">
                                        <label for="username" class="control-label col-sm-2"><?php echo $this->lang->line('login_username'); ?></label>
                                        <div class="col-sm-5">
                                            <input type="text" name="username" id="username" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="password" class="control-label col-sm-2"><?php echo $this->lang->line('login_password'); ?></label>
                                        <div class="col-sm-5">
                                            <input type="password" name="password" id="password" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="confirm_password" class="control-label col-sm-2">Confirm <?php echo $this->lang->line('login_password'); ?></label>
                                        <div class="col-sm-5">
                                            <input type="password" name="confirm_password" id="confirm_password" class="form-control"/>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="step-pane sample-pane alert" data-step="3">
                                <fieldset>
                                    <legend>
                                        <?php echo $this->lang->line('config_info'); ?>
                                        <span class="pull-right small"><small class="small"><em class="small"><?php echo $this->config->item('application_version'); ?></em></small></span>
                                    </legend>
                                    <div class="form-group">
                                        <label for="company" class="col-sm-2 control-label text-red"><?php echo $this->lang->line('config_company'); ?></label>
                                        <div class="col-sm-5">
                                            <input type="text" name="company" required id="company" class="form-control"/>
                                        </div><!-- /.col-sm-5 -->
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="company_address" class="col-sm-2 control-label text-red"><?php echo $this->lang->line('config_address'); ?></label>
                                        <div class="col-sm-5">
                                            <input type="text" name="company_address" id="company_address" class="form-control" required>
                                        </div><!-- /.col-sm-5 -->
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="company_city" class="col-sm-2 control-label"><?php echo $this->lang->line('config_city'); ?></label>
                                        <div class="col-sm-5">
                                            <input type="text" name="company_city" id="company_city" class="form-control"/>
                                        </div><!-- /.col-sm-5 -->
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="company_state" class="col-sm-2 control-label"><?php echo $this->lang->line('config_state'); ?></label>
                                        <div class="col-sm-5">
                                            <input type="text" name="company_state" id="company_state" class="form-control"/>
                                        </div><!-- /.col-sm-5 -->
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="company_zip" class="col-sm-2 control-label"><?php echo $this->lang->line('config_zip'); ?></label>
                                        <div class="col-sm-5">
                                            <input type="text" name="company_zip" id="company_zip" class="form-control"/>
                                        </div><!-- /.col-sm-5 -->
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="company_country" class="col-sm-2 control-label text-red"><?php echo $this->lang->line('config_country'); ?></label>
                                        <div class="col-sm-5">
                                            <input type="text" name="company_country" required id="company_country" class="form-control"/>
                                        </div><!-- /.col-sm-5 -->
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="company_phone" class="col-sm-2 control-label text-red"><?php echo $this->lang->line('config_phone'); ?></label>
                                        <div class="col-sm-5">
                                            <input type="tel" name="company_phone" required id="company_phone" class="form-control"/>
                                        </div><!-- /.col-sm-5 -->
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="company_fax" class="col-sm-2 control-label"><?php echo $this->lang->line('config_fax'); ?></label>
                                        <div class="col-sm-5">
                                            <input type="tel" name="company_fax" id="company_fax" class="form-control"/>
                                        </div><!-- /.col-sm-5 -->
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="company_email" class="col-sm-2 control-label"><?php echo $this->lang->line('common_email'); ?></label>
                                        <div class="col-sm-5">
                                            <input type="email" name="company_email" id="company_email" class="form-control"/>
                                        </div><!-- /.col-sm-5 -->
                                    </div><!-- /.form-group -->
                                </fieldset>
                                <fieldset>
                                    <legend>
                                        <?php echo $this->lang->line('config_app_info'); ?>
                                    </legend>
                                    <div class="form-group">
                                        <label for="language" class="col-sm-2 control-label"><?php echo $this->lang->line('config_language'); ?></label>
                                        <div class="col-sm-5">
                                            <?php
                                            echo form_dropdown('language', array('en' => 'English', 'id' => 'Indonesian'), '', 'class="form-control custom-dropdown readonly" id="language"');
                                            ?>
                                        </div><!-- /.col-sm-5 -->
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="timezone" class="col-sm-2 control-label"><?php echo $this->lang->line('config_timezone'); ?></label>
                                        <div class="col-sm-5">
                                            <?php
                                            echo form_dropdown('timezone', array(
                                                'Pacific/Midway' => '(GMT-11:00) Midway Island, Samoa',
                                                'America/Adak' => '(GMT-10:00) Hawaii-Aleutian',
                                                'Etc/GMT+10' => '(GMT-10:00) Hawaii',
                                                'Pacific/Marquesas' => '(GMT-09:30) Marquesas Islands',
                                                'Pacific/Gambier' => '(GMT-09:00) Gambier Islands',
                                                'America/Anchorage' => '(GMT-09:00) Alaska',
                                                'America/Ensenada' => '(GMT-08:00) Tijuana, Baja California',
                                                'Etc/GMT+8' => '(GMT-08:00) Pitcairn Islands',
                                                'America/Los_Angeles' => '(GMT-08:00) Pacific Time (US & Canada)',
                                                'America/Denver' => '(GMT-07:00) Mountain Time (US & Canada)',
                                                'America/Chihuahua' => '(GMT-07:00) Chihuahua, La Paz, Mazatlan',
                                                'America/Dawson_Creek' => '(GMT-07:00) Arizona',
                                                'America/Belize' => '(GMT-06:00) Saskatchewan, Central America',
                                                'America/Cancun' => '(GMT-06:00) Guadalajara, Mexico City, Monterrey',
                                                'Chile/EasterIsland' => '(GMT-06:00) Easter Island',
                                                'America/Chicago' => '(GMT-06:00) Central Time (US & Canada)',
                                                'America/New_York' => '(GMT-05:00) Eastern Time (US & Canada)',
                                                'America/Havana' => '(GMT-05:00) Cuba',
                                                'America/Bogota' => '(GMT-05:00) Bogota, Lima, Quito, Rio Branco',
                                                'America/Caracas' => '(GMT-04:30) Caracas',
                                                'America/Santiago' => '(GMT-04:00) Santiago',
                                                'America/La_Paz' => '(GMT-04:00) La Paz',
                                                'Atlantic/Stanley' => '(GMT-04:00) Faukland Islands',
                                                'America/Campo_Grande' => '(GMT-04:00) Brazil',
                                                'America/Goose_Bay' => '(GMT-04:00) Atlantic Time (Goose Bay)',
                                                'America/Glace_Bay' => '(GMT-04:00) Atlantic Time (Canada)',
                                                'America/St_Johns' => '(GMT-03:30) Newfoundland',
                                                'America/Araguaina' => '(GMT-03:00) UTC-3',
                                                'America/Montevideo' => '(GMT-03:00) Montevideo',
                                                'America/Miquelon' => '(GMT-03:00) Miquelon, St. Pierre',
                                                'America/Godthab' => '(GMT-03:00) Greenland',
                                                'America/Argentina/Buenos_Aires' => '(GMT-03:00) Buenos Aires',
                                                'America/Sao_Paulo' => '(GMT-03:00) Brasilia',
                                                'America/Noronha' => '(GMT-02:00) Mid-Atlantic',
                                                'Atlantic/Cape_Verde' => '(GMT-01:00) Cape Verde Is.',
                                                'Atlantic/Azores' => '(GMT-01:00) Azores',
                                                'Europe/Belfast' => '(GMT) Greenwich Mean Time : Belfast',
                                                'Europe/Dublin' => '(GMT) Greenwich Mean Time : Dublin',
                                                'Europe/Lisbon' => '(GMT) Greenwich Mean Time : Lisbon',
                                                'Europe/London' => '(GMT) Greenwich Mean Time : London',
                                                'Africa/Abidjan' => '(GMT) Monrovia, Reykjavik',
                                                'Europe/Amsterdam' => '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna',
                                                'Europe/Belgrade' => '(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague',
                                                'Europe/Brussels' => '(GMT+01:00) Brussels, Copenhagen, Madrid, Paris',
                                                'Africa/Algiers' => '(GMT+01:00) West Central Africa',
                                                'Africa/Windhoek' => '(GMT+01:00) Windhoek',
                                                'Asia/Beirut' => '(GMT+02:00) Beirut',
                                                'Africa/Cairo' => '(GMT+02:00) Cairo',
                                                'Asia/Gaza' => '(GMT+02:00) Gaza',
                                                'Africa/Blantyre' => '(GMT+02:00) Harare, Pretoria',
                                                'Asia/Jerusalem' => '(GMT+02:00) Jerusalem',
                                                'Europe/Minsk' => '(GMT+02:00) Minsk',
                                                'Asia/Damascus' => '(GMT+02:00) Syria',
                                                'Europe/Moscow' => '(GMT+03:00) Moscow, St. Petersburg, Volgograd',
                                                'Africa/Addis_Ababa' => '(GMT+03:00) Nairobi',
                                                'Asia/Tehran' => '(GMT+03:30) Tehran',
                                                'Asia/Dubai' => '(GMT+04:00) Abu Dhabi, Muscat',
                                                'Asia/Yerevan' => '(GMT+04:00) Yerevan',
                                                'Asia/Kabul' => '(GMT+04:30) Kabul',
                                                'Asia/Baku' => '(GMT+05:00) Baku', /* GARRISON ADDED 4/20/2013 */
                                                'Asia/Yekaterinburg' => '(GMT+05:00) Ekaterinburg',
                                                'Asia/Tashkent' => '(GMT+05:00) Tashkent',
                                                'Asia/Kolkata' => '(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi',
                                                'Asia/Katmandu' => '(GMT+05:45) Kathmandu',
                                                'Asia/Dhaka' => '(GMT+06:00) Astana, Dhaka',
                                                'Asia/Novosibirsk' => '(GMT+06:00) Novosibirsk',
                                                'Asia/Rangoon' => '(GMT+06:30) Yangon (Rangoon)',
                                                'Asia/Bangkok' => '(GMT+07:00) Bangkok, Hanoi, Jakarta',
                                                'Asia/Krasnoyarsk' => '(GMT+07:00) Krasnoyarsk',
                                                'Asia/Hong_Kong' => '(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi',
                                                'Asia/Irkutsk' => '(GMT+08:00) Irkutsk, Ulaan Bataar',
                                                'Australia/Perth' => '(GMT+08:00) Perth',
                                                'Australia/Eucla' => '(GMT+08:45) Eucla',
                                                'Asia/Tokyo' => '(GMT+09:00) Osaka, Sapporo, Tokyo',
                                                'Asia/Seoul' => '(GMT+09:00) Seoul',
                                                'Asia/Yakutsk' => '(GMT+09:00) Yakutsk',
                                                'Australia/Adelaide' => '(GMT+09:30) Adelaide',
                                                'Australia/Darwin' => '(GMT+09:30) Darwin',
                                                'Australia/Brisbane' => '(GMT+10:00) Brisbane',
                                                'Australia/Hobart' => '(GMT+10:00) Hobart',
                                                'Asia/Vladivostok' => '(GMT+10:00) Vladivostok',
                                                'Australia/Lord_Howe' => '(GMT+10:30) Lord Howe Island',
                                                'Etc/GMT-11' => '(GMT+11:00) Solomon Is., New Caledonia',
                                                'Asia/Magadan' => '(GMT+11:00) Magadan',
                                                'Pacific/Norfolk' => '(GMT+11:30) Norfolk Island',
                                                'Asia/Anadyr' => '(GMT+12:00) Anadyr, Kamchatka',
                                                'Pacific/Auckland' => '(GMT+12:00) Auckland, Wellington',
                                                'Etc/GMT-12' => '(GMT+12:00) Fiji, Kamchatka, Marshall Is.',
                                                'Pacific/Chatham' => '(GMT+12:45) Chatham Islands',
                                                'Pacific/Tongatapu' => '(GMT+13:00) Nuku\'alofa',
                                                'Pacific/Kiritimati' => '(GMT+14:00) Kiritimati'
                                                    ), $this->config->item('timezone') ? $this->config->item('timezone') : date_default_timezone_get(), 'class="form-control"');
                                            ?>
                                        </div><!-- /.col-sm-5 -->
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="date_format" class="col-sm-2 control-label"><?php echo $this->lang->line('config_date_format'); ?></label>
                                        <div class="col-sm-5">
                                            <select name="date_format" class="form-control" id="date_format">
                                                <option value="d/m/Y">30/12/2000</option>
                                                <option value="m/d/Y">12/30/2000</option>
                                                <option value="d-m-Y">30-12-2000</option>
                                                <option value="m-d-Y">12-30-2000</option>
                                            </select>
                                        </div><!-- /.col-sm-5 -->
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="time_format" class="col-sm-2 control-label"><?php echo $this->lang->line('config_time_format'); ?></label>
                                        <div class="col-sm-5">
                                            <select name="time_format" class="form-control" id="time_format">
                                                <option value="h:i A">01:00 PM</option>
                                                <option value="H:i">13:00</option>
                                                <option value="h:i:s A">01:30:59 PM</option>
                                                <option value="H:i:s">13:30:59</option>
                                            </select>
                                        </div><!-- /.col-sm-5 -->
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="currency_symbol" class="col-sm-2 control-label"><?php echo $this->lang->line('config_currency_symbol'); ?></label>
                                        <div class="col-sm-3">
                                            <input type="text" name="currency_symbol" id="currency_symbol" class="form-control"/>
                                        </div><!-- /.col-sm-5 -->
                                        <div class="col-sm-2">
                                            <label class="control-label">
                                                <?php echo $this->lang->line('config_currency_side'); ?> <input type="checkbox" name="currency_side">
                                            </label>
                                        </div>
                                    </div><!-- /.form-group -->
                                </fieldset>
                                <fieldset>
                                    <legend>
                                        <?php echo $this->lang->line('config_inv_info'); ?>
                                    </legend>
                                    <div class="form-group">
                                        <label for="fax" class="col-sm-2 control-label text-red"><?php echo $this->lang->line('config_stock_location'); ?></label>
                                        <div class="col-sm-5">
                                            <input type="text" name="stock_location" id="stock_location" class="form-control"/>
                                        </div><!-- /.col-sm-5 -->
                                    </div><!-- /.form-group -->
                                </fieldset>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>

        <!-- jQuery 2.1.3 -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

        <!-- Bootstrap -->
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

        <!-- wizard.js -->
        <script>
            /*
             * Fuel UX Wizard
             * https://github.com/ExactTarget/fuelux
             *
             * Copyright (c) 2014 ExactTarget
             * Licensed under the BSD New license.
             */

// -- BEGIN UMD WRAPPER PREFACE --

// For more information on UMD visit:
// https://github.com/umdjs/umd/blob/master/jqueryPlugin.js

            (function (factory) {
                if (typeof define === 'function' && define.amd) {
                    // if AMD loader is available, register as an anonymous module.
                    define(['jquery'], factory);
                } else {
                    // OR use browser globals if AMD is not present
                    factory(jQuery);
                }
            }(function ($) {
                // -- END UMD WRAPPER PREFACE --

                // -- BEGIN MODULE CODE HERE --

                var old = $.fn.wizard;

                // WIZARD CONSTRUCTOR AND PROTOTYPE

                var Wizard = function (element, options) {
                    var kids;

                    this.$element = $(element);
                    this.options = $.extend({}, $.fn.wizard.defaults, options);
                    this.options.disablePreviousStep = (this.$element.attr('data-restrict') === 'previous') ? true : this.options.disablePreviousStep;
                    this.currentStep = this.options.selectedItem.step;
                    this.numSteps = this.$element.find('.steps li').length;
                    this.$prevBtn = this.$element.find('button.btn-prev');
                    this.$nextBtn = this.$element.find('button.btn-next');

                    kids = this.$nextBtn.children().detach();
                    this.nextText = $.trim(this.$nextBtn.text());
                    this.$nextBtn.append(kids);

                    // handle events
                    this.$prevBtn.on('click.fu.wizard', $.proxy(this.previous, this));
                    this.$nextBtn.on('click.fu.wizard', $.proxy(this.next, this));
                    this.$element.on('click.fu.wizard', 'li.complete', $.proxy(this.stepclicked, this));

                    this.selectedItem(this.options.selectedItem);

                    if (this.options.disablePreviousStep) {
                        this.$prevBtn.attr('disabled', true);
                        this.$element.find('.steps').addClass('previous-disabled');
                    }
                };

                Wizard.prototype = {
                    constructor: Wizard,
                    destroy: function () {
                        this.$element.remove();
                        // any external bindings [none]
                        // empty elements to return to original markup [none]
                        // returns string of markup
                        return this.$element[0].outerHTML;
                    },
                    //index is 1 based
                    //second parameter can be array of objects [{ ... }, { ... }] or you can pass n additional objects as args
                    //object structure is as follows (all params are optional): { badge: '', label: '', pane: '' }
                    addSteps: function (index) {
                        var items = [].slice.call(arguments).slice(1);
                        var $steps = this.$element.find('.steps');
                        var $stepContent = this.$element.find('.step-content');
                        var i, l, $pane, $startPane, $startStep, $step;

                        index = (index === -1 || (index > (this.numSteps + 1))) ? this.numSteps + 1 : index;
                        if (items[0] instanceof Array) {
                            items = items[0];
                        }

                        $startStep = $steps.find('li:nth-child(' + index + ')');
                        $startPane = $stepContent.find('.step-pane:nth-child(' + index + ')');
                        if ($startStep.length < 1) {
                            $startStep = null;
                        }

                        for (i = 0, l = items.length; i < l; i++) {
                            $step = $('<li data-step="' + index + '"><span class="badge badge-info"></span></li>');
                            $step.append(items[i].label || '').append('<span class="chevron"></span>');
                            $step.find('.badge').append(items[i].badge || index);

                            $pane = $('<div class="step-pane" data-step="' + index + '"></div>');
                            $pane.append(items[i].pane || '');

                            if (!$startStep) {
                                $steps.append($step);
                                $stepContent.append($pane);
                            } else {
                                $startStep.before($step);
                                $startPane.before($pane);
                            }

                            index++;
                        }

                        this.syncSteps();
                        this.numSteps = $steps.find('li').length;
                        this.setState();
                    },
                    //index is 1 based, howMany is number to remove
                    removeSteps: function (index, howMany) {
                        var action = 'nextAll';
                        var i = 0;
                        var $steps = this.$element.find('.steps');
                        var $stepContent = this.$element.find('.step-content');
                        var $start;

                        howMany = (howMany !== undefined) ? howMany : 1;

                        if (index > $steps.find('li').length) {
                            $start = $steps.find('li:last');
                        } else {
                            $start = $steps.find('li:nth-child(' + index + ')').prev();
                            if ($start.length < 1) {
                                action = 'children';
                                $start = $steps;
                            }

                        }

                        $start[action]().each(function () {
                            var item = $(this);
                            var step = item.attr('data-step');
                            if (i < howMany) {
                                item.remove();
                                $stepContent.find('.step-pane[data-step="' + step + '"]:first').remove();
                            } else {
                                return false;
                            }

                            i++;
                        });

                        this.syncSteps();
                        this.numSteps = $steps.find('li').length;
                        this.setState();
                    },
                    setState: function () {
                        var canMovePrev = (this.currentStep > 1);//remember, steps index is 1 based...
                        var isFirstStep = (this.currentStep === 1);
                        var isLastStep = (this.currentStep === this.numSteps);

                        // disable buttons based on current step
                        if (!this.options.disablePreviousStep) {
                            this.$prevBtn.attr('disabled', (isFirstStep === true || canMovePrev === false));
                        }

                        // change button text of last step, if specified
                        var last = this.$nextBtn.attr('data-last');
                        if (last) {
                            this.lastText = last;
                            // replace text
                            var text = this.nextText;
                            if (isLastStep === true) {
                                text = this.lastText;
                                // add status class to wizard
                                this.$element.addClass('complete');
                            } else {
                                this.$element.removeClass('complete');
                            }

                            var kids = this.$nextBtn.children().detach();
                            this.$nextBtn.text(text).append(kids);
                        }

                        // reset classes for all steps
                        var $steps = this.$element.find('.steps li');
                        $steps.removeClass('active').removeClass('complete');
                        $steps.find('span.badge').removeClass('badge-info').removeClass('badge-success');

                        // set class for all previous steps
                        var prevSelector = '.steps li:lt(' + (this.currentStep - 1) + ')';
                        var $prevSteps = this.$element.find(prevSelector);
                        $prevSteps.addClass('complete');
                        $prevSteps.find('span.badge').addClass('badge-success');

                        // set class for current step
                        var currentSelector = '.steps li:eq(' + (this.currentStep - 1) + ')';
                        var $currentStep = this.$element.find(currentSelector);
                        $currentStep.addClass('active');
                        $currentStep.find('span.badge').addClass('badge-info');

                        // set display of target element
                        var $stepContent = this.$element.find('.step-content');
                        var target = $currentStep.attr('data-step');
                        $stepContent.find('.step-pane').removeClass('active');
                        $stepContent.find('.step-pane[data-step="' + target + '"]:first').addClass('active');

                        // reset the wizard position to the left
                        this.$element.find('.steps').first().attr('style', 'margin-left: 0');

                        // check if the steps are wider than the container div
                        var totalWidth = 0;
                        this.$element.find('.steps > li').each(function () {
                            totalWidth += $(this).outerWidth();
                        });
                        var containerWidth = 0;
                        if (this.$element.find('.actions').length) {
                            containerWidth = this.$element.width() - this.$element.find('.actions').first().outerWidth();
                        } else {
                            containerWidth = this.$element.width();
                        }

                        if (totalWidth > containerWidth) {
                            // set the position so that the last step is on the right
                            var newMargin = totalWidth - containerWidth;
                            this.$element.find('.steps').first().attr('style', 'margin-left: -' + newMargin + 'px');

                            // set the position so that the active step is in a good
                            // position if it has been moved out of view
                            if (this.$element.find('li.active').first().position().left < 200) {
                                newMargin += this.$element.find('li.active').first().position().left - 200;
                                if (newMargin < 1) {
                                    this.$element.find('.steps').first().attr('style', 'margin-left: 0');
                                } else {
                                    this.$element.find('.steps').first().attr('style', 'margin-left: -' + newMargin + 'px');
                                }

                            }

                        }

                        // only fire changed event after initializing
                        if (typeof (this.initialized) !== 'undefined') {
                            var e = $.Event('changed.fu.wizard');
                            this.$element.trigger(e, {
                                step: this.currentStep
                            });
                        }

                        this.initialized = true;
                    },
                    stepclicked: function (e) {
                        var li = $(e.currentTarget);
                        var index = this.$element.find('.steps li').index(li);

                        if (index < this.currentStep && this.options.disablePreviousStep) {//enforce restrictions
                            return;
                        } else {
                            var evt = $.Event('stepclicked.fu.wizard');
                            this.$element.trigger(evt, {
                                step: index + 1
                            });
                            if (evt.isDefaultPrevented()) {
                                return;
                            }

                            this.currentStep = (index + 1);
                            this.setState();
                        }
                    },
                    syncSteps: function () {
                        var i = 1;
                        var $steps = this.$element.find('.steps');
                        var $stepContent = this.$element.find('.step-content');

                        $steps.children().each(function () {
                            var item = $(this);
                            var badge = item.find('.badge');
                            var step = item.attr('data-step');

                            if (!isNaN(parseInt(badge.html(), 10))) {
                                badge.html(i);
                            }

                            item.attr('data-step', i);
                            $stepContent.find('.step-pane[data-step="' + step + '"]:last').attr('data-step', i);
                            i++;
                        });
                    },
                    previous: function () {
                        if (this.options.disablePreviousStep || this.currentStep === 1) {
                            return;
                        }

                        var e = $.Event('actionclicked.fu.wizard');
                        this.$element.trigger(e, {step: this.currentStep,
                            direction: 'previous'
                        });
                        if (e.isDefaultPrevented()) {
                            return;
                        }// don't increment ...what? Why?

                        this.currentStep -= 1;
                        this.setState();

                        // only set focus if focus is still on the $nextBtn (avoid stomping on a focus set programmatically in actionclicked callback)
                        if (this.$prevBtn.is(':focus')) {
                            var firstFormField = this.$element.find('.active').find('input, select, textarea')[0];

                            if (typeof firstFormField !== 'undefined') {
                                // allow user to start typing immediately instead of having to click on the form field.
                                $(firstFormField).focus();
                            } else if (this.$element.find('.active input:first').length === 0 && this.$prevBtn.is(':disabled')) {
                                //only set focus on a button as the last resort if no form fields exist and the just clicked button is now disabled
                                this.$nextBtn.focus();
                            }

                        }
                    },
                    next: function () {
                        if (this.currentStep < this.numSteps) {
                            var e = $.Event('actionclicked.fu.wizard');
                            this.$element.trigger(e, {
                                step: this.currentStep,
                                direction: 'next'
                            });
                            if (e.isDefaultPrevented()) {
                                return;
                            }// respect preventDefault in case dev has attached validation to step and wants to stop propagation based on it.

                            this.currentStep += 1;
                            this.setState();
                        } else {//is last step
                            this.$element.trigger('finished.fu.wizard');
                        }

                        // only set focus if focus is still on the $nextBtn (avoid stomping on a focus set programmatically in actionclicked callback)
                        if (this.$nextBtn.is(':focus')) {
                            var firstFormField = this.$element.find('.active').find('input, select, textarea')[0];

                            if (typeof firstFormField !== 'undefined') {
                                // allow user to start typing immediately instead of having to click on the form field.
                                $(firstFormField).focus();
                            } else if (this.$element.find('.active input:first').length === 0 && this.$nextBtn.is(':disabled')) {
                                //only set focus on a button as the last resort if no form fields exist and the just clicked button is now disabled
                                this.$prevBtn.focus();
                            }

                        }
                    },
                    selectedItem: function (selectedItem) {
                        var retVal, step;

                        if (selectedItem) {
                            step = selectedItem.step || -1;
                            //allow selection of step by data-name
                            step = isNaN(step) && this.$element.find('.steps li[data-name="' + step + '"]').first().attr('data-step') || step;

                            if (1 <= step && step <= this.numSteps) {
                                this.currentStep = step;
                                this.setState();
                            } else {
                                step = this.$element.find('.steps li.active:first').attr('data-step');
                                if (!isNaN(step)) {
                                    this.currentStep = parseInt(step, 10);
                                    this.setState();
                                }

                            }

                            retVal = this;
                        } else {
                            retVal = {
                                step: this.currentStep
                            };
                            if (this.$element.find('.steps li.active:first[data-name]').length) {
                                retVal.stepname = this.$element.find('.steps li.active:first').attr('data-name');
                            }

                        }

                        return retVal;
                    }
                };
                // WIZARD PLUGIN DEFINITION
                $.fn.wizard = function (option) {
                    var args = Array.prototype.slice.call(arguments, 1);
                    var methodReturn;

                    var $set = this.each(function () {
                        var $this = $(this);
                        var data = $this.data('fu.wizard');
                        var options = typeof option === 'object' && option;

                        if (!data) {
                            $this.data('fu.wizard', (data = new Wizard(this, options)));
                        }

                        if (typeof option === 'string') {
                            methodReturn = data[option].apply(data, args);
                        }
                    });

                    return (methodReturn === undefined) ? $set : methodReturn;
                };

                $.fn.wizard.defaults = {
                    disablePreviousStep: false,
                    selectedItem: {
                        step: -1
                    }//-1 means it will attempt to look for "active" class in order to set the step
                };
                $.fn.wizard.Constructor = Wizard;

                $.fn.wizard.noConflict = function () {
                    $.fn.wizard = old;
                    return this;
                };


                // DATA-API

                $(document).on('mouseover.fu.wizard.data-api', '[data-initialize=wizard]', function (e) {
                    var $control = $(e.target).closest('.wizard');
                    if (!$control.data('fu.wizard')) {
                        $control.wizard($control.data());
                    }
                });

                // Must be domReady for AMD compatibility
                $(function () {
                    $('[data-initialize=wizard]').each(function () {
                        var $this = $(this);
                        if ($this.data('fu.wizard'))
                            return;
                        $this.wizard($this.data());
                    });
                });

                // -- BEGIN UMD WRAPPER AFTERWORD --
            }));
            // -- END UMD WRAPPER AFTERWORD --
            $(function () {
                $('#setup_wizard').on('finished.fu.wizard', function () {
                    $('#data_form').submit();
                });
            });
        </script>
    </body>
</html>