<?php 

$lang["events_event_name"] = "Name";
$lang["events_event_desc"] = "Desc";
$lang["events_event_time"] = "Time";
$lang["events_name_complete_sales"] = "Sales Completed";
$lang["events_desc_complete_sales"] = "Completed new sales with ID";
$lang["events_name_sales_print_receipt"] = "Sales Receipt Printed";
$lang["events_desc_sales_print_receipt"] = "Printed sales receipt with ID";
$lang["events_name_sales_edited"] = "Sales Edited";
$lang["events_desc_sales_edited"] = "Edited sales with ID";
$lang["events_name_sales_suspended"] = "Sales Suspended";
$lang["events_desc_sales_suspended"] = "Suspended a sales";
$lang["events_name_unsuspend_sales"] = "Sales Unsuspended";
$lang["events_desc_unsuspend_sales"] = "Unsuspended a sales";
$lang["events_name_delete_suspended"] = "Deleted Suspended Sales";
$lang["events_desc_delete_suspended"] = "Deleted a suspended sales";
$lang["events_name_record_payment"] = "Receivables Payment";
$lang["events_desc_record_payment"] = "Receivables payment has been recorded on";
$lang["events_name_withdraw_payment"] = "Receivables Payment Withdrawn";
$lang["events_desc_withdraw_payment"] = "Withdrawn receivables payment on";
