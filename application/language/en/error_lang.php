<?php 

$lang["error_no_permission_module"] = "You do not have permission to access the module named";
$lang["error_unknown"] = "unknown";
$lang["error_404_oops"] = "Oops! Page not found.";
$lang["error_404_message"] = "The page you are looking for is not found. You may return to the dashboard.";
