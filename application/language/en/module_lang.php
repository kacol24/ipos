<?php 

$lang["module_home"] = "Dashboard";
$lang["module_home_desc"] = "Control Panel";
$lang["module_customers"] = "Customers";
$lang["module_customers_desc"] = "Add, Update, Delete, and Search Customers";
$lang["module_suppliers"] = "Suppliers";
$lang["module_suppliers_desc"] = "Add, Update, Delete, and Search Suppliers";
$lang["module_employees"] = "Employees";
$lang["module_employees_desc"] = "Add, Update, Delete, and Search Employees";
$lang["module_sales"] = "Sales";
$lang["module_sales_desc"] = "Process sales and returns";
$lang["module_reports"] = "Reports";
$lang["module_reports_desc"] = "View and generate reports";
$lang["module_items"] = "Items";
$lang["module_items_desc"] = "Add, Update, Delete, and Search items";
$lang["module_config"] = "Store Config";
$lang["module_config_desc"] = "Change the store's configuration";
$lang["module_receivings"] = "Receivings";
$lang["module_receivings_desc"] = "Process Purchase Orders";
$lang["module_giftcards"] = "Giftcards";
$lang["module_giftcards_desc"] = "Add, Update, Delete and Search Gift Cards";
$lang["module_item_kits"] = "Item Kits";
$lang["module_item_kits_desc"] = "Add, Update, Delete and Search Item Kits";
$lang["module_locations"] = "Locations";
$lang["module_locations_desc"] = "Add, Update, Delete, and Search Locations";
$lang["module_events"] = "Event Log";
$lang["module_events_desc"] = "View the event log";
