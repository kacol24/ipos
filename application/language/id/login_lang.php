<?php 

$lang["login_login"] = "Masuk";
$lang["login_username"] = "Nama Pengguna";
$lang["login_password"] = "Kata Sandi";
$lang["login_go"] = "Lanjutkan";
$lang["login_invalid_username_and_password"] = "Nama Pengguna/Kata Sandi Salah";
$lang["login_welcome_message"] = "Selamat Datang, silahkan login untuk masuk aplikasi, isikan Nama Pengguna dan Kata Sandi anda di bawah ini.";
$lang["login_logout"] = "Keluar";
