<?php 

$lang["item_kits_name"] = "Nama Paket Item";
$lang["item_kits_description"] = "Deskripsi Paket Item";
$lang["item_kits_no_item_kits_to_display"] = "Tidak ada paket item untuk ditampilkan";
$lang["item_kits_update"] = "Perbaruhi Paket Item";
$lang["item_kits_delete"] = "Hapus Paket Item";
$lang["item_kits_new"] = "Paket Item Baru";
$lang["item_kits_none_selected"] = "Anda belum memilih paket item.";
$lang["item_kits_info"] = "Informasi Paket Item";
$lang["item_kits_successful_adding"] = "Paket Item berhasil ditambahkan.";
$lang["item_kits_successful_updating"] = "Paket Item berhasil diperbaruhi.";
$lang["item_kits_error_adding_updating"] = "Kesalahan dalam menambah/memperbaruhi paket item.";
$lang["item_kits_successful_deleted"] = "Paket Item berhasil dihapus";
$lang["item_kits_confirm_delete"] = "Anda yakin akan menghapus paket item yang dipilih?";
$lang["item_kits_one_or_multiple"] = "Paket Item";
$lang["item_kits_cannot_be_deleted"] = "Tidak dapat menghapus paket item.";
$lang["item_kits_add_item"] = "Tambah Item";
$lang["item_kits_items"] = "Item";
$lang["item_kits_item"] = "Item";
$lang["item_kits_quantity"] = "Jumlah";
