<?php 

$lang["suppliers_new"] = "Pemasok Baru";
$lang["suppliers_supplier"] = "Pemasok Baru";
$lang["suppliers_update"] = "Ubah Data Pemasok";
$lang["suppliers_delete"] = "Hapus Pemasok";
$lang["suppliers_confirm_delete"] = "Apakah anda yakin akan menghapus pemasok yang dipilih?";
$lang["suppliers_none_selected"] = "Anda belum memilih pemasok untuk dihapus";
$lang["suppliers_error_adding_updating"] = "Kesalahan dalam menambah/memperbaruhi pemasok";
$lang["suppliers_successful_adding"] = "Anda telah berhasil menambah pemasok";
$lang["suppliers_successful_updating"] = "Anda telah berhasil memperbaruhi pemasok";
$lang["suppliers_successful_deleted"] = "Anda telah berhasil menghapus";
$lang["suppliers_one_or_multiple"] = "Pemasok";
$lang["suppliers_cannot_be_deleted"] = "Pemasok yang dipilih tidak dapat dihapus, pemasok yang dipilih memiliki penjualan.";
$lang["suppliers_basic_information"] = "Informasi Pemasok";
$lang["suppliers_account_number"] = "Nomor Rekening";
$lang["suppliers_company_name"] = "Nama Perusahaan";
$lang["suppliers_company_name_required"] = "Nama Perusahaan wajib diisi";
