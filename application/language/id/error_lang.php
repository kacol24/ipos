<?php 

$lang["error_no_permission_module"] = "Anda tidak memiliki izin untuk mengakses modul ini";
$lang["error_unknown"] = "tidak dikenal";
$lang["error_404_oops"] = "Oops! Halaman tidak ditemukan.";
$lang["error_404_message"] = "Halaman yang anda cari tidak ditemukan. Anda dapat kembali ke dasbor.";
