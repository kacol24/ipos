<?php 

$lang["items_item_number"] = "Kode Produk";
$lang["items_retrive_item_info"] = "Dapatkan Info Item";
$lang["items_description"] = "Deskripsi";
$lang["items_cannot_find_item"] = "Tidak dapat menemukan informasi mengenai item";
$lang["items_basic_information"] = "Informasi Item";
$lang["items_price_information"] = "Informasi Harga";
$lang["items_inventory_information"] = "Informasi Persediaan";
$lang["items_number_information"] = "Nomor Barang";
$lang["items_new"] = "Item Baru";
$lang["items_update"] = "Ubah Item";
$lang["items_delete"] = "Hapus Item";
$lang["items_item"] = "Item";
$lang["items_name"] = "Nama Item";
$lang["items_category"] = "Kategori";
$lang["items_cost_price"] = "Harga Pokok";
$lang["items_unit_price"] = "Harga Jual";
$lang["items_tax_1"] = "Pajak 1";
$lang["items_tax_2"] = "Pajak 2";
$lang["items_sales_tax_1"] = "Pajak Penjualan 1";
$lang["items_sales_tax_2"] = "Pajak Penjualan 2";
$lang["items_tax_percent"] = "Tarif Pajak";
$lang["items_tax_percents"] = "Tarif Pajak";
$lang["items_reorder_level"] = "Batas Pesan Ulang";
$lang["items_reorder_level_desc"] = "Batas pesan ulang = Perkiraan Waktu Stok Tiba x Rata-rata pengeluaran sehari + Stok Cadangan";
$lang["items_quantity"] = "Jumlah";
$lang["items_no_items_to_display"] = "Tidak ada item untuk ditampilkan";
$lang["items_confirm_delete"] = "Apakah anda yakin akan menghapus item yang dipilih?";
$lang["items_none_selected"] = "Anda belum memilih barang untuk diubah";
$lang["items_error_adding_updating"] = "Kesalahan saat memperbaruhi item";
$lang["items_successful_adding"] = "Item telah berhasil dibuat";
$lang["items_successful_updating"] = "Item telah berhasil diubah";
$lang["items_successful_deleted"] = "Item telah berhasil dihapus";
$lang["items_one_or_multiple"] = "item";
$lang["items_cannot_be_deleted"] = "Tidak dapat menghapus item, item terpilih memiliki penjualan";
$lang["items_name_required"] = "Nama Item wajib diisi";
$lang["items_category_required"] = "Ketogori wajib diisi";
$lang["items_buy_price_required"] = "Harga Beli wajib diisi";
$lang["items_unit_price_required"] = "Harga Jual wajib diisi";
$lang["items_cost_price_required"] = "Biaya Item wajib diisi";
$lang["items_tax_percent_required"] = "Tarif Pajak wajib diisi";
$lang["items_quantity_required"] = "Jumlah wajib diisi";
$lang["items_reorder_level_required"] = "Batas Pesan Ulang wajib diisi";
$lang["items_unit_price_number"] = "Harga satuan harus angka";
$lang["items_cost_price_number"] = "Biaya Item harus angka";
$lang["items_quantity_number"] = "Jumlah harus angka";
$lang["items_reorder_level_number"] = "Batas Pesan Ulang harus angka";
$lang["items_none"] = "Tidak ada item untuk ditampilkan";
$lang["items_supplier"] = "Pemasok";
$lang["items_generate_barcodes"] = "Buat barcode";
$lang["items_must_select_item_for_barcode"] = "Anda harus memilih 1 item untuk membuat barcode";
$lang["items_excel_import_failed"] = "Impor dari Excel tidak berhasil";
$lang["items_allow_alt_description"] = "Perbolehkan deskripsi alternatif";
$lang["items_is_serialized"] = "Nomor Serial";
$lang["items_serialized_items"] = "Item bernomor seri";
$lang["items_no_description_items"] = "Item tidak ada deskripsi";
$lang["items_search_custom_items"] = "Cari secara manual";
$lang["items_inventory_comments"] = "Keterangan";
$lang["items_count"] = "Mutasi/penyesuaian inventory";
$lang["items_details_count"] = "Detail Mutasi Item";
$lang["items_add_minus"] = "Jumlah untuk ditambah/dikurang";
$lang["items_current_quantity"] = "Jumlah saat ini";
$lang["items_quantity_required"] = "Jumlah wajib diisi";
$lang["items_do_nothing"] = "Tidak ada perubahan";
$lang["items_use_inventory_menu"] = "Gunakan menu Inv.";
$lang["items_manually_editing_of_quantity"] = "Perubahan Manual";
$lang["items_inventory"] = "Inventori";
$lang["items_location"] = "Lokasi Item";
$lang["items_is_deleted"] = "Item dihapus";
$lang["items_unit_quantity"] = "Jumlah item";
$lang["items_related_number"] = "Nomor item terkait";
$lang["items_stock_location"] = "Lokasi Stok";
$lang["items_inventory_tracking"] = "Mutasi Data Inventori";
$lang["items_update_inventory"] = "Ubah Inventori";
$lang["items_in_out"] = "Jumlah Keluar/Masuk";
$lang["items_remarks"] = "Catatan";
$lang["items_bulk_edit"] = "Ubah Masal";
$lang["items_change_all_to_serialized"] = "Ubah semua menggunakan nomor serial";
$lang["items_change_all_to_unserialized"] = "Ubah semua tidak menggunakan nomor serial";
$lang["items_do_nothing"] = "Tidak ada perubahan";
$lang["items_edit_fields_you_want_to_update"] = "Ubah bagian yang ingin Anda ubah untuk SEMUA Item yang dipilih";
$lang["items_edit_multiple_items"] = "Ubah Beberapa Item";
$lang["items_error_updating_multiple"] = "Kesalahan ketika memperbaruhi item";
$lang["items_confirm_bulk_edit"] = "Apakah Anda yakin ingin merubah semua item yang dipilih?";
$lang["items_successful_bulk_edit"] = "Anda telah berhasil memperbarui item yang dipilih";
$lang["items_custom_price"] = "Harga Kustom";
