<?php

/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
/*
  Returns the base table layout
 */

function base_table($headers, $data, $id = 'datatable')
{
    $table = '<table id="' . $id . '" class="table table-bordered table-hover dt-responsive" cellspacing="0" width="100%">';
    $table.='<thead><tr>';
    foreach ($headers as $header) {
        $table.="<th$header</th>";
    }
    $table.='</tr></thead><tbody>';
    $table.= $data;
    $table.='</tbody></table>';
    return $table;
}
/*
  Gets the html table to manage people.
 */

function get_people_manage_table($people, $controller)
{
    $CI = & get_instance();
    if ($controller == 'customers') {
        $headers = array(
            ' class="all">' . $CI->lang->line('common_first_name'),
            ' class="min-tablet-p">' . $CI->lang->line('common_last_name'),
            ' class="min-desktop">' . $CI->lang->line('common_email'),
            ' class="all">' . $CI->lang->line('common_phone_number'),
            ' class="all">' . $CI->lang->line('customers_courier'),
            ' class="min-tablet-l">' . $CI->lang->line('customers_discount'),
            ' class="min-desktop">' . $CI->lang->line('common_table_action')
        );
    } else {
        $headers = array(
            ' class="all">' . $CI->lang->line('common_first_name'),
            ' class="min-tablet">' . $CI->lang->line('common_last_name'),
            ' class="min-tablet">' . $CI->lang->line('common_email'),
            ' class="all">' . $CI->lang->line('common_phone_number'),
            ' class="desktop">' . $CI->lang->line('common_table_action')
        );
    }
    $table = base_table($headers, get_people_manage_table_data_rows($people, $controller));
    return $table;
}
/*
  Gets the html data rows for the people.
 */

function get_people_manage_table_data_rows($people, $controller)
{
    $CI = & get_instance();
    $table_data_rows = '';

    foreach ($people->result() as $person) {
        $table_data_rows.=get_person_data_row($person, $controller);
    }

    return $table_data_rows;
}

function get_person_data_row($person, $controller)
{
    $CI = & get_instance();
    $controller_name = strtolower(get_class($CI));

    $table_data_row = '<tr>';
    $table_data_row.='<td>' . character_limiter($person->first_name, 13) . '</td>';
    $table_data_row.='<td>' . character_limiter($person->last_name, 13) . '</td>';
    $table_data_row.='<td>' . mailto($person->email, character_limiter($person->email, 22)) . '</td>';
    $table_data_row.='<td><a href="tel:' . $person->phone_number . '">' . $person->phone_number . '</a></td>';
    if ($controller == 'customers') {
        $table_data_row .='<td>' . $person->courier . '</td>';
        $table_data_row .='<td class="text-right">' . $person->customer_discount . '%</td>';
    }
    $table_data_row.='<td>' . anchor($controller_name . "/update/$person->person_id", ' <i class="fa fa-edit"></i> ', array('class' => 'btn btn-sm btn-success', 'title' => $CI->lang->line($controller_name . '_update')));
    $table_data_row.='&nbsp;' . anchor($controller_name . "/delete/$person->person_id", ' <i class="fa fa-trash-o"></i> ', array('class' => 'btn btn-sm btn-danger delete_modal', 'title' => $CI->lang->line($controller_name . '_delete'), 'data-modal-body' => lang($controller_name . '_confirm_delete'))) . '</td>';
    $table_data_row.='</tr>';

    return $table_data_row;
}

function get_detailed_sales_data_row($sale, $controller)
{
    $table_data_row = '<tr>';
    $table_data_row.='<td><a href="#" class="expand">+</a></td>';
    foreach ($sale as $cell) {
        $table_data_row.='<td>';
        $table_data_row.=$cell;
        $table_data_row.='</td>';
    }
    $table_data_row.='</tr>';
    return $table_data_row;
}
/*
  Gets the html table to manage suppliers.
 */

function get_supplier_manage_table($suppliers, $controller)
{
    $CI = & get_instance();
    $headers = array(
        ' class="all">' . $CI->lang->line('suppliers_company_name'),
        ' class="min-tablet">' . $CI->lang->line('common_first_name'),
        ' class="min-tablet">' . $CI->lang->line('common_last_name'),
        ' class="min-tablet">' . $CI->lang->line('common_email'),
        ' class="all">' . $CI->lang->line('common_phone_number'),
        ' class="desktop">' . $CI->lang->line('common_table_action')
    );
    $table = base_table($headers, get_supplier_manage_table_data_rows($suppliers, $controller));
    return $table;
}
/*
  Gets the html data rows for the supplier.
 */

function get_supplier_manage_table_data_rows($suppliers, $controller)
{
    $CI = & get_instance();
    $table_data_rows = '';

    foreach ($suppliers->result() as $supplier) {
        $table_data_rows.=get_supplier_data_row($supplier, $controller);
    }

    return $table_data_rows;
}

function get_supplier_data_row($supplier, $controller)
{
    $CI = & get_instance();
    $controller_name = strtolower(get_class($CI));

    $table_data_row = '<tr>';
    $table_data_row.='<td>' . character_limiter($supplier->company_name, 13) . '</td>';
    $table_data_row.='<td>' . character_limiter($supplier->first_name, 13) . '</td>';
    $table_data_row.='<td>' . character_limiter($supplier->last_name, 13) . '</td>';
    $table_data_row.='<td>' . mailto($supplier->email, character_limiter($supplier->email, 22)) . '</td>';
    $table_data_row.='<td><a href="tel:' . $supplier->phone_number . '">' . $supplier->phone_number . '</a></td>';
    $table_data_row.='<td>' . anchor($controller_name . "/update/$supplier->person_id", ' <i class="fa fa-edit"></i> ', array('class' => 'btn btn-sm btn-success', 'title' => $CI->lang->line($controller_name . '_update')));
    $table_data_row.='&nbsp;' . anchor($controller_name . "/delete/$supplier->person_id", ' <i class="fa fa-trash-o"></i> ', array(
            'class' => 'btn btn-sm btn-danger delete_modal',
            'title' => $CI->lang->line($controller_name . '_delete'),
            'data-modal-body' => lang('suppliers_confirm_delete')
        )) . '</td>';
    $table_data_row.='</tr>';

    return $table_data_row;
}
/*
  Gets the html table to manage items.
 */

function get_items_manage_table($items, $controller)
{
    $CI = & get_instance();

    $headers = array(
        ' class="all text-center"><input type="checkbox" id="select_all" class="iCheck"/>',
        ' class="all control">' . $CI->lang->line('items_item_number'),
        ' class="not-mobile">' . $CI->lang->line('items_name'),
        ' class="min-tablet">' . $CI->lang->line('items_category'),
        ' class="desktop">' . $CI->lang->line('items_cost_price'),
        ' class="desktop">' . $CI->lang->line('items_unit_price'),
        ' class="min-tablet">' . $CI->lang->line('items_quantity'),
        ' class="min-tablet">' . $CI->lang->line('items_supplier'),
        ' class="all table-action">' . $CI->lang->line('common_table_action')
    );
    $data = get_items_manage_table_data_rows($items, $controller);
    $table = base_table($headers, $data, 'item_table');
    return $table;
}
/*
  Gets the html data rows for the items.
 */

function get_items_manage_table_data_rows($items, $controller)
{
    $CI = & get_instance();
    $table_data_rows = '';

    foreach ($items->result() as $item) {
        $table_data_rows.=get_item_data_row($item, $controller);
    }

    return $table_data_rows;
}

function get_item_data_row($item, $controller)
{
    $CI = & get_instance();
    $item_tax_info = $CI->Item_taxes->get_info($item->item_id);
    $tax_percents = '';
    foreach ($item_tax_info as $tax_info) {
        $tax_percents.=$tax_info['percent'] . '%, ';
    }
    $tax_percents = substr($tax_percents, 0, -2);
    $controller_name = strtolower(get_class($CI));

    if ($item->deleted) {
        $table_data_row = '<tr class="danger">';
    } else {
        $table_data_row = '<tr>';
    }
    $table_data_row .='<td class="text-center"><input type="checkbox" id="item_' . $item->item_id . '" value="' . $item->item_id . '" class="iCheck"></td>';
    $table_data_row.='<td>' . $item->item_number . '</td>';
    $table_data_row.='<td>' . $item->name . '</td>';
    $table_data_row.='<td>' . $item->category . '</td>';
    $table_data_row.='<td class="text-right">' . to_currency($item->cost_price) . '</td>';
    $table_data_row.='<td class="text-right">' . to_currency($item->unit_price) . '</td>';
    $table_data_row.='<td class="text-right">' . $item->quantity . '</td>';
    $table_data_row.='<td>' . $CI->Supplier->get_info($item->supplier_id)->company_name . '</td>';
    $table_data_row.='<td>' . anchor($controller_name . "/update/$item->item_id", ' <i class="fa fa-edit"></i> ', array('class' => 'btn btn-sm btn-success', 'title' => $CI->lang->line($controller_name . '_update')));

    //Ramel Inventory Tracking
    $table_data_row.='&nbsp;' . anchor($controller_name . "/count_details/$item->item_id", ' <i class="fa fa-eye"></i> ', array('class' => 'btn btn-sm btn-primary', 'title' => $CI->lang->line($controller_name . '_details_count'))); //inventory details
    if ( ! $item->deleted) {
        $table_data_row.='&nbsp;' . anchor("{$controller_name}/delete/$item->item_id", ' <i class="fa fa-trash-o"></i> ', array(
                'class' => 'btn btn-sm btn-danger delete_modal',
                'title' => $CI->lang->line('items_delete'),
                'data-modal-body' => lang('items_confirm_delete')
            )) . '</td>';
    }

    $table_data_row.='</tr>';
    return $table_data_row;
}
/*
  Gets the html table to manage giftcards.
 */

function get_giftcards_manage_table($giftcards, $controller)
{
    $CI = & get_instance();
    $headers = array(
        ' class="all">' . $CI->lang->line('giftcards_giftcard_number'),
        ' class="all">' . $CI->lang->line('common_first_name'),
        ' class="min-tablet">' . $CI->lang->line('common_last_name'),
        ' class="desktop">' . $CI->lang->line('giftcards_card_value'),
        ' class="desktop">' . $CI->lang->line('common_table_action')
    );
    $data = get_giftcards_manage_table_data_rows($giftcards, $controller);
    $table = base_table($headers, $data);
    return $table;
}
/*
  Gets the html data rows for the giftcard.
 */

function get_giftcards_manage_table_data_rows($giftcards, $controller)
{
    $CI = & get_instance();
    $table_data_rows = '';
    foreach ($giftcards->result() as $giftcard) {
        $table_data_rows.=

            get_giftcard_data_row($giftcard, $controller);
    } return $table_data_rows;
}

/** GARRISON MODIFIED 4/25/2013 * */
function get_giftcard_data_row($giftcard, $controller)
{
    $CI = & get_instance();
    $controller_name = strtolower(get_class($CI));

    if ($giftcard->value <= 0) {
        $table_data_row = '<tr class="danger">';
    } else {
        $table_data_row = '<tr>';
    }
    $table_data_row .= '<td>' . $giftcard->giftcard_number . '</td>';
    $table_data_row .= '<td>' . $giftcard->first_name . '</td>';
    $table_data_row .= '<td>' . $giftcard->last_name . '</td>';
    $table_data_row .= '<td class="text-right">' . to_currency($giftcard->value) . '</td>';
    $table_data_row.='<td>' . anchor($controller_name . "/update/$giftcard->giftcard_id", ' <i class="fa fa-edit"></i> ', array('class' => 'btn btn-sm btn-success', 'title' => $CI->lang->line($controller_name . '_update')));
    $table_data_row .='&nbsp;' . anchor($controller_name . "/delete/$giftcard->giftcard_id", ' <i class="fa fa-trash-o"></i> ', array(
            'class' => 'btn btn-sm btn-danger delete_modal',
            'title' => $CI->lang->line($controller_name . '_delete'),
            'data-modal-body' => lang('giftcards_confirm_delete')
        )) . '</td>';

    $table_data_row.='</tr>';

    return $table_data_row;
}

/** END GARRISON MODIFIED * */
/*
  Gets the html table to manage item kits.
 */ function get_item_kits_manage_table($item_kits, $controller)
{
    $CI = & get_instance();
    $headers = array(
        ' class="all">' . $CI->lang->line('item_kits_name'),
        ' class="all">' . $CI->lang->line('item_kits_description'),
        ' class="desktop">' . $CI->lang->line('common_table_action')
    );
    $data = get_item_kits_manage_table_data_rows($item_kits, $controller);
    $table = base_table($headers, $data);
    return $table;
}
/*
  Gets the html data rows for the item kits.
 */

function get_item_kits_manage_table_data_rows($item_kits, $controller)
{
    $CI = & get_instance();
    $table_data_rows = '';
    foreach ($item_kits->result() as $item_kit) {
        $table_data_rows.=get_item_kit_data_row($item_kit, $controller);
    }

    return $table_data_rows;
}

function get_item_kit_data_row($item_kit, $controller)
{
    $CI = & get_instance();
    $controller_name = strtolower(get_class($CI));

    $table_data_row = '<tr>';
    $table_data_row .= '<td>' . $item_kit->name . '</td>';
    $table_data_row.= '<td><span data-toggle="tooltip" title="' . $item_kit->description . '">' . character_limiter($item_kit->description, 50) . '</span></td>';
    $table_data_row.='<td>' . anchor($controller_name . "/update/$item_kit->item_kit_id", ' <i class="fa fa-edit"></i> ', array('class' => 'btn btn-sm btn-success', 'title' => $CI->lang->line($controller_name . '_update')));
    $table_data_row .='&nbsp;' . anchor($controller_name . "/delete/$item_kit->item_kit_id", ' <i class="fa fa-trash-o"></i> ', array(
            'class' => 'btn btn-sm btn-danger delete_modal',
            'title' => $CI->lang->line($controller_name . '_delete'),
            'data-modal-body' => lang($controller_name . '_confirm_delete')
        )) . '</td>';

    $table_data_row.='</tr>';
    return $table_data_row;
}
/*
  Gets the html table to manage locations.
 */

function get_stock_locations_manage_table($stock_locations, $controller)
{
    $CI = & get_instance();
    $headers = array(
        ' class="all">' . $CI->lang->line('item_kits_name'),
        ' class="all">' . $CI->lang->line('item_kits_description'),
        ' class="desktop">' . $CI->lang->line('common_table_action')
    );
    $data = get_item_kits_manage_table_data_rows($stock_locations, $controller);
    $table = base_table($headers, $data);
    return $table;
}
/*
  Gets the html data rows for the item kits.
 */

function get_locations_manage_table_data_rows($locations, $controller)
{
    $CI = & get_instance();
    $table_data_rows = '';
    foreach ($locations->result() as $location) {
        $table_data_rows.=get_item_kit_data_row($location, $controller);
    }

    return $table_data_rows;
}

function get_location_data_row($location, $controller)
{
    $CI = & get_instance();
    $controller_name = strtolower(get_class($CI));

    $table_data_row = '<tr>';
    $table_data_row .= '<td>' . $location->name . '</td>';
    $table_data_row.= '<td><span data-toggle="tooltip" title="' . $location->description . '">' . character_limiter($location->description, 50) . '</span></td>';
    $table_data_row.='<td>' . anchor($controller_name . "/update/$location->item_kit_id", '<i class="fa fa-edit"></i>', array('class' => 'btn btn-sm btn-default', 'title' => $CI->lang->line($controller_name . '_update'))) . '</td>';

    $table_data_row.='</tr>';
    return $table_data_row;
}

function get_event_log_manage_table($event_log, $controller)
{
    $CI = & get_instance();
    $headers = array(
        ' class="all">' . $CI->lang->line('events_event_name'),
        '>' . $CI->lang->line('events_event_desc'),
        ' class="all">' . $CI->lang->line('employees_employee'),
        ' data-sort="asc">' . $CI->lang->line('events_event_time')
    );
    $data = get_event_log_manage_table_data_rows($event_log, $controller);
    $table = base_table($headers, $data, 'event_log');
    return $table;
}

function get_event_log_manage_table_data_rows($event_log, $controller)
{
    $CI = &get_instance();
    $table_data_rows = '';
    foreach ($event_log->result() as $log) {
        $table_data_rows .=get_event_log_data_row($log, $controller);
    }
    return $table_data_rows;
}

function get_event_log_data_row($event, $controller)
{
    $CI = &get_instance();
    $controller_name = strtolower(get_class($CI));

    $table_data_row = '<tr>';

    $table_data_row.='<td>';
    $table_data_row.=lang($event->name);
    $table_data_row.='</td>';

    $event_description = explode('|', $event->description);
    $table_data_row.='<td>';
    $table_data_row.=lang($event_description[0]);
    $index = 1;
    while ($index < count($event_description)) {
        $table_data_row.=' ' . $event_description[$index];
        $index ++;
    }
    $table_data_row.='</td>';

    $table_data_row.='<td>';
    $table_data_row.=$event->first_name . ' ' . $event->last_name;
    $table_data_row.='</td>';

    $table_data_row.='<td data-order="' . strtotime($event->event_time) . '">';
    $table_data_row.=date($CI->config->item('date_format') . ' ' . $CI->config->item('time_format'), strtotime($event->event_time));
    $table_data_row.='</td>';

    $table_data_row .='</tr>';
    return $table_data_row;
}
/* End of file table_helper.php */
/* Location: ./application/helpers/table_helper.php */