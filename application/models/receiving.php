<?php

/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
class Receiving extends CI_Model
{

    private $_created = false;

    function get_info($receiving_id)
    {
        $this->db->from('receivings');
        $this->db->where('receiving_id', $receiving_id);
        return $this->db->get();
    }

    function exists($receiving_id)
    {
        $this->db->from('receivings');
        $this->db->where('receiving_id', $receiving_id);
        $query = $this->db->get();

        return ($query->num_rows() == 1);
    }

    function save($items, $supplier_id, $employee_id, $comment, $payment_type, $receiving_id = false)
    {
        if (count($items) == 0)
            return -1;

        $receivings_data = array(
            'supplier_id' => $this->Supplier->exists($supplier_id) ? $supplier_id : null,
            'employee_id' => $employee_id,
            'payment_type' => $payment_type,
            'comment' => $comment
        );

        //Run these queries as a transaction, we want to make sure we do all or nothing
        $this->db->trans_start();

        $this->db->insert('receivings', $receivings_data);
        $receiving_id = $this->db->insert_id();


        foreach ($items as $line => $item) {
            $cur_item_info = $this->Item->get_info($item['item_id']);

            $receivings_items_data = array
                (
                'receiving_id' => $receiving_id,
                'item_id' => $item['item_id'],
                'line' => $item['line'],
                'description' => $item['description'],
                'serialnumber' => $item['serialnumber'],
                'quantity_purchased' => $item['quantity'],
                'discount_percent' => $item['discount'],
                'item_cost_price' => $item['price'],
                'item_unit_price' => $cur_item_info->unit_price,
                'item_location' => $item['item_location']
            );

            $this->db->insert('receivings_items', $receivings_items_data);

            // if the price has changed
            if ($item['price'] != $cur_item_info->cost_price) {
                // update price in item to be the average
                $new_price = ($item['price'] + $cur_item_info->cost_price) / 2;
                $item_data = array(
                    'cost_price' => $new_price
                );
                $this->Item->save($item_data, $cur_item_info->item_id);
            }

            //Update stock quantity
            $item_quantity = $this->Item_quantities->get_item_quantity($item['item_id'], $item['item_location']);
            $this->Item_quantities->save(array('quantity' => $item_quantity->quantity + $item['quantity'],
                'item_id' => $item['item_id'],
                'location_id' => $item['item_location']), $item['item_id'], $item['item_location']);


            $qty_recv = $item['quantity'];
            $recv_remarks = 'RECV ' . $receiving_id;
            $inv_data = array
                (
                'trans_date' => date('Y-m-d H:i:s'),
                'trans_items' => $item['item_id'],
                'trans_user' => $employee_id,
                'trans_location' => $item['item_location'],
                'trans_comment' => $recv_remarks,
                'trans_inventory' => $qty_recv
            );
            $this->Inventory->insert($inv_data);

            $supplier = $this->Supplier->get_info($supplier_id);
        }
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            return -1;
        }

        return $receiving_id;
    }

    function get_receiving_items($receiving_id)
    {
        $this->db->from('receivings_items');
        $this->db->where('receiving_id', $receiving_id);
        return $this->db->get();
    }

    function get_supplier($receiving_id)
    {
        $this->db->from('receivings');
        $this->db->where('receiving_id', $receiving_id);
        return $this->Supplier->get_info($this->db->get()->row()->supplier_id);
    }

    //We create a temp table that allows us to do easy report/receiving queries
    public function create_receivings_items_temp_table()
    {
        if ( ! $this->_created) {
            $this->db->query("CREATE TEMPORARY TABLE IF NOT EXISTS " . $this->db->dbprefix('receivings_items_temp') . "
		(SELECT date(receiving_time) as receiving_date, " . $this->db->dbprefix('receivings_items') . ".receiving_id, comment,payment_type, employee_id,
		" . $this->db->dbprefix('items') . ".item_id, " . $this->db->dbprefix('receivings') . ".supplier_id, quantity_purchased, item_cost_price, item_unit_price,
		discount_percent, (item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100) as subtotal,
		" . $this->db->dbprefix('receivings_items') . ".line as line, serialnumber, " . $this->db->dbprefix('receivings_items') . ".description as description,
		(item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100) as total,
		(item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100) - (item_cost_price*quantity_purchased) as profit
		FROM " . $this->db->dbprefix('receivings_items') . "
		INNER JOIN " . $this->db->dbprefix('receivings') . " ON  " . $this->db->dbprefix('receivings_items') . '.receiving_id=' . $this->db->dbprefix('receivings') . '.receiving_id' . "
		INNER JOIN " . $this->db->dbprefix('items') . " ON  " . $this->db->dbprefix('receivings_items') . '.item_id=' . $this->db->dbprefix('items') . '.item_id' . "
		GROUP BY receiving_id, item_id, line)");
            $this->_created = TRUE;
        }
    }
}
