<?php

/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
class Payment_record extends CI_Model
{

    /**
     * Returns all payment records of a specific sale_id
     *
     * @param int $sale_id The sale_id
     * @param int $limit The limit of the record (default = 10000)
     * @param int $offset The offset of the record (default = 0)
     * @return object The db connection instance
     */
    function get_all($sale_id, $limit = 10000, $offset = 0)
    {
        $this->db->from('payment_records');
        $this->db->where('sale_id', $sale_id);
        $this->db->limit($limit);
        $this->db->offset($offset);
        return $this->db->get();
    }
    /*
     * Return info on particular record id
     */

    function get_info($record_id)
    {
        $this->db->from('payment_records');
        $this->db->where('record_id', $record_id);
        return $this->db->get();
    }
    /*
     * Add payment record to db, subtract amount from payments
     */

    function add_payment_record($sale_id, $payment_type, $payment_amount)
    {
        $data = array(
            'sale_id' => $sale_id,
            'payment_type' => $payment_type,
            'payment_amount' => $payment_amount,
            'payment_time' => date('Y-m-d H:i:s')
        );
        $this->load->model('Sales_payment');
        $payment = $this->Sales_payment->get_store_account($sale_id)->payment_amount;

        $this->db->trans_start();

        // insert payment records
        $this->db->insert('payment_records', $data);

        if ($this->Sales_payment->exists($sale_id, $payment_type)) {
            // update sales payment
            $existing_payment = $this->Sales_payment->get_info($sale_id, $payment_type)->row();
            $this->db->where(array(
                'sale_id' => $sale_id,
                'payment_type' => $payment_type
            ));
            $this->db->update('sales_payments', array(
                'payment_amount' => $existing_payment->payment_amount + $payment_amount
            ));
        } else {
            // insert to sales payment
            $this->db->insert('sales_payments', array(
                'sale_id' => $sale_id,
                'payment_type' => $payment_type,
                'payment_amount' => $payment_amount
            ));
        }

        // subtract payment amount in store account
        $this->db->where(array(
            'sale_id' => $sale_id,
            'payment_type' => 'sales_store_account'
        ));
        $this->db->update('sales_payments', array(
            'payment_amount' => $payment - $payment_amount
        ));

        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    function withdraw_payment_record($record_id)
    {
        $record = $this->get_info($record_id)->row();
        $this->load->model('Sales_payment');
        $sales_payment = $this->Sales_payment->get_info($record->sale_id, 'sales_store_account')->row();


        $this->db->trans_start();

        $this->db->delete('payment_records', array('record_id' => $record_id));

        $this->db->where(array(
            'sale_id' => $record->sale_id,
            'payment_type' => 'sales_store_account'
        ));
        $this->db->update('sales_payments', array(
            'payment_amount' => $sales_payment->payment_amount + $record->payment_amount
        ));

        $this->db->trans_complete();


        return $this->db->trans_status();
    }
}
