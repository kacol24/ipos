<?php

/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
class Module extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_module_info($module_id)
    {
        $query = $this->db->get_where('modules', array('module_id' => $module_id), 1);
        if ($query->num_rows() == 1) {
            return $query->row();
        }
    }

    function get_module_name($module_id)
    {
        $query = $this->db->get_where('modules', array('module_id' => $module_id), 1);

        if ($query->num_rows() == 1) {
            $row = $query->row();
            return $this->lang->line($row->name_lang_key);
        }

        return $this->lang->line('error_unknown');
    }

    function get_module_desc($module_id)
    {
        $query = $this->db->get_where('modules', array('module_id' => $module_id), 1);
        if ($query->num_rows() == 1) {
            $row = $query->row();
            return $this->lang->line($row->desc_lang_key);
        }

        return $this->lang->line('error_unknown');
    }

    function get_all_modules()
    {
        $this->db->from('modules');
        $this->db->where('sort !=', 0);
        $this->db->order_by("sort", "asc");
        return $this->db->get();
    }

    function get_allowed_modules($person_id)
    {
        $this->db->from('modules');
        $this->db->join('permissions', 'permissions.module_id=modules.module_id');
        $this->db->where("permissions.person_id", $person_id);
        $this->db->order_by("sort", "asc");
        return $this->db->get();
    }
}
