<?php

/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
class Sales_payment extends CI_Model
{
    /*
     * ------------------------------------------------------
     *  Determines if the sales payment exist
     * ------------------------------------------------------
     */

    function exists($sale_id, $payment_type)
    {
        $this->db->from('sales_payments');
        $this->db->where(array(
            'sale_id' => $sale_id,
            'payment_type' => $payment_type
        ));
        $query = $this->db->get();
        return $query->num_rows() === 1;
    }

    /**
     * Finds out if the sale_id still has a store account payment due
     *
     * @param   int $sale_id    The sale_id
     * @return  boolean         Has store account or not
     */
    function has_store_account($sale_id)
    {
        $this->db->from('sales_payments');
        $this->db->where(array(
            'sale_id' => $sale_id,
            'payment_type' => 'sales_store_account',
            'payment_amount >' => 0
        ));
        $query = $this->db->get();

        return ($query->num_rows() === 1);
    }
    /*
      Returns all the sales payments
     */

    function get_all($sale_id, $limit = 10000, $offset = 0)
    {
        $this->db->from('sales_payments');
        $this->db->where('sale_id', $sale_id);
        $this->db->limit($limit);
        $this->db->offset($offset);
        return $this->db->get();
    }

    /**
     * Returns the info of a specific sales payment
     *
     * @param   int $sale_id            The sale_id to search for
     * @param   string $payment_type    The payment_type
     * @return  object                  The db connection instance
     */
    function get_info($sale_id, $payment_type)
    {
        $this->db->from('sales_payments');
        $this->db->where(array(
            'sale_id' => $sale_id,
            'payment_type' => $payment_type
        ));
        return $this->db->get();
    }

    function get_store_account($sale_id)
    {
        $this->db->from('sales_payments');
        $this->db->where(array(
            'sale_id' => $sale_id,
            'payment_type' => 'sales_store_account'
        ));
        return $this->db->get()->row();
    }

    function update_payment($sale_id, $payments)
    {
        foreach ($payments as $payment_id => $payment) {
            $sales_payment_data = array(
                'payment_amount' => $payment['payment_amount']
            );
            $where = array(
                'sale_id' => $sale_id,
                'payment_type' => $payment['payment_type']
            );
            if ($payment['payment_type'] == 'sales_giftcard') {
                /* We have a gift card and we have to deduct the used value from the total value of the card. */
                $splitpayment = explode(':', $payment['payment_type']);
                $giftcard_number = $payment['giftcard_number'];
                $cur_giftcard_value = $this->Giftcard->get_giftcard_value($giftcard_number)->value;
                $this->Giftcard->update_giftcard_value($giftcard_number, $cur_giftcard_value - $payment['payment_amount']);
            }
            $this->db->update('sales_payments', $sales_payment_data, $where);
        }
    }

    /**
     * Insert payment to database
     *
     * @param int $sale_id The sale id
     * @param array $payments Payment data
     */
    function insert_payment($sale_id, $payments)
    {
        foreach ($payments as $payment_id => $payment) {
            $sales_payments_data = array(
                'sale_id' => $sale_id,
                'payment_type' => $payment['payment_type'],
                'payment_amount' => $payment['payment_amount'],
                'giftcard_number' => $payment['giftcard_number']
            );
            if ($payment['payment_type'] == 'sales_giftcard') {
                /* We have a gift card and we have to deduct the used value from the total value of the card. */
                $splitpayment = explode(':', $payment['payment_type']);
                $giftcard_number = $payment['giftcard_number'];
                $cur_giftcard_value = $this->Giftcard->get_giftcard_value($giftcard_number)->value;
                $this->Giftcard->update_giftcard_value($giftcard_number, $cur_giftcard_value - $payment['payment_amount']);
            }
            // save payments data (insert to sales_payments table)
            $this->db->insert('sales_payments', $sales_payments_data);
        }
    }
}
