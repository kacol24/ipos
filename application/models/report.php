<?php

/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inzertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
class Report extends CI_Model
{

    function exists(Array $report)
    {
        $this->db->from('reports');
        $this->db->where('name', $report['name']);
        $this->db->where('type', $report['type']);
        $query = $this->db->get();

        return ($query->num_rows() == 1);
    }
    /*
      Returns all the report modules
     */

    function get_all($limit = 10000, $offset = 0)
    {
        $this->db->from('reports');
        $this->db->where('deleted', false);
        $this->db->limit($limit);
        $this->db->offset($offset);
        return $this->db->get();
    }
    /*
      Returns all the pinned report modules
     */

    function get_all_pinned($limit = 10000, $offset = 0)
    {
        $this->db->from('reports');
        $this->db->where('pinned !=', 'NULL');
        $this->db->where('deleted', false);
        $this->db->order_by('pinned', 'ASC');
        $this->db->limit($limit);
        $this->db->offset($offset);
        return $this->db->get();
    }
    /*
      Gets information about a particular report
     */

    function get_info(Array $report)
    {
        $this->db->from('reports');
        $this->db->where('type', $report['type']);
        $this->db->where('name', $report['name']);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            //Get empty base parent object, as $giftcard_id is NOT an giftcard
            $report_obj = new stdClass();

            //Get all the fields from giftcards table
            $fields = $this->db->list_fields('reports');

            foreach ($fields as $field) {
                $report_obj->$field = '';
            }

            return $report_obj;
        }
    }
    /*
      pin a report
     */

    function pin(Array $report)
    {
        if ($this->exists($report)) {
            $this->db->where('name', $report['name']);
            $this->db->where('type', $report['type']);
            return $this->db->update('reports', array('pinned' => date('Y-m-d H:i:s')));
        }
    }
    /*
      unpin a report
     */

    function unpin(Array $report)
    {
        if ($this->exists($report)) {
            $this->db->where('name', $report['name']);
            $this->db->where('type', $report['type']);
            return $this->db->update('reports', array('pinned' => NULL));
        }
    }
}
