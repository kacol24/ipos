<?php

/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
class MY_Language extends Lang
{

    function MY_Language()
    {
        parent::Lang();
    }

    function switch_to($idiom)
    {
        $CI = & get_instance();
        if (is_string($idiom)) {
            $CI->config->set_item('language', $idiom);
            $loaded = $this->is_loaded;
            $this->is_loaded = array();

            foreach ($loaded as $file) {
                $this->load(str_replace('_lang.php', '', $file));
            }
        }
    }
}

/* End of file MY_Language.php */
/* Location: ./application/libraries/MY_Language.php */