<?php
/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */

/**
 * Format a number to a currency prefixed with currency symbol
 *
 * @param int $number The number to format
 * @return string Formatted number with currency symbol prefix
 */
function to_currency($number)
{
    $CI = & get_instance();
    $currency_symbol = $CI->config->item('currency_symbol') ? $CI->config->item('currency_symbol') : '$';
    if ($number >= 0) {
        if ($CI->config->item('currency_side') !== 'currency_side') {
            return $currency_symbol . number_format(floatval($number), 2, ',', '.');
        } else {
            return number_format(floatval($number), 2, ',', '.') . $currency_symbol;
        }
    } else {
        if ($CI->config->item('currency_side') !== 'currency_side') {
            return '- ' . $currency_symbol . number_format(abs(floatval($number)), 2, ',', '.');
        } else {
            return '- ' . number_format(abs(floatval($number)), 2, ',', '.') . $currency_symbol;
        }
    }
}

/**
 * Format a number to a currency with no prefix
 *
 * @param int $number The number to format
 * @return string Formatted number with no prefix
 */
function to_currency_no_money($number)
{
    return number_format(floatval($number), 2, '.', ',');
}
/* End of file currency_helper.php */
/* Location: ./application/helpers/currency_helper.php */