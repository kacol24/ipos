<?php
/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */

/**
 * Returns an encrypted string
 *
 * This method abstracts the encryption function so that any changes are
 * made to this file only
 *
 * @param any $string The string to encryp
 * @param any $salt a salt to use for the encryption
 */
function encrypt($string, $salt = false)
{
    $CI = &get_instance();
    if ($salt) {
        return hash('sha256', $salt . hash('md5', $string . $CI->config->item('encryption_key')));
    } else {
        return hash('sha256', random_string('unique') . hash('md5', $string . $CI->config->item('encryption_key')));
    }
}
/* End of file encryption_helper.php */
/* Location: ./application/helpers/encryption_helper.php */