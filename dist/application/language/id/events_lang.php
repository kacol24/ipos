<?php 

$lang["events_event_name"] = "Nama";
$lang["events_event_desc"] = "Desk";
$lang["events_event_time"] = "Waktu";
$lang["events_name_complete_sales"] = "Penjualan Dibuat";
$lang["events_desc_complete_sales"] = "Penjualan baru dibuat dengan ID";
$lang["events_name_sales_print_receipt"] = "Faktur Penjualan Dicetak";
$lang["events_desc_sales_print_receipt"] = "Mencetak faktur penjualan dengan ID";
$lang["events_name_sales_edited"] = "Penjualan Diubah";
$lang["events_desc_sales_edited"] = "Merubah penjualan dengan ID";
$lang["events_name_sales_suspended"] = "Penjualan Ditunda";
$lang["events_desc_sales_suspended"] = "Menunda sebuah penjualan";
$lang["events_name_unsuspend_sales"] = "Batal Tunda Penjualan";
$lang["events_desc_unsuspend_sales"] = "Membatalkan tunda penjualan";
$lang["events_name_delete_suspended"] = "Hapus Penjualan Ditunda";
$lang["events_desc_delete_suspended"] = "Menghapus penjualan tertunda";
$lang["events_name_record_payment"] = "Pembayaran Piutang";
$lang["events_desc_record_payment"] = "Pembayaran piutang direkam pada";
$lang["events_name_withdraw_payment"] = "Pembayaran Piutang Dibatalkan";
$lang["events_desc_withdraw_payment"] = "Membatalkan pembayaran piutang pada";
