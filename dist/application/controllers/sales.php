<?php
/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
require_once ("secure_area.php");

class Sales extends Secure_area
{

    function __construct()
    {
        parent::__construct('sales');
        $this->load->library('sale_lib');
    }

    /**
     * [GET] Display sales panel
     */
    function index()
    {
        $this->breadcrumbs->push($this->lang->line('module_sales'), '/sales');
        $this->_reload();
    }

    /**
     * [AJAX] Provides auto-suggest functionality to cash register
     *
     * @return JSON Suggestions
     */
    function item_search()
    {
        $query = $this->input->get('query');
        $temp = array();
        $items = $this->Item->get_item_search_suggestions($query);
        $item_kits = $this->Item_kit->get_item_kit_search_suggestions($query);
        if (count($items['suggestions']) > 0) {
            $temp = array_merge($temp, $items['suggestions']);
        }
        if (count($item_kits['suggestions']) > 0) {
            $temp = array_merge($temp, $item_kits['suggestions']);
        }
        $suggestions = array(
            'suggestions' => $temp
        );
        $this->output->set_content_type('application/json')->set_output(json_encode($suggestions));
    }

    /**
     * [AJAX] Provides autosuggest functionality to search customer
     *
     * @return JSON Suggestion
     */
    function customer_search()
    {
        $query = $this->input->get('query');
        $suggestions = $this->Customer->get_customer_search_suggestions($query);
        $this->output->set_content_type('application/json')->set_output(json_encode($suggestions));
    }

    /**
     * [POST] Select customer for transaction
     */
    function select_customer()
    {
        $customer_id = $this->input->post("customer_id");
        $this->sale_lib->set_customer($customer_id);

        /*
         * ------------------------------------------------------
         *  Update price with new one if customer has custom price
         * ------------------------------------------------------
         */
        if ($this->Item_custom_unit_price->customer_has_custom_price($customer_id) && $this->config->item('enable_custom_price')) {
            $cart = $this->sale_lib->get_cart();
            $custom_prices = $this->Item_custom_unit_price->get_items($customer_id)->result();
            foreach ($cart as $line => $item) {
                foreach ($custom_prices as $custom_price) {
                    if ($this->sale_lib->get_item_id($line) == $custom_price->item_id) {
                        $description = $item['description'];
                        $serialnumber = $item['serialnumber'];
                        $quantity = $item['quantity'];
                        $discount = $item['disocunt'];
                        $price = $custom_price->unit_price;
                        $this->sale_lib->edit_item($line, $description, $serialnumber, $quantity, $discount, $price);
                    }
                }
            }
        }
        redirect(site_url('sales'));
    }

    /**
     * [POST] Activate/deactivate customer discount
     *
     * For customers that has customer discount
     */
    function toggle_customer_discount()
    {
        $include = (bool) $this->input->post('include_discount');
        $percent = $this->input->post('discount_value');
        if ($include) {
            $this->sale_lib->set_customer_discount($percent);
        } else {
            $this->sale_lib->empty_customer_discount();
        }
        redirect(site_url('sales'));
    }

    /**
     * [POST] Change transaction mode
     */
    function change_mode()
    {
        $stock_location = $this->input->post("stock_location");
        if ( ! $stock_location || $stock_location == $this->sale_lib->get_sale_location()) {
            $this->sale_lib->clear_all();
            $mode = $this->input->post("mode");
            $this->sale_lib->set_mode($mode);
        } else {
            $this->sale_lib->set_sale_location($stock_location);
        }
        redirect(site_url('sales'));
    }

    /**
     * [POST] Set comment for the transaction
     */
    function set_comment()
    {
        $this->sale_lib->set_comment($this->input->post('comment'));
        redirect(site_url('sales'));
    }

    /**
     * This method might not be needed and therefore being deprecated
     *
     * @todo Remove on v1
     */
    function set_email_receipt()
    {
        // not yet functional
        $this->sale_lib->set_email_receipt($this->input->post('email_receipt'));
    }

    /**
     * [POST] Add payment to transaction
     *
     * This is used as a multiple payment option
     */
    function add_payment()
    {
        $data = array();
        $this->form_validation->set_rules('amount_tendered', 'lang:sales_amount_tendered', 'numeric');

        if ($this->form_validation->run() == FALSE) {
            if ($this->input->post('payment_type') == 'sales_giftcard') {
                set_notif('danger', $this->lang->line('sales_must_enter_numeric_giftcard'));
            } else {
                set_notif('danger', $this->lang->line('sales_must_enter_numeric'));
            }
            redirect(site_url('sales'));
        }

        $payment_type = $this->input->post('payment_type');
        if ($payment_type == 'sales_giftcard') {
            $customer = $this->sale_lib->get_customer();
            $payments = $this->sale_lib->get_payments();
            $payment_type = $this->input->post('payment_type');
            $giftcard_number = $this->input->post('amount_tendered');
            $current_payments_with_giftcard = isset($payments[$payment_type]) ? $payments[$payment_type]['payment_amount'] : 0;
            $giftcard_value = $this->Giftcard->get_customer_giftcard_value($this->input->post('amount_tendered'), $customer);
            if (count($giftcard_value) > 0) {
                $cur_giftcard_value = $giftcard_value->value - $current_payments_with_giftcard;
                if ($cur_giftcard_value <= 0) {
                    set_notif('danger', lang('giftcards_giftcard') . ' ' . lang('giftcards_balance_is') . ' ' . to_currency($giftcard_value->value) . '!');
                    redirect(site_url('sales'));
                }
                if (($giftcard_value->value - $this->sale_lib->get_amount_due()) >= 0) {
                    $new_giftcard_value = ( $new_giftcard_value >= 0 ) ? $new_giftcard_value : 0;
                }
                set_notif('info', lang('giftcards_giftcard') . ' ' . $this->input->post('amount_tendered') . ' ' . lang('giftcards_balance_is') . ' ' . to_currency($new_giftcard_value) . '!');
                $payment_amount = min($this->sale_lib->get_amount_due(), $giftcard_value->value);
            } else {
                set_notif('warning', lang('giftcards_giftcard') . ' ' . $this->input->post('amount_tendered') . ' ' . lang('giftcards_not_connected'));
                redirect(site_url('sales'));
            }
        } else {
            $payment_amount = $this->input->post('amount_tendered');
        }
        if ( ! $this->sale_lib->add_payment($payment_type, $payment_amount, $giftcard_number)) {
            set_notif('danger', lang('sales_error_add_payment'));
        }
        redirect(site_url('sales'));
    }

    /**
     * [GET] Delete selected payment from transaction
     *
     * @param string $payment_id The type of payment to delete from transaction
     */
    function delete_payment($payment_id)
    {
        $this->sale_lib->delete_payment($payment_id);
        redirect(site_url('sales'));
    }

    /**
     * [POST] Add item to sales transaction
     *
     * @param int $code Item number/product code
     */
    function add($code = null)
    {
        $mode = $this->sale_lib->get_mode();
        $item_id_or_number_or_item_kit_or_receipt = $code ? $code : $this->input->post("item_id");
        $receipt_id = $this->input->post('query');
        $quantity = ($mode == "return") ? -1 : 1;
        $item_location = $this->sale_lib->get_sale_location();

        if ($this->sale_lib->is_valid_receipt($receipt_id) && $mode == 'return') {
            $this->sale_lib->return_entire_sale($receipt_id);
        } elseif ($this->sale_lib->is_valid_item_kit($item_id_or_number_or_item_kit_or_receipt)) {
            $this->sale_lib->add_item_kit($item_id_or_number_or_item_kit_or_receipt, $item_location);
        } elseif ( ! $this->sale_lib->add_item($item_id_or_number_or_item_kit_or_receipt, $quantity, $item_location)) {
            set_notif('danger', $this->lang->line('sales_unable_to_add_item'));
        }

        if ($this->sale_lib->out_of_stock($item_id_or_number_or_item_kit_or_receipt, $item_location)) {
            set_notif('warning', $this->lang->line('sales_quantity_less_than_zero'));
        }
        redirect(site_url('sales'));
    }

    /**
     * [POST] Set/add global discount to transaction
     *
     * @param int $key default NULL, if not supplied, it's an add action, if supplied, it's a delete action
     */
    function global_discount($key = NULL)
    {
        $discount = $this->input->post('discount');
        if ($key != null) {
            $this->sale_lib->delete_global_discount($key);
        } else {
            $this->sale_lib->add_global_discount($discount);
        }
        redirect(site_url('sales'));
    }

    /**
     * [GET] Edit the item already in the transaction
     *
     * @param int $line
     */
    function edit_item($line)
    {
        $this->form_validation->set_rules('price', 'lang:items_price', 'numeric');
        $this->form_validation->set_rules('default_price', 'lang:items_price', 'numeric');
        $this->form_validation->set_rules('quantity', 'lang:items_quantity', 'numeric');
        $this->form_validation->set_rules('default_quantity', 'lang:items_quantity', 'numeric');

        $description = $this->input->post("description") != '' ? $this->input->post("description") : $this->input->post("default_description");
        $serialnumber = $this->input->post("serialnumber") != '' ? $this->input->post("serialnumber") : $this->input->post("default_serialnumber");
        $price = $this->input->post("price") != '' ? $this->input->post("price") : $this->input->post("default_price");
        $quantity = $this->input->post("quantity") != '' ? $this->input->post("quantity") : $this->input->post("default_quantity");
        $discount = $this->input->post("discount") != '' ? $this->input->post("discount") : $this->input->post("default_discount");
        $item_location = $this->input->post("location") != '' ? $this->input->post("location") : $this->input->post("default_location");

        if ($this->form_validation->run() != FALSE) {
            $this->sale_lib->edit_item($line, $description, $serialnumber, $quantity, $discount, $price);
        } else {
            set_notif('danger', $this->lang->line('sales_unable_to_add_item'));
        }

        if ($this->sale_lib->out_of_stock($this->sale_lib->get_item_id($line), $item_location)) {
            set_notif('warning', $this->lang->line('sales_quantity_less_than_zero'));
        }
        redirect(site_url('sales'));
    }

    /**
     * [GET] Delete item from transaction
     *
     * @param int $line Line number of the item in transaction
     */
    function delete_item($line)
    {
        $this->sale_lib->delete_item($line);
        redirect(site_url('sales'));
    }

    /**
     * [GET] Remove selected supplier from the transaction
     */
    function remove_customer()
    {
        $this->sale_lib->remove_customer();
        $this->sale_lib->empty_customer_discount();
        if ($this->config->item('enable_custom_price')) {

            $cart = $this->sale_lib->get_cart();
            foreach ($cart as $line => $item) {
                $original = $this->Item->get_info($item['item_id']);
                $description = $item['description'];
                $serialnumber = $item['serialnumber'];
                $quantity = $item['quantity'];
                $discount = $item['discount'];
                $price = $original->unit_price;
                $this->sale_lib->edit_item($line, $description, $serialnumber, $quantity, $discount, $price);
            }
        }
        redirect(site_url('sales'));
    }

    /**
     * [POST] Finish the sales transaction
     */
    function complete()
    {
        $this->breadcrumbs->push($this->lang->line('module_sales'), '/sales');
        $this->breadcrumbs->push($this->lang->line('sales_receipt'), '/receipt');
        $data['cart'] = $this->sale_lib->get_cart();
        $data['subtotal'] = $this->sale_lib->get_subtotal();
        $data['taxes'] = $this->sale_lib->get_taxes();
        $data['total'] = $this->sale_lib->get_total();
        $global_discounts = $this->sale_lib->get_global_discounts();
        $data['customer_discount'] = count($this->sale_lib->get_customer_discount()) > 0 ? $this->sale_lib->get_customer_discount() : NULL;
        $data['receipt_title'] = $this->lang->line('sales_receipt');
        $data['transaction_time'] = date($this->config->item('date_format') . ' ' . $this->config->item('time_format'));
        $stock_locations = $this->Stock_locations->get_undeleted_all()->result_array();
        $data['show_stock_locations'] = count($stock_locations) > 1;
        $customer_id = $this->sale_lib->get_customer();
        $employee_id = $this->Employee->get_logged_in_employee_info()->person_id;
        $comment = trim($this->sale_lib->get_comment()) == '' ? null : $this->sale_lib->get_comment();
        $emp_info = $this->Employee->get_info($employee_id);
        $data['payments'] = $this->sale_lib->get_payments();
        $data['amount_change'] = to_currency($this->sale_lib->get_amount_due() * -1);
        $data['employee'] = $emp_info->first_name . ' ' . $emp_info->last_name;

        if ($customer_id != -1) {
            $cust_info = $this->Customer->get_info($customer_id);
            $data['customer'] = $cust_info;
        }

        //SAVE sale to database
        $sale_id = $this->Sale->save($data['cart'], $customer_id, $employee_id, $comment, $data['payments'], false, $global_discounts, $data['customer_discount']);
        if ($sale_id == -1) {
            set_notif('danger', $this->lang->line('sales_transaction_failed'));
            redirect(site_url('sales'));
        } else {
            $data['sale_id'] = 'POS ' . $sale_id;
            $data['global_discounts'] = $this->sale_lib->fetch_global_discounts($sale_id);
            $data['controller_name'] = strtolower(get_class());
            $data['custom_title'] = $this->lang->line('sales_receipt');
            $data['custom_subtitle'] = $data['sale_id'];
            $data['set_invoice'] = true;
            $data['menu_hide'] = true;
            $data['print'] = false;
            $event_data = array(
                'name' => 'events_name_complete_sales',
                'description' => 'events_desc_complete_sales|' . anchor(site_url('sales/receipt/' . $sale_id), $data['sale_id'])
            );
            $this->Event->log($event_data);
            $this->load->view('template/header', $data);
            $this->load->view("sales/receipt", $data);
            $this->load->view('template/footer');
            $this->sale_lib->clear_all();
            $this->_remove_duplicate_cookies();
        }
    }

    /**
     * [GET] Display the sales receipt
     *
     * @param int $sale_id
     * @param boolean $print default FALSE, if TRUE directly print after showing the view
     */
    function receipt($sale_id, $print = FALSE)
    {
        $this->breadcrumbs->push($this->lang->line('module_sales'), '/sales');
        $this->breadcrumbs->push($this->lang->line('sales_receipt'), '/receipt');
        $sale_info = $this->Sale->get_info($sale_id)->row_array();
        if (count($sale_info) === 0) {
            show_404();
        }
        $this->sale_lib->copy_entire_sale($sale_id);
        $stock_locations = $this->Stock_locations->get_undeleted_all()->result_array();
        $data['show_stock_locations'] = count($stock_locations) > 1;
        $data['cart'] = $this->sale_lib->get_cart();
        $data['payments'] = $this->sale_lib->get_payments();
        $data['subtotal'] = $this->sale_lib->get_subtotal();
        $data['taxes'] = $this->sale_lib->get_taxes();
        $data['global_discounts'] = $this->sale_lib->fetch_global_discounts($sale_id);
        $data['customer_discount'] = $sale_info['customer_discount'];
        $data['total'] = $this->sale_lib->get_total();
        $data['receipt_title'] = $this->lang->line('sales_receipt');
        $data['transaction_time'] = date($this->config->item('date_format') . ' ' . $this->config->item('time_format'), strtotime($sale_info['sale_time']));
        $customer_id = $this->sale_lib->get_customer();
        $emp_info = $this->Employee->get_info($sale_info['employee_id']);
        $data['amount_change'] = to_currency($this->sale_lib->get_amount_due() * -1);
        $data['employee'] = $emp_info->first_name . ' ' . $emp_info->last_name;
        $data['current_employee'] = $this->Employee->get_logged_in_employee_info();
        if ($customer_id != -1) {
            $cust_info = $this->Customer->get_info($customer_id);
            $data['customer'] = $cust_info;
        }
        $data['sale_id'] = 'POS ' . $sale_id;
        $data['custom_title'] = $this->lang->line('sales_receipt');
        $data['custom_subtitle'] = $data['sale_id'];
        $data['set_invoice'] = true;
        $data['menu_hide'] = true;
        $data['print'] = $print;
        if ($print) {
            $event_data = array(
                'name' => 'events_name_sales_print_receipt',
                'description' => 'events_desc_sales_print_receipt|' . anchor(site_url('sales/receipt/' . $sale_id), 'POS ' . $sale_id)
            );
            $this->Event->log($event_data);
        }
        $this->load->model('Sales_payment');
        $data['has_store_account'] = $this->Sales_payment->has_store_account($sale_id);
        $this->load->model('Payment_record');
        $data['payment_records'] = $this->Payment_record->get_all($sale_id)->result();
        $data['controller_name'] = strtolower(get_class());
        $this->load->view('template/header', $data);
        $this->load->view("sales/receipt", $data);
        $this->load->view('template/footer');
        $this->sale_lib->clear_all();
        $this->_remove_duplicate_cookies();
    }

    /**
     * [GET] Display the edit sale form
     *
     * @param int $sale_id The sale_id to edit
     */
    function edit($sale_id)
    {
        $this->breadcrumbs->push($this->lang->line('module_sales'), '/sales');
        $this->breadcrumbs->push($this->lang->line('sales_edit'), '/edit');
        $data = array();
        $data['customers'] = array('' => 'No Customer');
        foreach ($this->Customer->get_all()->result() as $customer) {
            $data['customers'][$customer->person_id] = $customer->first_name . ' ' . $customer->last_name;
        }
        $data['employees'] = array();
        foreach ($this->Employee->get_all()->result() as $employee) {
            $data['employees'][$employee->person_id] = $employee->first_name . ' ' . $employee->last_name;
        }
        $sale_info = $this->Sale->get_info($sale_id)->row_array();
        $person_name = $sale_info['first_name'] . " " . $sale_info['last_name'];
        $data['selected_customer'] = ! empty($sale_info['customer_id']) ? array('customer_id' => $sale_info['customer_id'], 'name' => $person_name) : array('customer_id' => '', 'name' => '');
        $data['sale_info'] = $sale_info;
        $this->load->model('Sales_payment');
        $this->load->model('Payment_record');
        $data['has_store_account'] = $this->Sales_payment->has_store_account($sale_id);
        $data['payments'] = $this->Sales_payment->get_all($sale_id)->result();
        $data['payment_records'] = $this->Payment_record->get_all($sale_id)->result();
        $data['controller_name'] = strtolower(get_class());
        $data['custom_subtitle'] = $this->lang->line('sales_receipt_number') . $sale_info['sale_id'];
        $this->load->view('template/header', $data);
        $this->load->view('sales/form', $data);
        $this->load->view('template/footer');
    }

    /**
     * [GET] Delete an entire sale from database
     *
     * @param int $sale_id The sale_id to delete
     * @param boolean $update_inventory default TRUE
     */
    function delete($sale_id = -1, $update_inventory = TRUE)
    {
        $employee_id = $this->Employee->get_logged_in_employee_info()->person_id;
        $sale_ids = $sale_id == -1 ? $this->input->post('ids') : array($sale_id);

        if ($this->Sale->delete_list($sale_ids, $employee_id, $update_inventory)) {
            set_notif('success', $this->lang->line('sales_delete_successful') . ' ' . count($sale_ids) . ' ' . $this->lang->line('sales_one_or_multiple'));
        } else {
            set_notif('danger', $this->lang->line('sales_delete_unsuccessful'));
        }
    }

    /**
     * [PUT/PATCH] Save changes made to database
     *
     * @param int $sale_id
     */
    function save($sale_id)
    {
        $sale_data = array(
            'sale_time' => date('Y-m-d h:i:s', strtotime($this->input->post('date'))), // TODO fix date time issue, for now the input field is disabled
            'customer_id' => $this->input->post('customer_id') ? $this->input->post('customer_id') : null,
            'employee_id' => $this->input->post('employee_id'),
            'comment' => trim($this->input->post('comment')) == '' ? null : $this->input->post('comment')
        );

        if ($this->Sale->update($sale_data, $sale_id)) {
            set_notif('success', lang('sales_successfully_updated'));
            $event_data = array(
                'name' => 'events_name_sales_edited',
                'description' => 'events_desc_sales_edited|' . anchor(site_url('sales/edit/' . $sale_id), 'POS ' . $sale_id)
            );
            $this->Event->log($event_data);
        } else {
            set_notif('warning', lang('sales_unsuccessfully_updated'));
        }
        redirect(site_url('sales/edit/' . $sale_id));
    }

    /**
     * Determine if payments made have covered the total sales
     *
     * @access private
     *
     * @return boolean
     */
    function _payments_cover_total()
    {
        $total_payments = 0;

        foreach ($this->sale_lib->get_payments() as $payment) {
            $total_payments += $payment['payment_amount'];
        }

        /* Changed the conditional to account for floating point rounding */
        if (($this->sale_lib->get_mode() == 'sale') && (($this->sale_lib->get_total() - $total_payments ) > 1e-6 )) {
            return false;
        }

        return true;
    }

    /**
     * Handles data for sales panel
     *
     * @access private
     *
     * @param type $data
     */
    function _reload($data = array())
    {
        $person_info = $this->Employee->get_logged_in_employee_info();
        $data['cart'] = $this->sale_lib->get_cart();
        $data['modes'] = array('sale' => $this->lang->line('sales_sale'), 'return' => $this->lang->line('sales_return'));
        $data['mode'] = $this->sale_lib->get_mode();

        $data['stock_locations'] = array();
        $stock_locations = $this->Stock_locations->get_undeleted_all()->result_array();
        $show_stock_locations = count($stock_locations) > 1;
        if ($show_stock_locations) {
            foreach ($stock_locations as $location_data) {
                $data['stock_locations'][$location_data['location_id']] = $location_data['location_name'];
            }
            $data['stock_location'] = $this->sale_lib->get_sale_location();
        }
        $data['show_stock_locations'] = $show_stock_locations;

        $data['subtotal'] = $this->sale_lib->get_subtotal();
        $data['taxes'] = $this->sale_lib->get_taxes();
        $data['global_discounts'] = $this->sale_lib->get_global_discounts();
        $data['total'] = $this->sale_lib->get_total();
        $data['items_module_allowed'] = $this->Employee->has_permission('items', $person_info->person_id);
        $data['comment'] = $this->sale_lib->get_comment();
        $data['email_receipt'] = $this->sale_lib->get_email_receipt();
        $data['payments_total'] = $this->sale_lib->get_payments_total();
        $data['suspended_sales'] = $this->Sale_suspended->get_all()->result_array();
        $data['amount_due'] = $this->sale_lib->get_amount_due();
        $data['payments'] = $this->sale_lib->get_payments();
        foreach ($this->sale_lib->fetch_payments() as $type) {
            $payments[$type->payment_type] = lang($type->payment_type);
        }
        $data['payment_options'] = $payments;

        $customer_id = $this->sale_lib->get_customer();
        if ($customer_id != -1) {
            $info = $this->Customer->get_info($customer_id);
            $data['customer'] = $info->first_name . ' ' . $info->last_name;
            $data['customer_email'] = $info->email;
            $data['customer_discount'] = $info->customer_discount;
            $this->load->model('reports/Specific_customer');
            $model = $this->Specific_customer;
            $report_data = $model->getData(array('start_date' => date('Y-m-d', 0), 'end_date' => date('Y-m-d'), 'customer_id' => $info->person_id, 'sale_type' => 'all', 'limit' => 5));
            $data['recent_sales'] = $report_data['summary'];
            if ($data['mode'] == 'sale') {
                $data['payment_options']['sales_store_account'] = lang('sales_store_account');
            }
        }
        $data['has_customer_discount'] = $this->sale_lib->get_customer_discount();
        $data['payments_cover_total'] = $this->_payments_cover_total();
        $data['controller_name'] = strtolower(get_class());
        $data['menu_hide'] = true;
        $this->load->view("template/header", $data);
        $this->load->view("sales/register", $data);
        $this->load->view("template/footer");
        $this->_remove_duplicate_cookies();
    }

    /**
     * [GET] Cancel the sales transaction
     */
    function cancel_sale()
    {
        $this->sale_lib->clear_all();
        $this->_reload();
        redirect(site_url('/sales'));
    }

    /**
     * [POST] Suspend the sales
     *
     * If cancel button pressed, then it must be a cancel
     */
    function suspend()
    {
        if ($this->input->post('cancel')) {
            $this->cancel_sale();
        }
        $data['cart'] = $this->sale_lib->get_cart();
        $data['subtotal'] = $this->sale_lib->get_subtotal();
        $data['taxes'] = $this->sale_lib->get_taxes();
        $data['total'] = $this->sale_lib->get_total();
        $data['receipt_title'] = $this->lang->line('sales_receipt');
        $data['transaction_time'] = date($this->config->item('date_format') . ' ' . $this->config->item('time_format'));
        $customer_id = $this->sale_lib->get_customer();
        $employee_id = $this->Employee->get_logged_in_employee_info()->person_id;
        $comment = $this->sale_lib->get_comment() != 0 ? $this->sale_lib->get_comment() : null;
        $emp_info = $this->Employee->get_info($employee_id);
        $payment_type = $this->input->post('payment_type');
        $data['payment_type'] = $payment_type;
        //Alain Multiple payments
        $data['payments'] = $this->sale_lib->get_payments();
        $data['amount_change'] = to_currency($this->sale_lib->get_amount_due() * -1);
        $data['employee'] = $emp_info->first_name . ' ' . $emp_info->last_name;

        if ($customer_id != -1) {
            $cust_info = $this->Customer->get_info($customer_id);
            $data['customer'] = $cust_info->first_name . ' ' . $cust_info->last_name;
        }

        $total_payments = 0;

        foreach ($data['payments'] as $payment) {
            $total_payments += $payment['payment_amount'];
        }

        //SAVE sale to database
        $data['sale_id'] = 'POS ' . $this->Sale_suspended->save($data['cart'], $customer_id, $employee_id, $comment, $data['payments']);
        if ($data['sale_id'] == 'POS -1') {
            set_notif('danger', $this->lang->line('sales_fail_suspend_sale'));
            redirect(site_url('sales'));
        }
        $this->sale_lib->clear_all();
        $this->_reload();
        set_notif('success', $this->lang->line('sales_successfully_suspended_sale'));
        $event_data = array(
            'name' => 'events_name_sales_suspended',
            'description' => 'events_desc_sales_suspended'
        );
        $this->Event->log($event_data);
        redirect(site_url('sales'));
    }

    /**
     * [GET] Display the suspended sales modal
     *
     * This method is supposed to be access from a AJAX call for modal
     */
    function suspended()
    {
        $data = array();
        $data['suspended_sales'] = $this->Sale_suspended->get_all()->result_array();
        $this->load->view('sales/suspended', $data);
    }

    /**
     * [POST] Unsuspend the selected sales
     */
    function unsuspend()
    {
        $sale_id = $this->input->post('suspended_sale_id');
        if ($this->input->post('delete')) {
            $this->delete_suspended();
        }
        $this->sale_lib->clear_all();
        $this->sale_lib->copy_entire_suspended_sale($sale_id);
        $this->Sale_suspended->delete($sale_id);
        $event_data = array(
            'name' => 'events_name_unsuspend_sales',
            'description' => 'events_desc_unsuspend_sales'
        );
        $this->Event->log($event_data);
        redirect(site_url('sales'));
    }

    /**
     * [POST] Delete the suspended sales
     */
    function delete_suspended()
    {
        $sale_id = $this->input->post('suspended_sale_id');
        $this->sale_lib->clear_all();
        $this->Sale_suspended->delete($sale_id);
        $event_data = array(
            'name' => 'events_name_delete_suspended',
            'description' => 'events_desc_delete_suspended'
        );
        $this->Event->log($event_data);
        set_notif('success', lang('sales_suspended_success_delete'));
        redirect(site_url('sales'));
    }

    /**
     * [GET] Display the finish payment form
     *
     * This method is used to display the form to finish store account payment
     *
     * @param int $sale_id
     */
    function finish_payment($sale_id)
    {
        $this->breadcrumbs->push($this->lang->line('module_sales'), '/sales');
        $this->breadcrumbs->push('Finish Payment', '/edit');
        foreach ($this->sale_lib->fetch_payments() as $type) {
            if ($type->payment_type !== 'sales_giftcard') {
                $payments[$type->payment_type] = lang($type->payment_type);
            }
        }
        $sale_info = $this->Sale->get_info($sale_id)->row_array();
        $person_name = $sale_info['first_name'] . " " . $sale_info['last_name'];
        $data['customer'] = array('customer_id' => $sale_info['customer_id'], 'name' => $person_name);
        $employee_info = $this->Employee->get_info($sale_info['employee_id']);
        $data['employee'] = $employee_info->first_name . ' ' . $employee_info->last_name;
        $data['sale_info'] = $sale_info;
        $data['payment_options'] = $payments;
        $this->load->model('Sales_payment');
        $data['amount_due'] = $this->Sales_payment->get_store_account($sale_id)->payment_amount;
        $data['controller_name'] = strtolower(get_class());
        $this->load->view('template/header', $data);
        $this->load->view('sales/finish_payment', $data);
        $this->load->view('template/footer');
    }

    /**
     * [POST] Save payment from 'finish payment' to database
     *
     * @todo Fix language
     * @param int $sale_id
     */
    function record_payment($sale_id)
    {
        $payment_type = $this->input->post('payment_type');
        $payment_amount = trim($this->input->post('payment_amount'));

        $this->form_validation->set_rules('payment_amount', lang('sales_payment_amount'), 'trim|required|numeric|is_natural_no_zero');
        if ($this->form_validation->run() == FALSE) {
            set_notif('warning', 'Payment amount must be greater than 0');
            redirect($this->agent->referrer());
        }
        $this->load->model('Payment_record');
        if ($this->Payment_record->add_payment_record($sale_id, $payment_type, $payment_amount)) {
            $event_data = array(
                'name' => 'events_name_record_payment',
                'description' => 'events_desc_record_payment|' . anchor(site_url('sales/edit/' . $sale_id), 'POS ' . $sale_id)
            );
            $this->Event->log($event_data);
            redirect('sales/edit/' . $sale_id);
        } else {
            set_notif('danger', 'Cannot add payment due to some errors');
            redirect($this->agent->referrer());
        }
    }

    /**
     * [GET] Delete the recorded 'finish payment' from the sales
     *
     * @todo Fix language
     * @param int $record_id
     */
    function withdraw_payment($record_id)
    {
        $this->load->model('Payment_record');
        if ($this->Payment_record->withdraw_payment_record($record_id)) {
            $sale_id = $this->Payment_record->get_info($record_id)->row()->sale_id;
            set_notif('success', 'Payment has been withdrawn successfully!');
            $event_data = array(
                'name' => 'events_name_withdraw_payment',
                'description' => 'events_desc_withdraw_payment|' . anchor(site_url('sales/edit/' . $sale_id), 'POS ' . $sale_id)
            );
            $this->Event->log($event_data);
        } else {
            set_notif('danger', 'Something went wrong when trying to withdraw the payment');
        }
        redirect($this->agent->referrer());
    }
}

/* End of file sales.php */
/* Location: ./application/controllers/sales.php */