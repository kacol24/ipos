<?php

/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
/*
  This interface is implemented by any controller that keeps track of data items, such
  as the customers, employees, and items controllers.
 */

interface iData_controller
{

    public function index();

    public function search();

    public function suggest();

    public function get_row();

    public function view($data_item_id = -1);

    public function save($data_item_id = -1);

    public function delete($data_item_id);

//    public function get_form_width();
}
