<?php
/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */

/**
 * Controllers that are considered secure should extend Secure_area
 * Optionally a $module_id can be set to also check if a user
 * can access a particular module in the system
 */
class Secure_area extends CI_Controller
{

    function __construct($module_id = null)
    {
        parent::__construct();
        $this->load->model('Employee');
        if ( ! $this->Employee->is_logged_in()) {
            redirect('login');
        }

        if ( ! $this->Employee->has_permission($module_id, $this->Employee->get_logged_in_employee_info()->person_id)) {
            redirect('no_access/' . $module_id);
        }

        //load up global data
        $logged_in_employee_info = $this->Employee->get_logged_in_employee_info();
        $data['allowed_modules'] = $this->Module->get_allowed_modules($logged_in_employee_info->person_id);
        $data['user_info'] = $logged_in_employee_info;
        $data['current_module'] = $this->Module->get_module_info($module_id);
        $this->load->vars($data);
    }

    /**
     * Remove duplicate cookies
     *
     * @access protected
     */
    function _remove_duplicate_cookies()
    {
        //php < 5.3 doesn't have header remove so this function will fatal error otherwise
        if (function_exists('header_remove')) {
            $CI = &get_instance();

            // clean up all the cookies that are set...
            $headers = headers_list();
            $cookies_to_output = array();
            $header_session_cookie = '';
            $session_cookie_name = $CI->config->item('sess_cookie_name');

            foreach ($headers as $header) {
                list ($header_type, $data) = explode(':', $header, 2);
                $header_type = trim($header_type);
                $data = trim($data);

                if (strtolower($header_type) == 'set-cookie') {
                    header_remove('Set-Cookie');

                    $cookie_value = current(explode(';', $data));
                    list ($key, $val) = explode('=', $cookie_value);
                    $key = trim($key);

                    if ($key == $session_cookie_name) {
                        // OVERWRITE IT (yes! do it!)
                        $header_session_cookie = $data;
                        continue;
                    } else {
                        // Not a session related cookie, add it as normal. Might be a CSRF or some other cookie we are setting
                        $cookies_to_output[] = array('header_type' => $header_type, 'data' => $data);
                    }
                }
            }

            if ( ! empty($header_session_cookie)) {
                $cookies_to_output[] = array('header_type' => 'Set-Cookie', 'data' => $header_session_cookie);
            }

            foreach ($cookies_to_output as $cookie) {
                header("{$cookie['header_type']}: {$cookie['data']}", false);
            }
        }
    }

    /**
     * Set output to be jsonified
     *
     * @param mixed $data The data to be json_encoded
     * @return JSON Output as JSON string
     */
    function jsonify($data)
    {
        return $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }
}

/* End of file secure_area.php */
/* Location: ./application/controllers/secure_area.php */