<?php
/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */

/**
 * This class is going to be deprecated (un-scheduled)
 *
 * This class has issues with ci-session using database
 * @todo Deprecate this class (un-scheduled)
 */
class Install extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        if ($this->db->count_all_results($this->db->dbprefix('app_config'))) {
            die('Application has been setup properly! Please contact your application administrator to remove this setup script.');
        }
        $this->load->library('setup_lib');
    }

    /**
     * [GET] Display the install form
     */
    function index()
    {
        $this->load->view("install/form");
    }

    /**
     * [POST] Save the setup information to database
     */
    function setup()
    {
        /*
         * ------------------------------------------------------
         *  person info
         * ------------------------------------------------------
         */
        $this->form_validation->set_rules('first_name', 'lang:common_first_name', 'trim|required|alpha');
        $this->form_validation->set_rules('last_name', 'lang:common_last_name', 'trim|alpha');
        $this->form_validation->set_rules('email', 'lang:common_email', 'trim|valid_email');
        $this->form_validation->set_rules('phone_number', 'lang:common_phone_number', 'trim|numeric');
        $this->form_validation->set_rules('address_1', 'lang:common_address_1', 'trim');
        $this->form_validation->set_rules('address_2', 'lang:common_address_2', 'trim');
        $this->form_validation->set_rules('city', 'lang:common_city', 'trim');
        $this->form_validation->set_rules('state', 'lang:common_state', 'trim');
        $this->form_validation->set_rules('country', 'lang:common_country', 'trim');
        $this->form_validation->set_rules('comments', 'lang:common_comments', 'trim');

        /*
         * ------------------------------------------------------
         *  account info
         * ------------------------------------------------------
         */
        $this->form_validation->set_rules('username', 'lang:login_username', 'trim|required|min_length[4]|max_length[20]|alpha_dash');
        $this->form_validation->set_rules('password', 'lang:login_password', 'trim|required|min_length[4]|max_length[20]|alpha_numeric');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|matches[password]');

        /*
         * ------------------------------------------------------
         *  store info
         * ------------------------------------------------------
         */
        $this->form_validation->set_rules('company', 'lang:config_company', 'trim|required|max_length[20]');
        $this->form_validation->set_rules('company_address', 'lang:config_address', 'trim|required');
        $this->form_validation->set_rules('company_city', 'lang:config_city', 'trim');
        $this->form_validation->set_rules('company_state', 'lang:config_state', 'trim');
        $this->form_validation->set_rules('company_zip', 'lang:config_zip', 'trim');
        $this->form_validation->set_rules('company_country', 'lang:config_country', 'trim|required');
        $this->form_validation->set_rules('company_phone', 'lang:config_phone', 'trim|required|numeric');
        $this->form_validation->set_rules('company_fax', 'lang:config_fax', 'trim|numeric');
        $this->form_validation->set_rules('company_email', 'lang:config_email', 'trim|valid_email');
        $this->form_validation->set_rules('language', 'lang:config_language', 'trim|required');
        $this->form_validation->set_rules('timezone', 'lang:config_timezone', 'trim|required');
        $this->form_validation->set_rules('date_format', 'lang:config_date_format', 'trim|required');
        $this->form_validation->set_rules('time_format', 'lang:config_time_format', 'trim|required');
        $this->form_validation->set_rules('currency_symbol', 'lang:config_currency_symbol', 'trim');
        $this->form_validation->set_rules('currency_side', 'lang:config_currency_side', 'trim');
        $this->form_validation->set_rules('stock_location', 'lang:config_stock_location', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $this->db->trans_start();
//            $this->setup_lib->create_tables(); // as of now, it is not yet supported, issue with saving ci-session to database config
            $this->setup_lib->populate_defaults();
            $this->db->trans_complete();

            if ($this->db->trans_status()) {
                $this->db->trans_start();
                $this->setup_lib->populate_employees_table();
                $this->setup_lib->populate_permissions_table();
                $this->db->trans_complete();
                if ($this->db->trans_status()) {
                    $data['company_name'] = $this->config->item('company');
                    $data['employee'] = $this->Employee->get_info(1);
                    redirect(site_url('login'));
                } else {
                    echo 'Something went wrong saving your credentials!';
                }
            } else {
                echo 'Something went wrong trying to install defaults!';
            }
        } else {
            $this->load->view("install/form");
        }
    }
}

/* End of file install.php */
/* Location: ./application/controllers/install.php */