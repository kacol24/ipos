<?php
/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
require_once ("secure_area.php");

class Reports extends Secure_area
{

    function __construct()
    {
        parent::__construct('reports');
        $this->load->helper('report');
    }

    /**
     * [GET] Display the report listing
     */
    function index()
    {
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->load->model('Report');
        $data = $this->_get_common_report_data();
        $data['reports'] = $this->Report->get_all()->result();
        $data['reports_pinned'] = $this->Report->get_all_pinned()->result();
        $data['controller_name'] = strtolower(get_class());
        $this->load->view('template/header', $data);
        $this->load->view("reports/listing", $data);
        $this->load->view('template/footer');
    }

    /**
     * [GET] Pin the report
     *
     * @param string $report_name
     * @param string $report_type
     */
    function pin($report_name, $report_type)
    {
        $data['name'] = $report_name;
        $data['type'] = $report_type;
        $this->load->model('Report');
        $this->Report->pin($data);
        redirect(site_url('/reports'));
    }

    /**
     * [GET] Unpin the report
     *
     * @param string $report_name
     * @param string $report_type
     */
    function unpin($report_name, $report_type)
    {
        $data['name'] = $report_name;
        $data['type'] = $report_type;
        $this->load->model('Report');
        $this->Report->unpin($data);
        redirect(site_url('/reports'));
    }

    /**
     * Gets the date ranges for the report
     *
     * @return array
     */
    function _get_common_report_data()
    {
        $data = array();
        $data['report_date_range_simple'] = get_simple_date_ranges();
        $data['months'] = get_months();
        $data['days'] = get_days();
        $data['years'] = get_years();
        $data['selected_month'] = date('n');
        $data['selected_day'] = date('d');
        $data['selected_year'] = date('Y');

        return $data;
    }

    /**
     * [GET] Display the date input
     *
     * Input for reports that require only a date range and an export to excel.
     * See routes.php to see that all summary reports route here
     */
    function date_input_excel_export()
    {
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->breadcrumbs->push($this->lang->line('reports_summary_reports'), '/summary');
        $data = $this->_get_common_report_data();
        $data['controller_name'] = strtolower(get_class());
        $data['custom_title'] = $this->lang->line('reports_summary_reports');
        $data['custom_subtitle'] = '';
        $this->load->view('template/header', $data);
        $this->load->view("reports/date_input_excel_export", $data);
        $this->load->view('template/footer');
    }

    /**
     * [GET] Get data of detailed sales
     *
     * @param int $sale_id
     * @param int $sale_type default 1
     */
    function get_detailed_sales_row($sale_id, $sale_type = 1)
    {
        $this->load->model('reports/Detailed_sales');
        $model = $this->Detailed_sales;

        $report_data = $model->getDataBySaleId($sale_id, $sale_type);

        $summary_data = array(
            anchor('sales/edit/' . $report_data['sale_id'] . '/width:' . FORM_WIDTH, 'POS ' . $report_data['sale_id'], array('class' => 'thickbox')),
            $report_data['sale_date'],
            $report_data['items_purchased'],
            $report_data['employee_name'],
            $report_data['customer_name'],
            to_currency($report_data['subtotal']),
            to_currency($report_data['total']),
            to_currency($report_data['tax']),
            to_currency($report_data['profit']),
            $report_data['payment_type'],
            $report_data['comment']
        );
        echo get_detailed_sales_data_row($summary_data, $this);
    }

    /**
     * [GET] Get the data for summary sales
     *
     * @param string $start_date
     * @param string $end_date default NULL
     * @param int $sale_type default 0
     */
    function get_summary_data($start_date, $end_date = NULL, $sale_type = 0)
    {
        $end_date = $end_date ? : $start_date;
        $this->load->model('reports/Summary_sales');
        $model = $this->Summary_sales;
        $summary = $model->getSummaryData(array(
            'start_date' => $start_date,
            'end_date' => $end_date,
            'sale_type' => $sale_type));
        echo get_sales_summary_totals($summary, $this);
    }

    /**
     * [GET] Display the summary sales report
     *
     * @param string $start_date
     * @param string  $end_date
     * @param string $sale_type
     * @param int $export_excel default 0
     */
    function summary_sales($start_date, $end_date, $sale_type, $export_excel = 0)
    {
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->breadcrumbs->push($this->lang->line('reports_summary_reports'), '/reports/summary_sales');
        $this->breadcrumbs->push($this->lang->line('reports_sales_summary_report'), '/sales');
        $this->load->model('reports/Summary_sales');
        $model = $this->Summary_sales;
        $report_data = $model->getData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type));
        $tabular_data = array();
        if ($export_excel) {
            foreach ($report_data as $row) {
                $tabular_data[] = array(
                    $row['sale_date'],
                    $row['subtotal'],
                    $row['total'],
                    $row['tax'],
                    $row['profit']
                );
            }
        } else {
            foreach ($report_data as $row) {
                $tabular_data[] = array(
                    array(
                        'class' => '',
                        'value' => $row['sale_date']
                    ),
                    array(
                        'class' => ' class="text-right" data-order="' . $row['subtotal'] . '"',
                        'value' => to_currency($row['subtotal'])
                    ),
                    array(
                        'class' => ' class="text-right" data-order="' . $row['total'] . '"',
                        'value' => to_currency($row['total'])
                    ),
                    array(
                        'class' => ' class="text-right" data-order="' . $row['tax'] . '"',
                        'value' => to_currency($row['tax'])
                    ),
                    array(
                        'class' => ' class="text-right" data-order="' . $row['profit'] . '"',
                        'value' => to_currency($row['profit'])
                    )
                );
            }
        }
        $data = array(
            "custom_title" => $this->lang->line('reports_sales_summary_report'),
            "custom_subtitle" => date($this->config->item('date_format'), strtotime($start_date)) . ' - ' . date($this->config->item('date_format'), strtotime($end_date)),
            "headers" => $model->getDataColumns(),
            "data" => $tabular_data,
            "summary_data" => $model->getSummaryData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type)),
            "export_excel" => array(
                'report' => __FUNCTION__,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'sale_type' => $sale_type
            )
        );
        $data['controller_name'] = strtolower(get_class());
        if ($export_excel) {
            array_push($tabular_data, array(
                'SUM',
                $data['summary_data']['subtotal'],
                $data['summary_data']['total'],
                $data['summary_data']['tax'],
                $data['summary_data']['profit']
            ));
            $this->load->library('export');
            $this->export->to_excel($data['headers'], $tabular_data, str_replace(' ', '', date('Y-m-d') . '_' . $data['custom_title'] . '(' . date('Y.m.d', strtotime($start_date)) . '-' . date('Y.m.d', strtotime($end_date)) . ')'));
        } else {
            $this->load->view('template/header', $data);
            $this->load->view("reports/tabular", $data);
            $this->load->view('template/footer');
        }
    }

    /**
     * [GET] Display the summary categories report
     *
     * @param string $start_date
     * @param string $end_date
     * @param string $sale_type
     * @param int $export_excel default 0
     */
    function summary_categories($start_date, $end_date, $sale_type, $export_excel = 0)
    {
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->breadcrumbs->push($this->lang->line('reports_summary_reports'), '/reports/summary_categories');
        $this->breadcrumbs->push($this->lang->line('reports_categories_summary_report'), '/sales');
        $this->load->model('reports/Summary_categories');
        $model = $this->Summary_categories;
        $report_data = $model->getData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type));
        $tabular_data = array();
        if ($export_excel) {
            foreach ($report_data as $row) {
                $tabular_data[] = array(
                    $row['category'],
                    $row['subtotal'],
                    $row['total'],
                    $row['tax'],
                    $row['profit']
                );
            }
        } else {
            foreach ($report_data as $row) {
                $tabular_data[] = array(
                    array(
                        'class' => '',
                        'value' => $row['category']
                    ),
                    array(
                        'class' => ' class="text-right" data-order="' . $row['subtotal'] . '"',
                        'value' => to_currency($row['subtotal'])
                    ),
                    array(
                        'class' => ' class="text-right" data-order="' . $row['total'] . '"',
                        'value' => to_currency($row['total'])
                    ),
                    array(
                        'class' => ' class="text-right" data-order="' . $row['tax'] . '"',
                        'value' => to_currency($row['tax'])
                    ),
                    array(
                        'class' => ' class="text-right" data-order="' . $row['profit'] . '"',
                        'value' => to_currency($row['profit'])
                    )
                );
            }
        }
        $data = array(
            "custom_title" => $this->lang->line('reports_categories_summary_report'),
            "custom_subtitle" => date($this->config->item('date_format'), strtotime($start_date)) . ' - ' . date($this->config->item('date_format'), strtotime($end_date)),
            "headers" => $model->getDataColumns(),
            "data" => $tabular_data,
            "summary_data" => $model->getSummaryData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type)),
            "export_excel" => array(
                'report' => __FUNCTION__,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'sale_type' => $sale_type
            )
        );
        $data['controller_name'] = strtolower(get_class());
        if ($export_excel) {
            array_push($tabular_data, array(
                'SUM',
                $data['summary_data']['subtotal'],
                $data['summary_data']['total'],
                $data['summary_data']['tax'],
                $data['summary_data']['profit']
            ));
            $this->load->library('export');
            $this->export->to_excel($data['headers'], $tabular_data, str_replace(' ', '', date('Y-m-d') . '_' . $data['custom_title'] . '(' . date('Y.m.d', strtotime($start_date)) . '-' . date('Y.m.d', strtotime($end_date)) . ')'));
        } else {
            $this->load->view('template/header', $data);
            $this->load->view("reports/tabular", $data);
            $this->load->view('template/footer');
        }
    }

    /**
     * [GET] Display the summary customers report
     *
     * @param string $start_date
     * @param string $end_date
     * @param string $sale_type
     * @param int $export_excel default 0
     */
    function summary_customers($start_date, $end_date, $sale_type, $export_excel = 0)
    {
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->breadcrumbs->push($this->lang->line('reports_summary_reports'), '/reports/summary_customers');
        $this->breadcrumbs->push($this->lang->line('reports_customers_summary_report'), '/sales');
        $this->load->model('reports/Summary_customers');
        $model = $this->Summary_customers;
        $report_data = $model->getData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type));
        $tabular_data = array();
        if ($export_excel) {
            foreach ($report_data as $row) {
                $tabular_data[] = array(
                    $row['customer'],
                    $row['subtotal'],
                    $row['total'],
                    $row['tax'],
                    $row['profit']
                );
            }
        } else {
            foreach ($report_data as $row) {
                $tabular_data[] = array(
                    array(
                        'class' => '',
                        'value' => $row['customer']
                    ),
                    array(
                        'class' => ' class="text-right" data-order="' . $row['subtotal'] . '"',
                        'value' => to_currency($row['subtotal'])
                    ),
                    array(
                        'class' => ' class="text-right" data-order="' . $row['total'] . '"',
                        'value' => to_currency($row['total'])
                    ),
                    array(
                        'class' => ' class="text-right" data-order="' . $row['tax'] . '"',
                        'value' => to_currency($row['tax'])
                    ),
                    array(
                        'class' => ' class="text-right" data-order="' . $row['profit'] . '"',
                        'value' => to_currency($row['profit'])
                    )
                );
            }
        }

        $data = array(
            "custom_title" => $this->lang->line('reports_customers_summary_report'),
            "custom_subtitle" => date($this->config->item('date_format'), strtotime($start_date)) . ' - ' . date($this->config->item('date_format'), strtotime($end_date)),
            "headers" => $model->getDataColumns(),
            "data" => $tabular_data,
            "summary_data" => $model->getSummaryData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type)),
            "export_excel" => array(
                'report' => __FUNCTION__,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'sale_type' => $sale_type
            )
        );
        $data['controller_name'] = strtolower(get_class());
        if ($export_excel) {
            array_push($tabular_data, array(
                'SUM',
                $data['summary_data']['subtotal'],
                $data['summary_data']['total'],
                $data['summary_data']['tax'],
                $data['summary_data']['profit']
            ));
            $this->load->library('export');
            $this->export->to_excel($data['headers'], $tabular_data, str_replace(' ', '', date('Y-m-d') . '_' . $data['custom_title'] . '(' . date('Y.m.d', strtotime($start_date)) . '-' . date('Y.m.d', strtotime($end_date)) . ')'));
        } else {
            $this->load->view('template/header', $data);
            $this->load->view("reports/tabular", $data);
            $this->load->view('template/footer');
        }
    }

    /**
     * [GET] Display the summary suppliers report
     *
     * @param string $start_date
     * @param string $end_date
     * @param string $sale_type
     * @param int $export_excel default 0
     */
    function summary_suppliers($start_date, $end_date, $sale_type, $export_excel = 0)
    {
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->breadcrumbs->push($this->lang->line('reports_summary_reports'), '/reports/summary_suppliers');
        $this->breadcrumbs->push($this->lang->line('reports_suppliers_summary_report'), '/sales');
        $this->load->model('reports/Summary_suppliers');
        $model = $this->Summary_suppliers;
        $report_data = $model->getData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type));
        $tabular_data = array();
        if ($export_excel) {
            foreach ($report_data as $row) {
                $tabular_data[] = array(
                    $row['supplier'],
                    $row['subtotal'],
                    $row['total'],
                    $row['tax'],
                    $row['profit']
                );
            }
        } else {
            foreach ($report_data as $row) {
                $tabular_data[] = array(
                    array(
                        'class' => '',
                        'value' => $row['supplier']
                    ),
                    array(
                        'class' => ' class="text-right" data-order="' . $row['subtotal'] . '"',
                        'value' => to_currency($row['subtotal'])
                    ),
                    array(
                        'class' => ' class="text-right" data-order="' . $row['total'] . '"',
                        'value' => to_currency($row['total'])
                    ),
                    array(
                        'class' => ' class="text-right" data-order="' . $row['tax'] . '"',
                        'value' => to_currency($row['tax'])
                    ),
                    array(
                        'class' => ' class="text-right" data-order="' . $row['profit'] . '"',
                        'value' => to_currency($row['profit'])
                    )
                );
            }
        }
        $data = array(
            "custom_title" => $this->lang->line('reports_suppliers_summary_report'),
            "custom_subtitle" => date($this->config->item('date_format'), strtotime($start_date)) . ' - ' . date($this->config->item('date_format'), strtotime($end_date)),
            "headers" => $model->getDataColumns(),
            "data" => $tabular_data,
            "summary_data" => $model->getSummaryData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type)),
            "export_excel" => array(
                'report' => __FUNCTION__,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'sale_type' => $sale_type
            )
        );
        $data['controller_name'] = strtolower(get_class());
        if ($export_excel) {
            array_push($tabular_data, array(
                'SUM',
                $data['summary_data']['subtotal'],
                $data['summary_data']['total'],
                $data['summary_data']['tax'],
                $data['summary_data']['profit']
            ));
            $this->load->library('export');
            $this->export->to_excel($data['headers'], $tabular_data, str_replace(' ', '', date('Y-m-d') . '_' . $data['custom_title'] . '(' . date('Y.m.d', strtotime($start_date)) . '-' . date('Y.m.d', strtotime($end_date)) . ')'));
        } else {
            $this->load->view('template/header', $data);
            $this->load->view("reports/tabular", $data);
            $this->load->view('template/footer');
        }
    }

    /**
     * [GET] Display the summary items report
     *
     * @param string $start_date
     * @param string $end_date
     * @param string $sale_type
     * @param int $export_excel default 0
     */
    function summary_items($start_date, $end_date, $sale_type, $export_excel = 0)
    {
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->breadcrumbs->push($this->lang->line('reports_summary_reports'), '/reports/summary_items');
        $this->breadcrumbs->push($this->lang->line('reports_items_summary_report'), '/sales');
        $this->load->model('reports/Summary_items');
        $model = $this->Summary_items;
        $report_data = $model->getData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type));
        $tabular_data = array();
        if ($export_excel) {
            foreach ($report_data as $row) {
                $tabular_data[] = array(
                    $row['name'],
                    $row['quantity_purchased'],
                    $row['subtotal'],
                    $row['total'],
                    $row['tax'],
                    $row['profit']
                );
            }
        } else {
            foreach ($report_data as $row) {
                $tabular_data[] = array(
                    array(
                        'class' => '',
                        'value' => $row['name']
                    ),
                    array(
                        'class' => ' class="text-right"',
                        'value' => $row['quantity_purchased']
                    ),
                    array(
                        'class' => ' class="text-right" data-order="' . $row['subtotal'] . '"',
                        'value' => to_currency($row['subtotal'])
                    ),
                    array(
                        'class' => ' class="text-right" data-order="' . $row['total'] . '"',
                        'value' => to_currency($row['total'])
                    ),
                    array(
                        'class' => ' class="text-right" data-order="' . $row['tax'] . '"',
                        'value' => to_currency($row['tax'])
                    ),
                    array(
                        'class' => ' class="text-right" data-order="' . $row['profit'] . '"',
                        'value' => to_currency($row['profit'])
                    )
                );
            }
        }
        $data = array(
            "custom_title" => $this->lang->line('reports_items_summary_report'),
            "custom_subtitle" => date($this->config->item('date_format'), strtotime($start_date)) . ' - ' . date($this->config->item('date_format'), strtotime($end_date)),
            "headers" => $model->getDataColumns(),
            "data" => $tabular_data,
            "summary_data" => $model->getSummaryData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type)),
            "export_excel" => array(
                'report' => __FUNCTION__,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'sale_type' => $sale_type
            )
        );
        $data['controller_name'] = strtolower(get_class());
        if ($export_excel) {
            array_push($tabular_data, array(
                'SUM',
                $data['summary_data']['quantity_purchased'],
                $data['summary_data']['subtotal'],
                $data['summary_data']['total'],
                $data['summary_data']['tax'],
                $data['summary_data']['profit']
            ));
            $this->load->library('export');
            $this->export->to_excel($data['headers'], $tabular_data, str_replace(' ', '', date('Y-m-d') . '_' . $data['custom_title'] . '(' . date('Y.m.d', strtotime($start_date)) . '-' . date('Y.m.d', strtotime($end_date)) . ')'));
        } else {
            $this->load->view('template/header', $data);
            $this->load->view("reports/tabular", $data);
            $this->load->view('template/footer');
        }
    }

    /**
     * [GET] Display the summary employees report
     *
     * @param string $start_date
     * @param string $end_date
     * @param string $sale_type
     * @param int $export_excel default 0
     */
    function summary_employees($start_date, $end_date, $sale_type, $export_excel = 0)
    {
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->breadcrumbs->push($this->lang->line('reports_summary_reports'), '/reports/summary_employees');
        $this->breadcrumbs->push($this->lang->line('reports_employees_summary_report'), '/sales');
        $this->load->model('reports/Summary_employees');
        $model = $this->Summary_employees;
        $report_data = $model->getData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type));
        $tabular_data = array();
        if ($export_excel) {
            foreach ($report_data as $row) {
                $tabular_data[] = array(
                    $row['employee'],
                    $row['subtotal'],
                    $row['total'],
                    $row['tax'],
                    $row['profit']
                );
            }
        } else {
            foreach ($report_data as $row) {
                $tabular_data[] = array(
                    array(
                        'class' => '',
                        'value' => $row['employee']
                    ),
                    array(
                        'class' => ' class="text-right" data-order="' . $row['subtotal'] . '"',
                        'value' => to_currency($row['subtotal'])
                    ),
                    array(
                        'class' => ' class="text-right" data-order="' . $row['total'] . '"',
                        'value' => to_currency($row['total'])
                    ),
                    array(
                        'class' => ' class="text-right" data-order="' . $row['tax'] . '"',
                        'value' => to_currency($row['tax'])
                    ),
                    array(
                        'class' => ' class="text-right" data-order="' . $row['profit'] . '"',
                        'value' => to_currency($row['profit'])
                    )
                );
            }
        }
        $data = array(
            "custom_title" => $this->lang->line('reports_employees_summary_report'),
            "custom_subtitle" => date($this->config->item('date_format'), strtotime($start_date)) . ' - ' . date($this->config->item('date_format'), strtotime($end_date)),
            "headers" => $model->getDataColumns(),
            "data" => $tabular_data,
            "summary_data" => $model->getSummaryData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type)),
            "export_excel" => array(
                'report' => __FUNCTION__,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'sale_type' => $sale_type
            )
        );
        $data['controller_name'] = strtolower(get_class());
        if ($export_excel) {
            array_push($tabular_data, array(
                'SUM',
                $data['summary_data']['subtotal'],
                $data['summary_data']['total'],
                $data['summary_data']['tax'],
                $data['summary_data']['profit']
            ));
            $this->load->library('export');
            $this->export->to_excel($data['headers'], $tabular_data, str_replace(' ', '', date('Y-m-d') . '_' . $data['custom_title'] . '(' . date('Y.m.d', strtotime($start_date)) . '-' . date('Y.m.d', strtotime($end_date)) . ')'));
        } else {
            $this->load->view('template/header', $data);
            $this->load->view("reports/tabular", $data);
            $this->load->view('template/footer');
        }
    }

    /**
     * [GET] Display the summary taxes report
     *
     * @param string $start_date
     * @param string $end_date
     * @param string $sale_type
     * @param int $export_excel default 0
     */
    function summary_taxes($start_date, $end_date, $sale_type, $export_excel = 0)
    {
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->breadcrumbs->push($this->lang->line('reports_summary_reports'), '/reports/summary_taxes');
        $this->breadcrumbs->push($this->lang->line('reports_taxes_summary_report'), '/sales');
        $this->load->model('reports/Summary_taxes');
        $model = $this->Summary_taxes;
        $report_data = $model->getData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type));
        $tabular_data = array();
        if ($export_excel) {
            foreach ($report_data as $row) {
                $tabular_data[] = array(
                    $row['percent'],
                    $row['subtotal'],
                    $row['total'],
                    $row['tax']
                );
            }
        } else {
            foreach ($report_data as $row) {
                $tabular_data[] = array(
                    array(
                        'class' => ' class="text-right"',
                        'value' => $row['percent']
                    ),
                    array(
                        'class' => ' class="text-right" data-order="' . $row['subtotal'] . '"',
                        'value' => to_currency($row['subtotal'])
                    ),
                    array(
                        'class' => ' class="text-right" data-order="' . $row['total'] . '"',
                        'value' => to_currency($row['total'])
                    ),
                    array(
                        'class' => ' class="text-right" data-order="' . $row['tax'] . '"',
                        'value' => to_currency($row['tax'])
                    )
                );
            }
        }
        $data = array(
            "custom_title" => $this->lang->line('reports_taxes_summary_report'),
            "custom_subtitle" => date($this->config->item('date_format'), strtotime($start_date)) . ' - ' . date($this->config->item('date_format'), strtotime($end_date)),
            "headers" => $model->getDataColumns(),
            "data" => $tabular_data,
            "summary_data" => $model->getSummaryData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type)),
            "export_excel" => array(
                'report' => __FUNCTION__,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'sale_type' => $sale_type
            )
        );
        $data['controller_name'] = strtolower(get_class());
        if ($export_excel) {
            array_push($tabular_data, array(
                'SUM',
                $data['summary_data']['subtotal'],
                $data['summary_data']['total'],
                $data['summary_data']['tax']
            ));
            $this->load->library('export');
            $this->export->to_excel($data['headers'], $tabular_data, str_replace(' ', '', date('Y-m-d') . '_' . $data['custom_title'] . '(' . date('Y.m.d', strtotime($start_date)) . '-' . date('Y.m.d', strtotime($end_date)) . ')'));
        } else {
            $this->load->view('template/header', $data);
            $this->load->view("reports/tabular", $data);
            $this->load->view('template/footer');
        }
    }

    /**
     * [GET] Display the summary discounts report
     *
     * @param string $start_date
     * @param string $end_date
     * @param string $sale_type
     * @param int $export_excel default 0
     */
    function summary_discounts($start_date, $end_date, $sale_type, $export_excel = 0)
    {
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->breadcrumbs->push($this->lang->line('reports_summary_reports'), '/reports/summary_discounts');
        $this->breadcrumbs->push($this->lang->line('reports_discounts_summary_report'), '/sales');
        $this->load->model('reports/Summary_discounts');
        $model = $this->Summary_discounts;
        $report_data = $model->getData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type));
        $tabular_data = array();
        if ($export_excel) {
            foreach ($report_data as $row) {
                $tabular_data[] = array(
                    $row['discount_percent'],
                    $row['count']
                );
            }
        } else {
            foreach ($report_data as $row) {
                $tabular_data[] = array(
                    array(
                        'class' => ' class="text-right"',
                        'value' => $row['discount_percent']
                    ),
                    array(
                        'class' => ' class="text-right"',
                        'value' => $row['count']
                    )
                );
            }
        }
        $data = array(
            "custom_title" => $this->lang->line('reports_discounts_summary_report'),
            "custom_subtitle" => date($this->config->item('date_format'), strtotime($start_date)) . ' - ' . date($this->config->item('date_format'), strtotime($end_date)),
            "headers" => $model->getDataColumns(),
            "data" => $tabular_data,
            "summary_data" => $model->getSummaryData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type)),
            "export_excel" => array(
                'report' => __FUNCTION__,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'sale_type' => $sale_type
            )
        );
        $data['controller_name'] = strtolower(get_class());
        if ($export_excel) {
            array_push($tabular_data, array(
                'SUM',
                $data['summary_data']['count']
            ));
            $this->load->library('export');
            $this->export->to_excel($data['headers'], $tabular_data, str_replace(' ', '', date('Y-m-d') . '_' . $data['custom_title'] . '(' . date('Y.m.d', strtotime($start_date)) . '-' . date('Y.m.d', strtotime($end_date)) . ')'));
        } else {
            $this->load->view('template/header', $data);
            $this->load->view("reports/tabular", $data);
            $this->load->view('template/footer');
        }
    }

    /**
     * [GET] Display the summary payments report
     *
     * @param string $start_date
     * @param string $end_date
     * @param string $sale_type
     * @param int $export_excel default 0
     */
    function summary_payments($start_date, $end_date, $sale_type, $export_excel = 0)
    {
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->breadcrumbs->push($this->lang->line('reports_summary_reports'), '/reports/summary_payments');
        $this->breadcrumbs->push($this->lang->line('reports_payments_summary_report'), '/sales');
        $this->load->model('reports/Summary_payments');
        $model = $this->Summary_payments;
        $report_data = $model->getData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type));
        $tabular_data = array();
        if ($export_excel) {
            foreach ($report_data as $row) {
                $tabular_data[] = array(
                    lang($row['payment_type']),
                    $row['payment_amount']
                );
            }
        } else {
            foreach ($report_data as $row) {
                $tabular_data[] = array(
                    array(
                        'class' => '',
                        'value' => lang($row['payment_type'])
                    ),
                    array(
                        'class' => ' class="text-right" data-order="' . $row['payment_amount'] . '"',
                        'value' => to_currency($row['payment_amount'])
                    )
                );
            }
        }
        $data = array(
            "custom_title" => $this->lang->line('reports_payments_summary_report'),
            "custom_subtitle" => date($this->config->item('date_format'), strtotime($start_date)) . ' - ' . date($this->config->item('date_format'), strtotime($end_date)),
            "headers" => $model->getDataColumns(),
            "data" => $tabular_data,
            "summary_data" => $model->getSummaryData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type)),
            "export_excel" => array(
                'report' => __FUNCTION__,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'sale_type' => $sale_type
            )
        );
        $data['controller_name'] = strtolower(get_class());
        if ($export_excel) {
            $this->load->library('export');
            $this->export->to_excel($data['headers'], $tabular_data, str_replace(' ', '', date('Y-m-d') . '_' . $data['custom_title'] . '(' . date('Y.m.d', strtotime($start_date)) . '-' . date('Y.m.d', strtotime($end_date)) . ')'));
        } else {
            $this->load->view('template/header', $data);
            $this->load->view("reports/tabular", $data);
            $this->load->view('template/footer');
        }
    }

    /**
     * [GET] Input for reports that require only a date range.
     *
     * See routes.php to see that all graphical summary reports route here
     *
     * @param array $report_type
     */
    function date_input($report_type)
    {
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->breadcrumbs->push($this->lang->line('reports_' . $report_type . '_reports'), '/graphical');
        $data = $this->_get_common_report_data();
        $data['mode'] = 'sale';
        $data['controller_name'] = strtolower(get_class());
        $data['custom_title'] = $report_type == 'graphical' ? $this->lang->line('reports_graphical_reports') : $this->lang->line('reports_detailed_reports');
        $data['custom_subtitle'] = '';
        $this->load->view('template/header', $data);
        $this->load->view("reports/date_input", $data);
        $this->load->view('template/footer');
    }

    /**
     * [GET] Date range input for receivings
     */
    function date_input_recv()
    {
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->breadcrumbs->push($this->lang->line('reports_detailed_reports'), '/graphical');
        $data = $this->_get_common_report_data();
        $data['mode'] = 'receiving';
        $data['controller_name'] = strtolower(get_class());
        $data['custom_title'] = $this->lang->line('reports_detailed_reports');
        $data['custom_subtitle'] = '';
        $this->load->view('template/header', $data);
        $this->load->view("reports/date_input", $data);
        $this->load->view('template/footer');
    }

    /**
     * [GET] Date range input for requisition
     */
    function date_input_reqs()
    {
        $data = $this->_get_common_report_data();
        $data['mode'] = 'requisition';
        $this->load->view("reports/date_input", $data);
    }

    /**
     * [GET] Display graphical summary sales report
     *
     * @param string $start_date
     * @param string $end_date
     * @param string $sale_type
     */
    function graphical_summary_sales($start_date, $end_date, $sale_type)
    {
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->breadcrumbs->push($this->lang->line('reports_graphical_reports'), '/reports/graphical_summary_sales');
        $this->breadcrumbs->push($this->lang->line('reports_sales_summary_report'), '/sales');

        $this->load->model('reports/Summary_sales');

        $data = array(
            "custom_title" => $this->lang->line('reports_sales_summary_report'),
            "data_file" => $this->Summary_sales->graphical_summary_sales_graph($start_date, $end_date, $sale_type),
            "custom_subtitle" => '',
            "graph_heading" => date($this->config->item('date_format'), strtotime($start_date)) . ' - ' . date($this->config->item('date_format'), strtotime($end_date)),
            "summary_data" => $this->Summary_sales->getSummaryData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type))
        );
        $data['controller_name'] = strtolower(get_class());
        $this->load->view("template/header", $data);
        $this->load->view("reports/graphs/line", $data);
        $this->load->view("template/footer");
    }

    /**
     * [GET] DIsplay graphical summary items report
     *
     * @param string $start_date
     * @param string $end_date
     * @param string $sale_type
     */
    function graphical_summary_items($start_date, $end_date, $sale_type)
    {
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->breadcrumbs->push($this->lang->line('reports_graphical_reports'), '/reports/graphical_summary_items');
        $this->breadcrumbs->push($this->lang->line('reports_items_summary_report'), '/items');
        $this->load->model('reports/Summary_items');
        $model = $this->Summary_items;

        $data = array(
            "custom_title" => $this->lang->line('reports_items_summary_report'),
            "data_file" => $this->Summary_items->graphical_summary_items_graph($start_date, $end_date, $sale_type),
            "custom_subtitle" => '',
            "graph_heading" => date($this->config->item('date_format'), strtotime($start_date)) . ' - ' . date($this->config->item('date_format'), strtotime($end_date)),
            "summary_data" => $model->getSummaryData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type))
        );
        $data['controller_name'] = strtolower(get_class());
        $this->load->view('template/header', $data);
        $this->load->view("reports/graphs/bar", $data);
        $this->load->view('template/footer');
    }

    /**
     * [GET] DIsplay graphical specific items report
     *
     * @param string $start_date
     * @param string $end_date
     * @param int $item_id
     * @param string $sale_type
     */
    function graphical_specific_item($start_date, $end_date, $item_id, $sale_type)
    {
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->breadcrumbs->push($this->lang->line('reports_graphical_reports'), '/reports/graphical_specific_item');
        $this->breadcrumbs->push($this->lang->line('reports_items_summary_report'), '/items');
        $this->load->model('reports/Specific_item');
        $model = $this->Specific_item;

        $data = array(
            "custom_title" => $this->Item->get_info($item_id)->name,
            "data_file" => $model->graphical_summary_items_graph($start_date, $end_date, $item_id, $sale_type),
            "custom_subtitle" => '',
            "graph_heading" => date($this->config->item('date_format'), strtotime($start_date)) . ' - ' . date($this->config->item('date_format'), strtotime($end_date)),
            "summary_data" => $model->getSummaryData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type, 'item_id' => $item_id))
        );
        $data['controller_name'] = strtolower(get_class());
        $this->load->view('template/header', $data);
        $this->load->view("reports/graphs/line-item", $data);
        $this->load->view('template/footer');
    }

    /**
     * [GET] Display graphical summary categories report
     * @param string $start_date
     * @param string $end_date
     * @param string $sale_type
     */
    function graphical_summary_categories($start_date, $end_date, $sale_type)
    {
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->breadcrumbs->push($this->lang->line('reports_graphical_reports'), '/reports/graphical_summary_categories');
        $this->breadcrumbs->push($this->lang->line('reports_categories_summary_report'), '/categories');
        $this->load->model('reports/Summary_categories');
        $model = $this->Summary_categories;

        $data = array(
            "custom_title" => $this->lang->line('reports_categories_summary_report'),
            "data_file" => $this->Summary_categories->graphical_summary_categories_graph($start_date, $end_date, $sale_type),
            "custom_subtitle" => '',
            "graph_heading" => date($this->config->item('date_format'), strtotime($start_date)) . ' - ' . date($this->config->item('date_format'), strtotime($end_date)),
            "summary_data" => $model->getSummaryData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type))
        );
        $data['controller_name'] = strtolower(get_class());
        $this->load->view('template/header', $data);
        $this->load->view("reports/graphs/pie", $data);
        $this->load->view('template/footer');
    }

    /**
     * [GET] Display graphical summary suppliers report
     *
     * @param string $start_date
     * @param string $end_date
     * @param string $sale_type
     */
    function graphical_summary_suppliers($start_date, $end_date, $sale_type)
    {
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->breadcrumbs->push($this->lang->line('reports_graphical_reports'), '/reports/graphical_summary_suppliers');
        $this->breadcrumbs->push($this->lang->line('reports_suppliers_summary_report'), '/suppliers');
        $this->load->model('reports/Summary_suppliers');
        $model = $this->Summary_suppliers;

        $data = array(
            "custom_title" => $this->lang->line('reports_suppliers_summary_report'),
            "data_file" => $this->Summary_suppliers->graphical_summary_suppliers_graph($start_date, $end_date, $sale_type),
            "custom_subtitle" => '',
            "graph_heading" => date($this->config->item('date_format'), strtotime($start_date)) . ' - ' . date($this->config->item('date_format'), strtotime($end_date)),
            "summary_data" => $model->getSummaryData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type))
        );
        $data['controller_name'] = strtolower(get_class());
        $this->load->view('template/header', $data);
        $this->load->view("reports/graphs/pie", $data);
        $this->load->view('template/footer');
    }

    /**
     * [GET] Display graphical summary employees report
     *
     * @param string $start_date
     * @param string $end_date
     * @param string $sale_type
     */
    function graphical_summary_employees($start_date, $end_date, $sale_type)
    {
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->breadcrumbs->push($this->lang->line('reports_graphical_reports'), '/reports/graphical_summary_employees');
        $this->breadcrumbs->push($this->lang->line('reports_employees_summary_report'), '/employees');
        $this->load->model('reports/Summary_employees');
        $model = $this->Summary_employees;

        $data = array(
            "custom_title" => $this->lang->line('reports_employees_summary_report'),
            "data_file" => $this->Summary_employees->graphical_summary_employees_graph($start_date, $end_date, $sale_type),
            "custom_subtitle" => '',
            "graph_heading" => date($this->config->item('date_format'), strtotime($start_date)) . ' - ' . date($this->config->item('date_format'), strtotime($end_date)),
            "summary_data" => $model->getSummaryData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type))
        );
        $data['controller_name'] = strtolower(get_class());
        $this->load->view('template/header', $data);
        $this->load->view("reports/graphs/pie", $data);
        $this->load->view('template/footer');
    }

    /**
     * [GET] Display graphical summary taxes report
     *
     * @param string $start_date
     * @param string $end_date
     * @param string $sale_type
     */
    function graphical_summary_taxes($start_date, $end_date, $sale_type)
    {
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->breadcrumbs->push($this->lang->line('reports_graphical_reports'), '/reports/graphical_summary_taxes');
        $this->breadcrumbs->push($this->lang->line('reports_taxes_summary_report'), '/taxes');
        $this->load->model('reports/Summary_taxes');
        $model = $this->Summary_taxes;

        $data = array(
            "custom_title" => $this->lang->line('reports_taxes_summary_report'),
            "data_file" => $this->Summary_taxes->graphical_summary_taxes_graph($start_date, $end_date, $sale_type),
            "custom_subtitle" => '',
            "graph_heading" => date($this->config->item('date_format'), strtotime($start_date)) . ' - ' . date($this->config->item('date_format'), strtotime($end_date)),
            "summary_data" => $model->getSummaryData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type))
        );
        $data['controller_name'] = strtolower(get_class());
        $this->load->view("template/header", $data);
        $this->load->view("reports/graphs/pie", $data);
        $this->load->view("template/footer");
    }

    /**
     * [GET] Display grpahical summary customers report
     *
     * @param string $start_date
     * @param string $end_date
     * @param string $sale_type
     */
    function graphical_summary_customers($start_date, $end_date, $sale_type)
    {
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->breadcrumbs->push($this->lang->line('reports_graphical_reports'), '/reports/graphical_summary_customers');
        $this->breadcrumbs->push($this->lang->line('reports_customers_summary_report'), '/customers');
        $this->load->model('reports/Summary_customers');
        $model = $this->Summary_customers;

        $data = array(
            "custom_title" => $this->lang->line('reports_customers_summary_report'),
            "data_file" => $this->Summary_customers->graphical_summary_customers_graph($start_date, $end_date, $sale_type),
            "custom_subtitle" => '',
            "graph_heading" => date($this->config->item('date_format'), strtotime($start_date)) . ' - ' . date($this->config->item('date_format'), strtotime($end_date)),
            "summary_data" => $model->getSummaryData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type))
        );
        $data['controller_name'] = strtolower(get_class());
        $this->load->view('template/header', $data);
        $this->load->view("reports/graphs/bar", $data);
        $this->load->view('template/footer');
    }

    /**
     * [GET] Display graphical summary discounts report
     *
     * @param string $start_date
     * @param string $end_date
     * @param string $sale_type
     */
    function graphical_summary_discounts($start_date, $end_date, $sale_type)
    {
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->breadcrumbs->push($this->lang->line('reports_graphical_reports'), '/reports/graphical_summary_discounts');
        $this->breadcrumbs->push($this->lang->line('reports_discounts_summary_report'), '/discounts');
        $this->load->model('reports/Summary_discounts');
        $model = $this->Summary_discounts;

        $data = array(
            "custom_title" => $this->lang->line('reports_discounts_summary_report'),
            "data_file" => $this->Summary_discounts->graphical_summary_discounts_graph($start_date, $end_date, $sale_type),
            "custom_subtitle" => '',
            "graph_heading" => date('Y/m/d', strtotime($start_date)) . ' - ' . date('Y/m/d', strtotime($end_date)),
            "summary_data" => $model->getSummaryData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type))
        );
        $data['controller_name'] = strtolower(get_class());
        $this->load->view("template/header", $data);
        $this->load->view("reports/graphs/bar", $data);
        $this->load->view("template/footer");
    }

    /**
     * [GET] Display graphical summary payments report
     *
     * @param string $start_date
     * @param string $end_date
     * @param string $sale_type
     */
    function graphical_summary_payments($start_date, $end_date, $sale_type)
    {
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->breadcrumbs->push($this->lang->line('reports_graphical_reports'), '/reports/graphical_summary_payments');
        $this->breadcrumbs->push($this->lang->line('reports_payments_summary_report'), '/payments');
        $this->load->model('reports/Summary_payments');
        $model = $this->Summary_payments;

        $data = array(
            "custom_title" => $this->lang->line('reports_payments_summary_report'),
            "data_file" => $this->Summary_payments->graphical_summary_payments_graph($start_date, $end_date, $sale_type),
            "custom_subtitle" => '',
            "graph_heading" => date($this->config->item('date_format'), strtotime($start_date)) . ' - ' . date($this->config->item('date_format'), strtotime($end_date)),
            "summary_data" => $model->getSummaryData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type))
        );
        $data['controller_name'] = strtolower(get_class());
        $this->load->view("template/header", $data);
        $this->load->view("reports/graphs/pie", $data);
        $this->load->view("template/footer");
    }

    /**
     * [GET] Display input form for specific item reports
     */
    function specific_item_input($report_type = 'specific')
    {
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        if ($report_type == 'graphical') {
            $this->breadcrumbs->push($this->lang->line('reports_graphical_reports'), '/graphical');
            $data['custom_title'] = $this->lang->line('reports_graphical_reports');
        } else {
            $this->breadcrumbs->push($this->lang->line('reports_items'), '/detailed');
            $data['custom_title'] = $this->lang->line('reports_detailed_reports');
        }
        $data = $this->_get_common_report_data();
        $data['specific_input_name'] = $this->lang->line('reports_item');

        $data['controller_name'] = strtolower(get_class());
        $this->load->view('template/header', $data);
        $this->load->view("reports/specific_item_input", $data);
        $this->load->view('template/footer');
    }

    function specific_item($start_date, $end_date, $item_id, $sale_type, $export_excel = 0)
    {
        $item_info = $this->Item->get_info($item_id);
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->breadcrumbs->push($this->lang->line('reports_items'), '/specific_item');
        $this->breadcrumbs->push($item_info->name, '/items');
        $this->load->model('reports/Specific_item');
        $model = $this->Specific_item;
        $headers = $model->getDataColumns();
        $headers['summary_class'] = array(
            'all', // sale id
            'all', // date
            'min-tablet-l', // items purchased
            'min-tablet-p', // sold by
            'min-tablet-l' // sold_to
        );
        $headers['details_class'] = array(
            'min-tablet-p', // item_number
            'all', // name
            'min-desktop', // category
            'min-tablet-p', // qty
            'min-tablet-l', // subtotal
            'min-desktop', // tax
            'min-tablet-p', // total
            'none', // profit
            'min-desktop' // discounts
        );
        $report_data = $model->getDetailsData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type, 'item_id' => $item_id));
        $summary_data = array();
        $details_data = array();
        foreach ($report_data['summary'] as $key => $row) {
            $summary_data[] = array(
                array(
                    'class' => '',
                    'value' => anchor('sales/edit/' . $row['sale_id'], 'POS ' . $row['sale_id'], array('target' => '_blank'))
                ),
                array(
                    'class' => '',
                    'value' => $row['sale_time']
                ),
                array(
                    'class' => 'text-right',
                    'value' => $row['quantity_purchased']
                ),
                array(
                    'class' => '',
                    'value' => $row['employee_name']
                ),
                array(
                    'class' => '',
                    'value' => $row['customer_name']
                )
            );
            foreach ($report_data['details'][$key] as $drow) {
                $name = '<dl class="no-margin-bottom">';
                $name .= '<dt>';
                $name .= $drow['name'];
                $name .= '</dt>';
                if ($drow['description'] !== '' || $drow['serialnumber'] !== '') {
                    $name .='<dd>';
                    $name .= $drow['description'] !== '' ? "<small>" . $drow['description'] . "</small><br>" : '';
                    $name .= $drow['serialnumber'] !== '' && $drow['serialnumber'] !== '0' ? "<small><em>" . $drow['serialnumber'] . "</em></small>" : '';
                    $name .='</dd>';
                }
                $name .= '</dl>';
                $details_data[$key][] = array(
                    array(
                        'class' => '',
                        'value' => $drow['item_number']
                    ),
                    array(
                        'class' => '',
                        'value' => $name
                    ),
                    array(
                        'class' => '',
                        'value' => $drow['category']
                    ),
                    array(
                        'class' => 'text-right',
                        'value' => $drow['quantity_purchased']
                    ),
                    array(
                        'class' => 'text-right',
                        'value' => to_currency($drow['subtotal'])
                    ),
                    array(
                        'class' => 'text-right',
                        'value' => to_currency($drow['total'])
                    ),
                    array(
                        'class' => 'text-right',
                        'value' => to_currency($drow['tax'])
                    ),
                    array(
                        'class' => 'text-right',
                        'value' => to_currency($drow['profit'])
                    ),
                    array(
                        'class' => 'text-right',
                        'value' => $drow['discount_percent'] . '%'
                    )
                );
            }
        }
        $data = array(
            "custom_title" => $item_info->name,
            "custom_subtitle" => date($this->config->item('date_format'), strtotime($start_date)) . ' - ' . date($this->config->item('date_format'), strtotime($end_date)),
            "headers" => $headers,
            "summary_data" => $summary_data,
            "details_data" => $details_data,
            "overall_summary_data" => $model->getSummaryData(array('start_date' => $start_date, 'end_date' => $end_date, 'item_id' => $item_id, 'sale_type' => $sale_type)),
            "export_excel" => $export_excel
        );
        $data['controller_name'] = strtolower(get_class());
        $this->load->view("template/header", $data);
        $this->load->view("reports/tabular_details", $data);
        $this->load->view("template/footer");
    }

    /**
     * [GET] Display input form for specific customer reports
     */
    function specific_customer_input()
    {
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->breadcrumbs->push($this->lang->line('reports_customer'), '/graphical');
        $data = $this->_get_common_report_data();
        $data['specific_input_name'] = $this->lang->line('reports_customer');

        $customers = array();
        foreach ($this->Customer->get_all()->result() as $customer) {
            $customers[$customer->person_id] = $customer->first_name . ' ' . $customer->last_name;
        }
        $data['specific_input_data'] = $customers;
        $data['controller_name'] = strtolower(get_class());
        $this->load->view('template/header', $data);
        $this->load->view("reports/specific_input", $data);
        $this->load->view('template/footer');
    }

    /**
     * [GET] Display specific customer report
     *
     * @param string $start_date
     * @param string $end_date
     * @param string $customer_id
     * @param string $sale_type
     * @param int $export_excel default 0
     */
    function specific_customer($start_date, $end_date, $customer_id, $sale_type, $export_excel = 0)
    {
        $customer_info = $this->Customer->get_info($customer_id);
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->breadcrumbs->push($this->lang->line('reports_customer'), '/specific_customer');
        $this->breadcrumbs->push($customer_info->first_name . ' ' . $customer_info->last_name . ' ' . $this->lang->line('reports_report'), '/customer');
        $this->load->model('reports/Specific_customer');
        $model = $this->Specific_customer;
        $headers = $model->getDataColumns();
        $headers['summary_class'] = array(
            'all', // sale id
            'all', // date
            'min-tablet-l', // items purchased
            'min-tablet-p', // sold by
            'min-tablet-l', // subtotal
            'min-tablet-p', // total
            'min-tablet-l', // tax
            'all', // profit
            'none', // payment type
            'min-desktop' // comments
        );
        $headers['details_class'] = array(
            'min-tablet-p', // item_number
            'all', // name
            'min-desktop', // category
            'min-tablet-p', // qty
            'min-tablet-l', // subtotal
            'min-desktop', // tax
            'min-tablet-p', // total
            'none', // profit
            'min-desktop' // discounts
        );
        $report_data = $model->getData(array('start_date' => $start_date, 'end_date' => $end_date, 'customer_id' => $customer_id, 'sale_type' => $sale_type));
        $summary_data = array();
        $details_data = array();
        foreach ($report_data['summary'] as $key => $row) {
            $concat_payment = array();
            foreach ($row['payment_type'] as $type) {
                array_push($concat_payment, lang($type));
            }
            $summary_data[] = array(
                array(
                    'class' => '',
                    'value' => anchor('sales/edit/' . $row['sale_id'], 'POS ' . $row['sale_id'], array('target' => '_blank'))
                ),
                array(
                    'class' => '',
                    'value' => $row['sale_date']
                ),
                array(
                    'class' => 'text-right',
                    'value' => $row['items_purchased']
                ),
                array(
                    'class' => '',
                    'value' => $row['employee_name']
                ),
                array(
                    'class' => 'text-right',
                    'value' => to_currency_no_money($row['subtotal'])
                ),
                array(
                    'class' => 'text-right',
                    'value' => to_currency_no_money($row['total'])
                ),
                array(
                    'class' => 'text-right',
                    'value' => to_currency_no_money($row['tax'])
                ),
                array(
                    'class' => 'text-right',
                    'value' => to_currency_no_money($row['profit'])
                ),
                array(
                    'class' => '',
                    'value' => implode(', ', $concat_payment)
                ),
                array(
                    'class' => '',
                    'value' => $row['comment'])
            );
            foreach ($report_data['details'][$key] as $drow) {
                $name = '<dl class="no-margin-bottom">';
                $name .= '<dt>';
                $name .= $drow['name'];
                $name .= '</dt>';
                if ($drow['description'] !== '' || $drow['serialnumber'] !== '') {
                    $name .='<dd>';
                    $name .= $drow['description'] !== '' ? "<small>" . $drow['description'] . "</small><br>" : '';
                    $name .= $drow['serialnumber'] !== '' && $drow['serialnumber'] !== '0' ? "<small><em>" . $drow['serialnumber'] . "</em></small>" : '';
                    $name .='</dd>';
                }
                $name .= '</dl>';
                $details_data[$key][] = array(
                    array(
                        'class' => '',
                        'value' => $drow['item_number']
                    ),
                    array(
                        'class' => '',
                        'value' => $drow['name']
                    ),
                    array(
                        'class' => '',
                        'value' => $drow['category']
                    ),
                    array(
                        'class' => 'text-right',
                        'value' => $drow['quantity_purchased']
                    ),
                    array(
                        'class' => 'text-right',
                        'value' => to_currency($drow['subtotal'])
                    ),
                    array(
                        'class' => 'text-right',
                        'value' => to_currency($drow['total'])
                    ),
                    array(
                        'class' => 'text-right',
                        'value' => to_currency($drow['tax'])
                    ),
                    array(
                        'class' => 'text-right',
                        'value' => to_currency($drow['profit'])
                    ),
                    array(
                        'class' => 'text-right',
                        'value' => $drow['discount_percent'] . '%'
                    )
                );
            }
        }
        $data = array(
            "custom_title" => $customer_info->first_name . ' ' . $customer_info->last_name . ' ' . $this->lang->line('reports_report'),
            "custom_subtitle" => date($this->config->item('date_format'), strtotime($start_date)) . ' - ' . date($this->config->item('date_format'), strtotime($end_date)),
            "headers" => $headers,
            "summary_data" => $summary_data,
            "details_data" => $details_data,
            "overall_summary_data" => $model->getSummaryData(array('start_date' => $start_date, 'end_date' => $end_date, 'customer_id' => $customer_id, 'sale_type' => $sale_type)),
            "export_excel" => $export_excel
        );
        $data['controller_name'] = strtolower(get_class());
        $this->load->view('template/header', $data);
        $this->load->view("reports/tabular_details", $data);
        $this->load->view('template/footer');
    }

    /**
     * [GET] Display input form for specific employee
     */
    function specific_employee_input()
    {
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->breadcrumbs->push($this->lang->line('reports_employee'), '/graphical');
        $data = $this->_get_common_report_data();
        $data['specific_input_name'] = $this->lang->line('reports_employee');

        $employees = array();
        foreach ($this->Employee->get_all()->result() as $employee) {
            $employees[$employee->person_id] = $employee->first_name . ' ' . $employee->last_name;
        }
        $data['specific_input_data'] = $employees;
        $data['controller_name'] = strtolower(get_class());
        $this->load->view('template/header', $data);
        $this->load->view("reports/specific_input", $data);
        $this->load->view('template/footer');
    }

    /**
     * [GET] Display specific employee report
     *
     * @param string $start_date
     * @param string $end_date
     * @param string $employee_id
     * @param string $sale_type
     * @param int $export_excel default 0
     */
    function specific_employee($start_date, $end_date, $employee_id, $sale_type, $export_excel = 0)
    {
        $employee_info = $this->Employee->get_info($employee_id);
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->breadcrumbs->push($this->lang->line('reports_employee'), '/specific_employee');
        $this->breadcrumbs->push($employee_info->first_name . ' ' . $employee_info->last_name . ' ' . $this->lang->line('reports_report'), '/employee');
        $this->load->model('reports/Specific_employee');
        $model = $this->Specific_employee;
        $headers = $model->getDataColumns();
        $headers['summary_class'] = array(
            'all', // sale id
            'all', // date
            'min-tablet-l', // items purchased
            'min-tablet-p', // sold by
            'min-tablet-l', // subtotal
            'min-tablet-p', // total
            'min-tablet-l', // tax
            'all', // profit
            'min-desktop', // payment type
            'min-desktop' // comments
        );
        $headers['details_class'] = array(
            'min-tablet-p', // item_number
            'all', // name
            'min-desktop', // category
            'min-tablet-p', // qty
            'min-tablet-l', // subtotal
            'min-desktop', // tax
            'min-tablet-p', // total
            'none', // profit
            'min-desktop' // discounts
        );
        $report_data = $model->getData(array('start_date' => $start_date, 'end_date' => $end_date, 'employee_id' => $employee_id, 'sale_type' => $sale_type));
        $summary_data = array();
        $details_data = array();
        foreach ($report_data['summary'] as $key => $row) {
            $summary_data[] = array(
                array(
                    'class' => '',
                    'value' => anchor('sales/edit/' . $row['sale_id'], 'POS ' . $row['sale_id'], array('target' => '_blank'))
                ),
                array(
                    'class' => '',
                    'value' => $row['sale_date']
                ),
                array(
                    'class' => 'text-right',
                    'value' => $row['items_purchased']
                ),
                array(
                    'class' => '',
                    'value' => $row['customer_name']
                ),
                array(
                    'class' => 'text-right',
                    'value' => to_currency_no_money($row['subtotal'])
                ),
                array(
                    'class' => 'text-right',
                    'value' => to_currency_no_money($row['total'])
                ),
                array(
                    'class' => 'text-right',
                    'value' => to_currency_no_money($row['tax'])
                ),
                array(
                    'class' => 'text-right',
                    'value' => to_currency_no_money($row['profit'])
                ),
                array(
                    'class' => '',
                    'value' => $row['payment_type']
                ),
                array(
                    'class' => '',
                    'value' => $row['comment'])
            );
            foreach ($report_data['details'][$key] as $drow) {
                $name = '<dl class="no-margin-bottom">';
                $name .= '<dt>';
                $name .= $drow['name'];
                $name .= '</dt>';
                if ($drow['description'] !== '' || $drow['serialnumber'] !== '') {
                    $name .='<dd>';
                    $name .= $drow['description'] !== '' ? "<small>" . $drow['description'] . "</small><br>" : '';
                    $name .= $drow['serialnumber'] !== '' && $drow['serialnumber'] !== '0' ? "<small><em>" . $drow['serialnumber'] . "</em></small>" : '';
                    $name .='</dd>';
                }
                $name .= '</dl>';
                $details_data[$key][] = array(
                    array(
                        'class' => '',
                        'value' => $drow['item_number']
                    ),
                    array(
                        'class' => '',
                        'value' => $drow['name']
                    ),
                    array(
                        'class' => '',
                        'value' => $drow['category']
                    ),
                    array(
                        'class' => 'text-right',
                        'value' => $drow['quantity_purchased']
                    ),
                    array(
                        'class' => 'text-right',
                        'value' => to_currency($drow['subtotal'])
                    ),
                    array(
                        'class' => 'text-right',
                        'value' => to_currency($drow['total'])
                    ),
                    array(
                        'class' => 'text-right',
                        'value' => to_currency($drow['tax'])
                    ),
                    array(
                        'class' => 'text-right',
                        'value' => to_currency($drow['profit'])
                    ),
                    array(
                        'class' => 'text-right',
                        'value' => $drow['discount_percent'] . '%'
                    )
                );
            }
        }
        $data = array(
            "custom_title" => $employee_info->first_name . ' ' . $employee_info->last_name . ' ' . $this->lang->line('reports_report'),
            "custom_subtitle" => date($this->config->item('date_format'), strtotime($start_date)) . ' - ' . date($this->config->item('date_format'), strtotime($end_date)),
            "headers" => $headers,
            "summary_data" => $summary_data,
            "details_data" => $details_data,
            "overall_summary_data" => $model->getSummaryData(array('start_date' => $start_date, 'end_date' => $end_date, 'employee_id' => $employee_id, 'sale_type' => $sale_type)),
            "export_excel" => $export_excel
        );
        $data['controller_name'] = strtolower(get_class());
        $this->load->view('template/header', $data);
        $this->load->view("reports/tabular_details", $data);
        $this->load->view('template/footer');
    }

    /**
     * [GET] Display input form for specific discount
     *
     * This method is not used and will be deprecated
     * @todo Schedule removal
     */
    function specific_discount_input()
    {
        $data = $this->_get_common_report_data();
        $data['specific_input_name'] = $this->lang->line('reports_discount');

        $discounts = array();
        for ($i = 0; $i <= 100; $i += 10) {
            $discounts[$i] = $i . '%';
        }
        $data['specific_input_data'] = $discounts;
        $this->load->view("reports/specific_input", $data);
    }

    /**
     * [GET] Display the specific discount report
     *
     * This method is not used and will be deprecated
     * @todo Schedule removal
     *
     * @param string $start_date
     * @param string $end_date
     * @param string $discount
     * @param string $sale_type
     * @param int $export_excel default 0
     */
    function specific_discount($start_date, $end_date, $discount, $sale_type, $export_excel = 0)
    {
        $this->load->model('reports/Specific_discount');
        $model = $this->Specific_discount;

        $headers = $model->getDataColumns();
        $report_data = $model->getData(array('start_date' => $start_date, 'end_date' => $end_date, 'discount' => $discount, 'sale_type' => $sale_type));

        $summary_data = array();
        $details_data = array();

        foreach ($report_data['summary'] as $key => $row) {
            $summary_data[] = array(anchor('sales/receipt/' . $row['sale_id'], 'POS ' . $row['sale_id'], array('target' => '_blank')), $row['sale_date'], $row['items_purchased'], $row['customer_name'], to_currency($row['subtotal']), to_currency($row['total']), to_currency($row['tax']), /* to_currency($row['profit']), */ $row['payment_type'], $row['comment']);

            foreach ($report_data['details'][$key] as $drow) {
                $details_data[$key][] = array($drow['name'], $drow['category'], $drow['description'], $drow['quantity_purchased'], to_currency($drow['subtotal']), to_currency($drow['total']), to_currency($drow['tax']), /* to_currency($drow['profit']), */ $drow['discount_percent'] . '%');
            }
        }

        $data = array(
            "title" => $discount . '% ' . $this->lang->line('reports_discount') . ' ' . $this->lang->line('reports_report'),
            "subtitle" => date('m/d/Y', strtotime($start_date)) . '-' . date('m/d/Y', strtotime($end_date)),
            "headers" => $headers,
            "summary_data" => $summary_data,
            "details_data" => $details_data,
            "header_width" => intval(100 / count($headers['summary'])),
            "overall_summary_data" => $model->getSummaryData(array('start_date' => $start_date, 'end_date' => $end_date, 'discount' => $discount, 'sale_type' => $sale_type)),
            "export_excel" => $export_excel
        );

        $this->load->view("reports/tabular_details", $data);
    }

    /**
     * [GET] Display detailed sales report
     *
     * @param string $start_date
     * @param string $end_date
     * @param string $sale_type
     * @param int $export_excel default 0
     */
    function detailed_sales($start_date, $end_date, $sale_type, $export_excel = 0)
    {
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->breadcrumbs->push($this->lang->line('reports_detailed_reports'), '/reports/detailed_sales');
        $this->breadcrumbs->push($this->lang->line('reports_detailed_sales_report'), '/sales');
        $this->load->model('reports/Detailed_sales');
        $model = $this->Detailed_sales;
        $headers = $model->getDataColumns();
        $headers['summary_class'] = array(
            'all', //sale ID
            'all', //date
            'none', //items_purchased
            'min-desktop', //sold by
            'min-desktop', //sold to
            'min-tablet-p', // subtotal
            'min-tablet-l', // total
            'min-tablet-l', // tax
            'all', // profit
            'none', // payment_type
            'none' // comments
        );
        $headers['details_class'] = array(
            'min-tablet-p', // item_number
            'all', // name
            'min-desktop', // category
            'min-tablet-p', // qty
            'min-tablet-l', // subtotal
            'min-desktop', // tax
            'min-tablet-p', // total
            'none', // profit
            'min-desktop' // discounts
        );
        $report_data = $model->getData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type));
        $summary_data = array();
        $details_data = array();
        $this->load->model('Sales_payment');
        foreach ($report_data['summary'] as $key => $row) {
            $concat_payment = array();
            foreach ($row['payment_type'] as $type) {
                array_push($concat_payment, lang($type));
            }
            $has_store_account = $this->Sales_payment->has_store_account($row['sale_id']);
            $summary_data[] = array(
                array(
                    'class' => '',
                    'value' => anchor('sales/edit/' . $row['sale_id'], 'POS ' . $row['sale_id'], array('target' => '_blank')) . ($has_store_account ? '<a href="' . site_url('sales/finish_payment/' . $row['sale_id']) . '" class="btn btn-xs btn-success pull-right" target="_blank"><i class="fa fa-money"></i></a>' : '')
                ),
                array(
                    'class' => '',
                    'value' => $row['sale_time']
                ),
                array(
                    'class' => 'text-right',
                    'value' => $row['items_purchased']
                ),
                array(
                    'class' => '',
                    'value' => $row['employee_name']
                ),
                array(
                    'class' => '',
                    'value' => $row['customer_name']
                ),
                array(
                    'class' => 'text-right',
                    'value' => to_currency_no_money($row['subtotal'])
                ),
                array(
                    'class' => 'text-right',
                    'value' => to_currency_no_money($row['total'])
                ),
                array(
                    'class' => 'text-right',
                    'value' => to_currency_no_money($row['tax'])
                ),
                array(
                    'class' => 'text-right',
                    'value' => to_currency_no_money($row['profit'])
                ),
                array(
                    'class' => '',
                    'value' => implode(', ', $concat_payment)
                ),
                array(
                    'class' => '',
                    'value' => $row['comment'] == 0 ? '' : $row['comment']
                )
            );
            foreach ($report_data['details'][$key] as $drow) {
                $name = '<dl class="no-margin-bottom">';
                $name .= '<dt>';
                $name .= $drow['name'];
                $name .= '</dt>';
                if ($drow['description'] !== '' || $drow['serialnumber'] !== '') {
                    $name .='<dd>';
                    $name .= $drow['description'] !== '' ? "<small>" . $drow['description'] . "</small><br>" : '';
                    $name .= $drow['serialnumber'] !== '' && $drow['serialnumber'] !== '0' ? "<small><em>" . $drow['serialnumber'] . "</em></small>" : '';
                    $name .='</dd>';
                }
                $name .= '</dl>';
                $details_data[$key][] = array(
                    array(
                        'class' => '',
                        'value' => $drow['item_number']
                    ),
                    array(
                        'class' => '',
                        'value' => $name
                    ),
                    array(
                        'class' => '',
                        'value' => $drow['category']
                    ),
                    array(
                        'class' => 'text-right',
                        'value' => $drow['quantity_purchased']
                    ),
                    array(
                        'class' => 'text-right',
                        'value' => to_currency($drow['subtotal'])
                    ),
                    array(
                        'class' => 'text-right',
                        'value' => to_currency($drow['total'])
                    ),
                    array(
                        'class' => 'text-right',
                        'value' => to_currency($drow['tax'])
                    ),
                    array(
                        'class' => 'text-right',
                        'value' => to_currency($drow['profit'])
                    ),
                    array(
                        'class' => 'text-right',
                        'value' => $drow['discount_percent'] . '%'
                    )
                );
            }
        }
        $data = array(
            "custom_title" => $this->lang->line('reports_detailed_sales_report'),
            "custom_subtitle" => date($this->config->item('date_format'), strtotime($start_date)) . ' - ' . date($this->config->item('date_format'), strtotime($end_date)),
            "headers" => $headers,
            "editable" => true,
            "summary_data" => $summary_data,
            "details_data" => $details_data,
            "header_width" => intval(100 / count($headers['summary'])),
            "overall_summary_data" => $model->getSummaryData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type)),
            "export_excel" => $export_excel
        );
        $data['controller_name'] = strtolower(get_class());
        $this->load->view("template/header", $data);
        $this->load->view("reports/tabular_details", $data);
        $this->load->view("template/footer");
    }

    /**
     * [GET] Display detailed receivings report
     *
     * @param string $start_date
     * @param string $end_date
     * @param string $receiving_type
     * @param int $export_excel default 0
     */
    function detailed_receivings($start_date, $end_date, $receiving_type, $export_excel = 0)
    {
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->breadcrumbs->push($this->lang->line('reports_detailed_reports'), '/reports/detailed_receivings');
        $this->breadcrumbs->push($this->lang->line('reports_detailed_receivings_report'), '/sales');
        $this->load->model('reports/Detailed_receivings');
        $model = $this->Detailed_receivings;
        $headers = $model->getDataColumns();
        $report_data = $model->getData(array('start_date' => $start_date, 'end_date' => $end_date, 'receiving_type' => $receiving_type));
        $summary_data = array();
        $details_data = array();
        $headers['summary_class'] = array(
            'all', // id
            'min-tablet-p', // date
            'all', // items purhcased
            'min-tablet-l', // received bye
            'min-tablet-l', // supplied by
            'all', // total
            'min-tablet-p', // payment_type
            'min-desktop' // comments
        );
        $headers['details_class'] = array(
            'all', // item number
            'min-tablet-p', // name
            'min-tablet-l', // category
            'all', // qty
            'all', // total
            'min-desktop' // discounts
        );
        foreach ($report_data['summary'] as $key => $row) {
            $summary_data[] = array(
                array(
                    'class' => '',
                    'value' => anchor('receivings/receipt/' . $row['receiving_id'], 'RECV ' . $row['receiving_id'], array('target' => '_blank'))
                ),
                array(
                    'class' => '',
                    'value' => $row['receiving_date']
                ),
                array(
                    'class' => 'text-right',
                    'value' => $row['items_purchased']
                ),
                array(
                    'class' => '',
                    'value' => $row['employee_name']
                ),
                array(
                    'class' => '',
                    'value' => $row['supplier_name']
                ),
                array(
                    'class' => 'text-right',
                    'value' => to_currency($row['total'])
                ),
                array(
                    'class' => '',
                    'value' => $row['payment_type']
                ),
                array(
                    'class' => '',
                    'value' => $row['comment']
            ));
            foreach ($report_data['details'][$key] as $drow) {
                $details_data[$key][] = array(
                    array(
                        'class' => '',
                        'value' => $drow['item_number']
                    ),
                    array(
                        'class' => '',
                        'value' => $drow['name']
                    ),
                    array(
                        'class' => '',
                        'value' => $drow['category']
                    ),
                    array(
                        'class' => 'text-right',
                        'value' => $drow['quantity_purchased']
                    ),
                    array(
                        'class' => 'text-right',
                        'value' => to_currency($drow['total'])
                    ),
                    array(
                        'class' => 'text-right',
                        'value' => $drow['discount_percent'] . '%'
                    )
                );
            }
        }
        $data = array(
            "custom_title" => $this->lang->line('reports_detailed_receivings_report'),
            "custom_subtitle" => date('m/d/Y', strtotime($start_date)) . ' - ' . date('m/d/Y', strtotime($end_date)),
            "headers" => $headers,
            "summary_data" => $summary_data,
            "details_data" => $details_data,
            "overall_summary_data" => $model->getSummaryData(array('start_date' => $start_date, 'end_date' => $end_date, 'receiving_type' => $receiving_type)),
            "export_excel" => $export_excel
        );
        $data['controller_name'] = strtolower(get_class());
        $this->load->view('template/header', $data);
        $this->load->view("reports/tabular_details", $data);
        $this->load->view('template/footer');
    }

    /**
     * [GET] Display detailed requisition report
     *
     * @param string $start_date
     * @param string $end_date
     * @param int $export_excel default 0
     */
    function detailed_requisition($start_date, $end_date, $export_excel = 0)
    {
        $this->load->model('reports/Detailed_requisition');
        $model = $this->Detailed_requisition;
        $report_data = $model->getData(array('start_date' => $start_date, 'end_date' => $end_date));

        $summary_data = array();
        $details_data = array();

        foreach ($report_data['summary'] as $key => $row) {
            $summary_data[] = array(anchor('receivings/requisition_receipt/' . $row['requisition_id'], 'REQS ' . $row['requisition_id'], array('target' => '_blank')), $row['requisition_date'], $row['employee_name'], $row['comment']);

            foreach ($report_data['details'][$key] as $drow) {
                $details_data[$key][] = array($drow['name'], $drow['requisition_quantity'],
                    $drow['related_item_id'], $drow['related_item_quantity'],
                    $drow['related_item_total_quantity']);
            }
        }

        $data = array(
            "title" => $this->lang->line('reports_detailed_requisition_report'),
            "subtitle" => date('m/d/Y', strtotime($start_date)) . '-' . date('m/d/Y', strtotime($end_date)),
            "headers" => $model->getDataColumns(),
            "summary_data" => $summary_data,
            "details_data" => $details_data,
            "overall_summary_data" => '',
            "export_excel" => $export_excel
        );
        $this->load->view("reports/tabular_details", $data);
    }

    /**
     * [GET] Export report to excel (.xls) file
     */
    function excel_export()
    {
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->breadcrumbs->push($this->lang->line('reports_inventory_reports'), '/graphical');
        $data['controller_name'] = strtolower(get_class());
        $data['custom_title'] = $this->lang->line('reports_inventory_reports');
        $data['custom_subtitle'] = '';
        $this->load->view("template/header", $data);
        $this->load->view("reports/excel_export", array());
        $this->load->view("template/footer");
    }

    /**
     * [GET] Display inventory low report
     *
     * @param int $export_excel default 0
     */
    function inventory_low($export_excel = 0)
    {
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->breadcrumbs->push($this->lang->line('reports_inventory_reports'), '/reports/inventory_low');
        $this->breadcrumbs->push($this->lang->line('reports_low_inventory_report'), '/sales');
        $this->load->model('reports/Inventory_low');
        $model = $this->Inventory_low;
        $report_data = $model->getData(array());
        $tabular_data = array();
        if ($export_excel) {
            foreach ($report_data as $row) {
                $tabular_data[] = array(
                    $row['item_number'],
                    $row['name'],
                    $row['description'],
                    $row['quantity'],
                    $row['reorder_level'],
                    $row['location_name']
                );
            }
        } else {
            foreach ($report_data as $row) {
                $tabular_data[] = array(
                    array(
                        'class' => '',
                        'value' => $row['item_number']
                    ),
                    array(
                        'class' => '',
                        'value' => $row['name']
                    ),
                    array(
                        'class' => '',
                        'value' => $row['description']
                    ),
                    array(
                        'class' => ' class="text-right"',
                        'value' => $row['quantity']
                    ),
                    array(
                        'class' => '',
                        'value' => $row['reorder_level']
                    ),
                    array(
                        'class' => '',
                        'value' => $row['location_name']
                    )
                );
            }
        }
        $data = array(
            "custom_title" => $this->lang->line('reports_low_inventory_report'),
            "custom_subtitle" => '',
            "headers" => $model->getDataColumns(),
            "data" => $tabular_data,
            "summary_data" => $model->getSummaryData(array()),
            "export_excel" => array(
                'report' => __FUNCTION__
            )
        );
        $data['controller_name'] = strtolower(get_class());
        if ($export_excel) {
            $this->load->library('export');
            $this->export->to_excel($data['headers'], $tabular_data, str_replace(' ', '', date('Y-m-d') . '_' . $data['custom_title']));
        } else {
            $this->load->view('template/header', $data);
            $this->load->view("reports/tabular", $data);
            $this->load->view('template/footer');
        }
    }

    /**
     * [GET] Display inventory summary report
     *
     * @param int $export_excel default 0
     */
    function inventory_summary($export_excel = 0)
    {
        $this->breadcrumbs->push($this->lang->line('module_reports'), '/reports');
        $this->breadcrumbs->push($this->lang->line('reports_inventory_reports'), '/reports/inventory_low');
        $this->breadcrumbs->push($this->lang->line('reports_inventory_summary_report'), '/sales');
        $this->load->model('reports/Inventory_summary');
        $model = $this->Inventory_summary;
        $report_data = $model->getData(array());
        $tabular_data = array();
        if ($export_excel) {
            foreach ($report_data as $row) {
                $tabular_data[] = array(
                    $row['name'],
                    $row['item_number'],
                    $row['description'],
                    $row['quantity'],
                    $row['reorder_level'],
                    $row['location_name']
                );
            }
        } else {
            foreach ($report_data as $row) {
                $tabular_data[] = array(
                    array(
                        'class' => '',
                        'value' => $row['name']
                    ),
                    array(
                        'class' => '',
                        'value' => $row['item_number']
                    ),
                    array(
                        'class' => '',
                        'value' => $row['description']
                    ),
                    array(
                        'class' => ' class="text-right"',
                        'value' => $row['quantity']
                    ),
                    array(
                        'class' => ' class="text-right"',
                        'value' => $row['reorder_level']
                    ),
                    array(
                        'class' => '',
                        'value' => $row['location_name']
                    )
                );
            }
        }
        $data = array(
            "custom_title" => $this->lang->line('reports_inventory_summary_report'),
            "custom_subtitle" => '',
            "headers" => $model->getDataColumns(),
            "data" => $tabular_data,
            "summary_data" => $model->getSummaryData(array()),
            "export_excel" => array(
                'report' => __FUNCTION__
            )
        );
        $data['controller_name'] = strtolower(get_class());
        if ($export_excel) {
            $this->load->library('export');
            $this->export->to_excel($data['headers'], $tabular_data, str_replace(' ', '', date('Y-m-d') . '_' . $data['custom_title']));
        } else {
            $this->load->view('template/header', $data);
            $this->load->view("reports/tabular", $data);
            $this->load->view('template/footer');
        }
    }
}

/* End of file reports.php */
/* Location: ./application/controllers/reports.php */