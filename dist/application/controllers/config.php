<?php
/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
require_once ("secure_area.php");

class Config extends Secure_area
{

    function __construct()
    {
        parent::__construct('config');
    }

    /**
     * [GET] Display store config
     */
    function index()
    {
        $this->breadcrumbs->push($this->lang->line('module_config'), '/config');
        $location_names = array();
        $locations = $this->Stock_locations->get_location_names();
        foreach ($locations->result_array() as $array) {
            array_push($location_names, $array['location_name']);
        }
        $data['location_names'] = implode(',', $location_names);
        $data['controller_name'] = strtolower(get_class());
        $this->load->view('template/header', $data);
        $this->load->view("config", $data);
        $this->load->view('template/footer');
    }

    /**
     * [POST] Save configurations to database
     */
    function save()
    {
        $batch_save_data = array(
            //store config
            'company' => $this->input->post('company'),
            'address' => $this->input->post('address'),
            'city' => $this->input->post('city'),
            'state' => $this->input->post('state'),
            'zip' => $this->input->post('zip'),
            'country' => $this->input->post('country'),
            'phone' => $this->input->post('phone'),
            'fax' => $this->input->post('fax'),
            'email' => $this->input->post('email'),
            //app config
            'language' => $this->input->post('language'),
            'timezone' => $this->input->post('timezone'),
            'date_format' => $this->input->post('date_format'),
            'time_format' => $this->input->post('time_format'),
            'currency_symbol' => $this->input->post('currency_symbol'),
            'currency_side' => $this->input->post('currency_side'),
            //inv config
            'default_tax_1_rate' => $this->input->post('default_tax_1_rate'),
            'default_tax_1_name' => $this->input->post('default_tax_1_name'),
            'default_tax_2_rate' => $this->input->post('default_tax_2_rate'),
            'default_tax_2_name' => $this->input->post('default_tax_2_name'),
            'enable_custom_price' => (boolean) $this->input->post('enable_custom_price'),
            //sales config
            'print_after_sale' => (boolean) $this->input->post('print_after_sale'),
            'return_policy' => $this->input->post('return_policy') != 0 ? $this->input->post('return_policy') : '',
            'show_return_policy' => (boolean) $this->input->post('show_return_policy'),
            'show_payment_records' => (boolean) $this->input->post('show_payment_records')
        );

        $stock_locations = explode(',', $this->input->post('stock_location'));
        $stock_locations_trimmed = array();
        foreach ($stock_locations as $location) {
            array_push($stock_locations_trimmed, trim($location, ' '));
        }
        $current_locations = $this->Stock_locations->concat_location_names()->location_names;
        if ($this->input->post('stock_locations') != $current_locations) {
            $this->load->library('sale_lib');
            $this->sale_lib->clear_sale_location();
            $this->sale_lib->clear_all();
            $this->load->library('receiving_lib');
            $this->receiving_lib->clear_stock_source();
            $this->receiving_lib->clear_stock_destination();
            $this->receiving_lib->clear_all();
        }

        if ($this->Appconfig->batch_save($batch_save_data) && $this->Stock_locations->array_save($stock_locations_trimmed)) {
            set_notif('success', $this->lang->line('config_saved_successfully'));
            redirect('config');
        }
    }
}

/* End of file config.php */
/* Location: ./application/controllers/config.php */