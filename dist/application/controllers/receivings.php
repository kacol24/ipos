<?php
/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
require_once ("secure_area.php");

class Receivings extends Secure_area
{

    function __construct()
    {
        parent::__construct('receivings');
        $this->load->library('receiving_lib');
    }

    /**
     * [GET] Display the receivings panel
     */
    function index()
    {
        $this->breadcrumbs->push($this->lang->line('module_receivings'), '/receivings');
        $this->_reload();
    }

    /**
     * [AJAX] Gets the items for autocomplete suggestions
     */
    function item_search()
    {
        $query = $this->input->get('query');
        $suggestions = $this->Item->get_item_search_suggestions($query);
        $this->jsonify($suggestions);
    }

    /**
     * [AJAX] Gets the suplliers for autocomplete suggestions
     */
    function supplier_search()
    {
        $query = $this->input->get('query');
        $suggestions = $this->Supplier->get_suppliers_search_suggestions($query);
        $this->jsonify($suggestions);
    }

    /**
     * [POST] Select a supplier for the transaction
     */
    function select_supplier()
    {
        $supplier_id = $this->input->post("supplier_id");
        $this->receiving_lib->set_supplier($supplier_id);
        redirect(site_url('receivings'));
    }

    /**
     * [POST] Change transaction mode
     */
    function change_mode()
    {
        $stock_destination = $this->input->post('stock_destination');
        $stock_source = $this->input->post("stock_source");
        if (( ! $stock_source || $stock_source == $this->receiving_lib->get_stock_source()) &&
            ( ! $stock_destination || $stock_destination == $this->receiving_lib->get_stock_destination())) {
            $this->receiving_lib->empty_cart();
            $mode = $this->input->post("mode");
            $this->receiving_lib->set_mode($mode);
        } else {
            $this->receiving_lib->set_stock_source($stock_source);
            $this->receiving_lib->set_stock_destination($stock_destination);
        }
        redirect(site_url('receivings'));
    }

    /**
     * [POST] Add item to the transaction
     */
    function add()
    {
        $data = array();
        $mode = $this->receiving_lib->get_mode();
        $item_id_or_number_or_item_kit_or_receipt = $this->input->post("item_id");
        $quantity = ($mode == "receive" or $mode == "requisition") ? 1 : -1;
        $item_location = $this->receiving_lib->get_stock_source();
        if ($this->receiving_lib->is_valid_receipt($item_id_or_number_or_item_kit_or_receipt) && $mode == 'return') {
            $this->receiving_lib->return_entire_receiving($item_id_or_number_or_item_kit_or_receipt, $item_location);
        } elseif ($this->receiving_lib->is_valid_item_kit($item_id_or_number_or_item_kit_or_receipt)) {
            $this->receiving_lib->add_item_kit($item_id_or_number_or_item_kit_or_receipt, $item_location);
        } else {
            if ( ! $this->receiving_lib->add_item($item_id_or_number_or_item_kit_or_receipt, $quantity, $item_location)) {
                set_notif('danger', $this->lang->line('recvs_unable_to_add_item'));
            }
        }
        redirect(site_url('receivings'));
    }

    /**
     * [GET] Edit the item already in the transaction
     *
     * @param int $item_id item_id
     */
    function edit_item($item_id)
    {
        $data = array();

        $this->form_validation->set_rules('price', 'lang:items_price', 'numeric');
        $this->form_validation->set_rules('default_price', 'lang:items_price', 'numeric');
        $this->form_validation->set_rules('quantity', 'lang:items_quantity', 'numeric');
        $this->form_validation->set_rules('default_quantity', 'lang:items_quantity', 'numeric');
        $this->form_validation->set_rules('discount', 'lang:items_discount', 'numeric');
        $this->form_validation->set_rules('default_discount', 'lang:items_discount', 'numeric');

        $serialnumber = $this->input->post("serialnumber");
        $description = $this->input->post("description") != '' ? $this->input->post('description') : $this->input->post('default_description');
        $price = $this->input->post("price") != '' ? $this->input->post('price') : $this->input->post('default_price');
        $quantity = $this->input->post("quantity") != '' ? $this->input->post('quantity') : $this->input->post('default_quantity');
        $discount = $this->input->post("discount") != '' ? $this->input->post('discount') : $this->input->post('default_discount');
        $item_location = $this->input->post("location") != '' ? $this->input->post('location') : $this->input->post('default_location');

        if ($this->form_validation->run() != FALSE) {
            $this->receiving_lib->edit_item($item_id, $description, $serialnumber, $quantity, $discount, $price);
        } else {
            set_notif('danger', $this->lang->line('recvs_error_editing_item'));
        }
        redirect(site_url('receivings'));
    }

    /**
     * This method might not be needed and therefore being deprecated
     *
     * @todo Remove on v1
     */
    function edit_item_unit($item_id)
    {
        $data = array();

        $this->form_validation->set_rules('quantity', 'lang:items_quantity', 'required|integer');
        $quantity = $this->input->post("quantity");


        if ($this->form_validation->run() != FALSE) {
            $this->receiving_lib->edit_item_unit($item_id, $description, $quantity, 0, 0);
        } else {
            set_notif('danger', $this->lang->line('recvs_error_editing_item'));
        }
        redirect(site_url('receivings'));
    }

    /**
     * [GET] Remove item from the transaction
     *
     * @param int $line the line from cart
     */
    function delete_item($line)
    {
        $this->receiving_lib->delete_item($line);
        redirect(site_url('receivings'));
    }

    /**
     * [GET] Remove selected supplier from the transaction
     */
    function delete_supplier()
    {
        $this->receiving_lib->delete_supplier();
        redirect(site_url('receivings'));
    }

    /**
     * [POST] Finish the receivings transaction
     */
    function complete()
    {
        if ($this->input->post('cancel')) {
            $this->cancel_receiving();
        }
        $this->breadcrumbs->push($this->lang->line('module_receivings'), '/sales');
        $this->breadcrumbs->push($this->lang->line('recvs_receipt'), '/receipt');
        $data['cart'] = $this->receiving_lib->get_cart();
        $data['total'] = $this->receiving_lib->get_total();
        $data['mode'] = $this->receiving_lib->get_mode();
        $data['receipt_title'] = $this->lang->line('recvs_receipt');
        $data['transaction_time'] = date($this->config->item('date_format') . ' ' . $this->config->item('time_format'));
        $stock_locations = $this->Stock_locations->get_undeleted_all()->result_array();
        $data['show_stock_locations'] = count($stock_locations) > 1;
        $supplier_id = $this->receiving_lib->get_supplier();
        $employee_id = $this->Employee->get_logged_in_employee_info()->person_id;
        $comment = $this->input->post('comment');
        $emp_info = $this->Employee->get_info($employee_id);
        $payment_type = $this->input->post('payment_type');
        $data['payment_type'] = $this->input->post('payment_type');
        $data['stock_location'] = $this->receiving_lib->get_stock_source();

        if ($this->input->post('amount_tendered')) {
            $data['amount_tendered'] = $this->input->post('amount_tendered');
            $data['amount_change'] = to_currency($data['amount_tendered'] - round($data['total'], 2));
        }
        $data['employee'] = $emp_info->first_name . ' ' . $emp_info->last_name;

        if ($supplier_id != -1) {
            $suppl_info = $this->Supplier->get_info($supplier_id);
            $data['supplier'] = $suppl_info;
        }
        //SAVE receiving to database
        $data['receiving_id'] = 'RECV ' . $this->Receiving->save($data['cart'], $supplier_id, $employee_id, $comment, $payment_type, $data['stock_location']);

        if ($data['receiving_id'] == 'RECV -1') {
            $data['error_message'] = $this->lang->line('receivings_transaction_failed');
        }
        $data['custom_title'] = $this->lang->line('recvs_receipt');
        $data['custom_subtitle'] = $data['receiving_id'];
        $data['set_invoice'] = true;
        $data['menu_hide'] = true;
        $data['controller_name'] = strtolower(get_class());
        $this->load->view('template/header', $data);
        $this->load->view("receivings/receipt", $data);
        $this->load->view('template/footer');
        $this->receiving_lib->clear_all();
        $this->_remove_duplicate_cookies();
    }

    /**
     * [POST] Finish the requisition transaction
     */
    function requisition_complete()
    {
        if ($this->input->post('cancel')) {
            $this->cancel_receiving();
            return;
        }
        if ($this->receiving_lib->get_stock_source() != $this->receiving_lib->get_stock_destination()) {
            foreach ($this->receiving_lib->get_cart() as $item) {
                $this->receiving_lib->delete_item($item['line']);
                $this->receiving_lib->add_item($item['item_id'], $item['quantity'], $this->receiving_lib->get_stock_destination());
                $this->receiving_lib->add_item($item['item_id'], -$item['quantity'], $this->receiving_lib->get_stock_source());
            }
            $this->complete();
        } else {
            set_notif('danger', $this->lang->line('recvs_error_requisition'));
            redirect(site_url('receivings'));
        }
    }

    /**
     * [GET] Display the receivings receipt
     *
     * @param int $receiving_id receipt _id
     */
    function receipt($receiving_id)
    {
        $this->breadcrumbs->push($this->lang->line('module_receivings'), '/sales');
        $this->breadcrumbs->push($this->lang->line('recvs_receipt'), '/receipt');
        $receiving_info = $this->Receiving->get_info($receiving_id)->row_array();
        $this->receiving_lib->copy_entire_receiving($receiving_id);
        $data['cart'] = $this->receiving_lib->get_cart();
        $data['total'] = $this->receiving_lib->get_total();
        $data['mode'] = $this->receiving_lib->get_mode();
        $data['receipt_title'] = $this->lang->line('recvs_receipt');
        $data['transaction_time'] = date($this->config->item('date_format') . ' ' . $this->config->item('time_format'), strtotime($receiving_info['receiving_time']));
        $stock_locations = $this->Stock_locations->get_undeleted_all()->result_array();
        $data['show_stock_locations'] = count($stock_locations) > 1;
        $supplier_id = $this->receiving_lib->get_supplier();
        $emp_info = $this->Employee->get_info($receiving_info['employee_id']);
        $data['payment_type'] = $receiving_info['payment_type'];

        $data['employee'] = $emp_info->first_name . ' ' . $emp_info->last_name;

        if ($supplier_id != -1) {
            $supplier_info = $this->Supplier->get_info($supplier_id);
            $data['supplier'] = $supplier_info;
        }
        $data['receiving_id'] = 'RECV ' . $receiving_id;
        $data['custom_title'] = $this->lang->line('recvs_receipt');
        $data['custom_subtitle'] = $data['receiving_id'];
        $data['controller_name'] = strtolower(get_class());
        $data['set_invoice'] = true;
        $data['menu_hide'] = true;
        $this->load->view('template/header', $data);
        $this->load->view("receivings/receipt", $data);
        $this->load->view('template/footer');
        $this->receiving_lib->clear_all();
        $this->_remove_duplicate_cookies();
    }

    /**
     * Handles data for receivings panel
     *
     * @access private
     *
     * @param array $data all data passed will be passed to the view
     */
    function _reload($data = array())
    {
        $data['stock_locations'] = array();
        $stock_locations = $this->Stock_locations->get_undeleted_all()->result_array();
        $show_stock_locations = count($stock_locations) > 1;

        $person_info = $this->Employee->get_logged_in_employee_info();
        $data['cart'] = $this->receiving_lib->get_cart();
        $data['modes'] = array('receive' => $this->lang->line('recvs_receiving'), 'return' => $this->lang->line('recvs_return'));

        $data['mode'] = $this->receiving_lib->get_mode();

        if ($show_stock_locations) {
            $data['modes']['requisition'] = $this->lang->line('recvs_requisition');
            foreach ($stock_locations as $location_data) {
                $data['stock_locations'][$location_data['location_id']] = $location_data['location_name'];
            }

            $data['stock_source'] = $this->receiving_lib->get_stock_source();
            $data['stock_destination'] = $this->receiving_lib->get_stock_destination();
        }
        $data['show_stock_locations'] = $show_stock_locations;

        $data['total'] = $this->receiving_lib->get_total();
        $data['items_module_allowed'] = $this->Employee->has_permission('items', $person_info->person_id);
        $data['payment_options'] = array(
            $this->lang->line('sales_cash') => $this->lang->line('sales_cash'),
            $this->lang->line('sales_check') => $this->lang->line('sales_check'),
            $this->lang->line('sales_debit') => $this->lang->line('sales_debit'),
            $this->lang->line('sales_credit') => $this->lang->line('sales_credit')
        );

        $supplier_id = $this->receiving_lib->get_supplier();
        if ($supplier_id != -1) {
            $info = $this->Supplier->get_info($supplier_id);
            $data['supplier'] = '[' . $info->company_name . '] ' . $info->first_name . ' ' . $info->last_name;
        }
        $data['menu_hide'] = true;
        $data['controller_name'] = strtolower(get_class());
        $this->load->view("template/header", $data);
        $this->load->view("receivings/receiving", $data);
        $this->load->view("template/footer");
        $this->_remove_duplicate_cookies();
    }

    /**
     * [GET] Cancel the receiving transaction
     */
    function cancel_receiving()
    {
        $this->receiving_lib->clear_all();
        redirect(site_url('receivings'));
    }
}
