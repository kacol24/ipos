<?php
/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
if ( ! defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    /**
     * [GET] Display the login form
     */
    function index()
    {
        if ( ! $this->db->count_all_results($this->db->dbprefix('app_config'))) {
            die('Application is not setup properly, visit <a href="' . site_url('install') . '">this link</a> to setup');
        }
        if ($this->Employee->is_logged_in()) {
            redirect('home');
        } else {
            $this->form_validation->set_rules('username', 'lang:login_username', 'callback_login_check');
            $this->form_validation->set_error_delimiters('<div class="callout callout-danger"><p>', '</p></div>');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('login');
            } else {
                redirect('home');
            }
        }
    }

    /**
     * Custom callback function for checking login
     *
     * @param string $username
     * @return boolean Correct user/password or not
     */
    function login_check($username)
    {
        $password = $this->input->post("password");

        if ( ! $this->Employee->login($username, $password)) {
            $this->form_validation->set_message('login_check', $this->lang->line('login_invalid_username_and_password'));
            return false;
        }
        return true;
    }
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */