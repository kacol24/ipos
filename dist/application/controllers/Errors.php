<?php
/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
require_once ("secure_area.php");

class Errors extends Secure_area
{

    function __construct()
    {
        parent::__construct();
    }

    /**
     * [GET] Handles permission restriction
     *
     * For employees that doesn't have permission to access a module
     * passing in the module id that user tries to access
     *
     * @param string $module_id default ''
     */
    function no_access($module_id = '')
    {
        $this->output->set_status_header('401');
        $data['module_name'] = $this->Module->get_module_name($module_id);
        $data['custom_title'] = '';
        $data['custom_subtitle'] = '';
        $this->load->view('template/header', $data);
        $this->load->view('errors/no_access', $data);
        $this->load->view('template/footer');
    }

    /**
     * [GET] Handles error 404s
     */
    function error_404()
    {
        show_404();
    }
}

/* End of file Errors.php */
/* Location: ./application/controllers/Errors.php */