<?php

/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
class Inventory extends CI_Model
{

    function insert($inventory_data)
    {
        return $this->db->insert('inventory', $inventory_data);
    }

    function get_inventory_data_for_item($item_id, $location_id = false)
    {
        $this->db->from('inventory');
        $this->db->where('trans_items', $item_id);
        if ($location_id != false) {
            $this->db->where('trans_location', $location_id);
        }
        $this->db->order_by("trans_date", "desc");
        return $this->db->get();
    }
}
