<?php

/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
class Item_quantities extends CI_Model
{

    function exists($item_id, $location_id)
    {
        $this->db->from('item_quantities');
        $this->db->where('item_id', $item_id);
        $this->db->where('location_id', $location_id);
        $query = $this->db->get();

        return ($query->num_rows() == 1);
    }

    function save($location_detail, $item_id, $location_id)
    {
        if ( ! ($item_id && $location_id) or ! $this->exists($item_id, $location_id)) {
            if ($this->db->insert('item_quantities', $location_detail)) {
                return true;
            }
            return false;
        }

        $this->db->where('item_id', $item_id);
        $this->db->where('location_id', $location_id);
        return $this->db->update('item_quantities', $location_detail);
    }

    function get_item_quantity($item_id, $location_id)
    {
        $this->db->from('item_quantities');
        $this->db->where('item_id', $item_id);
        $this->db->where('location_id', $location_id);
        $result = $this->db->get()->row();
        if (empty($result) == true) {
            //Get empty base parent object, as $item_id is NOT an item
            $result = new stdClass();
            //Get all the fields from items table (TODO to be reviewed)
            $fields = $this->db->list_fields('item_quantities');
            foreach ($fields as $field) {
                $result->$field = '';
            }
            $result->quantity = 0;
        }
        return $result;
    }
}

?>