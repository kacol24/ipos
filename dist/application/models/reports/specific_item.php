<?php
/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
require_once("report.php");

class Specific_item extends Report
{

    function __construct()
    {
        parent::__construct();
    }

    public function getDataColumns()
    {
        return array(
            'summary' => array(
                $this->lang->line('reports_sale_id'),
                $this->lang->line('reports_date'),
                $this->lang->line('reports_quantity_purchased'),
                $this->lang->line('reports_sold_by'),
                $this->lang->line('reports_sold_to')
            ),
            'details' => array(
                $this->lang->line('reports_item_number'),
                $this->lang->line('reports_name'),
                $this->lang->line('reports_category'),
                $this->lang->line('sales_quantity'),
                $this->lang->line('reports_subtotal'),
                $this->lang->line('reports_tax'),
                $this->lang->line('reports_total'),
                $this->lang->line('reports_profit'),
                $this->lang->line('reports_discount')
            )
        );
    }

    public function getData(array $inputs)
    {
        $this->db->select('sale_date, name, sum(quantity_purchased) as quantity_purchased');
        $this->db->from('sales_items_temp');
        $this->db->join('items', 'sales_items_temp.item_id = items.item_id');
        $this->db->where('sales_items_temp.item_id', $inputs['item_id']);
        $this->db->where('sale_date BETWEEN "' . $inputs['start_date'] . '" and "' . $inputs['end_date'] . '"');
        if ($inputs['sale_type'] == 'sales') {
            $this->db->where('quantity_purchased > 0');
        } elseif ($inputs['sale_type'] == 'returns') {
            $this->db->where('quantity_purchased < 0');
        }
        $this->db->group_by('sales_items_temp.sale_date');
        $this->db->order_by('sales_items_temp.sale_date');

        return $this->db->get()->result_array();
    }

    public function getDetailsData(array $inputs)
    {
        $this->db->select('sale_id, sale_time, quantity_purchased, CONCAT(employee.first_name," ",employee.last_name) as employee_name, CONCAT(customer.first_name," ",customer.last_name) as customer_name', false);
        $this->db->join('people as employee', 'sales_items_temp.employee_id = employee.person_id');
        $this->db->join('people as customer', 'sales_items_temp.customer_id = customer.person_id', 'left');
        $this->db->from('sales_items_temp');
        $this->db->where('sales_items_temp.item_id', $inputs['item_id']);
        $this->db->where('sale_date BETWEEN "' . $inputs['start_date'] . '" and "' . $inputs['end_date'] . '"');
        if ($inputs['sale_type'] == 'sales') {
            $this->db->where('quantity_purchased > 0');
        } elseif ($inputs['sale_type'] == 'returns') {
            $this->db->where('quantity_purchased < 0');
        }
        $this->db->order_by('sales_items_temp.sale_date');

        $data = array();
        $data['summary'] = $this->db->get()->result_array();
        $data['details'] = array();

        foreach ($data['summary'] as $key => $value) {
            $this->db->select('item_number, name, category, quantity_purchased, serialnumber, sales_items_temp.description, subtotal,total, tax, profit, discount_percent');
            $this->db->from('sales_items_temp');
            $this->db->join('items', 'sales_items_temp.item_id = items.item_id');
            $this->db->where('sale_id = ' . $value['sale_id']);
            $data['details'][$key] = $this->db->get()->result_array();
        }

        return $data;
    }

    public function getSummaryData(array $inputs)
    {
        $this->db->select('sum(subtotal) as subtotal, sum(total) as total, sum(tax) as tax, sum(profit) as profit');
        $this->db->from('sales_items_temp');
        $this->db->join('items', 'sales_items_temp.item_id = items.item_id');
        $this->db->where('sales_items_temp.item_id', $inputs['item_id']);
        $this->db->where('sale_date BETWEEN "' . $inputs['start_date'] . '" and "' . $inputs['end_date'] . '"');
        if ($inputs['sale_type'] == 'sales') {
            $this->db->where('quantity_purchased > 0');
        } elseif ($inputs['sale_type'] == 'returns') {
            $this->db->where('quantity_purchased < 0');
        }
        return $this->db->get()->row_array();
    }

    /**
     * [GET] Gets graph data for graphical summary items
     *
     * @param string $start_date
     * @param string $end_date
     * @param string $sale_type
     * @return array
     */
    function graphical_summary_items_graph($start_date, $end_date, $item_id, $sale_type)
    {
        $report_data = $this->getData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type, 'item_id' => $item_id));

        $graph_data = array();
        $graph_title = '';

        foreach ($report_data as $row) {
            $graph_data[$row['sale_date']] = $row['quantity_purchased'];
            $graph_title = $row['name'];
        }

        $data = array(
            "title" => $graph_title,
            "xaxis_label" => $this->lang->line('reports_date'),
            "yaxis_label" => $this->lang->line('reports_quantity_purchased'),
            "graph" => $graph_data
        );
        return $data;
    }
}
