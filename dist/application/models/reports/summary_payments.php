<?php
/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
require_once("report.php");

class Summary_payments extends Report
{

    function __construct()
    {
        parent::__construct();
    }

    public function getDataColumns()
    {
        return array($this->lang->line('reports_payment_type'), $this->lang->line('reports_total'));
    }

    public function getData(array $inputs)
    {
        $this->db->select('sales_payments.payment_type, SUM(payment_amount) as payment_amount', false);
        $this->db->from('sales_payments');
        $this->db->join('sales', 'sales.sale_id=sales_payments.sale_id');
        $this->db->join('sales_items', 'sales_items.sale_id=sales_payments.sale_id');
        $this->db->where('date(sale_time) BETWEEN "' . $inputs['start_date'] . '" and "' . $inputs['end_date'] . '"');
        if ($inputs['sale_type'] == 'sales') {
            $this->db->where('quantity_purchased > 0');
        } elseif ($inputs['sale_type'] == 'returns') {
            $this->db->where('quantity_purchased < 0');
        }
        $this->db->group_by("payment_type");
        return $this->db->get()->result_array();
    }

    public function getSummaryData(array $inputs)
    {
        $this->db->select('sum(subtotal) as subtotal, sum(total) as total, sum(tax) as tax, sum(profit) as profit');
        $this->db->from('sales_items_temp');
        $this->db->join('items', 'sales_items_temp.item_id = items.item_id');
        $this->db->where('sale_date BETWEEN "' . $inputs['start_date'] . '" and "' . $inputs['end_date'] . '"');
        if ($inputs['sale_type'] == 'sales') {
            $this->db->where('quantity_purchased > 0');
        } elseif ($inputs['sale_type'] == 'returns') {
            $this->db->where('quantity_purchased < 0');
        }
        return $this->db->get()->row_array();
    }

    /**
     * [GET] Gets graph data for graphical summary payments
     *
     * @param string $start_date
     * @param string $end_date
     * @param string $sale_type
     * @return array
     */
    function graphical_summary_payments_graph($start_date, $end_date, $sale_type)
    {
        $report_data = $this->getData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type));

        $graph_data = array();
        foreach ($report_data as $row) {
            $graph_data[lang($row['payment_type'])] = $row['payment_amount'];
        }

        $data = array(
            "title" => $this->lang->line('reports_payments_summary_report'),
            "yaxis_label" => $this->lang->line('reports_revenue'),
            "xaxis_label" => $this->lang->line('reports_payment_type'),
            "graph" => $graph_data
        );

        return $data;
    }
}
