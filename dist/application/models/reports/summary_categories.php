<?php
/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
require_once("report.php");

class Summary_categories extends Report
{

    function __construct()
    {
        parent::__construct();
    }

    public function getDataColumns()
    {
        return array($this->lang->line('reports_category'), $this->lang->line('reports_subtotal'), $this->lang->line('reports_total'), $this->lang->line('reports_tax'), $this->lang->line('reports_profit'));
    }

    public function getData(array $inputs)
    {
        $this->db->select('category, sum(subtotal) as subtotal, sum(total) as total, sum(tax) as tax, sum(profit) as profit');
        $this->db->from('sales_items_temp');
        $this->db->join('items', 'sales_items_temp.item_id = items.item_id');
        $this->db->where('sale_date BETWEEN "' . $inputs['start_date'] . '" and "' . $inputs['end_date'] . '"');
        if ($inputs['sale_type'] == 'sales') {
            $this->db->where('quantity_purchased > 0');
        } elseif ($inputs['sale_type'] == 'returns') {
            $this->db->where('quantity_purchased < 0');
        }
        $this->db->group_by('category');
        $this->db->order_by('category');

        return $this->db->get()->result_array();
    }

    public function getSummaryData(array $inputs)
    {
        $this->db->select('sum(subtotal) as subtotal, sum(total) as total, sum(tax) as tax, sum(profit) as profit');
        $this->db->from('sales_items_temp');
        $this->db->join('items', 'sales_items_temp.item_id = items.item_id');
        $this->db->where('sale_date BETWEEN "' . $inputs['start_date'] . '" and "' . $inputs['end_date'] . '"');
        if ($inputs['sale_type'] == 'sales') {
            $this->db->where('quantity_purchased > 0');
        } elseif ($inputs['sale_type'] == 'returns') {
            $this->db->where('quantity_purchased < 0');
        }

        return $this->db->get()->row_array();
    }

    /**
     * [GET] Gets graph data for graphical summary categories
     *
     * @param string $start_date
     * @param string $end_date
     * @param string $sale_type
     * @return array
     */
    function graphical_summary_categories_graph($start_date, $end_date, $sale_type)
    {
        $report_data = $this->getData(array('start_date' => $start_date, 'end_date' => $end_date, 'sale_type' => $sale_type));

        $graph_data = array();
        foreach ($report_data as $row) {
            $graph_data[$row['category']] = $row['total'];
        }

        $data = array(
            "title" => $this->lang->line('reports_categories_summary_report'),
            "graph" => $graph_data
        );

        return $data;
    }
}
