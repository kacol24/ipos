<?php
/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
require_once("report.php");

class Inventory_low extends Report
{

    function __construct()
    {
        parent::__construct();
    }

    public function getDataColumns()
    {
        return array($this->lang->line('reports_item_number'), $this->lang->line('reports_item_name'), $this->lang->line('reports_description'), $this->lang->line('reports_count'), $this->lang->line('reports_reorder_level'), $this->lang->line('reports_stock_location'));
    }

    public function getData(array $inputs)
    {
        $this->db->from('items');
        $this->db->join('item_quantities', 'items.item_id=item_quantities.item_id');
        $this->db->join('stock_locations', 'item_quantities.location_id=stock_locations.location_id');

        $this->db->select('name, item_number, reorder_level, item_quantities.quantity,description,location_name');

        $this->db->where('item_quantities.quantity <= reorder_level');
        $this->db->where('items.deleted = 0');

//        $this->db->order_by('name');
        $this->db->order_by('(`reorder_level` - `' . $this->db->dbprefix('item_quantities') . '`.`quantity`)', 'DESC');

        return $this->db->get()->result_array();
    }

    public function getSummaryData(array $inputs)
    {
        return array();
    }
}
