<?php

/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
class Item_custom_unit_price extends CI_Model
{

    private $_table = 'item_custom_unit_prices';
    private $_item_id = 'item_id';
    private $_customer_id = 'person_id';
    private $_unit_price = 'unit_price';

    /**
     * Fetch the customers related to $item_id
     *
     * @param int $item_id ID of the item
     * @return object DB connection instance
     */
    function get_customers($item_id)
    {
        $this->db->from($this->_table);
        $this->db->join('people', 'people.person_id=' . $this->_table . '.' . $this->_customer_id);
        $this->db->where($this->_item_id, $item_id);
        return $this->db->get();
    }

    /**
     * Fetch the items related to $customer_id
     *
     * @param int $customer_id ID of the customer
     * @return object DB connection instance
     */
    function get_items($customer_id)
    {
        $this->db->select($this->_table . '.unit_price, ' . $this->_table . '.item_id, ' . $this->_table . '.person_id, items.item_number, items.name');
        $this->db->from($this->_table);
        $this->db->join('items', 'items.item_id=' . $this->_table . '.' . $this->_item_id);
        $this->db->where($this->_customer_id, $customer_id);
        return $this->db->get();
    }

    /**
     * Insert a custom unit price for the related customer/item
     *
     * @param array $data array containing the data to save
     * @param int $item_id the item_id currently being updated
     * @return boolean success/fail
     */
    function save($data, $item_id = false, $customer_id = false)
    {
        $this->delete($item_id, $customer_id);
        $counter = 0;
        foreach ($data as $row) {
            if ($this->db->insert($this->_table, $row)) {
                $counter ++;
            }
        }
        if ($counter == count($data)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Delete the related item_id-customer_id from the table
     *
     * @param int $item_id The ID of the item
     * @param int $customer_id The ID of the customer
     * @return boolean Success/failed delete
     */
    function delete($item_id = false, $customer_id = false)
    {
        if ($item_id) {
            $where[$this->_item_id] = $item_id;
        }
        if ($customer_id) {
            $where[$this->_customer_id] = $customer_id;
        }
        return $this->db->delete($this->_table, $where);
    }

    /**
     * Update the unit price of a specific item-customer
     *
     * @param int $item_id The ID of the item
     * @param int $customer_id The ID of the customer
     * @param int $unit_price The new unit price to update
     * @return boolean Success/failed update
     */
    function update($item_id, $customer_id, $unit_price)
    {
        $value = array(
            $this->_unit_price => $unit_price
        );
        $where = array(
            $this->_item_id => $item_id,
            $this->_customer_id => $customer_id
        );
        return $this->db->update($this->_table, $value, $where);
    }

    /**
     * Search the database if customer has custom price or not
     *
     * @param int $customer_id the id of the customer
     * @return boolean has custom price or not
     */
    function customer_has_custom_price($customer_id)
    {
        $this->db->from($this->_table);
        $this->db->where($this->_customer_id, $customer_id);
        $query = $this->db->get();
        return ($query->num_rows() > 0);
    }
}
