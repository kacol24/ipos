<?php

/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
class Event extends CI_Model
{

    private $_table = 'events';

    /**
     *
     * @param array $event_data Data of the event to log (name and description)
     * @param int $employee_id The currently logged in employee (optional)
     * @return int Last inserted id
     */
    public function log($event_data, $employee_id = null)
    {
        if ($employee_id == null) {
            $employee_id = $this->Employee->get_logged_in_employee_info()->person_id;
        }
        $event_data['employee_id'] = $employee_id;
        $this->db->insert($this->db->dbprefix($this->_table), $event_data);
        return $this->db->insert_id();
    }

    /**
     *
     * @param int $limit The limit number of rows to return (default 10000)
     * @param int $offset The offset of data for pagination (default 0)
     * @return dbObject Database object of the query
     */
    public function get_all($limit = 10000, $offset = 0)
    {
        $this->db->from($this->db->dbprefix($this->_table));
        $this->db->join('people', $this->db->dbprefix($this->_table) . '.employee_id=' . $this->db->dbprefix('people') . '.person_id');
        $this->db->order_by('event_id', 'desc');
        $this->db->limit($limit);
        $this->db->offset($offset);
        return $this->db->get();
    }
}
