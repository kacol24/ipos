<?php
/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
if ( ! defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Exceptions extends CI_Exceptions
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 404 Page Not Found Handler [custom]
     *
     * @access private
     * @param bool $log_error log error yes/no
     */
    function show_404($page = '', $log_error = TRUE)
    {
        $CI = & get_instance();
        if ($log_error) {
            if (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] != '') {
                $referer = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
                log_message('error', '404 Page Not Found [REFERRED] --> ' . $referer);
            } else {
                log_message('error', '404 Page Not Found --> ' . current_url());
            }
        }
        $CI->output->set_status_header('404');
        $data['custom_title'] = 'Error 404';
        $data['custom_subtitle'] = lang('error_404_oops');
        $CI->load->view('template/header', $data);
        $CI->load->view('errors/error_404');
        $CI->load->view('template/footer');
        echo $CI->output->get_output();
        exit;
    }

    function show_error($heading, $message, $template = 'error_general', $status_code = 500)
    {
        if ($status_code == 500) {
            $this->_report_error($message);
        }

        return parent::show_error($heading, $message, $template, $status_code);
    }

    function log_exception($severity, $message, $filepath, $line)
    {
        parent::log_exception($severity, $message, $filepath, $line);

        if ($severity != E_WARNING && $severity != E_NOTICE && $severity != E_STRICT) {
            $this->_report_error($message);
        }
    }

    function _get_debug_backtrace($br = "<BR>")
    {
        $trace = array_slice(debug_backtrace(), 3);
        $msg = '<code>';
        foreach ($trace as $index => $info) {
            if (isset($info['file'])) {
                $msg .= $info['file'] . ':' . $info['line'] . " -> " . $info['function'] . $br;
            }
        }
        $msg .= '</code>';
        return $msg;
    }

    function _report_error($subject)
    {
        $CI = & get_instance();

        $CI->load->library('email');

        $body = '';

        $body .= 'Request: <br/><br/><code>';
        foreach ($_REQUEST as $k => $v) {
            $body .= $k . ' => ' . $v . '<br/>';
        }
        $body .= '</code>';

        $body .= '<br/><br/>Server: <br/><br/><code>';
        foreach ($_SERVER as $k => $v) {
            $body .= $k . ' => ' . $v . '<br/>';
        }
        $body .= '</code>';

        $body .= '<br/><br/>Stacktrace: <br/><br/>';
        $body .= $this->_get_debug_backtrace();

//        $CI->email->from('iPOS.bug@domain.com', '[iPOS production] iPOS Exception Caught!');
//        $CI->email->to('kacol.bot@gmail.com');
//        $CI->email->subject($subject);
//        $CI->email->message($body);
//        $CI->email->send();
    }
}
