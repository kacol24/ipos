<?php

/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
class Setup_lib
{

    var $CI,
        $_db,
        $_dbforge;

    function __construct()
    {
        $this->CI = & get_instance();
        $this->CI->load->dbforge();
        $this->_dbforge = $this->CI->dbforge;
        $this->_db = $this->CI->db;
    }

    /**
     * Create tables on the db
     *
     * @return void
     */
    public function create_tables()
    {
        $dump = file_get_contents(APPPATH . '../dbdump/db.sql');
        return $this->_db->query($dump);

//        $this->_create_app_config_table();
//        $this->_create_customers_table();
//        $this->_create_employees_table();
//        $this->_create_giftcards_table();
//        $this->_create_inventory_table();
//        $this->_create_items_table();
//        $this->_create_items_taxes_table();
//        $this->_create_item_kits_table();
//        $this->_create_item_kit_items_table();
//        $this->_create_item_quantities_table();
//        $this->_create_modules_table();
//        $this->_create_payment_records_table();
//        $this->_create_payment_types_table(); //TODO system not yet complete
//        $this->_create_people_table();
//        $this->_create_permissions_table();
//        $this->_create_receivings_table();
//        $this->_create_receivings_items_table();
//        $this->_create_reports_table();
//        $this->_create_sales_table();
//        $this->_create_sales_discounts_table();
//        $this->_create_sales_items_table();
//        $this->_create_sales_items_taxes_table();
//        $this->_create_sales_payments_table();
//        $this->_create_sales_suspended_table();
//        $this->_create_sales_suspended_items_table();
//        $this->_create_sales_suspended_items_taxes_table();
//        $this->_create_sales_suspended_payments_table();
//        $this->_create_sessions_table(); // mandatory for codeigniter session storing
//        $this->_create_stock_locations_table();
//        $this->_create_suppliers_table();
    }

    /**
     * Insert default data to database
     *
     * @return void
     */
    public function populate_defaults()
    {
        $this->_populate_people_table();
//        $this->_populate_employees_table();
//        $this->_populate_permissions_table();
        $this->_populate_app_config_table();
        $this->_populate_stock_locations_table();
//        $this->_populate_modules_table(); // should be installed by default
//        $this->_populate_payment_types_table(); // should be installed by default
//        $this->_populate_reports_table(); // should be installed by default
    }

    protected function _prefix($table_name)
    {
        return $this->_db->dbprefix($table_name);
    }

    private function _create_app_config_table()
    {
        $fields = array(
            'key' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
            'value' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            )
        );
        $this->_dbforge->add_fields($fields);
        $this->_dbforge->add_key('key', TRUE);
        $this->_dbforge->create_table($this->_prefix('app_config'), TRUE);
    }

    private function _create_customers_table()
    {
        $fields = array(
            'person_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'account_number' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => TRUE,
                'default' => 'NULL'
            ),
            'customer_discount' => array(
                'type' => 'INT',
                'constraint' => 11,
                'default' => '0'
            ),
            'taxable' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => '1'
            ),
            'deleted' => array(
                'type' => 'INT',
                'constraint' => 1,
                'default' => '0'
            )
        );
        $this->_dbforge->add_fields($fields);
        $this->_dbforge->add_key('account_number');
        $this->_dbforge->add_key('person_id', TRUE);
        $this->_dbforge->create_table($this->_prefix('customers'), TRUE);
    }

    private function _create_employees_table()
    {

    }

    private function _create_giftcards_table()
    {

    }

    private function _create_inventory_table()
    {

    }

    private function _create_items_table()
    {

    }

    private function _create_items_taxes_table()
    {

    }

    private function _create_item_kits_table()
    {

    }

    private function _create_item_kit_items_table()
    {

    }

    private function _create_item_quantities_table()
    {

    }

    private function _create_modules_table()
    {

    }

    private function _create_payment_records_table()
    {

    }

    private function _create_payment_types_table()
    {

    }

    private function _create_people_table()
    {

    }

    private function _create_permissions_table()
    {

    }

    private function _create_receivings_table()
    {

    }

    private function _create_receivings_items_table()
    {

    }

    private function _create_reports_table()
    {

    }

    private function _create_sales_table()
    {

    }

    private function _create_sales_discounts_table()
    {

    }

    private function _create_sales_items_table()
    {

    }

    private function _create_sales_items_taxes_table()
    {

    }

    private function _create_sales_payments_table()
    {

    }

    private function _create_sales_suspended_table()
    {

    }

    private function _create_sales_suspended_items_table()
    {

    }

    private function _create_sales_suspended_items_taxes_table()
    {

    }

    private function _create_sales_suspended_payments_table()
    {

    }

    private function _create_sessions_table()
    {

    }

    private function _create_stock_locations_table()
    {

    }

    private function _create_suppliers_table()
    {

    }

    private function _populate_app_config_table()
    {
        $company = $this->CI->input->post('company');
        $address = $this->CI->input->post('company_address');
        $city = $this->CI->input->post('company_city');
        $state = $this->CI->input->post('company_state');
        $zip = $this->CI->input->post('company_zip');
        $country = $this->CI->input->post('company_country');
        $phone = $this->CI->input->post('company_phone');
        $fax = $this->CI->input->post('company_fax');
        $email = $this->CI->input->post('company_email');
        $language = $this->CI->input->post('language');
        $timezone = $this->CI->input->post('timezone');
        $date_format = $this->CI->input->post('date_format');
        $time_format = $this->CI->input->post('time_format');
        $currency_symbol = $this->CI->input->post('currency_symbol');
        $currency_side = (bool) $this->CI->input->post('currency_side');
        $data = array(
            array(
                'key' => 'company',
                'value' => $company
            ),
            array(
                'key' => 'address',
                'value' => $address
            ),
            array(
                'key' => 'city',
                'value' => $city
            ),
            array(
                'key' => 'state',
                'value' => $state
            ),
            array(
                'key' => 'zip',
                'value' => $zip
            ),
            array(
                'key' => 'country',
                'value' => $country
            ),
            array(
                'key' => 'phone',
                'value' => $phone
            ),
            array(
                'key' => 'fax',
                'value' => $fax
            ),
            array(
                'key' => 'email',
                'value' => $email
            ),
            array(
                'key' => 'language',
                'value' => $language
            ),
            array(
                'key' => 'timezone',
                'value' => $timezone
            ),
            array(
                'key' => 'date_format',
                'value' => $date_format
            ),
            array(
                'key' => 'time_format',
                'value' => $time_format
            ),
            array(
                'key' => 'currency_symbol',
                'value' => $currency_symbol
            ),
            array(
                'key' => 'currency_side',
                'value' => $currency_side
            )
        );
        return $this->_db->insert_batch($this->_prefix('app_config'), $data);
    }

    public function populate_employees_table()
    {
        $username = $this->CI->input->post('username');
        $password = $this->CI->input->post('password');
        $salt = random_string('unique');
        $data = array(
            'username' => $username,
            'password' => encrypt($password, $salt),
            'salt' => $salt,
            'person_id' => 1
        );
        $this->_db->insert($this->_prefix('employees'), $data);
    }

    private function _populate_modules_table()
    {
        $data = array(
            array(
                'name_lang_key' => 'module_home',
                'desc_lang_key' => 'module_home_desc',
                'sort' => 0,
                'module_id' => 'home',
                'module_icon' => 'fa fa-dashboard'
            ),
            array(
                'name_lang_key' => 'module_customers',
                'desc_lang_key' => 'module_customers_desc',
                'sort' => 10,
                'module_id' => 'customers',
                'module_icon' => 'fa fa-user'
            ),
            array(
                'name_lang_key' => 'module_items',
                'desc_lang_key' => 'module_items_desc',
                'sort' => 20,
                'module_id' => 'items',
                'module_icon' => 'fa fa-tags'
            ),
            array(
                'name_lang_key' => 'module_item_kits',
                'desc_lang_key' => 'module_item_kits_desc',
                'sort' => 30,
                'module_id' => 'item_kits',
                'module_icon' => 'fa fa-archive'
            ),
            array(
                'name_lang_key' => 'module_suppliers',
                'desc_lang_key' => 'module_suppliers_desc',
                'sort' => 40,
                'module_id' => 'suppliers',
                'module_icon' => 'fa fa-reply-all'
            ),
            array(
                'name_lang_key' => 'module_reports',
                'desc_lang_key' => 'module_reports_desc',
                'sort' => 50,
                'module_id' => 'reports',
                'module_icon' => 'fa fa-bar-chart-o'
            ),
            array(
                'name_lang_key' => 'module_receivings',
                'desc_lang_key' => 'module_receivings_desc',
                'sort' => 60,
                'module_id' => 'receivings',
                'module_icon' => 'fa fa-truck'
            ),
            array(
                'name_lang_key' => 'module_sales',
                'desc_lang_key' => 'module_sales_desc',
                'sort' => 70,
                'module_id' => 'sales',
                'module_icon' => 'fa fa-shopping-cart'
            ),
            array(
                'name_lang_key' => 'module_employees',
                'desc_lang_key' => 'module_employees_desc',
                'sort' => 80,
                'module_id' => 'employees',
                'module_icon' => 'fa fa-male'
            ),
            array(
                'name_lang_key' => 'module_giftcards',
                'desc_lang_key' => 'module_giftcards_desc',
                'sort' => 90,
                'module_id' => 'giftcards',
                'module_icon' => 'fa fa-credit-card'
            ),
            array(
                'name_lang_key' => 'module_config',
                'desc_lang_key' => 'module_config_desc',
                'sort' => 100,
                'module_id' => 'config',
                'module_icon' => 'fa fa-gears'
            )
        );
        return $this->_db->insert_batch($this->_prefix('app_config'), $data);
    }

    private function _populate_payment_types_table()
    {
        $data = array(
            array(
                'payment_type' => 'sales_cash'
            ),
            array(
                'payment_type' => 'sales_check'
            ),
            array(
                'payment_type' => 'sales_giftcard'
            ),
            array(
                'payment_type' => 'sales_debit'
            ),
            array(
                'payment_type' => 'sales_credit'
            )
        );
        return $this->_db->insert_batch($this->_prefix('payment_types'), $data);
    }

    private function _populate_people_table()
    {
        $fisrt_name = $this->CI->input->post('first_name');
        $last_name = $this->CI->input->post('last_name');
        $email = $this->CI->input->post('email');
        $phone_number = $this->CI->input->post('phone_number');
        $address_1 = $this->CI->input->post('address_1');
        $address_2 = $this->CI->input->post('address_2');
        $city = $this->CI->input->post('city');
        $state = $this->CI->input->post('state');
        $country = $this->CI->input->post('country');
        $comment = $this->CI->input->post('comment');

        $data = array(
            'first_name' => $fisrt_name,
            'last_name' => $last_name,
            'email' => $email,
            'phone_number' => $phone_number,
            'address_1' => $address_1,
            'address_2' => $address_2,
            'city' => $city,
            'state' => $state,
            'country' => $country,
            'comments' => $comment
        );
        $this->_db->insert($this->_prefix('people'), $data);
    }

    public function populate_permissions_table()
    {
        $data = array(
            array(
                'module_id' => 'home',
                'person_id' => 1
            ),
            array(
                'module_id' => 'customers',
                'person_id' => 1
            ),
            array(
                'module_id' => 'items',
                'person_id' => 1
            ),
            array(
                'module_id' => 'item_kits',
                'person_id' => 1
            ),
            array(
                'module_id' => 'suppliers',
                'person_id' => 1
            ),
            array(
                'module_id' => 'reports',
                'person_id' => 1
            ),
            array(
                'module_id' => 'receivings',
                'person_id' => 1
            ),
            array(
                'module_id' => 'sales',
                'person_id' => 1
            ),
            array(
                'module_id' => 'employees',
                'person_id' => 1
            ),
            array(
                'module_id' => 'giftcards',
                'person_id' => 1
            ),
            array(
                'module_id' => 'config',
                'person_id' => 1
            )
        );
        return $this->_db->insert_batch($this->_prefix('permissions'), $data);
    }

    private function _populate_reports_table()
    {
        $data = array(
            array(
                'name' => 'report_sales',
                'type' => 'reports_graphical_reports',
                'icon' => 'fa fa-shopping-cart',
                'link' => 'graphical_summary_sales'
            ),
            array(
                'name' => 'reports_categories',
                'type' => 'reports_graphical_reports',
                'icon' => 'fa fa-sitemap',
                'link' => 'graphical_summary_categories'
            ),
            array(
                'name' => 'reports_customers',
                'type' => 'reports_graphical_reports',
                'icon' => 'fa fa-user',
                'link' => 'graphical_summary_customers'
            ),
            array(
                'name' => 'reports_suppliers',
                'type' => 'reports_graphical_reports',
                'icon' => 'fa fa-reply-all',
                'link' => 'graphical_summary_suppliers'
            ),
            array(
                'name' => 'reports_items',
                'type' => 'reports_graphical_reports',
                'icon' => 'fa fa-tags',
                'link' => 'graphical_summary_items'
            ),
            array(
                'name' => 'reports_employees',
                'type' => 'reports_graphical_reports',
                'icon' => 'fa fa-male',
                'link' => 'graphical_summary_employees'
            ),
            array(
                'name' => 'reports_taxes',
                'type' => 'reports_graphical_reports',
                'icon' => 'fa fa-dollar',
                'link' => 'graphical_summary_taxes',
                'deleted' => 1
            ),
            array(
                'name' => 'reports_discounts',
                'type' => 'reports_graphical_reports',
                'icon' => 'fa',
                'link' => 'graphical_summary_discounts',
                'deleted' => 1
            ),
            array(
                'name' => 'reports_payments',
                'type' => 'reports_graphical_reports',
                'icon' => 'fa fa-money',
                'link' => 'graphical_summary_payments'
            ),
            array(
                'name' => 'reports_sales',
                'type' => 'reports_summary_reports',
                'icon' => 'fa fa-shopping-cart',
                'link' => 'summary_sales'
            ),
            array(
                'name' => 'reports_categories',
                'type' => 'reports_summary_reports',
                'icon' => 'fa fa-sitemap',
                'link' => 'summary_categories'
            ),
            array(
                'name' => 'reports_customers',
                'type' => 'reports_summary_reports',
                'icon' => 'fa fa-user',
                'link' => 'summary_customers'
            ),
            array(
                'name' => 'reports_suppliers',
                'type' => 'reports_summary_reports',
                'icon' => 'fa fa-reply-all',
                'link' => 'summary_suppliers'
            ),
            array(
                'name' => 'reports_items',
                'type' => 'reports_summary_reports',
                'icon' => 'fa fa-tags',
                'link' => 'summary_items'
            ),
            array(
                'name' => 'reports_employees',
                'type' => 'reports_summary_reports',
                'icon' => 'fa fa-male',
                'link' => 'summary_employees'
            ),
            array(
                'name' => 'reports_taxes',
                'type' => 'reports_summary_reports',
                'icon' => 'fa fa-dollar',
                'link' => 'summary_taxes',
                'deleted' => 1
            ),
            array(
                'name' => 'reports_discounts',
                'type' => 'reports_summary_reports',
                'icon' => 'fa',
                'link' => 'summary_discounts',
                'deleted' => 1
            ),
            array(
                'name' => 'reports_payments',
                'type' => 'reports_summary_reports',
                'icon' => 'fa fa-money',
                'link' => 'summary_payments'
            ),
            array(
                'name' => 'reports_sales',
                'type' => 'reports_detailed_reports',
                'icon' => 'fa fa-shopping-cart',
                'link' => 'detailed_sales'
            ),
            array(
                'name' => 'reports_receivings',
                'type' => 'reports_detailed_reports',
                'icon' => 'fa fa-truck',
                'link' => 'detailed_receivings'
            ),
            array(
                'name' => 'reports_customers',
                'type' => 'reports_detailed_reports',
                'icon' => 'fa fa-user',
                'link' => 'specific_customer'
            ),
            array(
                'name' => 'reports_employee',
                'type' => 'reports_detailed_reports',
                'icon' => 'fa fa-male',
                'link' => 'specific_employee'
            ),
            array(
                'name' => 'reports_low_inventory',
                'type' => 'reports_inventory_reports',
                'icon' => 'fa fa-warning',
                'link' => 'inventory_low'
            ),
            array(
                'name' => 'reports_inventory_summary',
                'type' => 'reports_inventory_reports',
                'icon' => 'fa fa-building-o',
                'link' => 'inventory_summary'
            )
        );
        return $this->_db->insert_batch($this->_prefix('reports'), $data);
    }

    private function _populate_stock_locations_table()
    {
        $stock_locations = explode(',', $this->CI->input->post('stock_location'));
        $stock_locations_trimmed = array();
        foreach ($stock_locations as $location) {
            array_push($stock_locations_trimmed, trim($location, ' '));
        }
        return $this->CI->Stock_locations->array_save($stock_locations_trimmed);
    }
}
