<?php

/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
class MY_Session extends CI_Session
{

    /**
     * Update an existing session
     *
     * @access    public
     * @return    void
     */
    function sess_update()
    {
        // skip the session update if this is an AJAX call! This is a bug in CI; see:
        // https://github.com/EllisLab/CodeIgniter/issues/154
        // http://codeigniter.com/forums/viewthread/102456/P15
        if ( ! ($this->CI->input->is_ajax_request())) {
            parent::sess_update();
        }
    }
}

/* End of file MY_Session.php */
/* Location: ./application/libraries/MY_Session.php */