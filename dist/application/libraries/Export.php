<?php
/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
if ( ! defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . "/third_party/PHPExcel/PHPExcel.php";

/**
 * Excel library for Code Igniter applications
 * Based on: Derek Allard, Dark Horse Consulting, www.darkhorse.to, April 2006
 * Tweaked by: Moving.Paper June 2013
 */
class Export extends PHPExcel
{

    function __construct()
    {
        parent::__construct();
    }

    function to_excel($header, $content, $filename)
    {
        // set active worksheet
        $this->setActiveSheetIndex(0);

        // set worksheet title
        $this->getActiveSheet()->setTitle('Sheet1');

        // set data heading
        $this->getActiveSheet()->fromArray($header);

        // set data content
        $this->getActiveSheet()->fromArray($content, null, 'A2');

        // set styling for heading
        $col = 0;
        while ($col < count($header)) {
            $this->getActiveSheet()->getStyleByColumnAndRow($col, 1)->getFont()->setBold(true);
            $this->getActiveSheet()->getStyleByColumnAndRow($col, 1)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $col ++;
        }

        // headers for browsers
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '.xls"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        // write data to file as excel 2003 file
        $objWriter = PHPExcel_IOFactory::createWriter($this, 'Excel5');

        // force download the file
        $objWriter->save('php://output');
    }

    function prepare_data($data)
    {
        $final_data = array();

        foreach ($data as $index => $row) {
            foreach ($row as $value) {
                $final_data[$index][] = $value['value'];
            }
        }
        return $final_data;
    }
}

/* End of file Export.php */
/* Location: ./application/libraries/Export.php */