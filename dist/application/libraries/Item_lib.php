<?php

/**
 * iPOS
 *
 * Accounting and Point of Sales system for Hoggy Djaya
 *
 * Copyright (c) 2015, Inertia WebDevelopment
 *
 * This software is licensed specifically for Hoggy Djaya.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 *
 * @author      Kevin Chandra, Robby Lukito
 * @copyright	Copyright (c) 2015, Inertia WebDevelopment (http://movelikeinertia.com)
 * @license     http://ipos.movelikeinertia.com/license.txt
 * @link        http://ipos.movelikeinertia.com/
 * @since       Version 1.0.0
 */
class Item_lib
{

    var $CI;

    function __construct()
    {
        $this->CI = & get_instance();
    }

    function get_item_location()
    {
        if ( ! $this->CI->session->userdata('item_location')) {
            $location_id = $this->CI->Stock_locations->get_undeleted_all(1)->result();
            $this->set_item_location($location_id[0]->location_id);
        }
        return $this->CI->session->userdata('item_location');
    }

    function set_item_location($location)
    {
        $this->CI->session->set_userdata('item_location', $location);
    }

    function clear_item_location()
    {
        $this->CI->session->unset_userdata('item_location');
    }
}

/* End of file Item_lib.php */
/* Location: ./application/libraries/Item_lib.php */