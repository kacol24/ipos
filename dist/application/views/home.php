<div class="col-lg-3 col-xs-6">
    <div class="small-box bg-green">
        <div class="inner">
            <h3><?php echo count($sales_today['summary']); ?></h3>
            <p><?php echo lang('reports_sales_today'); ?></p>
        </div>
        <div class="icon no-transition">
            <i class="fa fa-shopping-cart no-transition"></i>
        </div>
        <a href="<?php echo site_url('reports/summary_sales/' . date('Y-m-d') . '/' . date('Y-m-d') . '/sales'); ?>" class="small-box-footer">
            <?php echo lang('common_more_info'); ?> <i class="fa fa-arrow-circle-right"></i>
        </a>
    </div>
</div>
<div class="col-lg-3 col-xs-6">
    <div class="small-box bg-aqua">
        <div class="inner">
            <h3><?php echo count($receivings_today['summary']); ?></h3>
            <p><?php echo lang('reports_receivings_today'); ?></p>
        </div>
        <div class="icon no-transition">
            <i class="fa fa-truck no-transition"></i>
        </div>
        <a href="<?php echo site_url('reports/detailed_receivings/' . date('Y-m-d') . '/' . date('Y-m-d') . '/receiving'); ?>" class="small-box-footer">
            <?php echo lang('common_more_info'); ?> <i class="fa fa-arrow-circle-right"></i>
        </a>
    </div>
</div>
<div class="col-lg-3 col-xs-6">
    <div class="small-box bg-red">
        <div class="inner">
            <h3><?php echo count($low_inventory); ?></h3>
            <p><?php echo lang('reports_low_inventory'); ?></p>
        </div>
        <div class="icon no-transition">
            <i class="ion ion-alert no-transition"></i>
        </div>
        <a href="<?php echo site_url('reports/inventory_low/0'); ?>" class="small-box-footer">
            <?php echo lang('common_more_info'); ?> <i class="fa fa-arrow-circle-right"></i>
        </a>
    </div>
</div>
<div class="col-lg-3 col-xs-6">
    <div class="small-box bg-yellow">
        <div class="inner">
            <h3><?php echo count($suspended_sales); ?></h3>
            <p><?php echo lang('sales_suspended_sales'); ?></p>
        </div>
        <div class="icon no-transition">
            <i class="fa fa-inbox no-transition"></i>
        </div>
        <a href="<?php echo site_url('sales'); ?>" class="small-box-footer">
            <?php echo lang('common_more_info'); ?> <i class="fa fa-arrow-circle-right"></i>
        </a>
    </div>
</div>
<div class="col-xs-12">
    <div class="box box-primary box-solid">
        <div class="box-header">
            <h3 class="box-title"><?php echo lang('common_quick_access'); ?></h3>
            <div class="box-tools pull-right">
                <button class="btn btn-sm btn-primary" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body text-center no-transition">
            <?php foreach ($allowed_modules->result() as $module): ?>
                <a href="<?php echo site_url($module->module_id); ?>" class="btn btn-app">
                    <i class="<?php echo $module->module_icon; ?>"></i> <span><?php echo $this->lang->line($module->name_lang_key); ?></span>
                </a>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<?php if ($this->Employee->has_permission('reports', $user_info->person_id)): ?>
    <div class="col-xs-12">
        <div class="box box-primary box-solid">
            <div class="box-header">
                <h3 class="box-title"><?php echo lang('common_quick_reports') . ' - ' . lang('reports_sales'); ?></h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-sm btn-primary" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body chart-responsive text-center no-transition">
                <div class="chart no-transition" id="line-chart"></div>
            </div>
        </div>
    </div>
<?php endif; ?>
<script>
    $(document).ready(function () {
        var element = 'line-chart';
        var data = [
<?php foreach ($summary_sales['graph'] as $date => $value): ?>
                {"date": "<?php echo $date; ?>", "revenue": <?php echo $value != "null" ? '"' . $value . '"' : $value; ?>},
<?php endforeach; ?>
        ];
        Morris.Line({
            element: element,
            data: data,
            xkey: 'date',
            ykeys: ['revenue'],
            labels: ['<?php echo $this->lang->line('reports_revenue'); ?>'],
            resize: true,
            xLabelAngle: 60,
            preUnits: '<?php echo $this->config->item('currency_symbol'); ?>',
            smooth: false,
            hoverCallback: function (index, options, content, row) {
                var labels = options.labels;
                return "<div class='morris-hover-row-label'>" + row['date'] + "</div><div class='morris-hover-point' style='color: #0b62a4'>" + labels[0] + ": <?php echo $this->config->item('currency_symbol'); ?>" + accounting.formatNumber(row['revenue']) + "</div>";
            }
        });
    });
</script>