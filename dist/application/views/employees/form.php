<div class="col-xs-12">
    <div class="box box-solid">
        <div class="box-body">
            <?php echo form_open("employees/save/$person_info->person_id", array('id' => 'employee_form', 'class' => 'form-horizontal')); ?>
            <div class="col-sm-6">
                <fieldset>
                    <legend>
                        <?php echo $this->lang->line('employees_basic_information'); ?>
                    </legend>
                    <?php $this->load->view('people/form_basic_info'); ?>
                </fieldset>
            </div>
            <div class="col-sm-6">
                <fieldset id="employee_login_info">
                    <legend>
                        <?php echo $this->lang->line('employees_login_info'); ?>
                    </legend>
                    <div class="form-group">
                        <label for="username"class="control-label col-sm-4 text-red"><?php echo $this->lang->line('employees_username'); ?></label>
                        <div class="col-sm-8">
                            <input type="text" name="username" id="username" class="form-control" value="<?php echo $person_info->username; ?>"/>
                        </div>
                    </div>
                    <?php
                    $password_label_attributes = $person_info->person_id == '' ? 'required' : '';
                    ?>
                    <div class="form-group">
                        <label for="password" class="control-label col-sm-4"><?php echo $this->lang->line('employees_password'); ?></label>
                        <div class="col-sm-8">
                            <input type="password" name="password" id="password" class="form-control" <?php echo $password_label_attributes; ?>/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="repeat_password" class="control-label col-sm-4"><?php echo $this->lang->line('employees_repeat_password'); ?></label>
                        <div class="col-sm-8">
                            <input type="password" name="repeat_password" id="repeat_password" class="form-control"/>
                        </div>
                    </div>
                </fieldset>
                <fieldset id="employee_permission_info">
                    <legend>
                        <?php echo $this->lang->line('employees_permission_info'); ?>
                    </legend>
                    <p>
                        <?php echo $this->lang->line('employees_permission_desc'); ?>
                    </p>
                    <?php foreach ($all_modules->result() as $module): ?>
                        <div class="form-group">
                            <label class="col-xs-12">
                                <input type="checkbox" name="permissions[]" value="<?php echo $module->module_id; ?>" class="form-control iCheck checkbox" <?php echo $this->Employee->has_permission($module->module_id, $person_info->person_id) ? 'checked' : ''; ?>>
                                &nbsp;<?php echo $this->lang->line('module_' . $module->module_id); ?>:
                                <small>
                                    <em>
                                        <?php echo $this->lang->line('module_' . $module->module_id . '_desc'); ?>
                                    </em>
                                </small>
                            </label>
                        </div>
                    <?php endforeach; ?>
                </fieldset>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <input type="submit" name="submit" id="submit" class="btn btn-primary pull-right" value="<?php echo $this->lang->line('common_submit'); ?>"/>
                </div><!-- /.col-sm-7 -->
            </div><!-- /.form-group -->
            <?php echo form_close(); ?>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div><!-- /.col-xs-12 -->