<div class="col-md-8"><!-- cash register section -->
    <div class="panel panel-default">
        <div class="panel-heading clearfix">
            <?php echo form_open('receivings/change_mode', array('id' => 'mode_form', 'class' => 'form-inline')); ?>
            <div class="input-group form-group">
                <label for="mode" class="control-label input-group-addon"><?php echo $this->lang->line('recvs_mode'); ?></label>
                <?php echo form_dropdown('mode', $modes, $mode, 'class="form-control" id="mode" onchange="$(\'#mode_form\').submit();"'); ?>
            </div>
            <?php if ($show_stock_locations): ?>
                <div class="input-group form-group">
                    <label for="stock_source" class="control-label input-group-addon"><?php echo $this->lang->line('recvs_stock_source'); ?></label>
                    <?php echo form_dropdown('stock_source', $stock_locations, $stock_source, 'class="form-control" id="stock_source" onchange="$(\'#mode_form\').submit();"'); ?>
                </div>
                <?php if ($mode == 'requisition'): ?>
                    <div class="input-group form-group">
                        <label for="stock_destination" class="control-label input-group-addon"><?php echo $this->lang->line('recvs_stock_destination'); ?></label>
                        <?php echo form_dropdown('stock_destination', $stock_locations, $stock_destination, 'class="form-control" id="stock_source" onchange="$(\'#mode_form\').submit();"'); ?>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
            <?php echo form_close(); ?>
        </div><!-- /.panel-heading -->
        <div class="panel-body">
            <?php echo form_open("receivings/add", array('id' => 'add_item_form', 'class' => '')); ?>
            <div class="input-group">
                <label id="item_label" for="item" class="control-label input-group-addon">
                    <?php
                    if ($mode == 'receive' or $mode == 'requisition') {
                        echo $this->lang->line('recvs_find_or_scan_item');
                    } else {
                        echo $this->lang->line('recvs_find_or_scan_item_or_receipt');
                    }
                    ?>
                </label>
                <input type="hidden" id="item_id" name="item_id" value=""/>
                <input type="text" id="item" name="query" autocomplete="off" class="form-control col-xs-12" placeholder="<?php echo $this->lang->line('sales_start_typing_item_name'); ?>" autofocus="on" tabindex="1"/>
                <span class="input-group-btn">
                    <a href="<?php echo site_url('/items/new'); ?>" class="btn btn-primary"><?php echo $this->lang->line('sales_new_item'); ?></a>
                </span>
            </div>
            <?php echo form_close(); ?>
            <table id="recvs" class="table table-bordered table-hover dt-responsive no-wrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="all">
                            <?php echo $this->lang->line('recvs_item_number'); ?>
                        </th>
                        <th class="min-tablet-l">
                            <?php echo $this->lang->line('recvs_item_name'); ?>
                        </th>
                        <th class="min-desktop">
                            <?php echo $this->lang->line('recvs_cost'); ?>
                        </th>
                        <th class="all">
                            <?php echo $this->lang->line('recvs_quantity'); ?>
                        </th>
                        <th class="none">
                            <?php echo $this->lang->line('recvs_discount'); ?>
                        </th>
                        <th class="min-tablet">
                            <?php echo $this->lang->line('recvs_total'); ?>
                        </th>
                        <th class="min-tablet">
                            <?php echo $this->lang->line('common_delete'); ?>
                        </th>
                        <th class="none">
                            <?php echo $this->lang->line('recvs_description_abbrv'); ?>
                        </th>
                    </tr>
                </thead>
                <tbody id="cart-contents">
                    <?php
                    $sum_quantity = 0;
                    foreach ($cart as $line => $item):
                        $cur_item_info = $this->Item->get_info($item['item_id']);
                        $sum_quantity += $item['quantity'];
                        ?>
                        <tr>
                            <td class="control">
                                <?php echo $item['item_number']; ?>
                                <?php echo form_open("receivings/edit_item/$line", array('id' => 'edit_item_form_' . $line)); ?>
                                <input type="hidden" name="default_price" value="<?php echo $item['price']; ?>"/>
                                <input type="hidden" name="default_quantity" value="<?php echo $item['quantity']; ?>"/>
                                <input type="hidden" name="default_location" value="<?php echo $item['item_location']; ?>"/>
                                <input type="hidden" name="default_discount" value="<?php echo $item['discount']; ?>"/>
                                <input type="hidden" name="default_description" value="<?php echo $item['description']; ?>"/>
                                <?php echo form_close(); ?>
                            </td>
                            <td>
                                <div data-toggle="tooltip" title="<?php echo $item['name'] . ' (' . $item['in_stock'] . ')[' . $item['stock_name'] . ']'; ?>">
                                    <?php echo character_limiter($item['name'] . ' (' . $item['in_stock'] . ') [' . $item['stock_name'] . ']', 15); ?>
                                </div>
                            </td>
                            <td class="text-center">
                                <input form="edit_item_form_<?php echo $line; ?>" type="number" name="price" class="text-right form-control input-sm" min="0" max="9999999999" value="<?php echo $item['price']; ?>"/>
                            </td>
                            <td class="text-center">
                                <input form="edit_item_form_<?php echo $line; ?>" type="number" name="quantity" value="<?php echo $item['quantity']; ?>" min="1" max="9999" autocomplete="off" class="text-right form-control input-sm"/>
                            </td>
                            <td>
                                <input form="edit_item_form_<?php echo $line; ?>" type="number" name="discount" value="<?php echo $item['discount']; ?>" min="0" max="100" autocomplete="off" class="text-right form-control input-sm"/>
                            </td>
                            <td class="text-right">
                                <?php echo to_currency($item['price'] * $item['quantity'] - $item['price'] * $item['quantity'] * $item['discount'] / 100); ?>
                            </td>
                            <td class="text-center">
                                <?php echo anchor("receivings/delete_item/$line", ' <i class="fa fa-trash-o"></i> ', array('class' => 'btn btn-sm btn-danger', 'tabindex' => '3')); ?>
                            </td>
                            <td>
                                <?php
                                if ($item['description'] != ''):
                                    echo $item['description'];
                                else:
                                    echo $this->lang->line('sales_no_description');
                                endif;
                                ?>
                            </td>
                        </tr>
                        <?php
                    endforeach;
                    ?>
                </tbody>
            </table>
        </div><!-- /.box-body -->
        <div class="panel-footer">
            <div class="row">
                <div class="pull-left col-sm-4 col-xs-5">
                    <label class="form-control-static">
                        <?php echo $this->lang->line('sales_items_in_cart') . ': ' . $sum_quantity . '(' . count($cart) . ')'; ?>
                    </label>
                </div>
            </div>
        </div>
    </div><!-- /.box -->
</div><!-- /.col-md-8 -->
<div class="col-md-4 sales-info"><!-- customer/transaction info section -->
    <div class="panel panel-default">
        <div class="panel-heading clearfix">
            <?php if (isset($supplier)): ?>
                <div class="input-group">
                    <label class="control-label input-group-addon"><?php echo $this->lang->line('recvs_supplier'); ?></label>
                    <input type="text" class="form-control" value="<?php echo $supplier; ?>" readonly>
                    <a href="<?php echo site_url('/receivings/delete_supplier'); ?>" class="input-group-addon"><i class="fa fa-times text-red"></i></a>
                </div>
            <?php else: ?>
                <?php echo form_open('receivings/select_supplier', array('id' => 'select_supplier_form', 'class' => '')); ?>
                <div class="input-group">
                    <label id="supplier_label" for="supplier" class="control-label input-group-addon"><?php echo $this->lang->line('recvs_select_supplier'); ?></label>
                    <div class="">
                        <input type="text" id="supplier" name="query" autocomplete="off" class="form-control" placeholder="<?php echo $this->lang->line('recvs_start_typing_supplier_name'); ?>"/>
                        <input type="hidden" id="supplier_id" name="supplier_id" value=""/>
                    </div><!-- /.col-sm-3 -->
                </div>
                <?php echo form_close(); ?>
            <?php endif; ?>
        </div><!-- /.panel-heading -->
        <div class="panel-body row">
            <table class="table no-margin">
                <tbody>
                    <tr<?php echo count($cart) > 0 ? ' class="bg-success"' : ''; ?>>
                        <th class="no-border">
                            <?php echo lang('sales_grand_total'); ?> :
                        </th>
                        <td class="text-right no-border">
                            <strong><?php echo to_currency($total); ?></strong>
                        </td>
                    </tr>
                    <?php if (count($cart) > 0): ?>
                        <tr>
                            <td colspan="2" class="no-border">
                                <textarea form="finish_sale_form" class="form-control" name="comment" placeholder="<?php echo $this->lang->line('common_comments'); ?>"></textarea>
                            </td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div><!-- /.panel-body -->
        <?php if (count($cart) > 0): ?>
            <?php if ($mode != 'requisition'): ?>
                <div class="panel-footer">
                    <?php echo form_open('receivings/complete', array('id' => 'finish_sale_form', 'class' => 'form-horizontal')); ?>
                    <div class="form-group">
                        <label for="payment_types" class="control-label col-sm-5 force-text-left"><?php echo $this->lang->line('sales_payment'); ?> <span class="text-right">:</span></label>
                        <div class="col-sm-7">
                            <?php echo form_dropdown('payment_type', $payment_options, array(), 'id="payment_types" class="form-control"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="amount_tendered" class="control-label col-sm-5 force-text-left"><?php echo $this->lang->line('sales_amount_tendered'); ?>:</label>
                        <div class="col-sm-7 input-group add-padding-right-left">
                            <div class="input-group-addon"><?php echo $this->config->item('currency_symbol'); ?></div><input type="number" name="amount_tendered" id="amount_tendered" class="text-right form-control" autocomplete="off"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-6">
                            <button class="btn btn-sm btn-warning" type="button" name="cancel" value="true"><?php echo $this->lang->line('common_cancel'); ?></button>
                        </div>
                        <div class="col-xs-6 text-right">
                            <button type="submit" class="btn btn-sm btn-success"><?php echo $this->lang->line('recvs_complete_receiving'); ?></button>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div><!-- /.panel-footer -->
            <?php endif; ?>
        <?php endif; ?>
    </div><!-- /.panel .panel-default -->
</div><!-- /.col-md-4 -->
<script src="<?php echo base_url(); ?>assets/js/jquery.autocomplete.min.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
// General functions
        $('#amount_tendered').focus(function () {
            $(this).select();
        });
        var quantity_changed = false;
        $("input[name='quantity'], input[name='price'], input[name='discount']").focus(function () {
            $(this).select();
        }).change(function () {
            quantity_changed = true;
        }).blur(function () {
            if (quantity_changed) {
                var form = $(this).attr('form');
                $("#" + form).submit();
            }
        }).keyup(function (e) {
            if (e.keyCode === 13 && quantity_changed) {
                var form = $(this).attr('form');
                $("#" + form).submit();
            }
        });
        $('#comment_placard').placard({
            onAccept: function () {
                $('#comment_form').submit();
            }
        });
        $('#recvs .control').on('click', function () {
            var changed = false;
            setTimeout(function () {
                $('.edit_item_placard').placard({
                    onAccept: function () {
                        $('#edit_item_form').submit();
                    }
                });
                $('input[name="discount"]').focus(function () {
                    $(this).select();
                }).change(function () {
                    changed = true;
                }).keypress(function (e) {
                    if (e.keyCode === 13) {
                        e.preventDefault();
                        if (changed) {
                            $('#edit_item_form').submit();
                        }
                    }
                });
            }, 500);
        });
        $('#payment_types').change(function () {
            $('#amount_tendered').select();
        });
// end of General functions


// Hotkey assignments
        $('#item, #supplier').jkey('esc', function () {
            $(this).val('');
        });
// end of Hotkey assignments


// Autocomplete initialization
        $('#supplier').devbridgeAutocomplete({
            serviceUrl: '<?php echo site_url(); ?>/receivings/supplier_search',
            minChars: 2,
            onSearchStart: function (query) {
//                console.log(query);
                $(this).addClass('loading-state');
            },
            onSearchComplete: function (query, suggestions) {
//                console.log(query);
//                console.log(suggestions);
                $(this).removeClass('loading-state');
            },
            onSelect: function (suggestion) {
                $('#supplier_id').val(suggestion.data);
                $("#select_supplier_form").submit();
            },
            showNoSuggestionNotice: true,
            noSuggestionNotice: '<?php echo $this->lang->line('sales_no_customer_found'); ?>'
        });
        $('#item').devbridgeAutocomplete({
        serviceUrl: '<?php echo site_url(); ?>/receivings/item_search',
                minChars: 2,
                onSearchStart: function (query) {
                    console.log(query);
                    $(this).addClass('loading-state');
                },
                onSearchComplete: function (query, suggestions) {
                    console.log(query);
                    console.log(suggestions);
                    $(this).removeClass('loading-state');
                },
                onSelect: function (suggestion) {
                    $('#item_id').val(suggestion.data);
                    $("#add_item_form").submit();
                }
<?php if ($mode == 'sale'): ?>
            , showNoSuggestionNotice: true,
                    noSuggestionNotice: '<?php echo $this->lang->line('sales_no_item_found'); ?>'
<?php endif; ?>
    });
    });
// end of Autocomplete initialization
</script>