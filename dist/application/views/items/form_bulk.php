<div class="modal fade" id="bulk_edit_modal" tabindex="-1" role="dialog" aria-labelledby="bulk_edit_modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <?php echo lang('items_edit_multiple_items'); ?>
                </h4>
            </div>
            <div class="modal-body">
                <?php echo form_open("items/bulk_update", array('id' => 'bulk_edit_form', 'class' => 'form-horizontal')); ?>
                <fieldset>
                    <legend>
                        <?php echo $this->lang->line('items_basic_information'); ?>
                    </legend>
                    <div class="form-group">
                        <label for="category" class="control-label col-sm-4"><?php echo $this->lang->line('items_category'); ?></label>
                        <div class="col-sm-8">
                            <input type="text" name="category" id="category" class="form-control" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="supplier" class="control-label col-sm-4"><?php echo $this->lang->line('items_supplier'); ?></label>
                        <div class="col-sm-8">
                            <?php echo form_dropdown('supplier_id', $suppliers, '', 'class="form-control" id="supplier"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="control-label col-sm-4"><?php echo $this->lang->line('items_description'); ?></label>
                        <div class="col-sm-8">
                            <textarea name="description" id="description" class="form-control"></textarea>
                        </div>
                    </div>
                    <!-- modify this -->
                    <div class="form-group">
                        <label for="is_serialized" class="control-label col-sm-4 no-padding-top"><?php echo $this->lang->line('items_is_serialized'); ?></label>
                        <div class="col-sm-8">
                            <?php echo form_dropdown('is_serialized', $serialization_choices, '', 'class="form-control" id="is_serialized"'); ?>
                        </div>
                    </div>
                    <!-- MODIFY -->
                </fieldset>
                <fieldset>
                    <legend>
                        <?php echo $this->lang->line('items_price_and_tax_information'); ?>
                    </legend>
                    <div class="form-group">
                        <label for="cost_price" class="control-label col-sm-4"><?php echo $this->lang->line('items_cost_price'); ?></label>
                        <div class="col-sm-8 input-group add-padding-right-left">
                            <div class="input-group-addon"><?php echo $this->config->item('currency_symbol'); ?></div>
                            <input type="number" name="cost_price" id="cost_price" class="form-control text-right" min="0" max="999999999999" step="50">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="unit_price" class="control-label col-sm-4"><?php echo $this->lang->line('items_unit_price'); ?></label>
                        <div class="col-sm-8 input-group add-padding-right-left">
                            <div class="input-group-addon"><?php echo $this->config->item('currency_symbol'); ?></div>
                            <input type="number" name="unit_price" id="unit_price" class="form-control text-right" min="0" max="999999999999" step="50">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tax_name_1" class="control-label col-sm-4"><?php echo $this->lang->line('items_tax_1'); ?></label>
                        <div class="col-sm-3">
                            <input type="text" name="tax_names[]" id="tax_name_1" class="form-control" value="<?php echo isset($item_tax_info[0]['name']) ? $item_tax_info[0]['name'] : $this->config->item('default_tax_1_name'); ?>"/>
                        </div>
                        <div class="input-group col-sm-4 add-padding-right-left">
                            <input type="number" name="tax_percents[]" id="tax_percent_name_1" class="form-control text-right" min="0" max="100" value="<?php echo isset($item_tax_info[0]['percent']) ? $item_tax_info[0]['percent'] : ''; ?>"/>
                            <div class="input-group-addon">%</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tax_name_2" class="control-label col-sm-4"><?php echo $this->lang->line('items_tax_2'); ?></label>
                        <div class="col-sm-3">
                            <input type="text" name="tax_names[]" id="tax_name_2" class="form-control" value="<?php echo isset($item_tax_info[1]['name']) ? $item_tax_info[1]['name'] : $this->config->item('default_tax_2_name'); ?>"/>
                        </div>
                        <div class="input-group col-sm-4 add-padding-right-left">
                            <input type="number" name="tax_percents[]" id="tax_percent_name_2" class="form-control text-right" min="0" max="100" value="<?php echo isset($item_tax_info[1]['percent']) ? $item_tax_info[1]['percent'] : ''; ?>"/>
                            <div class="input-group-addon">%</div>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend>
                        <?php echo $this->lang->line('items_inventory_information'); ?>
                    </legend>
                    <div class="form-group">
                        <label for="reorder_level" class="control-label col-sm-4"><?php echo $this->lang->line('items_reorder_level'); ?></label>
                        <div class="col-sm-8"  data-toggle="tooltip" title="<?php echo $this->lang->line('items_reorder_level_desc'); ?>" data-placement="right">
                            <input type="number" name="reorder_level" id="reorder_level" class="form-control text-right" min="0" max="999999">
                        </div>
                    </div>
                </fieldset>
            </div><!-- /.box-body -->
            <?php echo form_close(); ?>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('common_cancel'); ?></button>
                <a type="button" class="btn btn-primary" id="bulk_edit_submit" onclick="submit_bulk()"><?php echo lang('common_submit'); ?></a>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#category').devbridgeAutocomplete({
            serviceUrl: '<?php echo site_url(); ?>/items/suggest_category',
            minChars: 1,
            onSearchStart: function (query) {
                console.log(query);
                $(this).addClass('loading-state');
            },
            onSearchComplete: function (query, suggestions) {
                console.log(query);
                console.log(suggestions);
                $(this).removeClass('loading-state');
            }
        });
    });
    function get_selected_values() {
        var selected_values = new Array();
        $("#item_table tbody :checkbox:checked").each(function () {
            selected_values.push($(this).val());
        });
        return selected_values;
    }
    function submit_bulk() {
        var selected_item = get_selected_values();
        for (k = 0; k < selected_item.length; k++) {
            $('#bulk_edit_form').append("<input type='hidden' name='item_ids[]' value='" + selected_item[k] + "'>");
        }
        $('#bulk_edit_form').submit();
    }
</script>