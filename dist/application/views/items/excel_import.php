<div class="col-xs-12">
    <div class="box box-solid">
        <div class="box-body">
            <?php echo form_open_multipart('items/do_excel_import', array('class' => 'form-horizontal')) ?>
            <fieldset>
                <legend>
                    Import Items
                </legend>
                <div class="form-group">
                    <label for="file_path" class="col-sm-2 control-label">Upload File</label>
                    <div class="col-sm-5">
                        <?php
                        echo form_upload(array(
                            'name' => 'file_path',
                            'id' => 'file_path',
                            'class' => 'form-control-static'
                        ));
                        ?>
                    </div>
                </div>
            </fieldset>
            <div class="form-group">
                <div class="col-sm-7">
                    <input type="submit" name="submit" id="submit" class="btn btn-primary pull-right" value="<?php echo $this->lang->line('common_submit'); ?>"/>
                </div><!-- /.col-sm-7 -->
            </div><!-- /.form-group -->
        </div><!-- /.box-body -->
        <?php echo form_close(); ?>
    </div><!-- /.box -->
</div><!-- /.col-xs-12 -->