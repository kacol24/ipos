<?php get_notif(); ?>
<div class="col-xs-12">
    <div class="box box-solid">
        <div class="box-body">
            <?php echo form_open("sales/record_payment/" . $sale_info['sale_id'], array('id' => 'customer_form', 'class' => 'form-horizontal')); ?>
            <fieldset>
                <legend>
                    <?php echo $this->lang->line('sales_basic_information'); ?>
                </legend>
                <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo $this->lang->line('sales_receipt'); ?></label>
                    <div class="col-sm-5">
                        <p class="form-control-static"><?php echo $this->lang->line('sales_receipt_number') . $sale_info['sale_id']; ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="customer" class="control-label col-sm-2"><?php echo $this->lang->line('sales_customer'); ?></label>
                    <div class="col-sm-5">
                        <p class="form-control-static"><?php echo $customer['name']; ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="employee_id" class="control-label col-sm-2"><?php echo $this->lang->line('sales_employee'); ?></label>
                    <div class="col-sm-5">
                        <p class="form-control-static"><?php echo $employee; ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo lang('sales_amount_due'); ?></label>
                    <div class="col-sm-5">
                        <p class="form-control-static"><?php echo to_currency($amount_due); ?></p>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo lang('sales_payment'); ?></label>
                    <div class="col-sm-5">
                        <?php echo form_dropdown('payment_type', $payment_options, array(), 'id="payment_types" class="form-control"'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2"><?php echo lang('sales_amount_tendered'); ?></label>
                    <div class="col-sm-5">
                        <input type="number" name="payment_amount" class="form-control text-right" min="0">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-7">
                        <input type="submit" name="submit" id="submit" class="btn btn-primary pull-right" value="<?php echo $this->lang->line('common_submit'); ?>"/>
                    </div><!-- /.col-sm-7 -->
                </div><!-- /.form-group -->
            </fieldset>
            <?php echo form_close(); ?>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div><!-- /.col-xs-12 -->