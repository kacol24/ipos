<div class="modal fade" id="sales_suspended_modal" tabindex="-1" role="dialog" aria-labelledby="sales_suspended_modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php echo $this->lang->line('sales_suspended_sales'); ?></h4>
            </div>
            <div class="modal-body no-padding">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>
                                    <?php
                                    echo $this->lang->line('sales_date') . '/' . $this->lang->line('sales_sale_time');
                                    ?>
                                </th>
                                <th>
                                    <?php echo $this->lang->line('sales_customer'); ?>
                                </th>
                                <th>
                                    <?php echo $this->lang->line('sales_comments'); ?>
                                </th>
                                <th>
                                    <?php echo $this->lang->line('common_table_action'); ?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($suspended_sales as $suspended_sale): ?>
                                <tr>
                                    <td>
                                        <?php echo ($suspended_sale['sale_time']); ?>
                                    </td>
                                    <td>
                                        <?php
                                        if (isset($suspended_sale['customer_id']))
                                        {
                                            $cust_info = $this->Customer->get_info($suspended_sale['customer_id']);
                                            echo $cust_info->first_name . ' ' . $cust_info->last_name;
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <?php echo $suspended_sale['comment']; ?>
                                    </td>
                                    <td>
                                        <?php echo form_open('sales/unsuspend'); ?>
                                        <input type="hidden" name="suspended_sale_id" value="<?php echo $suspended_sale['sale_id']; ?>">
                                        <input type="submit" value="<?php echo $this->lang->line('sales_unsuspend'); ?>" class="btn btn-sm btn-warning">
                                        <button type="submit" name="delete" value="1" class="btn btn-sm btn-danger"> <i class="fa fa-trash-o"></i> </button>
                                        <?php echo form_close(); ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div><!-- /.table-responsive -->
            </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
    </div><!-- /.,modal-dialog -->
</div><!-- /.modal -->