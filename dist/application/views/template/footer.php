</div><!-- /.row -->
</section><!-- /.content -->
<?php if (isset($custom_title) && isset($set_invoice)): ?>
    <div class="copy_holder visible-print-block"></div>
<?php endif; ?>
<div class="col-xs-12 no-print">
    <?php if (ENVIRONMENT !== 'production'): ?>
        <div class="text-muted text-right"><small><em>Rendered: {elapsed_time}s</em></small></div>
        <div class="text-muted text-right"><small><em>Memory: {memory_usage}</em></small></div>
    <?php endif; ?>
    <div class="text-muted text-center">
        <small>
            <em>
                Copyright &copy; <?php echo date('Y'); ?> - <a href="http://movelikeinertia.com">Inertia WebDevelopment</a>
            </em>
        </small>
    </div>
</div>
</aside><!-- /.right-side -->
</div><!-- ./wrapper -->

<!-- compiled -->
<script src="<?php echo base_url(); ?>assets/js/app.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function () {
        $('input, form').attr('autocomplete', 'off');
        $.each(['home', 'customers', 'items', 'item_kits', 'suppliers', 'reports', 'receivings', 'sales', 'employees', 'giftcards', 'events', 'config'], function (key, value) {
            $('#module_' + value + ' em').html('(F' + (key + 1) + ')');
            $(window).jkey('f' + (key + 1), function () {
                window.location = BASE_URL + '/' + value;
            });
        });
        $(window).jkey('ctrl+space', function () {
            $("[data-toggle='offcanvas']").trigger('click');
        });
<?php
$dtz = new DateTimeZone($this->config->item('timezone'));
$now = new DateTime('now', $dtz);
$timezone = $dtz->getOffset($now) / 60 / 60;
?>
        $('#clockbox').easyClock({
            language: '<?php echo $this->config->item('language'); ?>',
            timezone: <?php echo $timezone; ?>
        });
//        var loadstate = $('#loadstate');
//        loadstate.hide();
        $("#datatable").DataTable({
            language: {
                url: "<?php echo $this->config->item('language') == 'en' ? '' : 'assets/datatables-lang/indonesian.json'; ?>"
            },
            columnDefs: [{
                    className: 'table-action',
                    orderable: false,
                    searchable: false,
                    targets: -1
                }]
        });
        $("#event_log").DataTable({
            "order": [[3, "desc"]]
        });
        var report = $('#report-table').DataTable({
            language: {
                url: "<?php echo $this->config->item('language') == 'en' ? '' : 'assets/datatables-lang/indonesian.json'; ?>"
            },
            responsive: {
                details: {
                    type: 'column'
                }
            },
            columnDefs: [{
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    targets: 0
                }],
            order: [2, 'desc']
        });
        $('#report-table tbody').on('click', 'td.control', function () {
            $('#report-table tr.child ul').addClass('no-transition');
            $('#report-table tr.child ul').width($('#report-table_wrapper').width() - 10);
        });
        $("#register, #recvs, .inner-table").DataTable({
            ordering: false,
            paging: false,
            searching: false,
            info: false,
            language: {
                url: "<?php echo $this->config->item('language') == 'en' ? 'assets/datatables-lang/sales-english.json' : 'assets/datatables-lang/sales-indonesian.json'; ?>"
            },
            retrieve: true
        });
        $('.ajax_modal').click(function (e) {
            e.preventDefault();
            var url = $(this).attr('href');
            $.get(url, function (response) {
                $('#modal_holder').empty();
                $('#modal_holder').html(response);
                $('#modal_holder').find('.modal').modal('show');
            }).success(function () {
                $('input:text:visible:first').focus();
            });
        });
    });
</script>

<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/js/custom.js" type="text/javascript"></script>

<div id="modal_holder"></div>
<div class="modal fade" id="confirm_delete" tabindex="-1" role="dialog" aria-labelledby="confirm_delete" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('common_cancel'); ?></button>
                <a type="button" class="btn btn-primary btn-ok"><?php echo lang('common_submit'); ?></a>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('.delete_modal').click(function (e) {
            e.preventDefault();
            var modal = $('#confirm_delete');
            modal.find('.modal-title').html($(this).attr('title') + '?');
            modal.find('.modal-body').html($(this).data('modal-body'));
            modal.find('.btn-ok').attr('href', $(this).attr('href'));
            modal.modal({
                backdrop: 'static'
            });
        });
    });
</script>
</body>
</html>