<div class="form-group">
    <label for="first_name" class="control-label <?php echo $controller_name == 'employees' ? 'col-sm-4' : 'col-sm-2'; ?> text-red"><?php echo $this->lang->line('common_first_name'); ?></label>
    <div class="<?php echo $controller_name == 'employees' ? 'col-sm-8' : 'col-sm-5'; ?>">
        <input type="text" name="first_name" id="first_name" class="form-control" value="<?php echo $person_info->first_name; ?>" autofocus/>
    </div>
</div>
<div class="form-group">
    <label for="last_name" class="control-label <?php echo $controller_name == 'employees' ? 'col-sm-4' : 'col-sm-2'; ?> text-red"><?php echo $this->lang->line('common_last_name'); ?></label>
    <div class="<?php echo $controller_name == 'employees' ? 'col-sm-8' : 'col-sm-5'; ?>">
        <input type="text" name="last_name" id="last_name" class="form-control" value="<?php echo $person_info->last_name; ?>"/>
    </div>
</div>
<div class="form-group">
    <label for="email" class="control-label <?php echo $controller_name == 'employees' ? 'col-sm-4' : 'col-sm-2'; ?>"><?php echo $this->lang->line('common_email'); ?></label>
    <div class="<?php echo $controller_name == 'employees' ? 'col-sm-8' : 'col-sm-5'; ?>">
        <input type="email" name="email" id="email" class="form-control" value="<?php echo $person_info->email; ?>"/>
    </div>
</div>
<div class="form-group">
    <label for="phone_number" class="control-label <?php echo $controller_name == 'employees' ? 'col-sm-4' : 'col-sm-2'; ?>"><?php echo $this->lang->line('common_phone_number'); ?></label>
    <div class="<?php echo $controller_name == 'employees' ? 'col-sm-8' : 'col-sm-5'; ?>">
        <input type="text" name="phone_number" id="phone_number" class="form-control" value="<?php echo $person_info->phone_number; ?>"/>
    </div>
</div>
<div class="form-group">
    <label for="address_1" class="control-label <?php echo $controller_name == 'employees' ? 'col-sm-4' : 'col-sm-2'; ?>"><?php echo $this->lang->line('common_address_1'); ?></label>
    <div class="<?php echo $controller_name == 'employees' ? 'col-sm-8' : 'col-sm-5'; ?>">
        <input type="text" name="address_1" id="address_1" class="form-control" value="<?php echo $person_info->address_1; ?>"/>
    </div>
</div>
<div class="form-group">
    <label for="address_2" class="control-label <?php echo $controller_name == 'employees' ? 'col-sm-4' : 'col-sm-2'; ?>"><?php echo $this->lang->line('common_address_2'); ?></label>
    <div class="<?php echo $controller_name == 'employees' ? 'col-sm-8' : 'col-sm-5'; ?>">
        <input type="text" name="address_2" id="address_2" class="form-control" value="<?php echo $person_info->address_2; ?>"/>
    </div>
</div>
<div class="form-group">
    <label for="city" class="control-label <?php echo $controller_name == 'employees' ? 'col-sm-4' : 'col-sm-2'; ?>"><?php echo $this->lang->line('common_city'); ?></label>
    <div class="<?php echo $controller_name == 'employees' ? 'col-sm-8' : 'col-sm-5'; ?>">
        <input type="text" name="city" id="city" class="form-control" value="<?php echo $person_info->city; ?>"/>
    </div>
</div>
<div class="form-group">
    <label for="state" class="control-label <?php echo $controller_name == 'employees' ? 'col-sm-4' : 'col-sm-2'; ?>"><?php echo $this->lang->line('common_state'); ?></label>
    <div class="<?php echo $controller_name == 'employees' ? 'col-sm-8' : 'col-sm-5'; ?>">
        <input type="text" name="state" id="state" class="form-control" value="<?php echo $person_info->state; ?>"/>
    </div>
</div>
<div class="form-group">
    <label for="zip" class="control-label <?php echo $controller_name == 'employees' ? 'col-sm-4' : 'col-sm-2'; ?>"><?php echo $this->lang->line('common_zip'); ?></label>
    <div class="<?php echo $controller_name == 'employees' ? 'col-sm-8' : 'col-sm-5'; ?>">
        <input type="text" name="zip" id="zip" class="form-control" value="<?php echo $person_info->zip; ?>"/>
    </div>
</div>
<div class="form-group">
    <label for="country" class="control-label <?php echo $controller_name == 'employees' ? 'col-sm-4' : 'col-sm-2'; ?>"><?php echo $this->lang->line('common_country'); ?></label>
    <div class="<?php echo $controller_name == 'employees' ? 'col-sm-8' : 'col-sm-5'; ?>">
        <input type="text" name="country" id="country" class="form-control" value="<?php echo $person_info->country; ?>"/>
    </div>
</div>
<div class="form-group">
    <label for="comments" class="control-label <?php echo $controller_name == 'employees' ? 'col-sm-4' : 'col-sm-2'; ?>"><?php echo $this->lang->line('common_comments'); ?></label>
    <div class="<?php echo $controller_name == 'employees' ? 'col-sm-8' : 'col-sm-5'; ?>">
        <textarea name="comments" id="comments" class="form-control"><?php echo $person_info->comments; ?></textarea>
    </div>
</div>