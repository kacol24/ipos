<div class="col-xs-12">
    <div class="box">
        <div class="box-header clearfix row">
            <?php foreach ($overall_summary_data as $key => $value): ?>
                <div class="col-md-3">
                    <div class="box box-solid bg-aqua">
                        <div class="box-header">
                            <h3 class="box-title"><?php echo $this->lang->line('reports_' . $key); ?></h3>
                        </div>
                        <div class="box-body text-right">
                            <p class="lead"><strong><u><?php echo to_currency($value); ?></u></strong></p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="box-body">
            <table id="report-table" class="table table-bordered table-condensed table-hover dt-responsive no-wrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th></th>
                        <?php foreach ($headers['summary'] as $key => $header): ?>
                            <th class="<?php echo $headers['summary_class'][$key]; ?>"><?php echo $header; ?></th>
                        <?php endforeach; ?>
                        <th class="none">Details</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($summary_data as $key => $row): ?>
                        <tr>
                            <td></td>
                            <?php foreach ($row as $column): ?>
                                <td class="<?php echo $column['class']; ?>"><?php echo $column['value']; ?></td>
                            <?php endforeach; ?>
                            <td>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed no-wrap" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <?php foreach ($headers['details'] as $index => $detail_header): ?>
                                                    <th class="<?php echo $headers['details_class'][$index]; ?>"><?php echo $detail_header; ?></th>
                                                <?php endforeach; ?>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($details_data[$key] as $data): ?>
                                                <tr>
                                                    <?php foreach ($data as $cell): ?>
                                                        <td class="<?php echo $cell['class']; ?>"><?php echo $cell['value']; ?></td>
                                                    <?php endforeach; ?>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div><!-- /.col-xs-12 -->