<div class="col-md-12">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title"><?php echo $graph_heading; ?></h3>
            <div class="pull-right">
                <label for="trendline" class="control-label">Trend Line</label>
                <input type="checkbox" name="trendline" id="trendline" class="form-control iCheck checkbox-inline checkbox"/>
            </div>
        </div>
        <div class="box-body chart-responsive text-center">
            <div class="chart no-transition" id="line-chart"></div>
        </div>
        <div class="box-footer clearfix">
            <?php foreach ($summary_data as $key => $value): ?>
                <div class="col-md-3">
                    <div class="box box-solid bg-aqua">
                        <div class="box-header">
                            <h3 class="box-title"><?php echo $this->lang->line('reports_' . $key); ?></h3>
                        </div>
                        <div class="box-body text-right">
                            <p class="lead"><?php echo to_currency($value); ?></p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<script>
    // calc slope and intercept
    // then use resulting y = mx + b to create trendline
    lineFit = function (points) {
        sI = slopeAndIntercept(points);
        if (sI) {
            // we have slope/intercept, get points on fit line
            var N = points.length;
            var rV = [];
            rV.push([points[0][0], sI.slope * points[0][0] + sI.intercept]);
            rV.push([points[N - 1][0], sI.slope * points[N - 1][0] + sI.intercept]);
            return rV;
        }
        return [];
    };
    // simple linear regression
    slopeAndIntercept = function (points) {
        var rV = {},
                N = points.length,
                sumX = 0,
                sumY = 0,
                sumXx = 0,
                sumYy = 0,
                sumXy = 0;
        // can't fit with 0 or 1 point
        if (N < 2) {
            return rV;
        }
        for (var i = 0; i < N; i++) {
            var x = points[i][0],
                    y = points[i][1];
            sumX += x;
            sumY += y;
            sumXx += (x * x);
            sumYy += (y * y);
            sumXy += (x * y);
        }
        // calc slope and intercept
        rV['slope'] = ((N * sumXy) - (sumX * sumY)) / (N * sumXx - (sumX * sumX));
        rV['intercept'] = (sumY - rV['slope'] * sumX) / N;
        rV['rSquared'] = Math.abs((rV['slope'] * (sumXy - (sumX * sumY) / N)) / (sumYy - ((sumY * sumY) / N)));
        return rV;
    };
    $(document).ready(function () {
        var element = 'line-chart';
        var data = [
<?php foreach ($data_file['graph'] as $date => $value): ?>
                {"date": "<?php echo $date; ?>", "revenue": <?php echo $value != "null" ? '"' . $value . '"' : $value; ?>},
<?php endforeach; ?>
        ];

        var trend = [];
        for (var i = 0; i < data.length; i++) {
            trend.push([i, parseInt(data[i]['revenue'])]);
        }

        var morris = Morris.Line({
        element: element,
                data: data,
                xkey: 'date',
                ykeys: ['revenue'<?php echo count($data_file['graph']) > 2 ? ", 'trend'" : ""; ?>],
                labels: ['<?php echo $this->lang->line('reports_revenue'); ?>'<?php echo count($data_file['graph']) > 2 ? ", 'Trend'" : ""; ?>],
                resize: true,
                xLabelAngle: 60,
                preUnits: '<?php echo $this->config->item('currency_symbol'); ?>',
                smooth: false,
                hoverCallback: function (index, options, content, row) {
                var labels = options.labels;
                return "<div class='morris-hover-row-label'>" + row['date'] + "</div><div class='morris-hover-point' style='color: #0b62a4'>" + labels[0] + ": <?php echo $this->config->item('currency_symbol'); ?>" + accounting.formatNumber(row['revenue']) + "</div>";
                }
    });
    trendLine = lineFit(trend);
    firstPoint = data[0];
    secondPoint = data[data.length - 1];
    var overlay = $('<div class="overlay"></div><div class="loading-img"></div>');
    var box = $('#line-chart').parent().parent();
    $('#trendline').on('ifChecked', function () {
        box.append(overlay);
        firstPoint['trend'] = trendLine[0][1].toString();
        secondPoint['trend'] = trendLine[1][1].toString();
        morris.setData(data);
        setTimeout(function () {
            box.find(overlay).remove();
        }, 1000);
    }).on('ifUnchecked', function () {
        box.append(overlay);
        firstPoint['trend'] = null;
        secondPoint['trend'] = null;
        morris.setData(data);
        setTimeout(function () {
            box.find(overlay).remove();
        }, 1000);
    });
    });
</script>