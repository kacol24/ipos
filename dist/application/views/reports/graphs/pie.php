<div class="col-md-12">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title"><?php echo $graph_heading; ?></h3>
        </div>
        <div class="box-body chart-responsive text-center">
            <div class="chart no-transition" id="pie-chart"></div>
        </div>
        <div class="box-footer clearfix">
            <?php foreach ($summary_data as $key => $value): ?>
                <div class="col-md-3">
                    <div class="box box-solid bg-aqua">
                        <div class="box-header">
                            <h3 class="box-title"><?php echo $this->lang->line('reports_' . $key); ?></h3>
                        </div>
                        <div class="box-body text-right">
                            <p class="lead"><?php echo to_currency($value); ?></p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var element = 'pie-chart';
        var data = [
<?php if (count($data_file['graph']) > 0): ?>
    <?php foreach ($data_file['graph'] as $key => $value): ?>
                {"label": "<?php echo $key; ?>", "value": "<?php echo $value; ?>"},
    <?php endforeach; ?>
<?php else: ?>
            {"label": "No Data", "value": "0"}
<?php endif; ?>
        ];
                Morris.Donut({
                    element: element,
                    data: data,
                    resize: true
                });

    });
</script>