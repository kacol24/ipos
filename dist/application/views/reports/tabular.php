<div class="col-xs-12">
    <div class="box">
        <div class="box-header clearfix">
            <?php foreach ($summary_data as $key => $value): ?>
                <div class="col-md-3">
                    <div class="box box-solid bg-aqua">
                        <div class="box-header">
                            <h3 class="box-title"><?php echo $this->lang->line('reports_' . $key); ?></h3>
                        </div>
                        <div class="box-body text-right">
                            <p class="lead"><?php echo to_currency($value); ?></p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="box-body">
            <table id="datatable" class="table table-bordered table-hover table-condensed dt-responsive no-wrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <?php foreach ($headers as $header): ?>
                            <th class=""><?php echo $header; ?></th>
                        <?php endforeach; ?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data as $row): ?>
                        <tr>
                            <?php foreach ($row as $column): ?>
                                <td<?php echo $column['class']; ?>><?php echo $column['value']; ?></td>
                            <?php endforeach; ?>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div><!-- /.box-body -->
        <div class="box-footer clearfix">
            <?php if (isset($export_excel['start_date'])): ?>
                <a href="<?php echo site_url('/reports/' . $export_excel['report'] . '/' . $export_excel['start_date'] . '/' . $export_excel['end_date'] . '/' . $export_excel['sale_type'] . '/1'); ?>" class="btn btn-primary"><i class="fa fa-download"></i> Export to Excel</a>
            <?php else: ?>
                <a href="<?php echo site_url('/reports/' . $export_excel['report'] . '/1'); ?>" class="btn btn-primary"><i class="fa fa-download"></i> Export to Excel</a>
            <?php endif; ?>
        </div>
    </div><!-- /.box -->
</div><!-- /.col-xs-12 -->