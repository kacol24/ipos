<pre>
    <?php print_r($reports); ?>
</pre>
<div class="col-sm-4 col-sm-push-8">
    <div class="box box-solid box-success">
        <div class="box-header">
            <i class="fa fa-thumb-tack"></i>
            <h3 class="box-title">Pinned Reports</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-sm btn-default" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body no-transition">
            <ul class="list-unstyled">
                <li>lipsum</li>
                <li>lipsum</li>
                <li>lipsum</li>
            </ul>
        </div>
    </div>
</div>
<div class="col-sm-8 col-sm-pull-4">
    <div class="box box-solid box-info">
        <div class="box-header">
            <i class="fa fa-bar-chart-o"></i>
            <h3 class="box-title"><?php echo $this->lang->line('reports_category') ?></h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="box-group" id="accordion">
                <div class="panel box">
                    <div class="box-header">
                        <h3 class="box-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                <?php echo $this->lang->line('reports_graphical_reports'); ?>
                            </a>
                        </h3>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="box-body">
                            <ul class="list-unstyled">
                                <li>
                                    <div class="input-group margin">
                                        <a href="<?php echo site_url('reports/graphical_summary_sales'); ?>" class="btn btn-block btn-social btn-default"><i class="fa fa-shopping-cart"></i> <?php echo $this->lang->line('reports_sales'); ?></a>
                                        <span class="input-group-btn"><button class="btn btn-default"><i class="fa fa-thumb-tack"></i></button></span>
                                    </div>
                                </li>
                                <li>
                                    <div class="input-group margin">
                                        <a href="<?php echo site_url('reports/graphical_summary_categories'); ?>" class="btn btn-block btn-social btn-default"><i class="fa fa-sitemap"></i> <?php echo $this->lang->line('reports_categories'); ?></a>
                                        <span class="input-group-btn"><button class="btn btn-default"><i class="fa fa-thumb-tack"></i></button></span>
                                    </div>
                                </li>
                                <li>
                                    <div class="input-group margin">
                                        <a href="<?php echo site_url('reports/graphical_summary_customers'); ?>" class="btn btn-block btn-social btn-default"><i class="fa fa-user"></i> <?php echo $this->lang->line('reports_customers'); ?></a>
                                        <span class="input-group-btn"><button class="btn btn-default"><i class="fa fa-thumb-tack"></i></button></span>
                                    </div>
                                </li>
                                <li>
                                    <div class="input-group margin">
                                        <a href="<?php echo site_url('reports/graphical_summary_suppliers'); ?>" class="btn btn-block btn-social btn-default"><i class="fa fa-reply-all"></i> <?php echo $this->lang->line('reports_suppliers'); ?></a>
                                        <span class="input-group-btn"><button class="btn btn-default"><i class="fa fa-thumb-tack"></i></button></span>
                                    </div>
                                </li>
                                <li>
                                    <div class="input-group margin">
                                        <a href="<?php echo site_url('reports/graphical_summary_items'); ?>" class="btn btn-block btn-social btn-default"><i class="fa fa-tags"></i> <?php echo $this->lang->line('reports_items'); ?></a>
                                        <span class="input-group-btn"><button class="btn btn-default"><i class="fa fa-thumb-tack"></i></button></span>
                                    </div>
                                </li>
                                <li>
                                    <div class="input-group margin">
                                        <a href="<?php echo site_url('reports/graphical_summary_employees'); ?>" class="btn btn-block btn-social btn-default"><i class="fa fa-male"></i> <?php echo $this->lang->line('reports_employees'); ?></a>
                                        <span class="input-group-btn"><button class="btn btn-default"><i class="fa fa-thumb-tack"></i></button></span>
                                    </div>
                                </li>
                                <li>
                                    <div class="input-group margin">
                                        <a href="<?php echo site_url('reports/graphical_summary_taxes'); ?>" class="btn btn-block btn-social btn-default"><i class="fa fa-dollar"></i> <?php echo $this->lang->line('reports_taxes'); ?></a>
                                        <span class="input-group-btn"><button class="btn btn-default"><i class="fa fa-thumb-tack"></i></button></span>
                                    </div>
                                </li>
                                <li>
                                    <div class="input-group margin">
                                        <a href="<?php echo site_url('reports/graphical_summary_discounts'); ?>" class="btn btn-block btn-social btn-default"><i class="fa">%</i> <?php echo $this->lang->line('reports_discounts'); ?></a>
                                        <span class="input-group-btn"><button class="btn btn-default"><i class="fa fa-thumb-tack"></i></button></span>
                                    </div>
                                </li>
                                <li>
                                    <div class="input-group margin">
                                        <a href="<?php echo site_url('reports/graphical_summary_payments'); ?>" class="btn btn-block btn-social btn-default"><i class="fa fa-money"></i><?php echo $this->lang->line('reports_payments'); ?></a>
                                        <span class="input-group-btn"><button class="btn btn-default"><i class="fa fa-thumb-tack"></i></button></span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel box">
                    <div class="box-header">
                        <h3 class="box-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                <?php echo $this->lang->line('reports_summary_reports'); ?>
                            </a>
                        </h3>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="box-body">
                            <ul>
                                <li><a href="<?php echo site_url('reports/summary_sales'); ?>"><?php echo $this->lang->line('reports_sales'); ?></a></li>
                                <li><a href="<?php echo site_url('reports/summary_categories'); ?>"><?php echo $this->lang->line('reports_categories'); ?></a></li>
                                <li><a href="<?php echo site_url('reports/summary_customers'); ?>"><?php echo $this->lang->line('reports_customers'); ?></a></li>
                                <li><a href="<?php echo site_url('reports/summary_suppliers'); ?>"><?php echo $this->lang->line('reports_suppliers'); ?></a></li>
                                <li><a href="<?php echo site_url('reports/summary_items'); ?>"><?php echo $this->lang->line('reports_items'); ?></a></li>
                                <li><a href="<?php echo site_url('reports/summary_employees'); ?>"><?php echo $this->lang->line('reports_employees'); ?></a></li>
                                <li><a href="<?php echo site_url('reports/summary_taxes'); ?>"><?php echo $this->lang->line('reports_taxes'); ?></a></li>
                                <li><a href="<?php echo site_url('reports/summary_discounts'); ?>"><?php echo $this->lang->line('reports_discounts'); ?></a></li>
                                <li><a href="<?php echo site_url('reports/summary_payments'); ?>"><?php echo $this->lang->line('reports_payments'); ?></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel box">
                    <div class="box-header">
                        <h3 class="box-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                <?php echo $this->lang->line('reports_detailed_reports'); ?>
                            </a>
                        </h3>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                        <div class="box-body">
                            <ul>
                                <li><a href="<?php echo site_url('reports/detailed_sales'); ?>"><?php echo $this->lang->line('reports_sales'); ?></a></li>
                                <li><a href="<?php echo site_url('reports/detailed_receivings'); ?>"><?php echo $this->lang->line('reports_receivings'); ?></a></li>
                                <li><a href="<?php echo site_url('reports/specific_customer'); ?>"><?php echo $this->lang->line('reports_customer'); ?></a></li>
                                <li><a href="<?php echo site_url('reports/specific_discount'); ?>"><?php echo $this->lang->line('reports_discount'); ?></a></li>
                                <li><a href="<?php echo site_url('reports/specific_employee'); ?>"><?php echo $this->lang->line('reports_employee'); ?></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel box">
                    <div class="box-header">
                        <h3 class="box-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                <?php echo $this->lang->line('reports_inventory_reports'); ?>
                            </a>
                        </h3>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse">
                        <div class="box-body">
                            <ul>
                                <li><a href="<?php echo site_url('reports/inventory_low'); ?>"><?php echo $this->lang->line('reports_low_inventory'); ?></a></li>
                                <li><a href="<?php echo site_url('reports/inventory_summary'); ?>"><?php echo $this->lang->line('reports_inventory_summary'); ?></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div>
<script>
    $(document).ready(function () {
        $('#daterange-btn').daterangepicker({
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                'Last 7 Days': [moment().subtract('days', 6), moment()],
                'Last 30 Days': [moment().subtract('days', 29), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')],
                'All Time': []
            },
            startDate: moment(),
            endDate: moment()
        }, function (start, end) {
            $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        });
        $('input[name="report_type"]').parent().hide();
        var category;
        $('input[name="report_category"]').change(function () {
            category = $(this).attr('value');
            if (category !== 'inventory_low' || category !== 'inventory_summary') {
                $('input[name="report_type"]').parent().fadeIn();
            }
        });
    });
</script>