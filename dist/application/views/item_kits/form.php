<div class="col-xs-12">
    <div class="box box-solid">
        <div class="box-body">
            <?php echo form_open("item_kits/save/" . $item_kit_info->item_kit_id, array('id' => 'item_kit_form', 'class' => 'form-horizontal')); ?>
            <fieldset>
                <legend>
                    <?php echo $this->lang->line('item_kits_info'); ?>
                </legend>
                <div class="form-group">
                    <label for="name" class="control-label col-sm-2 text-red"><?php echo $this->lang->line('item_kits_name'); ?></label>
                    <div class="col-sm-5">
                        <input type="text" name="name" id="item_kit_name" class="form-control" value="<?php echo $item_kit_info->name; ?>" autofocus>
                    </div>
                </div>
                <div class="form-group">
                    <label for="item_kit_description" class="control-label col-sm-2"><?php echo $this->lang->line('item_kits_description'); ?></label>
                    <div class="col-sm-5">
                        <textarea name="description" id="item_kit_description" class="form-control"><?php echo $item_kit_info->description; ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="item_kit_item" class="control-label col-sm-2"><?php echo $this->lang->line('item_kits_add_item'); ?></label>
                    <div class="col-sm-5">
                        <input type="text" name="query" id="item_kit_item" class="form-control" value="" autocomplete="off">
                        <input type="hidden" name="item_id" value="">
                    </div>
                </div>
                <div class="form-group">
                    <div class="table-responsive add-padding-right-left col-sm-7">
                        <table class="table table-bordered" id="item_info">
                            <thead>
                                <tr>
                                    <th><?php echo $this->lang->line('item_kits_item'); ?></th>
                                    <th><?php echo $this->lang->line('item_kits_quantity'); ?></th>
                                    <th><?php echo $this->lang->line('common_table_action'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($this->Item_kit_items->get_info($item_kit_info->item_kit_id) as $item_kit_item): ?>
                                    <tr>
                                        <?php $item_info = $this->Item->get_info($item_kit_item['item_id']); ?>
                                        <td>
                                            <?php echo $item_info->name; ?>
                                        </td>
                                        <td>
                                            <input type="number" class='form-control text-right' id='item_kit_item_<?php echo $item_kit_item['item_id'] ?>' name=item_kit_item[<?php echo $item_kit_item['item_id'] ?>] value='<?php echo $item_kit_item['quantity'] ?>'/>
                                        </td>
                                        <td>
                                            <a href="#" class="btn btn-sm btn-danger" onclick="return delete_item_kit_row(this);"> <i class="fa fa-trash-o"></i> </a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-7">
                        <input type="submit" name="submit" id="submit" class="btn btn-primary pull-right" value="<?php echo $this->lang->line('common_submit'); ?>"/>
                    </div><!-- /.col-sm-7 -->
                </div><!-- /.form-group -->
            </fieldset>
        </div><!-- /.box-body -->
        <?php echo form_close(); ?>
    </div><!-- /.box -->
</div><!-- /.col-xs-12 -->
<script src="<?php echo base_url(); ?>assets/js/jquery.autocomplete.min.js" type="text/javascript"></script>
<script>
                                                function delete_item_kit_row(link) {
                                                    $(link).parent().parent().remove();
                                                    return false;
                                                }
                                                $(document).ready(function () {
                                                    $('#item_kit_item').devbridgeAutocomplete({
                                                        serviceUrl: '<?php echo site_url(); ?>/items/item_search',
                                                        minChars: 2,
                                                        onSearchStart: function (query) {
                                                            console.log(query);
                                                            $(this).addClass('loading-state');
                                                        },
                                                        onSearchComplete: function (query, suggestions) {
                                                            console.log(query);
                                                            console.log(suggestions);
                                                            $(this).removeClass('loading-state');
                                                        },
                                                        onSelect: function (suggestion) {
                                                            var construct_table = '';
                                                            construct_table += '<tr>';
                                                            construct_table += '<td>';
                                                            construct_table += suggestion['name'];
                                                            construct_table += '</td>';
                                                            construct_table += '<td>';
                                                            construct_table += '<input type="number" name="item_kit_item[' + suggestion['data'] + ']" class="form-control text-right" value="1">';
                                                            construct_table += '</td>';
                                                            construct_table += '<td>';
                                                            construct_table += '<button class="btn btn-sm btn-danger" onclick="return delete_item_kit_row(this);"> <i class="fa fa-trash-o"></i> </button>';
                                                            construct_table += '</td>';

                                                            construct_table += '</tr>';
                                                            $('#item_info tbody').append(construct_table);
                                                            $(this).val('');
                                                        },
                                                        showNoSuggestionNotice: true,
                                                        noSuggestionNotice: '<?php echo $this->lang->line('sales_no_item_found'); ?>'
                                                    });
                                                });


</script>