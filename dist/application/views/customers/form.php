<div class="col-xs-12">
    <div class="box box-solid">
        <div class="box-body">
            <?php echo form_open("customers/save/$person_info->person_id/$from_sales", array('id' => 'customer_form', 'class' => 'form-horizontal')); ?>
            <fieldset>
                <legend>
                    <?php echo $this->lang->line('customers_basic_information'); ?>
                </legend>
                <?php $this->load->view('people/form_basic_info'); ?>
                <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo $this->lang->line('customers_discount'); ?></label>
                    <div class="col-sm-5">
                        <div class="input-group">
                            <input type="number" class="form-control text-right" min="0" max="100" id="customer_discount" name="customer_discount" value="<?php echo $person_info->customer_discount; ?>">
                            <span class="input-group-addon">%</span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo $this->lang->line('customers_courier'); ?></label>
                    <div class="col-sm-5">
                        <input type="text" id="courier" class="form-control" name="courier" value="<?php echo $person_info->courier; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="taxable" class="col-sm-2 control-label no-padding-top"><?php echo $this->lang->line('customers_taxable'); ?></label>
                    <div class="col-sm-5">
                        <input type="checkbox" name="taxable" id="taxable" class="form-control iCheck" <?php echo $person_info->taxable ? 'checked' : ''; ?>/>
                    </div>
                </div>
            </fieldset>
            <?php if ($this->config->item('enable_custom_price')): ?>
                <fieldset>
                    <legend>
                        <?php echo lang('customers_custom_item_prices'); ?>
                    </legend>
                    <div class="form-group">
                        <label class="control-label col-sm-2"><?php echo lang('items_custom_price'); ?></label>
                        <div class="col-sm-5">
                            <input type="text" id="item" name="query" autocomplete="off" class="form-control col-xs-12" placeholder="<?php echo $this->lang->line('sales_start_typing_item_name'); ?>" autofocus/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12 col-sm-7">
                            <div class="table-responsive">
                                <table class="table table-bordered table-condensed no-wrap" cellspacing='0' id="custom_price_table">
                                    <thead>
                                        <tr>
                                            <th><?php echo lang('items_item_number'); ?></th>
                                            <th><?php echo lang('items_name'); ?></th>
                                            <th><?php echo lang('items_unit_price'); ?></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($custom_price as $row): ?>
                                            <tr>
                                                <td class="vertical-middle"><?php echo $row->item_number; ?></td>
                                                <td class="vertical-middle"><?php echo $row->name; ?></td>
                                                <td>
                                                    <input type="number" name="custom_price[<?php echo $row->item_id; ?>]" class="form-control text-right" min="0" max="999999999999" value="<?php echo $row->unit_price; ?>">
                                                </td>
                                                <td class="text-center">
                                                    <button class="btn btn-sm btn-danger" onclick="return delete_custom_price_row(this);"> <i class="fa fa-trash-o"> </i></button>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </fieldset>
            <?php endif; ?>
            <div class="form-group">
                <div class="col-sm-7">
                    <input type="submit" name="submit" id="submit" class="btn btn-primary pull-right" value="<?php echo $this->lang->line('common_submit'); ?>"/>
                </div><!-- /.col-sm-7 -->
            </div><!-- /.form-group -->
            <?php echo form_close(); ?>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div><!-- /.col-xs-12 -->
<script>
    function delete_custom_price_row(link) {
        var row = $(link).parent().parent();
        row.fadeOut();
        setTimeout(function () {
            row.remove();
        }, 500);
        return false;
    }
    $(function () {
        $('#courier').devbridgeAutocomplete({
            serviceUrl: '<?php echo site_url('customers/suggest_courier'); ?>',
            minChars: 1,
            onSearchStart: function (query) {
                console.log(query);
                $(this).addClass('loading-state');
            },
            onSearchComplete: function (query, suggestions) {
                console.log(query);
                console.log(suggestions);
                $(this).removeClass('loading-state');
            }
        });
        $('#item').devbridgeAutocomplete({
            serviceUrl: '<?php echo site_url('sales/item_search'); ?>',
            minChars: 1,
            onSearchStart: function (query) {
                console.log(query);
                $(this).addClass('loading-state');
            },
            onSearchComplete: function (query, suggestions) {
                console.log(query);
                console.log(suggestions);
                $(this).removeClass('loading-state');
            },
            onSelect: function (suggestion) {
                var construct_table = '';
                construct_table += '<tr>';
                construct_table += '<td>';
                construct_table += suggestion['item_number'];
                construct_table += '</td>';
                construct_table += '<td>';
                construct_table += suggestion['name'];
                construct_table += '</td>';
                construct_table += '<td>';
                construct_table += '<input type="number" name="custom_price[' + suggestion['data'] + ']" class="form-control text-right" min="0" max="999999999999" value="' + suggestion['unit_price'] + '">';
                construct_table += '</td>';
                construct_table += '<td class="text-center">';
                construct_table += '<button class="btn btn-sm btn-danger" onclick="return delete_custom_price_row(this);"> <i class="fa fa-trash-o"></i> </button>';
                construct_table += '</td>';
                construct_table += '</tr>';
                $('#custom_price_table tbody').append(construct_table);
                $(this).val('');
            }
        });
    });
</script>