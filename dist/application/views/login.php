<!DOCTYPE html>
<html lang='<?php echo $this->config->item('language'); ?>' class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title><?php echo $this->config->item('company'); ?> | <?php echo $this->lang->line('login_login'); ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.2.0 -->
        <link href = "//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel = "stylesheet" type = "text/css" />

        <!--font Awesome -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">

        <!--Theme style -->
        <link href = "<?php echo base_url(); ?>assets/css/AdminLTE.css" rel = "stylesheet" type = "text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="bg-black">

        <div class="form-box" id="login-box">
            <div class="header"><?php echo $this->lang->line('login_login'); ?></div>
            <?php echo form_open('login'); ?>
            <div class="body bg-gray">
                <?php echo validation_errors(); ?>
                <div class="form-group">
                    <input type="text" name="username" class="form-control" placeholder="<?php echo $this->lang->line('login_username'); ?>" autofocus value="<?php echo set_value('username'); ?>"/>
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="<?php echo $this->lang->line('login_password'); ?>"/>
                </div>
            </div>
            <div class="footer">
                <button type="submit" class="btn bg-olive btn-block">Sign me in</button>
            </div>
            <?php echo form_close(); ?>
        </div>


        <!-- jQuery 2.1.3 -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

        <!-- Bootstrap -->
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

    </body>
</html>