<div class="col-xs-12">
    <div class="box box-solid">
        <div class="box-body">
            <?php echo form_open("suppliers/save/$person_info->person_id", array('id' => 'supplier_form', 'class' => 'form-horizontal')); ?>
            <fieldset>
                <legend>
                    <?php echo $this->lang->line('suppliers_basic_information'); ?>
                </legend>
                <div class="form-group">
                    <label for="company_name" class="control-label col-sm-2 text-red"><?php echo $this->lang->line('suppliers_company_name'); ?></label>
                    <div class="col-sm-5">
                        <input type="text" name="company_name" id="company_name" class="form-control" value="<?php echo $person_info->company_name; ?>" required autofocus/>
                    </div>
                </div>
                <?php $this->load->view('people/form_basic_info'); ?>
                <div class="form-group">
                    <div class="col-sm-7">
                        <input type="submit" name="submit" id="submit" class="btn btn-primary pull-right" value="<?php echo $this->lang->line('common_submit'); ?>"/>
                    </div><!-- /.col-sm-7 -->
                </div><!-- /.form-group -->
            </fieldset>
            <?php echo form_close(); ?>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div><!-- /.col-xs-12 -->