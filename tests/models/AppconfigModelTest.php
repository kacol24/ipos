<?php
/**
 * Description of AppconfigModelTest
 *
 * @author Kevin Chandra <kacol.bot@gmail.com>
 */

/**
 * @group Model
 */
class AppconfigModelTest extends CIUnit_TestCase
{

    protected $tables = array(
        'app_config' => 'app_config'
    );
    private $_model;

    public function __construct($name = NULL, array $data = array(), $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    public function setUp()
    {
        parent::setUp();

        $this->CI->load->model('Appconfig');
        $this->_model = $this->CI->Appconfig;
    }

    public function tearDown()
    {
        parent::tearDown();
    }

    public function test_exists()
    {
        $appconfig = $this->_model->exists('company');
        $this->assertTrue($appconfig, 'Store configuration is not set properly');
    }

    public function test_exists_fail()
    {
        $appconfig = $this->_model->exists('non existing key');
        $this->assertFalse($appconfig, 'exists() method fails');
    }

    public function test_get_all()
    {
        $appconfig = $this->_model->get_all()->result_array();
        $this->assertNotEmpty($appconfig, 'Store configuration does not contain any key=>value');
    }

    public function test_get()
    {
        $company = $this->_model->get('company');
        $this->assertEquals('Hoggy Djaya', $company, 'get() method fails');
    }

    public function test_save()
    {
        $test_key = 'test_key';
        $test_value = 'test_value';
        $this->_model->save($test_key, $test_value);
        $config = $this->_model->get($test_key);
        $this->assertEquals($test_value, $config, 'failed to save');
    }

    public function test_batch_save()
    {
        $key1 = 'key1';
        $key2 = 'key2';
        $value1 = 'value1';
        $value2 = 'value2';
        $data[$key1] = $value1;
        $data[$key2] = $value2;
        $this->assertTrue($this->_model->batch_save($data));
        $result_value1 = $this->_model->get('key1');
        $result_value2 = $this->_model->get('key2');
        $this->assertEquals($value1, $result_value1, 'value 1 was not saved');
        $this->assertEquals($value2, $result_value2, 'value 2 was not saved');
    }

    public function test_delete()
    {
        $test_key = 'test_key';
        $this->assertTrue($this->_model->delete($test_key));
        $this->assertFalse($this->_model->exists($test_key));
    }

    public function test_delete_all()
    {
        $this->_model->delete_all();
        $appconfig = $this->_model->get_all()->result_array();
        $this->assertCount(0, $appconfig, 'delete_all() fails');
    }
}
